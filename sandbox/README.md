A testing area where you can testing the output from the /dist-folder.
Paths are relative from /root, and an example import could look like:

```js
// sandbox/index.js
import { DSBComponent } from './../dist/js/dsb.esm.js'
```

```css
/* sandbox/style.css */
@import './../dist/dsb.css'
```