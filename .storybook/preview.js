import { addParameters, addDecorator } from '@storybook/html';
import { withKnobs } from '@storybook/addon-knobs';
import { addReadme } from 'storybook-readme/html';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { render } from 'lit';
import { withHTML } from '@whitespace/storybook-addon-html/html';

/**
 * Projects main js file
 */
import './../src/polyfills';

/**
 * Projects main scss file
 */
import './../src/utils.scss';
import './../src/scss/settings/globals.scss'

addDecorator((story) => {
  //if (component instanceof TemplateResult) {
  const el = document.createElement('div'); // this element probably can br reused.
  render(story(), el);
  return el.children[0];
  //}
  //return story();
});
addDecorator(addReadme);
addDecorator(withKnobs);
addDecorator(
  withHTML({
    prettier: {
      printWidth: 160,
      tabWidth: 2,
      useTabs: false,
    },
    removeEmptyComments: true,
  })
);

addParameters({
  // options: {
  // 	storySort: (a, b) =>
  // 		a[1].kind === b[1].kind ? 0 : a[1].id.localeCompare(b[1].id, undefined, {
  // 			numeric: true
  // 		}),
  // },
  a11y: {},
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
  layout: 'fullscreen',
  backgrounds: {
    values: [{ name: 'Red', value: '#b41730' }],
  },
});
