const path = require('path');
const custom = require('../config/webpack.dev');
const aliases = require('../config/aliases');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  /**
   * Stories
   *
   * Where to look for files
   */
  stories: [
    './default-stories/changelog.stories.js',
    './default-stories/readme.stories.js',
    '../src/**/*.stories.js',
  ],
  /**
   * Addons
   *
   * Register the addons to our storybook-setup.
   */
  addons: [
    /**
     * HTML
     *
     * Extracts and displays compiled syntax-highlighted HTML.
     */
    '@whitespace/storybook-addon-html',
    /**
     * A11Y
     *
     * Can be helpful to make your UI components more accessible.
     */
    '@storybook/addon-a11y',

    '@storybook/addon-essentials',

    '@storybook/addon-postcss',
  ],
  /**
   * Webpack Final
   *
   * Merge with the rest of Storybooks default Webpack config.
   */
  webpackFinal: (config) => {
    return {
      ...config,
      resolve: {
        ...config.resolve,
        /**
         * Alias
         *
         * Include the global Webpack alias object
         */
        alias: aliases,
      },
      module: {
        ...config.module,
        rules: custom.module.rules,
      },
      plugins: [
        ...config.plugins,
        // ...custom.plugins
        /**
         * Stylelint,
         * placed here directly b/c importing all webpack-plugins breaks the build.
         */
        new StylelintPlugin({
          configFile: '.stylelintrc',
          files: '**/*.s?(a|c)ss',
        }),
      ],
    };
  },
};
