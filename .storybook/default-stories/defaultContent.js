import { html } from 'lit';

export const defaultContent = () => html`<div
  class="p-2 flex flex-column"
  style="
    color: var(--color-white);
    background-image: url('https://bornholm.info/lejrskole/wp-content/media/sites/4/2019/06/dsbfrirejse_bornholm.jpg');
    height: 100vh;
    background-size: cover;
    box-sizing: border-box;"
>
  <h1 class="ts-headline-1 m-0">DSB Storybook</h1>
  <p class="ts-hero">Links:</p>
  <ul>
    <li>
      <a class="ts-headline-3" href="https://dsb.zeroheight.com/styleguides">
        Zeroheight
      </a>
    </li>
    <li>
      <a
        class="ts-headline-3"
        href="https://bitbucket.org/sorthvid_dk/dsb_designsystem"
      >
        Bitbucket
      </a>
    </li>
  </ul>
</div>`;
