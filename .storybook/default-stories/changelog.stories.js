import readme from './../../CHANGELOG.md';
import { defaultContent } from './defaultContent.js';

export default {
  title: 'README/CHANGELOG',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const CHANGELOG = () => defaultContent();
