import readme from './../../README.md';
import { defaultContent } from './defaultContent.js';

export default {
  title: 'README/README',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const README = () => defaultContent();