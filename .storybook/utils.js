import { unsafeHTML } from 'lit-html/directives/unsafe-html';

/**
 * Mockup images
 */
import jpgs from '../mock-data/images/jpgs';
import pngs from '../mock-data/images/pngs';

/**
 * Mockup texts
 */
import articles from '../mock-data/texts/articles';
import cities from '../mock-data/texts/cities';
import ctas from '../mock-data/texts/ctas';
import headlines from '../mock-data/texts/headlines';
import paragraphs from '../mock-data/texts/paragraphs';
import questions from '../mock-data/texts/questions';
import sentences from '../mock-data/texts/sentences';

/**
 * Mockup videos
 */
import mp4s from '../mock-data/videos/mp4s';



/**
 * Used to overwrite a storybook knobs with a fixed value, i.e remove a knob
 * @param {*} value value that might overwrite
 * @param {fn} fallback an addon knob, wrapped in a function, that might be overwritten
 */
export const ifdef = (value, fallback) =>
  value !== undefined
    ? value
    : typeof fallback === 'function'
    ? fallback()
    : fallback;



/**
 * Simple wrapper for a ternary operator that fallback to empty string
 * @param {*} val
 * @param {*} or
 */
export const put = (val, or = '') => (val ? val : or);



/**
 * Maybe
 * When the if-statement is true, component will be created,
 * with content. Or else it returns an empty string.
 * @param {component} - a lit-html component to insert
 * @param {if} - returns the component if this is truthy, with this arg as parameter
 * @param {content} - [opt] custom content, when 'if' is not the content
 * @returns {lit-html|''} - a lit-html element or an empty string
 */
export const maybe = obj => {
  const data = obj.content ? obj.content : obj.if;
  return obj.if ? obj.component(data) : '';
};



/**
 * Runs cb n amout of times and returns the results as a array
 * Good for creating many lit-html elements or mockup content on the fly.
 * In order to get a different value each time, you should pass cb wrapped in a function
 * The callback have access to itself and its index in the repeat-loop
 * i.e. repeat(5, (item, index) => myFunc(item, index))
 * @param {*} n amount of iterations
 * @param {*} cb what to perform each time
 */
export const repeat = (n, cb) =>
  Array(n)
    .fill(0)
    .map((x, i) => (typeof cb === 'function' ? cb(x, i) : cb));



/**
 * Returns a random int between 0 and n
 * @param {*} n - max value
 */
export const randomInt = n => Math.floor(Math.random() * n);



/**
 * [private]
 * Returns a random element from an array
 * @param {*} arr - array to get element from
 */
const _randomArrEl = arr => arr[randomInt(arr.length)];



/**
 * [private]
 * Returns a random value of a key from a passed object
 * @param {*} dataObj object containing arrays of content
 * @param {*} type the key to get a value from
 */
const _getRandomContent = (dataObj, type) => {
  if (!Object.keys(dataObj).includes(type)) {
    console.warn(
      `The passed parameter doesn't exist in the data base.\nAvailable types are: ${Object.keys(
        dataObj
      )}.\nPassed type was: ${type}`
    );
    return 'ERROR: check console for info.';
  }

  return _randomArrEl(dataObj[type]);
};



/**
 * Returns a random content, either image or text.
 * Good for generating and testing components with new content each load.
 * @param {String} type - either 'png' or 'jpg', or any of the texts-keys
 */
export const random = type => {
  const images = {
    jpg: Object.values(jpgs),
    png: Object.values(pngs)
  };

  const texts = {
    article: articles,
    city: cities,
    cta: ctas,
    headline: headlines,
    paragraph: paragraphs,
    question: questions,
    sentence: sentences
  };

  const videos = {
    video: Object.values(mp4s)
  };

  return _getRandomContent(
    type === 'jpg' || type === 'png' ? images : type === 'video' ? videos : texts,
    type
  );
};



/**
 * Wrapper to keep control over icons in one place
 * @param {Svg-string} icon - What icon to get, inline
 */
export const getIcon = icon => unsafeHTML(icon);



/**
 * Generates a random string that's good for id's
 */
export const randomId = () =>
  '-' +
  Math.random()
    .toString(36)
    .substr(2, 9);
