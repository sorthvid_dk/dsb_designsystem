const path = require('path');

const defaultFolder = 'src';
const templateFolder = 'plop-templates';
const litTemplate = path.join(templateFolder, 'lit.hbs');
const sbTemplate = path.join(templateFolder, 'story.hbs');
const sbPagesTemplate = path.join(templateFolder, '/page.hbs');
const scssTemplate = path.join(templateFolder, '/scss.hbs');
const readmeTemplate = path.join(templateFolder, '/readme.hbs');
// const stateTemplate = path.join(templateFolder, '/state.hbs');
// const sbCETemplate = path.join(templateFolder, '/story-custom-element.hbs');
// const jsCETemplate = path.join(templateFolder, '/js-custom-element.hbs');
// const scssCETemplate = path.join(templateFolder, '/scss-custom-element.hbs');

module.exports = function (plop) {
  plop.setGenerator('basics', {
    description: 'this is a skeleton plopfile',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Component name:',
      },
      {
        type: 'list',
        name: 'type',
        choices: ['components', 'page'],
        default: 'components',
      },
    ],
    actions: function (data) {
      var actions = [];

      if (data.type === 'page') {
        actions.push({
          type: 'add',
          path: path.join(defaultFolder, '/pages/{{name}}/{{name}}.stories.js'),
          templateFile: sbPagesTemplate,
        });
      } else if (data.type === 'components') {
        let pathString = path.join(defaultFolder, 'components');

        actions.push({
          type: 'add',
          path: path.join(pathString + '/{{name}}/{{name}}.js'),
          templateFile: litTemplate,
        });

        actions.push({
          type: 'add',
          path: path.join(pathString + '/{{name}}/{{name}}.stories.js'),
          templateFile: sbTemplate,
        });
        actions.push({
          type: 'add',
          path: path.join(pathString + '/{{name}}/README.md'),
          templateFile: readmeTemplate,
        });
        actions.push({
          type: 'add',
          path: path.join(pathString + '/{{name}}/{{name}}.scss'),
          templateFile: scssTemplate,
        });
        actions.push({
          type: 'append',
          path: path.join(defaultFolder + '/index.js'),
          pattern: `/* PLOP_INJECT_EXPORT */`,
          template: `export { DSB{{pascalCase name}} } from './components/{{name}}/{{name}}.js';`,
        });
      }
      return actions;
    },
  });
};
