# Changelog

## 2.0.5
- Hero component mobile view
- Storybook opens with the changelog as default page

## 2.0.4
- Added example pages to showcase components in a context
- On new release, a tagged git-branch is created automatically

## 2.0.3
- Updated README.md with new css-import syntax

## 2.0.2
- Added default exports to package.json

## 2.0.1
- Moved unused files to folders named \_\_depricated\_\_
- Header - Added backdrop

## 2.0.0
- Project rewritten with `lit`
- New documentation.
- TODO: Remove depricated and unused code

<!-- 1.0.9 not yet published to npm -->
## 1.0.9
- Fixed margins on Header-form

## 1.0.8
- included requirement field to README.md.
- added resolve-url-loader as dev-dependency
- Badge component updated
    - It stays round all the time.
    - Currently suffixed "-new" to not clash with the older implementation.
- Disabled animations in storybook
- Fixed scroll bug when opening accordions
- Header preview in storybook, is wrapped in a div

## 1.0.3
- Switched mockup images and videos for urls, to reduce total file size

## 1.0.2
- Fixed bug in SCSS exports where url's were absolute and couldn't be found from outside.

## 1.0.1
- Updated README and added `/docs`
- Added exports of SCSS files, directly from `/src`

## 1.0.0
- Deployed to npm
