/**
 * Id
 * Generates a random string that's good for id's
 */
var randomId = () =>
  '-' +
  Math.random()
    .toString(36)
    .substr(2, 9);

export { randomId as r };
