import { w, h, T } from './lit-element-a21c046d.js';
import { i, s, t } from './directive-b9dab5a2.js';

/**
 * @license
 * Copyright 2018 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const e=i(class extends s{constructor(t$1){var s;if(super(t$1),t$1.type!==t.ATTRIBUTE||"class"!==t$1.name||(null===(s=t$1.strings)||void 0===s?void 0:s.length)>2)throw Error("`classMap()` can only be used in the `class` attribute and must be the only part in the attribute.")}render(t){return Object.keys(t).filter((s=>t[s])).join(" ")}update(s,[r]){if(void 0===this.bt){this.bt=new Set;for(const t in r)r[t]&&this.bt.add(t);return this.render(r)}const i=s.element.classList;this.bt.forEach((t=>{t in r||(i.remove(t),this.bt.delete(t));}));for(const t in r){const s=!!r[t];s!==this.bt.has(t)&&(s?(i.add(t),this.bt.add(t)):(i.remove(t),this.bt.delete(t)));}return w}});

var css_248z = ":host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px;--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px}:host{position:relative;display:inline-block;height:-webkit-fill-available;width:100%}.gradient{position:absolute;width:100%;height:100%;z-index:get-z(\"image-block-gradient\")}.gradient.top{background:-webkit-gradient(linear,left bottom,left top,from(transparent),color-stop(4.4%,rgba(0,0,0,.006)),color-stop(9.5%,rgba(0,0,0,.024)),color-stop(15.2%,rgba(0,0,0,.052)),color-stop(21.5%,rgba(0,0,0,.088)),color-stop(28.1%,rgba(0,0,0,.13)),color-stop(35.2%,rgba(0,0,0,.176)),color-stop(42.5%,rgba(0,0,0,.225)),color-stop(50%,rgba(0,0,0,.275)),color-stop(57.6%,rgba(0,0,0,.324)),color-stop(65.2%,rgba(0,0,0,.37)),color-stop(72.7%,rgba(0,0,0,.412)),color-stop(80%,rgba(0,0,0,.448)),color-stop(87.1%,rgba(0,0,0,.476)),color-stop(93.7%,rgba(0,0,0,.494)),to(rgba(0,0,0,.5)));background:linear-gradient(0deg,transparent 0,rgba(0,0,0,.006) 4.4%,rgba(0,0,0,.024) 9.5%,rgba(0,0,0,.052) 15.2%,rgba(0,0,0,.088) 21.5%,rgba(0,0,0,.13) 28.1%,rgba(0,0,0,.176) 35.2%,rgba(0,0,0,.225) 42.5%,rgba(0,0,0,.275) 50%,rgba(0,0,0,.324) 57.6%,rgba(0,0,0,.37) 65.2%,rgba(0,0,0,.412) 72.7%,rgba(0,0,0,.448) 80%,rgba(0,0,0,.476) 87.1%,rgba(0,0,0,.494) 93.7%,rgba(0,0,0,.5))}.gradient.right{background:-webkit-gradient(linear,left top,right top,from(transparent),color-stop(4.4%,rgba(0,0,0,.006)),color-stop(9.5%,rgba(0,0,0,.024)),color-stop(15.2%,rgba(0,0,0,.052)),color-stop(21.5%,rgba(0,0,0,.088)),color-stop(28.1%,rgba(0,0,0,.13)),color-stop(35.2%,rgba(0,0,0,.176)),color-stop(42.5%,rgba(0,0,0,.225)),color-stop(50%,rgba(0,0,0,.275)),color-stop(57.6%,rgba(0,0,0,.324)),color-stop(65.2%,rgba(0,0,0,.37)),color-stop(72.7%,rgba(0,0,0,.412)),color-stop(80%,rgba(0,0,0,.448)),color-stop(87.1%,rgba(0,0,0,.476)),color-stop(93.7%,rgba(0,0,0,.494)),to(rgba(0,0,0,.5)));background:linear-gradient(90deg,transparent 0,rgba(0,0,0,.006) 4.4%,rgba(0,0,0,.024) 9.5%,rgba(0,0,0,.052) 15.2%,rgba(0,0,0,.088) 21.5%,rgba(0,0,0,.13) 28.1%,rgba(0,0,0,.176) 35.2%,rgba(0,0,0,.225) 42.5%,rgba(0,0,0,.275) 50%,rgba(0,0,0,.324) 57.6%,rgba(0,0,0,.37) 65.2%,rgba(0,0,0,.412) 72.7%,rgba(0,0,0,.448) 80%,rgba(0,0,0,.476) 87.1%,rgba(0,0,0,.494) 93.7%,rgba(0,0,0,.5))}.gradient.bottom{background:-webkit-gradient(linear,left top,left bottom,from(transparent),color-stop(4.4%,rgba(0,0,0,.011)),color-stop(9.5%,rgba(0,0,0,.041)),color-stop(15.2%,rgba(0,0,0,.088)),color-stop(21.5%,rgba(0,0,0,.149)),color-stop(28.1%,rgba(0,0,0,.22)),color-stop(35.2%,rgba(0,0,0,.299)),color-stop(42.5%,rgba(0,0,0,.383)),color-stop(50%,rgba(0,0,0,.467)),color-stop(57.6%,rgba(0,0,0,.551)),color-stop(65.2%,rgba(0,0,0,.63)),color-stop(72.7%,rgba(0,0,0,.701)),color-stop(80%,rgba(0,0,0,.762)),color-stop(87.1%,rgba(0,0,0,.809)),color-stop(93.7%,rgba(0,0,0,.839)),to(rgba(0,0,0,.85)));background:linear-gradient(180deg,transparent 0,rgba(0,0,0,.011) 4.4%,rgba(0,0,0,.041) 9.5%,rgba(0,0,0,.088) 15.2%,rgba(0,0,0,.149) 21.5%,rgba(0,0,0,.22) 28.1%,rgba(0,0,0,.299) 35.2%,rgba(0,0,0,.383) 42.5%,rgba(0,0,0,.467) 50%,rgba(0,0,0,.551) 57.6%,rgba(0,0,0,.63) 65.2%,rgba(0,0,0,.701) 72.7%,rgba(0,0,0,.762) 80%,rgba(0,0,0,.809) 87.1%,rgba(0,0,0,.839) 93.7%,rgba(0,0,0,.85))}.gradient.left{background:-webkit-gradient(linear,right top,left top,from(transparent),color-stop(4.4%,rgba(0,0,0,.011)),color-stop(9.5%,rgba(0,0,0,.041)),color-stop(15.2%,rgba(0,0,0,.088)),color-stop(21.5%,rgba(0,0,0,.149)),color-stop(28.1%,rgba(0,0,0,.22)),color-stop(35.2%,rgba(0,0,0,.299)),color-stop(42.5%,rgba(0,0,0,.383)),color-stop(50%,rgba(0,0,0,.467)),color-stop(57.6%,rgba(0,0,0,.551)),color-stop(65.2%,rgba(0,0,0,.63)),color-stop(72.7%,rgba(0,0,0,.701)),color-stop(80%,rgba(0,0,0,.762)),color-stop(87.1%,rgba(0,0,0,.809)),color-stop(93.7%,rgba(0,0,0,.839)),to(rgba(0,0,0,.85)));background:linear-gradient(270deg,transparent 0,rgba(0,0,0,.011) 4.4%,rgba(0,0,0,.041) 9.5%,rgba(0,0,0,.088) 15.2%,rgba(0,0,0,.149) 21.5%,rgba(0,0,0,.22) 28.1%,rgba(0,0,0,.299) 35.2%,rgba(0,0,0,.383) 42.5%,rgba(0,0,0,.467) 50%,rgba(0,0,0,.551) 57.6%,rgba(0,0,0,.63) 65.2%,rgba(0,0,0,.701) 72.7%,rgba(0,0,0,.762) 80%,rgba(0,0,0,.809) 87.1%,rgba(0,0,0,.839) 93.7%,rgba(0,0,0,.85))}.box,.container{display:-webkit-box;display:-ms-flexbox;display:flex;height:inherit;width:inherit}.aspect{--aspect-0:var(--aspect-x,16);--aspect-1:var(--aspect-y,9);padding-bottom:calc(100%/var(--aspect-0)*var(--aspect-1))}.aspect .image,.aspect img{position:absolute}.image{background-size:cover;background-size:var(--object-fit,cover);background-position:50%;background-repeat:no-repeat}.image,img{width:100%;height:100%;-o-object-fit:cover;object-fit:cover;-o-object-fit:var(--object-fit,cover);object-fit:var(--object-fit,cover)}img{padding:0;padding:var(--image-padding,0);-webkit-box-sizing:border-box;box-sizing:border-box}slot{display:block;position:absolute;padding:24px 24px 0;z-index:1}@media (min-width:56.25em){slot{padding:36px 36px 0}}slot[name=right]{right:0}";

// import { ifDefined } from 'lit/directives/if-defined.js';

class ImageBlock extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      bg: { type: String },
      src: { type: String },
      alt: { type: String },
      aspect: { type: String },
      gradient: { type: String },
    };
  }

  constructor() {
    super();
    this.bg = '';
    this.src = '';
    this.alt = '';
    this.aspect = '';
    this.gradient = '';
  }

  getAspect() {
    return this.aspect
      .split('/')
      .map((a, i) => `--aspect-${i}: ${a}`)
      .join('; ') + ';';
  }

  // ?data-passepartout="${attributes.includes('passepartout')}"

  render() {
    return T`
      <div class="container">
        <slot></slot>
        <slot name="right"></slot>

        ${this.gradient
          ? T` <div class="gradient ${this.gradient}"></div> `
          : ''}

        <div
          class="${e({ box: true, aspect: this.aspect || this.bg })}"
          style=${this.aspect ? this.getAspect() : undefined}
        >
          ${this.bg
            ? T`<div
                class="image"
                style="background-image: url(${this.bg});"
              ></div>`
            : T`<img src=${this.src} alt=${this.alt} />`}
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-image-block', ImageBlock);

export { ImageBlock as I, e };
