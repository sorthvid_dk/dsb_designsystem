import { h, T } from '../lit-element-a21c046d.js';
import { r as randomId } from '../id-839a82d1.js';
import { f as flip } from '../flip-60377d92.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host{--icon-color:var(--color-red);width:100%;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex}:host([isopen]) .plus{-webkit-transform:rotate(45deg);transform:rotate(45deg)}.plus{-webkit-transition:-webkit-transform .2s var(--ease-in-out);transition:-webkit-transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out),-webkit-transform .2s var(--ease-in-out)}.details{width:100%;padding:0 calc(min(100vw, var(--page-width))/24*1);-webkit-box-sizing:border-box;box-sizing:border-box;background-color:var(--color-grey-1);border-radius:var(--border-radius)}.content{height:auto;overflow:hidden}.content[hidden]{display:block;height:0}slot{display:block;padding:0 0 var(--units-2)}button{--font-family:var(--font-primary);--font-size:16px;--line-height:28px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);width:100%;border:0;padding:var(--units-2) 0;background-color:transparent;display:grid;grid-auto-flow:column;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:var(--units-2);gap:var(--units-2);text-align:left;cursor:pointer}@media (min-width:105.0625em){button{--font-size:18px;--line-height:30px}}";

class FlatcardAccordion extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      label: { type: String },
      isOpen: { type: Boolean, reflect: true },
      id: { type: String },
    };
  }

  constructor() {
    super();
    this.label = '';
    this.isOpen = false;
    this.id = randomId();
  }

  connectedCallback() {
    super.connectedCallback();
    // TODO: can't find element directly
    setTimeout(() => {
      this.button = this.renderRoot.querySelector('button');
      this.content = this.renderRoot.querySelector('.content');
      this.button.addEventListener('click', this.toggle.bind(this));
    }, 10);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.button.removeEventListener('click', this.toggle.bind(this));
  }

  toggle() {
    const state = flip.getState(this.content);
    console.log(state);
    this.isOpen = !this.isOpen;
    flip.expand(state);
  }

  render() {
    return T`
      <div class="details">
        <button aria-expanded=${this.isOpen} aria-controls=${this.id}>
          <div>${this.label}</div>
          <dsb-icon class="plus" icon="plus"></dsb-icon>
        </button>
        <div
          class="content"
          ?hidden=${!this.isOpen}
          aria-hidden=${!this.isOpen}
          id=${this.id}
        >
          <slot></slot>
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-flatcard-accordion', FlatcardAccordion);

export { FlatcardAccordion };
