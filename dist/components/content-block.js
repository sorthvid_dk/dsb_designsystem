import { h, T } from '../lit-element-a21c046d.js';
import { s as stringToDocumentFragment } from '../utils-4e70fb44.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif}::slotted(h1),::slotted(h2),::slotted(h3),::slotted(h4),::slotted(h5),::slotted(h6),h1,h2,h3,h4,h5,h6{--font-family:var(--font-primary);--font-size:32px;--line-height:42px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;margin:0 0 .618em}@media (min-width:78.125em){::slotted(h1),::slotted(h2),::slotted(h3),::slotted(h4),::slotted(h5),::slotted(h6),h1,h2,h3,h4,h5,h6{--font-size:45px;--line-height:58px}}@media (min-width:105.0625em){::slotted(h1),::slotted(h2),::slotted(h3),::slotted(h4),::slotted(h5),::slotted(h6),h1,h2,h3,h4,h5,h6{--font-size:55px;--line-height:70px}}@media (max-width:56.24em){::slotted(h1),::slotted(h2),::slotted(h3),::slotted(h4),::slotted(h5),::slotted(h6),h1,h2,h3,h4,h5,h6{max-width:25em}}::slotted(p),p{--font-family:var(--font-secondary);--font-size:16px;--line-height:28px;--letter-spacing:0.03em;--font-weight:300;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:35em;margin:0 0 1.618em}@media (min-width:78.125em){::slotted(p),p{--font-size:18px;--line-height:30px}}@media (min-width:105.0625em){::slotted(p),p{--font-size:22px;--line-height:36px}}@media (max-width:37.49em){::slotted(p),p{display:none}}.trumpet,::slotted(.trumpet){--font-family:var(--font-primary);--font-size:18px;--line-height:28px;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);display:block;margin-bottom:.618em}@media (min-width:78.125em){.trumpet,::slotted(.trumpet){--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){.trumpet,::slotted(.trumpet){--font-size:24px;--line-height:34px}}a{color:var(--color-red);color:var(--link-color,var(--color-red))}";

class ContentBlock extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      trumpet: { type: String },
      headline: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.trumpet = '';
    this.headline = '';
    this.text = '';
  }

  getTextWithoutParagraph(text) {
    const includesParagraphTag = text && text.startsWith('<p>') && text.endsWith('</p>');
    if (includesParagraphTag) {
      return text = text.substring(3, text.lastIndexOf('</p>'));
    }
    return text;
  }

  render() {
    const textWithoutParagraph = this.getTextWithoutParagraph(this.text);

    return T`<div>
      ${this.trumpet ? T`<span class="trumpet">${this.trumpet}</span>` : ''}
      ${this.headline ? T`<h1>${this.headline}</h1>` : ''}
      ${this.text ? T`<p>${stringToDocumentFragment(textWithoutParagraph)}</p>` : ''}
      <slot></slot>
    </div>`;
  }
}

customElements.define('dsb-content-block', ContentBlock);

export { ContentBlock };
