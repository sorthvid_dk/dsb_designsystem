import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host{display:grid;grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1);width:calc(min(100vw, var(--page-width))/24*22);margin:0 auto}@media (min-width:37.5em){:host{grid-template-columns:1fr 1fr;width:calc(min(100vw, var(--page-width))/24*20);grid-gap:var(--units-2);gap:var(--units-2)}}@media (min-width:56.25em){:host{grid-template-columns:1fr 1fr 1fr}}";

class CenteredGrid extends h {
  static get styles() {
    return [css_248z];
  }

  render() {
    return T` <slot></slot> `;
  }
}

customElements.define('dsb-centered-grid', CenteredGrid);

export { CenteredGrid };
