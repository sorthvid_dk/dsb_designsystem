import { h, T } from '../lit-element-a21c046d.js';
import { s as stringToDocumentFragment } from '../utils-4e70fb44.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}section{position:relative}:host .text{--font-family:var(--font-secondary);--font-size:18px;--font-weight:300;--line-height:30px;--letter-spacing:0.04em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:27em;margin-top:0;margin-bottom:1.618em}@media (min-width:78.125em){:host .text{--font-size:22px;--line-height:34px}}@media (min-width:105.0625em){:host .text{--font-size:26px;--line-height:40px}}:host .text a{color:var(--color-white)}@media (max-width:56.24em){:host{color:var(--color-black)}:host .text a{color:var(--color-red)}}:host([variant=split]){color:var(--color-black)}:host([variant=split]) .text,:host([variant=split]) .text a{color:var(--color-white)}@media (min-width:56.25em){:host([variant=split]) .text-container{color:var(--color-white)}}@media (max-width:56.24em){:host([variant=split]) .text-container{--button-background:var(--color-red);--button-color:var(--color-white)}:host([variant=split]) .text-container .headline,:host([variant=split]) .text-container .text,:host([variant=split]) .text-container .trumpet{color:var(--color-black)}:host([variant=split]) .text-container .headline a,:host([variant=split]) .text-container .text a,:host([variant=split]) .text-container .trumpet a{color:var(--color-red)}}:host([variant=full]){color:var(--color-black)}:host([variant=full]) .breadcrumbs{padding-top:var(--header-show-headline-expanded)}@media (min-width:56.25em){:host([variant=full]){color:var(--color-white)}:host([variant=full]) .content{position:relative;grid-template-columns:1fr 0}:host([variant=full]) .image{position:absolute;left:0;right:0;height:100%;z-index:0}:host([variant=full]) .image:after{content:\"\";background:-webkit-gradient(linear,right top,left top,from(transparent),color-stop(4.4%,rgba(0,0,0,.006)),color-stop(9.5%,rgba(0,0,0,.024)),color-stop(15.2%,rgba(0,0,0,.052)),color-stop(21.5%,rgba(0,0,0,.088)),color-stop(28.1%,rgba(0,0,0,.13)),color-stop(35.2%,rgba(0,0,0,.176)),color-stop(42.5%,rgba(0,0,0,.225)),color-stop(50%,rgba(0,0,0,.275)),color-stop(57.6%,rgba(0,0,0,.324)),color-stop(65.2%,rgba(0,0,0,.37)),color-stop(72.7%,rgba(0,0,0,.412)),color-stop(80%,rgba(0,0,0,.448)),color-stop(87.1%,rgba(0,0,0,.476)),color-stop(93.7%,rgba(0,0,0,.494)),to(rgba(0,0,0,.5)));background:linear-gradient(270deg,transparent 0,rgba(0,0,0,.006) 4.4%,rgba(0,0,0,.024) 9.5%,rgba(0,0,0,.052) 15.2%,rgba(0,0,0,.088) 21.5%,rgba(0,0,0,.13) 28.1%,rgba(0,0,0,.176) 35.2%,rgba(0,0,0,.225) 42.5%,rgba(0,0,0,.275) 50%,rgba(0,0,0,.324) 57.6%,rgba(0,0,0,.37) 65.2%,rgba(0,0,0,.412) 72.7%,rgba(0,0,0,.448) 80%,rgba(0,0,0,.476) 87.1%,rgba(0,0,0,.494) 93.7%,rgba(0,0,0,.5));position:absolute;top:0;right:0;bottom:0;left:0}:host([variant=full]) .text-container{position:relative;z-index:1}}@media (max-width:56.24em){:host([variant=full]) .text-container{--button-background:var(--color-red);--button-color:var(--color-white)}}:host([variant=sub]){color:var(--color-white)}:host([variant=sub]) .background{--position:100%;overflow:hidden}:host([variant=sub]) .image div{top:0;right:0;bottom:0;left:0;position:absolute}:host([variant=sub]) .content{position:relative;grid-template-columns:1fr 0}@media (max-width:56.24em){:host([variant=sub]) .text-container{padding-left:0;padding-right:0}}.deck{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0 calc(min(100vw, var(--page-width))/24*1);max-width:var(--page-width);margin:0 auto}.badge{position:absolute;padding-top:calc(min(100vw, var(--page-width))/24*1);right:calc(min(100vw, var(--page-width))/24*2);z-index:5}.background{--position:50%;background:-webkit-gradient(linear,left top,left bottom,from(var(--color-red)),color-stop(0,transparent));background:linear-gradient(var(--color-red) var(--position),transparent 0);width:101%;height:101%;position:absolute;z-index:-1;top:0;left:0}@media (min-width:56.25em){.background{background:var(--color-red)}}.background .image,.background .image:after{position:absolute;top:0;right:0;bottom:0;left:0}.background .image:after{content:\"\";background:-webkit-gradient(linear,right top,left top,from(transparent),color-stop(4.4%,rgba(0,0,0,.006)),color-stop(9.5%,rgba(0,0,0,.024)),color-stop(15.2%,rgba(0,0,0,.052)),color-stop(21.5%,rgba(0,0,0,.088)),color-stop(28.1%,rgba(0,0,0,.13)),color-stop(35.2%,rgba(0,0,0,.176)),color-stop(42.5%,rgba(0,0,0,.225)),color-stop(50%,rgba(0,0,0,.275)),color-stop(57.6%,rgba(0,0,0,.324)),color-stop(65.2%,rgba(0,0,0,.37)),color-stop(72.7%,rgba(0,0,0,.412)),color-stop(80%,rgba(0,0,0,.448)),color-stop(87.1%,rgba(0,0,0,.476)),color-stop(93.7%,rgba(0,0,0,.494)),to(rgba(0,0,0,.5)));background:linear-gradient(270deg,transparent 0,rgba(0,0,0,.006) 4.4%,rgba(0,0,0,.024) 9.5%,rgba(0,0,0,.052) 15.2%,rgba(0,0,0,.088) 21.5%,rgba(0,0,0,.13) 28.1%,rgba(0,0,0,.176) 35.2%,rgba(0,0,0,.225) 42.5%,rgba(0,0,0,.275) 50%,rgba(0,0,0,.324) 57.6%,rgba(0,0,0,.37) 65.2%,rgba(0,0,0,.412) 72.7%,rgba(0,0,0,.448) 80%,rgba(0,0,0,.476) 87.1%,rgba(0,0,0,.494) 93.7%,rgba(0,0,0,.5))}.background:after{content:\"\";background:url('data:image/svg+xml;utf8,<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1440 103\"><path fill=\"%23FFFFFF\" fill-rule=\"evenodd\" d=\"M0 39.27L239.06 83.6a400 400 0 00103.51 5.53l1.8-.14L1440 0v102.9H0V39.27z\"/></svg>');background-repeat:no-repeat;background-position:bottom;background-size:101%;position:absolute;top:0;width:100%;height:calc(var(--position) + 2px);pointer-events:none}@media (min-width:56.25em){.background:after{height:calc(100% + 2px)}}.breadcrumbs{z-index:1;padding-top:var(--header-no-headline-expanded)}@media (min-width:56.25em){.breadcrumbs{margin-left:calc(min(100vw, var(--page-width))/24*1)}}.content{display:grid;grid-template-rows:auto auto}@media (min-width:56.25em){.content{grid-template-rows:auto;grid-template-columns:1fr 1fr;grid-gap:12px;gap:12px}}.image,.image>div{position:relative}.image>div{width:100%;padding-bottom:75%}@media (min-width:56.25em){.image>div{height:100%;padding-bottom:0}}img{position:absolute;width:100%;height:100%;-o-object-fit:cover;object-fit:cover}.trumpet{--font-family:var(--font-primary);--font-size:18px;--line-height:28px;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);margin-top:0;margin-bottom:.618em}@media (min-width:78.125em){.trumpet{--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){.trumpet{--font-size:24px;--line-height:34px}}.headline{--font-family:var(--font-primary);--font-size:32px;--line-height:42px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;margin-top:0;margin-bottom:.618em}@media (min-width:37.5em){.headline{--font-size:38px;--line-height:48px}}@media (min-width:78.125em){.headline{--font-size:60px;--line-height:74px}}@media (min-width:105.0625em){.headline{--font-size:80px;--line-height:100px}}.text-container{max-width:100%;z-index:4;padding:0 calc(min(100vw, var(--page-width))/24*1);text-align:left;margin:32px 0 64px}@media (max-width:56.24em){.text-container{-webkit-box-ordinal-group:2;-ms-flex-order:1;order:1}}@media (min-width:37.5em){.text-container{text-align:left;padding:32px calc(min(100vw, var(--page-width))/24*1) 96px calc(min(100vw, var(--page-width))/24*1)}}@media (min-width:56.25em){.text-container{position:relative;display:inline-block;-webkit-box-sizing:border-box;box-sizing:border-box;margin:0;padding-top:64px;padding-bottom:160px}}";

class Hero extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      src: { type: String },
      alt: { type: String },
      headline: { type: String },
      trumpet: { type: String },
      variant: { type: String, reflect: true },
      text: { type: String },
      ctaPrefix: { type: String },
      ctaInfo: { type: String },
      buttonLabel: { type: String },
      buttonIcon:  { type: String },
    };
  }

  constructor() {
    super();
    this.src = '';
    this.alt = '';
    this.text = '';
    this.trumpet = '';
    this.headline = '';
    this.variant = 'full';
    this.ctaPrefix = '';
    this.ctaInfo = '';
    this.buttonLabel = '';
    this.buttonIcon = '';
  }

  getButtonColorForHero() {
    let result = '';

    switch (this.variant) {
      case 'full':
        result = 'red';
        break;
      case 'sub':
        result = this.src ? 'red' : 'ghost';
        break;
      default:
        result = 'ghost';
        break;
    }
    return result;
  }

  imageBlock() {
    if (!this.src) {
      return '';
    }

    return T`
      <div class="image">
        <div>
          <img src=${this.src} alt=${this.alt} />
        </div>
      </div>
    `;
  }

  badgeSlot() {
    if (this.variant !== 'sub' || (this.variant === 'sub' && this.src)) {
      return T`
        <div class="badge"><slot name="badge"></slot></div>
      `;
    } else {
      return '';
    }
  }

  render() {
    return T`
      <section>
        <div class="deck">
          <span class="background">
            ${this.variant === 'sub' ? this.imageBlock() : ''}
          </span>
          <div class="breadcrumbs"><slot name="breadcrumbs"></slot></div>
          ${this.badgeSlot()}

          <div class="content">
            <div class="text-container">
              ${this.trumpet
                ? T`<p class="trumpet">${this.trumpet}</p>`
                : ''}

              <h1 class="headline">${this.headline}</h1>

              <div class="text">${stringToDocumentFragment(this.text)}</div>

              <div class="cta">
                <dsb-cta
                  .prefix=${this.ctaPrefix}
                  .info=${this.ctaInfo}
                >
                  ${this.buttonLabel
                    ? T`<dsb-button
                        label=${this.buttonLabel}
                        icon=${this.buttonIcon}
                        variant=${this.getButtonColorForHero()}
                      ></dsb-button>`
                    : ''}
                </dsb-cta>

              </div>

            </div>

            ${this.variant !== 'sub' ? this.imageBlock() : ''}
          </div>
        </div>
      </section>
    `;
  }
}

customElements.define('dsb-hero', Hero);

export { Hero };
