import { h, T } from '../lit-element-a21c046d.js';

var css_248z = "@charset \"UTF-8\";:host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host{display:block;-webkit-box-sizing:border-box;box-sizing:border-box}@media (min-width:56.25em){:host{padding:0 calc(min(100vw, var(--page-width))/24*1)}}:host([background]) .content{padding:var(--layout-5) calc(min(100vw, var(--page-width))/24*1)}:host([background=red]) .content{background-color:var(--color-red);color:var(--color-white)}:host([background=blue]) .content{background-color:var(--color-blue);color:var(--color-white)}:host([background=grey]) .content{background-color:var(--color-grey-1)}.headline{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start;justify-items:flex-start;place-items:flex-start;grid-gap:var(--units-2);gap:var(--units-2)}@media (min-width:56.25em){.headline{grid-gap:var(--units-4);gap:var(--units-4)}}.content{display:grid;-webkit-box-sizing:border-box;box-sizing:border-box;grid-gap:var(--units-2);gap:var(--units-2);padding:0 calc(min(100vw, var(--page-width))/24*1)}@media (min-width:37.5em){.content{grid-template-columns:40% calc(60% - min(100vw, var(--page-width))/24*1)}}h2{--font-family:var(--font-primary);--font-size:22px;--line-height:32px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;margin:0}@media (min-width:37.5em){h2{--font-size:20px;--line-height:30px}}@media (min-width:56.25em){h2{--font-size:22px;--line-height:32px}}@media (min-width:78.125em){h2{--font-size:24px;--line-height:34px}}@media (min-width:105.0625em){h2{--font-size:32px;--line-height:44px}}";

class Article extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      headline: { type: String },
      background: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.background = '';
  }

  render() {
    return T`
      <div class="content">
        <div class="headline">
          <h2>${this.headline}</h2>
          <slot name="subheadline"></slot>
        </div>
        <slot></slot>
      </div>
    `;
  }
}

customElements.define('dsb-article', Article);

export { Article };
