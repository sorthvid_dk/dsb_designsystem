import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}:host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host{--icon-size:1.2em;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;height:48px}:host([fullwidth]){width:100%}:host([fullwidth]) button{padding-left:0;padding-right:0}:host([disabled]){pointer-events:none}:host([disabled]) button{opacity:.4;pointer-events:none}button{--font-family:var(--font-primary);--font-size:16px;--line-height:28px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);-webkit-transition:background-color .2s;transition:background-color .2s;-webkit-box-flex:1;-ms-flex:auto;flex:auto;display:inline-grid;grid-auto-columns:auto;grid-auto-flow:column;grid-gap:1em;gap:1em;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;background-color:var(--color-red);background-color:var(--button-background,var(--color-red));color:var(--color-white);color:var(--button-color,var(--color-white));border:0;border-radius:var(--border-radius);padding:0 var(--units-2);white-space:nowrap;letter-spacing:.05em;cursor:pointer;z-index:0;position:relative}button.focus-visible,button:focus-visible,button:focus:not(.focus-visible){outline:none}button.focus-visible:before,button:focus-visible:before{content:\"\";position:absolute;width:100%;height:100%;padding:.33rem;left:-.33rem;top:-.33rem;-webkit-box-shadow:0 0 0 2px var(--color-black);box-shadow:0 0 0 2px var(--color-black);border-radius:var(--border-radius);-webkit-box-sizing:content-box;box-sizing:content-box}.bg--blue button.focus-visible:before,.bg--blue button:focus-visible:before,.bg--darkgrey button.focus-visible:before,.bg--darkgrey button:focus-visible:before,.bg--red button.focus-visible:before,.bg--red button:focus-visible:before{-webkit-box-shadow:0 0 0 2px var(--color-white);box-shadow:0 0 0 2px var(--color-white)}@media (min-width:105.0625em){button{--font-size:18px;--line-height:30px}}@media (min-width:145em){.big{height:var(--units-5);min-width:200px;padding:0 var(--units-5)}.big--ghost{padding:0 calc(var(--units-5) - var(--border-width))}}.secondary{background-color:var(--color-blue);background-color:var(--button-background,var(--color-blue))}.white{background-color:var(--color-white);background-color:var(--button-background,var(--color-white));color:var(--color-black);color:var(--button-color,var(--color-black))}.white .hover{background-color:var(--color-black)}.ghost{background-color:transparent;background-color:var(--button-background,transparent);border:var(--border-width) solid;padding:0 calc(var(--units-4) - var(--border-width))}.ghost .hover{background-color:var(--color-black)}.hover{-webkit-transition:-webkit-transform .3s var(--ease-in-out);transition:-webkit-transform .3s var(--ease-in-out);transition:transform .3s var(--ease-in-out);transition:transform .3s var(--ease-in-out),-webkit-transform .3s var(--ease-in-out);-webkit-transform:scaleX(0);transform:scaleX(0);-webkit-transform-origin:left;transform-origin:left;position:absolute;top:0;right:0;bottom:0;left:0;border-radius:inherit;background-color:var(--color-white);opacity:.1;z-index:-1}:hover .hover{-webkit-transform:scaleX(1);transform:scaleX(1)}";

//import { classMap } from 'lit/directives/class-map.js';

class Button extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      icon: { type: String },
      label: { type: String },
      variant: { type: String },
      big: { type: Boolean },
      disabled: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.icon = '';
    this.label = '';
    this.variant = 'primary';
    this.big = false;
    this.disabled = false;
    this.fullwidth = false;
  }

  render() {
    return T`<button
      ?disabled="${this.disabled}"
      ?fullwidth="${this.fullwidth}"
      aria-label="${this.label || this.icon}"
      class="${this.variant} ${this.big ? 'big' : ''}"
    >
      ${this.icon ? T`<dsb-icon icon=${this.icon}></dsb-icon>` : ''}
      <span>${this.label}</span>
      <slot></slot>
      <span class="hover" />
    </button>`;
  }

  // createRenderRoot() {
  //   return this.attachShadow({ mode: 'open', delegatesFocus: true });
  // }
}

customElements.define('dsb-button', Button);

export { Button };
