import { h, T } from '../lit-element-a21c046d.js';
import './image-link.js';
import './icon.js';
import './getIcon.js';
import '../image-block-72b49e4f.js';
import '../directive-b9dab5a2.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}:host{--split-wide-column:60%;display:block;max-width:var(--page-width);margin:0 auto;padding:0 calc(min(100vw, var(--page-width))/24*1);-webkit-box-sizing:border-box;box-sizing:border-box}.sixtyfourty{display:grid;grid-template-columns:1fr;grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1)}@media (min-width:56.25em){.sixtyfourty{grid-template-columns:var(--split-wide-column) 1fr}}";

class DSBSixtyFourty extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      sixtyHref: { type: String },
      sixtyText: { type: String },
      sixtyHeadline: { type: String },
      sixtySrc: { type: String },
      sixtyOverlap: { type: Boolean },
      sixtyAlt: { type: String },
      fourtyHref: { type: String },
      fourtyText: { type: String },
      fourtyHeadline: { type: String },
      fourtySrc: { type: String },
      fourtyOverlap: { type: Boolean },
      fourtyAlt: { type: String },
    }
  }

  constructor() {
    super();
    this.sixtyHref = '#';
    this.sixtyText = 'Sixty Description...';
    this.sixtyHeadline = 'Sixty headline';
    this.sixtySrc = 'https://i.ibb.co/M2K3DSN/boats.jpg';
    this.sixtyAlt = 'sixty alt text';
    this.sixtyOverlap = false;
    this.fourtyHref = '#';
    this.fourtyText = 'Fourty description...';
    this.fourtyHeadline = 'Fourty headline';
    this.fourtySrc = 'https://i.ibb.co/pd5X6kx/image-dsb.jpg';
    this.fourtyOverlap = false;
    this.fourtyAlt = 'fourty alt text';
  }

  render() {
    return T`
    <div class="sixtyfourty">

      <div class="sixty">
          <dsb-image-link
            href=${this.sixtyHref}
            headline=${this.sixtyHeadline}
            src=${this.sixtySrc}
            text=${this.sixtyText}
            .overlap=${this.sixtyOverlap}
            alt=${this.sixtyAlt}>
            <slot name="sixtybadge"></slot>
          </dsb-image-link>
      </div>

      <div class="fourty">
          <dsb-image-link
            href=${this.fourtyHref}
            headline=${this.fourtyHeadline}
            src=${this.fourtySrc}
            text=${this.fourtyText}
            .overlap=${this.fourtyOverlap}
            alt=${this.fourtyAlt}>
            <slot name="fourtybadge"></slot>
          </dsb-image-link>
      </div>

    </div>`;
  }
}

customElements.define('dsb-sixty-fourty', DSBSixtyFourty);

export { DSBSixtyFourty };
