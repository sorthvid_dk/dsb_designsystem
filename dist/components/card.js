import { h, T } from '../lit-element-a21c046d.js';
import './icon.js';
import './getIcon.js';

var css_248z = ":host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}:host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif}:host{display:inline-block;width:100%}:host div{position:relative;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;width:100%;height:100%;-webkit-box-sizing:border-box;box-sizing:border-box;padding:48px 24px;border-radius:var(--border-radius);background-color:var(--color-grey-1);text-align:left}:host div:hover dsb-icon{-webkit-transform:translateX(.25em);transform:translateX(.25em)}:host .label{--font-size:12px;--font-weight:400;--line-height:20px;position:absolute;top:32px;color:var(--color-red)}:host .label,:host .title{--font-family:var(--font-primary);--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0)}:host .title{--font-weight:bold;--font-size:18px;--line-height:28px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:.618em;gap:.618em;margin-top:0;margin-bottom:.618em;width:-webkit-fit-content;width:-moz-fit-content;width:fit-content}@media (min-width:78.125em){:host .title{--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){:host .title{--font-size:24px;--line-height:34px}}:host .title dsb-icon{--size:1em;-webkit-transition:-webkit-transform .1s;transition:-webkit-transform .1s;transition:transform .1s;transition:transform .1s,-webkit-transform .1s;color:var(--color-red);-ms-flex-negative:0;flex-shrink:0;margin-bottom:.15em}:host .text{--font-family:var(--font-secondary);--font-size:16px;--line-height:28px;--font-weight:300;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);margin:0}@media (min-width:105.0625em){:host .text{--font-size:18px;--line-height:30px}}";

class Card extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      active: { type: Boolean, state: true },
      headline: { type: String },
      label: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.active = false;
    this.headline = '';
    this.label = '';
    this.text = '';
  }

  render() {
    return T` <div class="${this.active ? '-active' : ''}">
      <span class="label">${this.label}</span>
      <p class="title">
        ${this.headline}
        <dsb-icon icon="arrow--right" />
      </p>
      <p class="text">${this.text}<slot></slot></p>
    </div>`;
  }
}

customElements.define('dsb-card', Card);

export { Card };
