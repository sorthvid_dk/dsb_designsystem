import { h, T } from '../lit-element-a21c046d.js';
import { r as randomId } from '../id-839a82d1.js';
import { f as flip } from '../flip-60377d92.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}:host{display:inline-block;width:100%;background-color:var(--color-grey-1);border-radius:var(--border-radius)}:host([isopen]) dsb-icon{-webkit-transform:rotate(45deg) scale(1.05);transform:rotate(45deg) scale(1.05)}dsb-icon{-webkit-transition:-webkit-transform .2s var(--ease-in-out);transition:-webkit-transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out),-webkit-transform .2s var(--ease-in-out)}.title{display:grid;grid-auto-flow:column;grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1)}@media (min-width:37.5em){.title{grid-gap:calc(min(100vw, var(--page-width))/24*2);gap:calc(min(100vw, var(--page-width))/24*2)}}[hidden]{height:0;display:block}.index{color:var(--color-red)}.details{--icon-size:var(--units-2);--icon-color:var(--color-red);display:-webkit-box;display:-ms-flexbox;display:flex;grid-gap:var(--units-2);gap:var(--units-2);-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-sizing:border-box;box-sizing:border-box;text-align:left;width:100%;min-height:var(--units-6);padding:var(--units-1) calc(min(100vw, var(--page-width))/24*1);cursor:pointer;border:0;background-color:transparent;position:relative}.details.focus-visible,.details:focus-visible,.details:focus:not(.focus-visible){outline:none}.details.focus-visible:before,.details:focus-visible:before{content:\"\";position:absolute;width:100%;height:100%;padding:.33rem;left:-.33rem;top:-.33rem;-webkit-box-shadow:0 0 0 2px var(--color-black);box-shadow:0 0 0 2px var(--color-black);border-radius:var(--border-radius);-webkit-box-sizing:content-box;box-sizing:content-box}.bg--blue .details.focus-visible:before,.bg--blue .details:focus-visible:before,.bg--darkgrey .details.focus-visible:before,.bg--darkgrey .details:focus-visible:before,.bg--red .details.focus-visible:before,.bg--red .details:focus-visible:before{-webkit-box-shadow:0 0 0 2px var(--color-white);box-shadow:0 0 0 2px var(--color-white)}@media (min-width:37.5em){.details{padding-left:calc(min(100vw, var(--page-width))/24*2)}}.details span{--font-family:var(--font-secondary);--font-size:16px;--line-height:28px;--letter-spacing:0.03em;--font-weight:300;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:35em;--font-family:var(--font-primary)}@media (min-width:78.125em){.details span{--font-size:18px;--line-height:30px}}@media (min-width:105.0625em){.details span{--font-size:22px;--line-height:36px}}.summary{overflow:hidden}.summary>div{padding:0 calc(min(100vw, var(--page-width))/24*1) var(--units-2)}@media (min-width:37.5em){.summary>div{padding:0 calc(min(100vw, var(--page-width))/24*3) var(--units-3) calc(min(100vw, var(--page-width))/24*2)}}";

class AccordionItem extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      headline: { type: String },
      isOpen: { type: Boolean, reflect: true },
      index: { type: Number },
      id: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.isOpen = false;
    this.index = -1;
    this.id = randomId();
  }

  connectedCallback() {
    super.connectedCallback();
    const items = [
      ...this.parentElement.querySelectorAll('dsb-accordion-item'),
    ];
    this.index = items.findIndex((i) => i === this) + 1;
  }

  toggle() {
    const state = flip.getState(this.shadowRoot.querySelector('.summary'));
    this.isOpen = !this.isOpen;
    flip.expand(state);
  }

  render() {
    return T`
      <button
        class="details"
        aria-controls=${this.id}
        aria-expanded=${this.isOpen}
        @click=${this.toggle}
      >
        <div class="title">
          <span class="index">${this.index}</span>
          <span>${this.headline}</span>
        </div>
        <slot name="headline"></slot>
        <dsb-icon icon="plus"></dsb-icon>
      </button>
      <div
        class="summary"
        id=${this.id}
        aria-hidden=${!this.isOpen}
        ?hidden=${!this.isOpen}
      >
        <div>
          <slot></slot>
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-accordion-item', AccordionItem);

export { AccordionItem };
