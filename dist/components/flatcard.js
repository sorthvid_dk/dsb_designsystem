import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px;--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif}:host{display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;width:100%;-webkit-box-sizing:border-box;box-sizing:border-box;min-height:var(--units-6);padding:var(--units-1) calc(min(100vw, var(--page-width))/24*1);border-radius:var(--border-radius);background-color:var(--color-grey-1)}@media (min-width:37.5em){:host{padding-top:0;padding-bottom:0}}a{--font-family:var(--font-primary);--font-size:16px;--line-height:28px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);color:var(--color-black);text-decoration:none;display:grid;grid-auto-flow:column;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;width:inherit}@media (min-width:105.0625em){a{--font-size:18px;--line-height:30px}}a:hover dsb-icon{-webkit-transform:translateX(var(--spacing-2));transform:translateX(var(--spacing-2))}div,span{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}dsb-icon{-webkit-transition:-webkit-transform .2s var(--ease-in-out);transition:-webkit-transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out),-webkit-transform .2s var(--ease-in-out)}span{display:-webkit-box;display:-ms-flexbox;display:flex;grid-gap:var(--units-1);gap:var(--units-1)}.right{--icon-color:var(--color-red)}.price{--font-family:var(--font-primary);--font-size:22px;--line-height:32px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;color:var(--color-red)}@media (min-width:37.5em){.price{--font-size:20px;--line-height:30px}}@media (min-width:56.25em){.price{--font-size:22px;--line-height:32px}}@media (min-width:78.125em){.price{--font-size:24px;--line-height:34px}}@media (min-width:105.0625em){.price{--font-size:32px;--line-height:44px}}.mini{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0)}";

//import { classMap } from 'lit/directives/class-map.js';

class Flatcard extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      url: { type: String },
      label: { type: String },
      from: { type: String },
      to: { type: String },
      prefix: { type: String },
      price: { type: String },
    };
  }

  constructor() {
    super();
    this.url = '';
    this.label = '';
    this.from = '';
    this.to = '';
    this.price = '';
    this.prefix = '';
  }

  render() {
    return T`
      <a href=${this.url}>
        <div>
          ${this.label
            ? this.label
            : T` <span>
                ${this.from}
                <dsb-icon icon="arrow--right"></dsb-icon>
                ${this.to}
              </span>`}
        </div>
        <div class="right">
          ${!this.price
            ? T`<dsb-icon icon="arrow--right"></dsb-icon>`
            : T`<span
                ><span class="mini">${this.prefix}</span>
                <span class="price">${this.price}</span></span
              >`}
        </div>
      </a>
    `;
  }
}

customElements.define('dsb-flatcard', Flatcard);

export { Flatcard };
