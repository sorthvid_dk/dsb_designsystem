import { h, T } from '../lit-element-a21c046d.js';
import './breadcrumbs-item.js';

var css_248z = "ul{width:100%;padding:0;margin:0;display:grid;grid-auto-flow:column;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start;list-style:none}";

class Breadcrumbs extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      items: { type: Array}
    };
  }

  constructor() {
    super();
    this.items = [];
  }

  render() {
    return this.items.length > 1 ?
      T`
        <ul>
          ${this.items.map(item => T`
            <dsb-breadcrumbs-item label=${item.text} href=${item.url} />
          `)}
        </ul>
      ` : '';
  }
}

customElements.define('dsb-breadcrumbs', Breadcrumbs);

export { Breadcrumbs };
