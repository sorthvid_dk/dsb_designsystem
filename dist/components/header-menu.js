import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}@-webkit-keyframes slide-in{0%{-webkit-transform:translate(-20px);transform:translate(-20px)}to{opacity:1}}@keyframes slide-in{0%{-webkit-transform:translate(-20px);transform:translate(-20px)}to{opacity:1}}:host{display:grid;grid-gap:var(--units-5);gap:var(--units-5);width:calc(min(100vw, var(--page-width))/24*22);padding:var(--layout-6) 0}@media (min-width:56.25em){:host{width:calc(min(100vw, var(--page-width))/24*20);grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1);grid-auto-flow:column;grid-template-columns:calc(min(100vw, var(--page-width))/24*9) 1fr;padding:var(--layout-7) 0}}@media (max-width:56.24em){.left{-webkit-box-ordinal-group:2;-ms-flex-order:1;order:1}}slot[name=left]{--font-family:var(--font-primary);--font-size:18px;--line-height:28px;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);display:grid;grid-gap:var(--units-2);gap:var(--units-2)}@media (min-width:78.125em){slot[name=left]{--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){slot[name=left]{--font-size:24px;--line-height:34px}}slot:not([name]){--font-family:var(--font-primary);--font-size:32px;--line-height:42px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;--font-weight:normal;display:grid;grid-gap:var(--units-2);gap:var(--units-2)}@media (min-width:78.125em){slot:not([name]){--font-size:45px;--line-height:58px}}@media (min-width:105.0625em){slot:not([name]){--font-size:55px;--line-height:70px}}";

class HeaderMenu extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return T`
      <div class="left">
        <slot name="left"></slot>
      </div>
      <div class="right">
        <slot></slot>
      </div>
    `;
  }
}

customElements.define('dsb-header-menu', HeaderMenu);

export { HeaderMenu };
