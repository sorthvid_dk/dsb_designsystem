import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host{display:inline-block}a{background-image:-webkit-gradient(linear,left top,left bottom,from(currentColor),to(currentColor));background-image:linear-gradient(currentColor,currentColor);background-position:0 100%;background-repeat:no-repeat;background-size:0 var(--spacing-1);-webkit-transition:background-size .2s ease;transition:background-size .2s ease;text-decoration:none;color:currentColor}a:hover{background-size:100% var(--spacing-1)}";

class UnderlineLink extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      href: { type: String },
      label: { type: String },
    };
  }

  constructor() {
    super();
    this.href = '';
    this.label = '';
  }

  render() {
    return T` <a href=${this.href}>${this.label}<slot></slot></a> `;
  }
}

customElements.define('dsb-underline-link', UnderlineLink);

export { UnderlineLink };
