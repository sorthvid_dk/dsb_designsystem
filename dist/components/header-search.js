import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}:host{background:var(--color-red);color:var(--color-white);color:var(--header-search-text-color,var(--color-white));display:block;padding:var(--units-2)}.input{--icon-size:1em;--font-family:var(--font-primary);--font-size:32px;--line-height:42px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;border-bottom:var(--border-width) solid;display:grid;grid-auto-flow:column;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:start;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:var(--units-2);gap:var(--units-2);max-width:none}@media (min-width:78.125em){.input{--font-size:45px;--line-height:58px}}@media (min-width:105.0625em){.input{--font-size:55px;--line-height:70px}}input{font-family:var(--font-primary);font-size:1em;line-height:2;font-weight:400;color:currentColor;width:100%;background-color:transparent;border:0;outline:0;padding:0}input::-webkit-input-placeholder{color:currentColor}input::-moz-placeholder{color:currentColor}input:-ms-input-placeholder{color:currentColor}input::-ms-input-placeholder{color:currentColor}input::placeholder{color:currentColor}.header-search{position:relative;width:100%;max-width:800px;margin:0 auto;color:var(--color-white)}.header-search__form{--font-family:var(--font-primary);--font-size:32px;--line-height:42px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;width:100%;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;border-bottom:var(--border-width) solid;margin-bottom:1.618em}@media (min-width:78.125em){.header-search__form{--font-size:45px;--line-height:58px}}@media (min-width:105.0625em){.header-search__form{--font-size:55px;--line-height:70px}}.header-search__form svg{height:1em;margin-bottom:.1em;margin-right:.25em;-ms-flex-negative:0;flex-shrink:0;fill:currentColor}.header-search__suggestions{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;max-width:600px}.header-search__suggestions .mini{width:100%;margin-bottom:1.618em}.header-search .headline-2{font-weight:400}.header-search .tag{display:inline-block;border:var(--border-width) solid;padding:var(--units-1) var(--units-2);margin-right:var(--units-1);margin-bottom:var(--units-1);line-height:.8;white-space:nowrap;position:relative}.header-search .tag.focus-visible,.header-search .tag:focus-visible,.header-search .tag:focus:not(.focus-visible){outline:none}.header-search .tag.focus-visible:before,.header-search .tag:focus-visible:before{content:\"\";position:absolute;width:100%;height:100%;padding:.33rem;left:-.33rem;top:-.33rem;-webkit-box-shadow:0 0 0 2px var(--color-black);box-shadow:0 0 0 2px var(--color-black);border-radius:var(--border-radius);-webkit-box-sizing:content-box;box-sizing:content-box}.bg--blue .header-search .tag.focus-visible:before,.bg--blue .header-search .tag:focus-visible:before,.bg--darkgrey .header-search .tag.focus-visible:before,.bg--darkgrey .header-search .tag:focus-visible:before,.bg--red .header-search .tag.focus-visible:before,.bg--red .header-search .tag:focus-visible:before{-webkit-box-shadow:0 0 0 2px var(--color-white);box-shadow:0 0 0 2px var(--color-white)}.header-search .tag:hover{outline:1px solid}";

class HeaderSearch extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      headline: { type: String },
      placeholder: { type: String },
      ariaLabel: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.placeholder = '';
    this.ariaLabel = '';
  }

  //   connectedCallback() {
  //     super.connectedCallback();
  //     this.addEventListener('click', this.expand.bind(this));
  //   }

  //   disconnectedCallback() {
  //     super.disconnectedCallback();
  //     this.removeEventListener('click', this.expand.bind(this));
  //   }

  render() {
    return T`<section>
      <div class="input">
        <dsb-icon icon="magnifying-glass"></dsb-icon>
        <input
          type="search"
          id="site-search"
          name="q"
          aria-label=${this.ariaLabel}
          .placeholder=${this.placeholder}
        />
      </div>
      <slot></slot>
    </section>`;
  }
}

customElements.define('dsb-header-search', HeaderSearch);

export { HeaderSearch };
