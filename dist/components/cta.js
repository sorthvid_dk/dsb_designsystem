import { h, T } from '../lit-element-a21c046d.js';
import './button.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}.cta{display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;text-align:left}.main{margin-right:16px}.action{--font-family:var(--font-primary);--font-size:16px;--line-height:28px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);margin:0}@media (min-width:105.0625em){.action{--font-size:18px;--line-height:30px}}.label{padding-right:16px;border-right:1px solid}.info-container{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.info{--font-family:var(--font-secondary);--font-size:18px;--font-weight:300;--line-height:30px;--letter-spacing:0.04em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:27em;--font-weight:bold;margin:-.2em 0 0}@media (min-width:78.125em){.info{--font-size:22px;--line-height:34px}}@media (min-width:105.0625em){.info{--font-size:26px;--line-height:40px}}.mini{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);margin:0 8px 0 0}";

class Cta extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      label: { type: String },
      sublabel: { type: String },
      prefix: { type: String },
      info: { type: String },
    };
  }

  constructor() {
    super();
    this.label = '';
    this.sublabel = '';
    this.prefix = '';
    this.info = '';
  }

  render() {
    return T`
      <div class="cta">
        <div class="main">
          <slot></slot>
          ${this.label
            ? T`
                <div class="label">
                  <p class="action">${this.label}</p>
                  <p class="mini">${this.sublabel}</p>
                </div>
              `
            : ''}
        </div>
        ${this.info
          ? T`
              <div class="info-container">
                <p class="mini">${this.prefix}</p>
                <p class="info">${this.info}</p>
              </div>
            `
          : ''}
      </div>
    `;
  }
}

customElements.define('dsb-cta', Cta);

export { Cta };
