import { h, T } from '../lit-element-a21c046d.js';

var css_248z$1 = "@charset \"UTF-8\";li{font-weight:400;list-style:none}li>*{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:1.618em;gap:1.618em}li>:before{color:var(--color-red);-ms-flex-negative:0;flex-shrink:0;text-indent:0}.bullet>:before{content:\"•\"}.check{--icon-color:var(--color-red)}.check:before{margin-right:1.1em}.check:before,.number .index{width:1.618em;height:1.618em;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex}.number .index{font-family:var(--font-primary);color:var(--color-red);content:counter(list-counter);border:1px solid;border-radius:100px;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-ms-flex-negative:0;flex-shrink:0}";

var css_248z = "ol,ul{list-style:none;display:grid;grid-gap:.618em;gap:.618em;margin:0;padding:0}";

class Li extends h {
  static get styles() {
    return [css_248z$1];
  }

  static get properties() {
    return {
      variant: { type: String },
      number: { type: Number },
    };
  }

  constructor() {
    super();
    this.variant = '';
    this.number = 0;
  }

  connectedCallback() {
    super.connectedCallback();
    const items = [...this.parentElement.querySelectorAll('dsb-li')];
    this.number = items.findIndex((i) => i === this) + 1;
  }

  render() {
    return T`
      <li class=${this.variant}>
        <span>
          ${this.variant === 'check'
            ? T`<dsb-icon icon="check"></dsb-icon>`
            : ''}
          ${this.variant === 'number'
            ? T`<span class="index">${this.number}</span>`
            : ''}

          <slot></slot>
        </span>
      </li>
    `;
  }
}

customElements.define('dsb-li', Li);

/**
 * OL / UL
 */
class Ol extends h {
  static get styles() {
    return [css_248z];
  }

  render() {
    return T`
      <ol>
        <slot></slot>
      </ol>
    `;
  }
}

customElements.define('dsb-ol', Ol);

class Ul extends h {
  static get styles() {
    return [css_248z];
  }

  render() {
    return T`<ul>
      <slot></slot>
    </ul>`;
  }
}

customElements.define('dsb-ul', Ul);

export { Li, Ol, Ul };
