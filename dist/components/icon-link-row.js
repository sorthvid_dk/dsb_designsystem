import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}.split{display:grid;grid-template-columns:1fr;grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1)}@media (min-width:56.25em){.split{grid-template-columns:repeat(2,1fr)}}@media (min-width:56.25em){.split.uneven{--gap:calc(min(100vw, var(--page-width))/24*1);grid-template-columns:calc(60% - var(--gap)) 40%}}.flex{display:-webkit-box;display:-ms-flexbox;display:flex}.flex-column{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.align-center{-webkit-box-align:center;-ms-flex-align:center;align-items:center}.align-end{-webkit-box-align:end;-ms-flex-align:end;align-items:flex-end}.justify-center{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.justify-end{-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.deck{width:100%;min-height:inherit;margin-bottom:var(--layout-4);-webkit-box-sizing:border-box;box-sizing:border-box}@media (min-width:56.25em){.deck{margin-bottom:var(--layout-6)}}@media (min-width:78.125em){.deck{margin-bottom:var(--layout-7)}}@media (min-width:145em){.deck{margin-bottom:var(--layout-8)}}.bg--blue .deck,.bg--darkgrey .deck,.bg--grey .deck,.bg--red .deck,.deck.bg--blue,.deck.bg--darkgrey,.deck.bg--grey,.deck.bg--red{padding-top:var(--layout-4);padding-bottom:var(--layout-4)}@media (min-width:56.25em){.bg--blue .deck,.bg--darkgrey .deck,.bg--grey .deck,.bg--red .deck,.deck.bg--blue,.deck.bg--darkgrey,.deck.bg--grey,.deck.bg--red{padding-top:var(--layout-5);padding-bottom:var(--layout-5)}}@media (min-width:78.125em){.bg--blue .deck,.bg--darkgrey .deck,.bg--grey .deck,.bg--red .deck,.deck.bg--blue,.deck.bg--darkgrey,.deck.bg--grey,.deck.bg--red{padding-top:var(--layout-6);padding-bottom:var(--layout-6)}}@media (min-width:145em){.bg--blue .deck,.bg--darkgrey .deck,.bg--grey .deck,.bg--red .deck,.deck.bg--blue,.deck.bg--darkgrey,.deck.bg--grey,.deck.bg--red{padding-top:var(--layout-7);padding-bottom:var(--layout-7)}}.deck--collapse{margin-bottom:0}.page-width{max-width:var(--page-width);margin-left:auto;margin-right:auto}.px-1-col,.px-2-col{padding-left:calc(min(100vw, var(--page-width))/24*1);padding-right:calc(min(100vw, var(--page-width))/24*1)}@media (min-width:56.25em){.px-2-col,.px-2-col-m{padding-left:calc(min(100vw, var(--page-width))/24*2);padding-right:calc(min(100vw, var(--page-width))/24*2)}}@media (max-width:56.24em){:host .deck{border-top:1px solid var(--color-grey-1);border-bottom:1px solid var(--color-grey-1)}}:host .deck__content{display:grid;-ms-flex-line-pack:justify;align-content:space-between;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;place-content:space-between;grid-auto-flow:column;grid-gap:1px}@media (max-width:56.24em){:host .deck__content{grid-auto-flow:row;grid-template-columns:repeat(2,1fr)}}";

class IconLinkRow extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return T`
      <div class="icon-link-row">
        <div class="deck">
          <div class="deck__inner">
            <div class="deck__content">
              <slot></slot>
            </div>
          </div>
        </div>
      </div>
      `;
  }
}

customElements.define('dsb-icon-link-row', IconLinkRow);

export { IconLinkRow };
