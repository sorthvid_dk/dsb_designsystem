import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}li{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0)}a{background-image:-webkit-gradient(linear,left top,left bottom,from(currentColor),to(currentColor));background-image:linear-gradient(currentColor,currentColor);background-position:0 100%;background-repeat:no-repeat;background-size:0 var(--spacing-1);-webkit-transition:background-size .2s ease;transition:background-size .2s ease;text-decoration:none;color:currentColor}a:hover{background-size:100% var(--spacing-1)}a{position:relative}a.focus-visible,a:focus-visible,a:focus:not(.focus-visible){outline:none}a.focus-visible:before,a:focus-visible:before{content:\"\";position:absolute;width:100%;height:100%;padding:.33rem;left:-.33rem;top:-.33rem;-webkit-box-shadow:0 0 0 2px var(--color-black);box-shadow:0 0 0 2px var(--color-black);border-radius:var(--border-radius);-webkit-box-sizing:content-box;box-sizing:content-box}.bg--blue a.focus-visible:before,.bg--blue a:focus-visible:before,.bg--darkgrey a.focus-visible:before,.bg--darkgrey a:focus-visible:before,.bg--red a.focus-visible:before,.bg--red a:focus-visible:before{-webkit-box-shadow:0 0 0 2px var(--color-white);box-shadow:0 0 0 2px var(--color-white)}@media (max-width:37.49em){a{display:none}}.dimmed{opacity:.5}span:not(.dimmed){margin:0 4px;color:currentColor}@media (max-width:37.49em){span:not(.dimmed){display:none}}";

class BreadcrumbsItem extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      href: { type: String },
      label: { type: String },
    };
  }

  constructor() {
    super();
    this.href = '';
    this.label = '';
  }

  render() {
    return T`
      <li>
        ${this.href
          ? T`<a href=${this.href}>${this.label}</a><span>&gt;</span>`
          : T`<span class="dimmed">${this.label}</span>`}
      </li>
    `;
  }
}

customElements.define('dsb-breadcrumbs-item', BreadcrumbsItem);

export { BreadcrumbsItem };
