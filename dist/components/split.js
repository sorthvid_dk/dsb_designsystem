import { h, T } from '../lit-element-a21c046d.js';
import '../image-block-72b49e4f.js';
import './content-block.js';
import '../directive-b9dab5a2.js';
import '../utils-4e70fb44.js';

var css_248z = ":host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}:host{--split-wide-column:50%;display:block;max-width:var(--page-width);margin:0 auto;padding:0 calc(min(100vw, var(--page-width))/24*1);-webkit-box-sizing:border-box;box-sizing:border-box}:host([inverted]) .split{grid-template-columns:1fr var(--split-wide-column)}:host([inverted]) .content{padding-left:calc(min(100vw, var(--page-width))/24*1);padding-right:0}@media (min-width:56.25em){:host([inverted]) .image{-webkit-box-ordinal-group:2;-ms-flex-order:1;order:1}}:host([contain]){--object-fit:contain;--image-padding:calc(min(100vw, var(--page-width))/24*1)}:host([background=grey]) .split{background-color:var(--color-grey-1)}.split{display:grid;grid-template-columns:1fr;grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1)}@media (min-width:56.25em){.split{grid-template-columns:var(--split-wide-column) 1fr}}.content{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start;padding:calc(min(100vw, var(--page-width))/24*2) calc(min(100vw, var(--page-width))/24*1);padding-left:0}.content,.image{display:-webkit-box;display:-ms-flexbox;display:flex}.image{position:relative}";

class Split extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      inverted: { type: Boolean, reflect: true },
      src: { type: String },
      alt: { type: String },
      trumpet: { type: String },
      headline: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.inverted = false;
    this.src = '';
    this.alt = '';
    this.trumpet = '';
    this.headline = '';
    this.text = '';
  }

  render() {
    return T` <div class="split">
      <div class="image">
        <dsb-image-block aspect="1/1" src=${this.src} alt=${this.alt}>
          <slot name="badge"></slot>
          <slot name="right" slot="right"></slot>
        </dsb-image-block>
      </div>
      <div class="content">
        <dsb-content-block
          trumpet=${this.trumpet}
          headline=${this.headline}
          text=${this.text}
        >
          <slot></slot>
        </dsb-content-block>
      </div>
    </div>`;
  }
}

customElements.define('dsb-split', Split);

export { Split };
