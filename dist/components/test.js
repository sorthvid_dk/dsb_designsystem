import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif}:host{--font-family:var(--font-primary);--font-size:16px;--line-height:28px;--font-weight:bold;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);display:inline-block;padding:var(--units-2);border:1px dotted var(--color-red);border-radius:var(--border-radius)}@media (min-width:105.0625em){:host{--font-size:18px;--line-height:30px}}@media (min-width:56.25em){:host{margin:var(--units-2)}}:host([fancy]){--font-family:var(--font-primary);--font-size:22px;--line-height:32px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:12em;background:linear-gradient(140deg,#0ff,#7fffd4);color:var(--color-blue)}@media (min-width:37.5em){:host([fancy]){--font-size:20px;--line-height:30px}}@media (min-width:56.25em){:host([fancy]){--font-size:22px;--line-height:32px}}@media (min-width:78.125em){:host([fancy]){--font-size:24px;--line-height:34px}}@media (min-width:105.0625em){:host([fancy]){--font-size:32px;--line-height:44px}}";

class Test extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      headline: { type: String },
      fancy: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.headline = 'Default headline';
    this.fancy = false;
  }

  render() {
    return T`<p>
      My new test. With a ${this.headline}. And <slot></slot>
    </p>`;
  }
}

customElements.define('dsb-test', Test);

export { Test };
