import { h, T } from '../lit-element-a21c046d.js';
import './icon.js';
import './getIcon.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}:host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}.m-0{margin:0}.mt-0{margin-top:0}.mb-0{margin-bottom:0}.ml-0{margin-left:0}.mr-0{margin-right:0}.p-0{padding:0}.pt-0{padding-top:0}.pb-0{padding-bottom:0}.pl-0{padding-left:0}.pr-0{padding-right:0}.m-1{margin:12px}.mt-1{margin-top:12px}.mb-1{margin-bottom:12px}.ml-1{margin-left:12px}.mr-1{margin-right:12px}.p-1{padding:12px}.pt-1{padding-top:12px}.pb-1{padding-bottom:12px}.pl-1{padding-left:12px}.pr-1{padding-right:12px}.m-2{margin:24px}.mt-2{margin-top:24px}.mb-2{margin-bottom:24px}.ml-2{margin-left:24px}.mr-2{margin-right:24px}.p-2{padding:24px}.pt-2{padding-top:24px}.pb-2{padding-bottom:24px}.pl-2{padding-left:24px}.pr-2{padding-right:24px}.m-3{margin:36px}.mt-3{margin-top:36px}.mb-3{margin-bottom:36px}.ml-3{margin-left:36px}.mr-3{margin-right:36px}.p-3{padding:36px}.pt-3{padding-top:36px}.pb-3{padding-bottom:36px}.pl-3{padding-left:36px}.pr-3{padding-right:36px}.m-4{margin:48px}.mt-4{margin-top:48px}.mb-4{margin-bottom:48px}.ml-4{margin-left:48px}.mr-4{margin-right:48px}.p-4{padding:48px}.pt-4{padding-top:48px}.pb-4{padding-bottom:48px}.pl-4{padding-left:48px}.pr-4{padding-right:48px}.m-5{margin:64px}.mt-5{margin-top:64px}.mb-5{margin-bottom:64px}.ml-5{margin-left:64px}.mr-5{margin-right:64px}.p-5{padding:64px}.pt-5{padding-top:64px}.pb-5{padding-bottom:64px}.pl-5{padding-left:64px}.pr-5{padding-right:64px}.m-6{margin:80px}.mt-6{margin-top:80px}.mb-6{margin-bottom:80px}.ml-6{margin-left:80px}.mr-6{margin-right:80px}.p-6{padding:80px}.pt-6{padding-top:80px}.pb-6{padding-bottom:80px}.pl-6{padding-left:80px}.pr-6{padding-right:80px}.m-7{margin:96px}.mt-7{margin-top:96px}.mb-7{margin-bottom:96px}.ml-7{margin-left:96px}.mr-7{margin-right:96px}.p-7{padding:96px}.pt-7{padding-top:96px}.pb-7{padding-bottom:96px}.pl-7{padding-left:96px}.pr-7{padding-right:96px}.m-8{margin:112px}.mt-8{margin-top:112px}.mb-8{margin-bottom:112px}.ml-8{margin-left:112px}.mr-8{margin-right:112px}.p-8{padding:112px}.pt-8{padding-top:112px}.pb-8{padding-bottom:112px}.pl-8{padding-left:112px}.pr-8{padding-right:112px}.m-9{margin:128px}.mt-9{margin-top:128px}.mb-9{margin-bottom:128px}.ml-9{margin-left:128px}.mr-9{margin-right:128px}.p-9{padding:128px}.pt-9{padding-top:128px}.pb-9{padding-bottom:128px}.pl-9{padding-left:128px}.pr-9{padding-right:128px}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif}:host{--font-family:var(--font-secondary);--font-size:16px;--line-height:28px;--letter-spacing:0.03em;--font-weight:300;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:35em;--font-family:var(--font-primary);--icon-size:3em;display:inline-block}@media (min-width:78.125em){:host{--font-size:18px;--line-height:30px}}@media (min-width:105.0625em){:host{--font-size:22px;--line-height:36px}}@media (min-width:56.25em){:host{--icon-size:2.5em}}:host([mini]){--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);--icon-size:1em}@media (min-width:56.25em){:host([mini]){--icon-size:1em}}:host([inline]) .content{grid-auto-flow:column;padding:0}:host([inline]) a{white-space:nowrap;outline:0;color:currentColor}:host([arrow]){--icon-size:1em;--icon-color:var(--color-red);color:var(--color-black)}:host([arrow]) dsb-icon{-webkit-transition:-webkit-transform .2s var(--ease-in-out);transition:-webkit-transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out);transition:transform .2s var(--ease-in-out),-webkit-transform .2s var(--ease-in-out)}:host([arrow]) a:hover span{background:none}:host([arrow]) a:hover dsb-icon{-webkit-transform:translateX(.25em);transform:translateX(.25em)}:host:not([inline]){color:var(--color-blue)}@media (max-width:56.24em){:host([fill-space]){width:100%;height:100%}}a{text-decoration:none;text-align:center;color:var(--color-blue);display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;width:100%;height:100%;outline:1px solid var(--color-grey-1);position:relative}a.focus-visible,a:focus-visible,a:focus:not(.focus-visible){outline:none}a.focus-visible:before,a:focus-visible:before{content:\"\";position:absolute;width:100%;height:100%;padding:.33rem;left:-.33rem;top:-.33rem;-webkit-box-shadow:0 0 0 2px var(--color-black);box-shadow:0 0 0 2px var(--color-black);border-radius:var(--border-radius);-webkit-box-sizing:content-box;box-sizing:content-box}.bg--blue a.focus-visible:before,.bg--blue a:focus-visible:before,.bg--darkgrey a.focus-visible:before,.bg--darkgrey a:focus-visible:before,.bg--red a.focus-visible:before,.bg--red a:focus-visible:before{-webkit-box-shadow:0 0 0 2px var(--color-white);box-shadow:0 0 0 2px var(--color-white)}a span{background-image:-webkit-gradient(linear,left top,left bottom,from(currentColor),to(currentColor));background-image:linear-gradient(currentColor,currentColor);background-position:0 100%;background-repeat:no-repeat;background-size:0 var(--spacing-1);-webkit-transition:background-size .2s ease;transition:background-size .2s ease;text-decoration:none}a:hover span{background-size:100% var(--spacing-1)}@media (min-width:56.25em){a{text-align:left;outline:0}}.content{display:inline-grid;grid-auto-flow:row;grid-gap:.618em;gap:.618em;-webkit-box-align:center;-ms-flex-align:center;align-items:center;justify-items:center;place-items:center;padding:40px 16px;width:-webkit-fit-content;width:-moz-fit-content;width:fit-content}@media (min-width:56.25em){.content{padding:0;grid-auto-flow:column;text-align:left}}";

class IconLink extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      href: { type: String },
      icon: { type: String },
      trailingIcon: { type: String },
      label: { type: String },
      mini: { type: Boolean, reflect: true },
      inline: { type: Boolean, reflect: true },
      arrow: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.href = '';
    this.icon = '';
    this.trailingIcon = '';
    this.label = '';
    this.mini = false;
    this.inline = false;
    this.arrow = false;
  }

  render() {
    return T`
      <a href=${this.href} aria-label=${this.icon}>
        <div class="content">
          ${this.icon ? T`<dsb-icon icon=${this.icon}></dsb-icon>` : ''}
          <span>${this.label}</span>
          ${this.arrow ? T`<dsb-icon icon="arrow--right"></dsb-icon>` : ''}
          ${this.trailingIcon
            ? T`<dsb-icon icon=${this.trailingIcon}></dsb-icon>`
            : ''}
        </div>
      </a>
    `;
  }
}

customElements.define('dsb-icon-link', IconLink);

export { IconLink };
