import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}.badge{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end;z-index:5;pointer-events:none}.badge .dot{background-color:var(--color-red);background-color:var(--badge-background-color,var(--color-red));color:var(--color-white);color:var(--badge-color,var(--color-white));font-family:var(--font-primary);border-radius:50%}.badge .inner{display:grid;position:relative;padding:var(--units-2);-webkit-box-align:center;-ms-flex-align:center;align-items:center;justify-items:center;place-items:center;min-width:var(--units-7);-webkit-box-sizing:border-box;box-sizing:border-box}@media (min-width:37.5em){.badge .inner{min-width:var(--units-8)}}@media (min-width:78.125em){.badge .inner{min-width:var(--units-9)}}.badge .inner:before{content:\"\";display:block;padding-bottom:100%;grid-area:1/1/2/2}.badge .content{display:grid;width:100%;grid-area:1/1/2/2;-webkit-box-align:center;-ms-flex-align:center;align-items:center;justify-items:center;place-items:center}.badge .content *{margin:0;line-height:1}.badge .label{--font-size:12px;--letter-spacing:0.03em;--font-weight:400;--line-height:20px}.badge .label,.badge .text{--font-family:var(--font-primary);font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0)}.badge .text{--font-weight:bold;--font-size:22px;--line-height:32px}@media (min-width:37.5em){.badge .text{--font-size:24px;--line-height:34px}}@media (min-width:78.125em){.badge .text{--font-size:32px;--line-height:42px}}@media (min-width:105.0625em){.badge .text{--font-size:36px;--line-height:48px}}.badge.left{-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start}.badge.right{-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end}.badge.white .dot{background-color:var(--color-white);background-color:var(--badge-background-color,var(--color-white));color:var(--color-black);color:var(--badge-color,var(--color-black))}";

// @customElement('dsb-button')
class Badge extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      placement: { type: String },
      label: { type: String },
      text: { type: String },
      color: { type: String },
    };
  }

  constructor() {
    super();
    this.placement = 'left';
    this.label = '';
    this.text = '';
    this.color = '';
  }

  render() {
    return T`
      <div class="badge ${this.placement} ${this.color}">
        <div class="dot">
          <div class="inner">
            <div class="content">
              <span class="label">${this.label}</span>
              <span class="text">${this.text}</span>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-badge', Badge);

export { Badge };
