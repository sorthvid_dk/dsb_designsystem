import { w, A, h, T } from '../lit-element-a21c046d.js';
import { i, s as s$1, t } from '../directive-b9dab5a2.js';
import { h as hasFocus } from '../utils-4e70fb44.js';

var css_248z = ":host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}:host{display:inline-block;width:100%}.container{display:inline-grid;grid-gap:var(--spacing-2);gap:var(--spacing-2);width:100%;-webkit-box-sizing:border-box;box-sizing:border-box;position:relative}label{--font-size:12px;--font-family:var(--font-primary);--font-weight:400;--line-height:20px;padding-left:var(--spacing-3)}.input,label{--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0)}.input{--font-family:var(--font-secondary);--font-size:16px;--line-height:28px;--font-weight:300;max-width:35em;--font-family:var(--font-primary);-webkit-transition:border-color .1s var(--ease-in-out);transition:border-color .1s var(--ease-in-out);position:relative;display:grid;grid-auto-flow:column;grid-gap:var(--spacing-3);gap:var(--spacing-3);-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:start;grid-template-columns:auto 1fr;border:var(--border-width) solid var(--color-grey-1);border-radius:var(--border-radius);background-color:var(--color-white);color:var(--color-black);padding:0 var(--units-1);height:var(--units-4);max-width:none;min-width:-webkit-fill-available;-webkit-box-sizing:border-box;box-sizing:border-box;cursor:text}@media (min-width:78.125em){.input{--font-size:18px;--line-height:30px}}@media (min-width:105.0625em){.input{--font-size:22px;--line-height:36px}}.input[focus-within]{-webkit-box-shadow:0 1px 5px rgba(0,0,0,.5);box-shadow:0 1px 5px rgba(0,0,0,.5)}.input:focus-within{-webkit-box-shadow:0 1px 5px rgba(0,0,0,.5);box-shadow:0 1px 5px rgba(0,0,0,.5)}input{padding:0;font:inherit;border:0;height:100%;width:100%}input::-webkit-input-placeholder{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);color:var(--color-grey-2)}input::-moz-placeholder{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);color:var(--color-grey-2)}input:-ms-input-placeholder{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);color:var(--color-grey-2)}input::-ms-input-placeholder{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);color:var(--color-grey-2)}input::placeholder{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);color:var(--color-grey-2)}input:focus{outline:0}ul{margin:0}.select{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);border-top:0;position:absolute;top:100%;width:100%}::slotted(option){height:var(--units-4);padding:0 var(--units-1);background:var(--color-white);display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;cursor:pointer}";

/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const d=o=>void 0===o.strings,s={},f=(o,t=s)=>o.H=t;

/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const l=i(class extends s$1{constructor(r){if(super(r),r.type!==t.PROPERTY&&r.type!==t.ATTRIBUTE&&r.type!==t.BOOLEAN_ATTRIBUTE)throw Error("The `live` directive is not allowed on child or event bindings");if(!d(r))throw Error("`live` bindings can only contain a single expression")}render(r){return r}update(i,[t$1]){if(t$1===w||t$1===A)return t$1;const o=i.element,l=i.name;if(i.type===t.PROPERTY){if(t$1===o[l])return w}else if(i.type===t.BOOLEAN_ATTRIBUTE){if(!!t$1===o.hasAttribute(l))return w}else if(i.type===t.ATTRIBUTE&&o.getAttribute(l)===t$1+"")return w;return f(i),t$1}});

// import { repeat } from 'lit/directives/repeat';
// import { classMap } from 'lit/directives/class-map.js';

// TODO(erik): value isn't reflected in the DOM

class Textfield extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      value: { attribute: true, reflect: true },
      label: { type: String },
      type: { type: String },
      prefix: { type: String },
      placeholder: { type: String },
      disabled: { type: Boolean, reflect: true },
      hiddenLabel: { type: Boolean },

      isOpen: { state: true },
      activeItem: { type: Number, state: true },
    };
  }

  constructor() {
    super();
    this.value = '';
    this.label = '';
    this.type = 'text';
    this.prefix = '';
    this.placeholder = '';
    this.disabled = false;
    this.required = false;
    this.hiddenLabel = false;

    this.isOpen = false;
    this.activeItem = -1;
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('click', this.onFocus.bind(this));
    window.addEventListener('keydown', this.changeOption.bind(this));

    // connectedCallback() {
    //   const input = this.querySelector('input');
    //   const options = this.querySelector('[data-options]');

    //   [...options.children].forEach(child => child.setAttribute('tabindex', '-1'))

    //   // first one to connect creates a keydown event
    //   if (!getSubscription('keydown'))
    //     document.body.addEventListener('keydown', e => publish('keydown', e));

    //   subscribe('keydown', dropdownControl({ input, options }));
    // }
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('click', this.onFocus.bind(this));
    window.removeEventListener('keydown', this.changeOption.bind(this));
  }

  onFocus() {
    const input = this.renderRoot.querySelector('input');

    if (document.activeElement === input) {
      return;
    }

    if(input) input.focus();
  }

  toggleSelectOptions(shouldOpen) {
    this.isOpen = shouldOpen;
  }

  changeOption(event) {
    if (
      (event.key !== 'ArrowUp' && event.key !== 'ArrowDown') ||
      !hasFocus(this)
    ) {
      return;
    }

    this.activeItem += event.key === 'ArrowUp' ? -1 : 1;

    // const newActiveItem = this.renderRoot.querySelector('slot')
  }

  render() {
    return T`
      <span class="container">
        <label .hidden=${this.hiddenLabel || !this.label}>
          ${this.label}
        </label>

        <span class="input">
          ${this.prefix ? T`<span class="prefix">${this.prefix}</span>` : ''}

          <input
            aria-labelledby="label"
            type=${this.type}
            .value="${l(this.value)}"
            ?disabled="${this.disabled}"
            placeholder="${this.placeholder}"
            ?required="${this.required}"
            ?readonly="${this.readOnly}"
            @focus=${() => this.toggleSelectOptions(true)}
            @blur=${() => this.toggleSelectOptions(false)}
          />
        </span>

        <div class="select">
          <slot></slot>
        </div>
      </span>
    `;
  }
}

//  .hidden=${!this.isOpen}

// <div class="input-text__placeholder">
//   ${Text(mini({ text: placeholder }))}
// </div>

// ${maybe({
//   component: InputDropdown,
//   if: dropdown,
// })}

customElements.define('dsb-textfield', Textfield);

export { Textfield };
