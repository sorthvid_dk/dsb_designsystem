import { h, T } from '../lit-element-a21c046d.js';
import './icon.js';
import { e } from '../image-block-72b49e4f.js';
import './getIcon.js';
import '../directive-b9dab5a2.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111}.bg--red{background-color:var(--color-red)}.bg--blue,.bg--red{color:var(--color-white)}.bg--blue{background-color:var(--color-blue)}.bg--grey{background-color:var(--color-grey-1);color:var(--color-black)}.bg--darkgrey{background-color:var(--color-grey-3);color:var(--color-white)}a{display:block;color:var(--color-black);text-decoration:none;height:100%;position:relative}a.focus-visible,a:focus-visible,a:focus:not(.focus-visible){outline:none}a.focus-visible:before,a:focus-visible:before{content:\"\";position:absolute;width:100%;height:100%;padding:.33rem;left:-.33rem;top:-.33rem;-webkit-box-shadow:0 0 0 2px var(--color-black);box-shadow:0 0 0 2px var(--color-black);border-radius:var(--border-radius);-webkit-box-sizing:content-box;box-sizing:content-box}.bg--blue a.focus-visible:before,.bg--blue a:focus-visible:before,.bg--darkgrey a.focus-visible:before,.bg--darkgrey a:focus-visible:before,.bg--red a.focus-visible:before,.bg--red a:focus-visible:before{-webkit-box-shadow:0 0 0 2px var(--color-white);box-shadow:0 0 0 2px var(--color-white)}.headline{--font-family:var(--font-primary);--font-weight:bold;--font-size:22px;--line-height:32px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);margin-bottom:var(--units-1);display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:.618em;gap:.618em}@media (min-width:37.5em){.headline{--font-size:24px;--line-height:34px}}@media (min-width:78.125em){.headline{--font-size:32px;--line-height:42px}}@media (min-width:105.0625em){.headline{--font-size:36px;--line-height:48px}}dsb-icon{--size:1.5em;color:var(--color-red)}.text{--font-family:var(--font-secondary);--font-size:16px;--line-height:28px;--letter-spacing:0.03em;--font-weight:300;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);max-width:35em}@media (min-width:78.125em){.text{--font-size:18px;--line-height:30px}}@media (min-width:105.0625em){.text{--font-size:22px;--line-height:36px}}.content{margin-top:var(--units-2)}.content,img{will-change:transform;-webkit-transition:-webkit-transform .7s var(--ease-in-out);transition:-webkit-transform .7s var(--ease-in-out);transition:transform .7s var(--ease-in-out);transition:transform .7s var(--ease-in-out),-webkit-transform .7s var(--ease-in-out)}a:hover img{-webkit-transform:scale(1.05);transform:scale(1.05)}a:hover .content{-webkit-transform:translateY(calc(var(--spacing-2)*-1));transform:translateY(calc(var(--spacing-2)*-1))}.overlap{position:relative;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-ms-flex-pack:end;justify-content:flex-end;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;color:var(--color-white)}.overlap dsb-icon{color:currentColor}.overlap dsb-image-block{position:absolute;top:0;left:0;width:100%;height:100%;margin-bottom:0}.overlap dsb-image-block:after{content:\"\";position:absolute;height:100%;width:100%;top:0;background:-webkit-gradient(linear,left top,left bottom,from(transparent),color-stop(4.4%,rgba(0,0,0,.006)),color-stop(9.5%,rgba(0,0,0,.024)),color-stop(15.2%,rgba(0,0,0,.052)),color-stop(21.5%,rgba(0,0,0,.088)),color-stop(28.1%,rgba(0,0,0,.13)),color-stop(35.2%,rgba(0,0,0,.176)),color-stop(42.5%,rgba(0,0,0,.225)),color-stop(50%,rgba(0,0,0,.275)),color-stop(57.6%,rgba(0,0,0,.324)),color-stop(65.2%,rgba(0,0,0,.37)),color-stop(72.7%,rgba(0,0,0,.412)),color-stop(80%,rgba(0,0,0,.448)),color-stop(87.1%,rgba(0,0,0,.476)),color-stop(93.7%,rgba(0,0,0,.494)),to(rgba(0,0,0,.5)));background:linear-gradient(180deg,transparent 0,rgba(0,0,0,.006) 4.4%,rgba(0,0,0,.024) 9.5%,rgba(0,0,0,.052) 15.2%,rgba(0,0,0,.088) 21.5%,rgba(0,0,0,.13) 28.1%,rgba(0,0,0,.176) 35.2%,rgba(0,0,0,.225) 42.5%,rgba(0,0,0,.275) 50%,rgba(0,0,0,.324) 57.6%,rgba(0,0,0,.37) 65.2%,rgba(0,0,0,.412) 72.7%,rgba(0,0,0,.448) 80%,rgba(0,0,0,.476) 87.1%,rgba(0,0,0,.494) 93.7%,rgba(0,0,0,.5))}.overlap .content{position:relative;width:100%;-webkit-box-sizing:border-box;box-sizing:border-box;padding:5%;z-index:get-z(\"content\")}@media (max-width:56.24em){.overlap .content{margin-top:128px}}";

// @customElement('dsb-button')
class ImageLink extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      overlap: { type: Boolean },
      href: { type: String },
      src: { type: String },
      alt: { type: String },
      headline: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.overlap = false;
    this.href = '';
    this.src = '';
    this.alt = '';
    this.headline = '';
    this.text = '';
  }

  // <slot></slot>
  // <slot name="right"></slot>

  // <div class="image">
  //   <div class="boundaries">
  //     <img src=${this.src} alt=${this.alt} />
  //   </div>
  // </div>
  render() {
    return T`
      <a href=${this.href} class="${e({ overlap: this.overlap })}">
        <dsb-image-block src=${this.src} alt=${this.alt} aspect="16/9">
          <slot></slot>
          <slot name="right"></slot>
        </dsb-image-block>
        <div class="content">
          <div class="headline">
            ${this.headline} <dsb-icon icon="arrow--right"></dsb-icon>
          </div>
          <div class="text">${this.text}</div>
        </div>
      </a>
    `;
  }
}

customElements.define('dsb-image-link', ImageLink);

export { ImageLink };
