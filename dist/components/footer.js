import { h, T } from '../lit-element-a21c046d.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host{display:block;background-color:var(--color-grey-1)}footer *{list-style:none}.top{display:grid;grid-template-columns:calc(min(100vw, var(--page-width))/24*2) 1fr;border-bottom:1px solid var(--color-grey-2);padding:var(--units-5) calc(min(100vw, var(--page-width))/24*2);grid-gap:calc(min(100vw, var(--page-width))/24*2);gap:calc(min(100vw, var(--page-width))/24*2)}@media (min-width:37.5em){.top{grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1);padding:var(--units-5) calc(min(100vw, var(--page-width))/24*1)}}@media (min-width:37.5em){.one{grid-column:span 2}}slot[name=one]::slotted(li){padding-bottom:var(--units-1)}@media (min-width:37.5em){slot[name=one]::slotted(li){padding-bottom:var(--units-2)}}.two{--font-family:var(--font-primary);--font-size:18px;--line-height:28px;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0)}@media (min-width:78.125em){.two{--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){.two{--font-size:24px;--line-height:34px}}slot[name=two]::slotted(li){padding-bottom:var(--units-1)}.three{--font-family:var(--font-primary);--font-size:18px;--line-height:28px;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);padding-bottom:var(--units-4)}@media (min-width:78.125em){.three{--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){.three{--font-size:24px;--line-height:34px}}.four{color:var(--color-black)}.body{display:grid;padding-bottom:var(--layout-6);grid-gap:calc(min(100vw, var(--page-width))/24*1);gap:calc(min(100vw, var(--page-width))/24*1)}@media (min-width:37.5em){.body{grid-auto-flow:column;grid-template-columns:calc(min(100vw, var(--page-width))/24*10) 1fr;grid-template-rows:auto auto}}@media (min-width:56.25em){.body{grid-template-rows:auto;grid-template-columns:1fr calc(min(100vw, var(--page-width))/24*5) calc(min(100vw, var(--page-width))/24*5)}}@media (min-width:78.125em){.body{grid-template-columns:1fr calc(min(100vw, var(--page-width))/24*6) calc(min(100vw, var(--page-width))/24*6);grid-gap:0;gap:0}}.bottom{--font-size:12px;--font-family:var(--font-primary);--letter-spacing:0.03em;--font-weight:400;--line-height:20px;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);display:grid;padding:var(--units-3) 0 var(--units-3) calc(min(100vw, var(--page-width))/24*6)}@media (min-width:37.5em){.bottom{grid-template-columns:1fr calc(min(100vw, var(--page-width))/24*4);padding:var(--units-2) calc(min(100vw, var(--page-width))/24*2) var(--units-2) calc(min(100vw, var(--page-width))/24*2)}}@media (min-width:56.25em){.bottom{padding:var(--units-2) calc(min(100vw, var(--page-width))/24*2) var(--units-2) calc(min(100vw, var(--page-width))/24*4);grid-template-columns:1fr calc(min(100vw, var(--page-width))/24*2)}}.bottom-row{display:inline-grid;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:start;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:var(--units-2);gap:var(--units-2);padding-bottom:var(--units-2)}@media (min-width:37.5em){.bottom-row{grid-gap:var(--units-4);gap:var(--units-4);grid-auto-flow:column;padding-bottom:0}}.home-link{--icon-color:var(--color-red);--icon-size:calc(min(100vw, var(--page-width))/24*2)}@media (min-width:56.25em){.home-link{--icon-size:calc(min(100vw, var(--page-width))/24*1)}}p,ul{margin:0;padding:0}";

class Footer extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      homeUrl: { type: String },
    };
  }

  constructor() {
    super();
    this.homeUrl = '';
  }

  render() {
    return T`
      <footer>
        <div class="top">
          <!-- col 1 -->
          <div class="home-link">
            <dsb-icon icon="dsb-logo"></dsb-icon>
          </div>
          <!-- col 2 -->
          <div class="body">
            <ul class="one">
              <slot name="one"></slot>
            </ul>
            <ul class="two">
              <slot name="two"></slot>
            </ul>
            <div>
              <ul class="three">
                <slot name="three"></slot>
              </ul>
              <ul class="four">
                <slot name="four"></slot>
              </ul>
            </div>
          </div>
        </div>

        <div class="bottom">
          <ul class="bottom-row">
            <slot name="bottom"></slot>
          </ul>
          <ul>
            <li class="england">
              <dsb-icon icon="flag--england"></dsb-icon>England
            </li>
            <!-- <li class="denmark">
              <dsb-icon icon="flag--denmark"></dsb-icon>Denmark
            </li> -->
          </ul>
        </div>
      </footer>
    `;
  }
}

customElements.define('dsb-footer', Footer);

export { Footer };
