import { h, x } from '../lit-element-a21c046d.js';
import { getIcon } from './getIcon.js';

var css_248z = ":host{display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex}:host svg{width:20px;width:var(--icon-size,20px);height:20px;height:var(--icon-size,20px);fill:currentColor;fill:var(--icon-color,currentColor)}";

class Icon extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      icon: { type: String },
    };
  }

  constructor() {
    super();
    this.icon = '';
  }

  render() {
    return x`${getIcon(this.icon)}`;
  }
}

customElements.define('dsb-icon', Icon);

export { Icon };
