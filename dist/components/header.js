import { h, T } from '../lit-element-a21c046d.js';
import { FLYOUT_WAS_TOGGLED } from './flyout.js';
import { HEADER_FORM_WAS_TOGGLED } from './header-form.js';
import '../id-839a82d1.js';
import '../flip-60377d92.js';
import './textfield.js';
import '../directive-b9dab5a2.js';
import '../utils-4e70fb44.js';

var css_248z = ":host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}:host{position:fixed;width:100vw;top:0;z-index:10;background:var(--color-red);color:var(--color-white);display:grid;grid-auto-flow:column;grid-template-rows:calc(var(--dsb-header-height-collapsed)/2) 1fr;-webkit-box-sizing:border-box;box-sizing:border-box;padding:0 calc(min(100vw, var(--page-width))/24*1)}@media (min-width:56.25em){:host{grid-template-rows:auto;grid-template-columns:20% 1fr 20%;padding:0 var(--units-2)}}.left{--icon-size:var(--units-4);position:relative;width:var(--icon-size);height:100%;display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin-right:auto;z-index:14}@media (min-width:37.5em){.left{height:calc(var(--units-4) + var(--units-1)*2)}}.left::slotted(*){display:-webkit-box;display:-ms-flexbox;display:flex}.middle{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.middle,.middle::slotted(*){display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;height:100%}@media (min-width:56.25em){.middle::slotted(*){max-width:calc(min(100vw, var(--page-width))/24*14)}}@media (min-width:105.0625em){.middle::slotted(*){max-width:calc(min(100vw, var(--page-width))/24*10)}}.right{--icon-size:var(--units-2);display:inline-grid;-webkit-box-align:center;-ms-flex-align:center;align-items:center;grid-gap:var(--units-2);gap:var(--units-2);grid-auto-flow:column;height:100%;margin-left:auto}@media (min-width:37.5em){.right{height:calc(var(--units-4) + var(--units-1)*2)}}.hide-on-mobile{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:end;-ms-flex-align:end;align-items:flex-end;overflow:hidden;grid-row:2;grid-column:span 2}@media (min-width:56.25em){.hide-on-mobile{grid-row:1;grid-column:span 1;min-height:var(--dsb-header-height-collapsed)}}";

class Header extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      flyoutIsOpen: { state: true },
      contentRoot: { type: String },
      translateTargetElement: { state: true },
      // formIsOpen: { state: true },
    };
  }

  constructor() {
    super();
    this.flyoutIsOpen = false;
    this.contentRoot = '';
    this.translateTargetElement = null;
    // this.formIsOpen = false;
  }

  connectedCallback() {
    super.connectedCallback();

    window.addEventListener(
      FLYOUT_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );

    window.addEventListener(
      HEADER_FORM_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );
  }

  disconnectedCallback() {
    super.disconnectedCallback();

    window.removeEventListener(
      FLYOUT_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );

    window.removeEventListener(
      HEADER_FORM_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );
  }

  translateAppContent(event) {
    // this.formIsOpen = event.detail.formIsToggled && event.detail.isOpen;

    if (!this.contentRoot) return;

    this.flyoutIsOpen = event.detail.isOpen;
    this.translateValue = event.detail.contentSize.height;

    this.translateTargetElement = document.body.querySelector(this.contentRoot);
    const headerHeight = this.getBoundingClientRect().height;

    this.translateTargetElement.style.transform = this.flyoutIsOpen
      ? `translateY(${this.translateValue - headerHeight}px)`
      : '';
  }

  render() {
    return T`
      <slot class="left" name="topbarLeft"></slot>
      <div class="hide-on-mobile">
        <slot class="middle" name="topbarMiddle"></slot>
      </div>
      <slot class="right" name="topbarRight"></slot>
    `;
  }
}

customElements.define('dsb-header', Header);

export { Header };
