import { h, T } from '../lit-element-a21c046d.js';
import './icon.js';
import './getIcon.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px}:host{display:-webkit-inline-box;display:-ms-inline-flexbox;display:inline-flex;position:relative;width:100%}.ui{--icon-button-background-color:var(--color-grey-3);--icon-color:var(--color-white);--icon-button-padding:var(--units-1);--icon-size:var(--units-2);position:absolute;left:var(--units-2);bottom:var(--units-2);display:-webkit-box;display:-ms-flexbox;display:flex;grid-gap:var(--units-2);gap:var(--units-2)}@media (min-width:56.25em){.ui{--icon-button-padding:var(--units-2);--icon-size:var(--units-3);left:var(--units-3);bottom:var(--units-3);grid-gap:var(--units-3);gap:var(--units-3)}}video{width:100%;height:100%;-o-object-fit:cover;object-fit:cover}";

// @customElement('dsb-button')
class Video extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      src: { type: String },
      type: { type: String },
      toggle: { type: Boolean, reflect: true },
      playing: { type: Boolean, state: true },
    };
  }

  constructor() {
    super();
    this.src = '';
    this.type = '';
    this.toggle = false;
    this.playing = false;
  }

  togglePlay(shouldPlay) {
    if (shouldPlay === undefined) {
      this.playing = !this.playing;
      if (this.playing) {
        this.renderRoot.querySelector('video').play();
      } else {
        this.renderRoot.querySelector('video').pause();
      }

      return;
    }

    if (shouldPlay) {
      this.playing = true;
      this.renderRoot.querySelector('video').play();
    } else {
      this.playing = false;
      this.renderRoot.querySelector('video').pause();
    }
  }

  render() {
    return T`
      <video>
        <source src="${this.src}" type=${this.type} />

        Sorry, your browser doesn't support embedded videos.
      </video>

      <div class="ui">
        ${this.toggle
          ? T`<dsb-icon-button
              icon=${this.playing ? 'pause' : 'play'}
              round
              @click=${() => this.togglePlay()}
            ></dsb-icon-button>`
          : T`
              <dsb-icon-button
                icon="play"
                round
                @click=${() => this.togglePlay(true)}
              ></dsb-icon-button>
              <dsb-icon-button
                icon="pause"
                round
                @click=${() => this.togglePlay(false)}
              ></dsb-icon-button>
            `}
      </div>
    `;
  }
}

customElements.define('dsb-video', Video);

export { Video };
