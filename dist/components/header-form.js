import { h, T } from '../lit-element-a21c046d.js';
import './textfield.js';
import { f as flip } from '../flip-60377d92.js';
import '../directive-b9dab5a2.js';
import '../utils-4e70fb44.js';

var css_248z = ":host,:root{--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif}[hidden]{display:inline-block}.hide-s{overflow:hidden;height:auto}@media (max-width:37.49em){:host([isopen=false]) .hide-s{height:0}}.curtain-content{overflow:hidden;height:0}.curtain-content hr{grid-column:1/-1;opacity:0}:host{display:grid;grid-template-columns:repeat(2,1fr);grid-gap:var(--units-1) var(--units-1);gap:var(--units-1) var(--units-1);-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%}@media (min-width:56.25em){:host{grid-template-columns:repeat(4,1fr)}}:host(.is-opening){-webkit-transition:margin-top .3s var(--ease-in-out-quint);transition:margin-top .3s var(--ease-in-out-quint)}:host([isopen=true]) .curtain-content{height:auto}:host([isopen=true]) .backdrop{opacity:1;pointer-events:auto}h1{--font-family:var(--font-primary);--font-size:18px;--line-height:28px;--letter-spacing:0.03em;font-family:--font-secondary;font-family:var(--font-family,--font-secondary);font-size:16px;font-size:var(--font-size,16px);line-height:20px;line-height:var(--line-height,20px);font-weight:400;font-weight:var(--font-weight,normal);letter-spacing:0;letter-spacing:var(--letter-spacing,0);position:relative;grid-column:1/-1;text-align:center;margin:0}@media (min-width:78.125em){h1{--font-size:20px;--line-height:30px}}@media (min-width:105.0625em){h1{--font-size:24px;--line-height:34px}}@media (min-width:56.25em){h1{padding-top:var(--units-2)}}slot{display:inline-block}.curtain-content{grid-column:span 2;display:grid;grid-template-columns:repeat(2,1fr);grid-gap:var(--units-2) var(--units-1);gap:var(--units-2) var(--units-1);-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%}@media (min-width:56.25em){.curtain-content{grid-column:span 4;grid-template-columns:repeat(4,1fr)}}slot[name=one],slot[name=three],slot[name=two]{grid-column:span 2}@media (min-width:37.5em){slot[name=one],slot[name=three],slot[name=two]{grid-column:span 1}}@media (min-width:56.25em){slot[name=one],slot[name=three],slot[name=two]{grid-column:span 2}}.backdrop{-webkit-transition:opacity .3s ease;transition:opacity .3s ease;position:absolute;background-color:rgba(0,0,0,.6);top:100%;right:0;bottom:0;left:0;height:100vh;opacity:0;pointer-events:none}";

const clamp = (min, val, max) => Math.max(min, Math.min(max, val));

const MOBILE_BREAKPOINT = 600;

const HEADER_FORM_WAS_TOGGLED = 'header-form-was-toggled';

class HeaderForm extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      headline: { type: String },
      isOpen: { attribute: true, reflect: true },
      currentScroll: { type: Number, state: true },
      isMobile: { state: true },
      isOpening: { state: true },
      showHeadline: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.isOpen = false;
    this.isMobile = null;
    this.lastTopMargin = 0;
    this.headlineHeight = 0;
    this.firstFormFieldHeight = 0;
    this.inputElements = null;
    this.isOpening = false;
    this.showHeadline = false;
    this.backdrop = null;
  }

  connectedCallback() {
    super.connectedCallback();

    setTimeout(() => {
      this.setMargin();
      this.resizeHandler();

      this.inputElements = [...this.querySelectorAll('dsb-textfield')];
      this.inputElements.forEach((input) => {
        input.addEventListener('focus', this.setOpen.bind(this));
        // input.addEventListener('blur', this.setOpen.bind(this));
      });

      this.backdrop = this.shadowRoot.querySelector('.backdrop');
      this.backdrop &&
        this.backdrop.addEventListener('click', this.setClose.bind(this));
    }, 10);

    window.addEventListener('keydown', this.setClose.bind(this));
    window.addEventListener('wheel', this.setMargin.bind(this));
    window.addEventListener('resize', this.resizeHandler.bind(this));
  }

  disconnectedCallback() {
    super.disconnectedCallback();

    window.removeEventListener('keydown', this.setClose.bind(this));
    window.removeEventListener('wheel', this.setMargin.bind(this));
    window.removeEventListener('resize', this.resizeHandler.bind(this));
    this.inputElements.forEach((input) => {
      input.removeEventListener('focus', this.setOpen.bind(this));
      // input.removeEventListener('blur', this.setOpen.bind(this));
    });
    this.backdrop.removeEventListener('click', this.setClose.bind(this));
  }

  setClose(event) {
    if (!this.isOpen || (event.key && event.key !== 'Escape')) return;

    this.setOpen(null, false);
    this.inputElements.forEach((input) => input.blur());
  }

  setOpen(event, overwrite) {
    this.isOpening = true;
    this.classList.toggle('is-opening');

    const state = flip.getState(
      this.shadowRoot.querySelector('.curtain-content')
    );

    this.isOpen = overwrite !== undefined ? overwrite : true;

    flip.expand(state, () => {
      this.classList.toggle('is-opening');
      this.isOpening = false;
    });

    this.setMargin();

    setTimeout(() => {
      this.dispatchEvent(
        new CustomEvent(HEADER_FORM_WAS_TOGGLED, {
          bubbles: true,
          detail: {
            message: `<dsb-header-form> was toggled`,
            isOpen: this.isOpen,
            contentSize: this.getBoundingClientRect(),
            formIsToggled: true,
          },
        })
      );
    }, 1);
  }

  getHeights() {
    const headline = this.shadowRoot.querySelector('.headline');
    this.headlineHeight = headline
      ? headline.getBoundingClientRect().height
      : 0;
    this.height = this.getBoundingClientRect().height;
  }

  /**
   * setMargin()
   * makes the form slide up and down on scroll.
   * behaves differently on mobile/desktop
   */
  setMargin({ deltaY = 0 } = {}) {
    const oldMarginValue = parseInt(this.style.marginTop) || 0;
    const newMarginValue = oldMarginValue - deltaY;

    
    if (this.isOpen) {
      this.style.marginTop = 0 + 'px';
      return;
    }
    
    if (!this.showHeadline && !this.isMobile) {
      console.log(this.isMobile, this.headlineHeight);
      this.style.marginTop = -this.headlineHeight + 'px';
      return;
    }

    if (this.isMobile) {
      const minValue = this.getBoundingClientRect().height * -1;

      console.log(minValue);

      this.style.marginTop =
        window.pageYOffset > this.headlineHeight || !this.showHeadline
          ? clamp(minValue, newMarginValue, -this.headlineHeight) + 'px'
          : clamp(minValue, newMarginValue, 0) + 'px';

      return;
    }

    /**
     * is desktop
     */
    this.style.marginTop =
      window.pageYOffset > this.headlineHeight
        ? -this.headlineHeight + 'px'
        : clamp(-this.headlineHeight, newMarginValue, 0) + 'px';
  }

  resizeHandler() {
    this.isMobile = window.innerWidth < MOBILE_BREAKPOINT;
    this.getHeights();
    const curtain = this.shadowRoot.querySelector('.curtain-content');
    curtain.removeAttribute('style');
  }

  render() {
    return T`
      ${this.headline ? T`<h1 class="headline">${this.headline}</h1>` : ''}

      <slot name="one"></slot>

      ${!this.isMobile ? T`<slot name="two"></slot>` : ''}

      <div class="curtain-content" ?hidden=${!this.isOpen}>
        ${this.isMobile ? T`<slot name="two"></slot>` : ''}
        <slot name="three"></slot>
        <slot name="four"></slot>
        <slot name="five"></slot>
        <slot name="six"></slot>
        <slot name="seven"></slot>
        <!-- <slot></slot> -->
        <hr />
      </div>

      <div class="backdrop"></div>
    `;
  }
}

customElements.define('dsb-header-form', HeaderForm);

export { HEADER_FORM_WAS_TOGGLED, HeaderForm };
