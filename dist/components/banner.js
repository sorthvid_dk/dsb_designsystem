import { h, T } from '../lit-element-a21c046d.js';
import './content-block.js';
import '../utils-4e70fb44.js';

var css_248z = ":host,:root{--ease-in-out-quint:cubic-bezier(0.86,0,0.07,1);--ease-in-out:cubic-bezier(0.25,0.1,0.25,1)}.flip-is-opening{overflow:hidden}:host,:root{--color-red:#b41730;--color-dark-red:#9d152c;--color-blue:#00233c;--color-white:#fff;--color-grey-1:#eeeff0;--color-grey-2:#ccc;--color-grey-3:#4c4c4c;--color-black:#111;--dsb-breakpoints-s:600px;--dsb-breakpoints-m:900px;--dsb-breakpoints-l:1250px;--dsb-breakpoints-xl:1681px;--dsb-breakpoints-max:2320px;--border-radius:5px;--border-width:1px;--page-width:1920px;--dsb-header-height-collapsed:var(--units-9);--dsb-header-height-expanded:var(--units-9);--flyout-transition:transform 0.6s var(--ease-in-out-quint);--header-collapsed:64px;--header-no-headline-expanded:136px;--header-show-headline-expanded:164px}@media (min-width:56.25em){:host,:root{--dsb-header-height-collapsed:calc(var(--units-4) + var(--units-1)*2);--dsb-header-height-expanded:124px;--header-collapsed:72px;--header-no-headline-expanded:var(--header-collapsed);--header-show-headline-expanded:124px}}@media (min-width:78.125em){:host,:root{--header-show-headline-expanded:130px}}:host,:root{--font-primary:\"Via Office\",serif;--font-secondary:\"Helvetica Neue\",sans-serif;--units-1:12px;--units-2:24px;--units-3:36px;--units-4:48px;--units-5:64px;--units-6:80px;--units-7:96px;--units-8:112px;--units-9:128px;--spacing-1:2px;--spacing-2:4px;--spacing-3:8px;--spacing-4:12px;--spacing-5:16px;--spacing-6:24px;--spacing-7:32px;--spacing-8:40px;--spacing-9:48px;--layout-1:16px;--layout-2:24px;--layout-3:32px;--layout-4:48px;--layout-5:64px;--layout-6:96px;--layout-7:160px;--layout-8:224px;--layout-9:288px}:host{display:block;color:var(--color-white)}section{overflow:hidden}.content,section{position:relative}.content{--banner-content-padding-top:var(--layout-7);--banner-content-padding-bottom:var(--layout-4);z-index:1;width:100%;max-width:var(--page-width);-webkit-box-sizing:border-box;box-sizing:border-box;padding:0 calc(min(100vw, var(--page-width))/24*2);padding-top:var(--banner-content-padding-top);padding-bottom:var(--banner-content-padding-bottom);margin:0 auto}@media (min-width:37.5em){.content{--banner-content-padding-top:var(--layout-6);--banner-content-padding-bottom:var(--layout-6)}}@media (min-width:56.25em){.content{--banner-content-padding-top:var(--layout-7);--banner-content-padding-bottom:var(--layout-7)}}@media (min-width:145em){.content{--banner-content-padding-top:var(--layout-8);--banner-content-padding-bottom:var(--layout-8)}}.image{position:absolute;top:0;left:0;width:100%;height:100%}";

class Banner extends h {
  static get styles() {
    return [css_248z];
  }

  static get properties() {
    return {
      trumpet: { type: String },
      headline: { type: String },
      text: { type: String },
      src: { type: String },
      alt: { type: String },
    };
  }

  constructor() {
    super();
    this.trumpet = '';
    this.headline = '';
    this.text = '';
    this.src = '';
    this.alt = '';
  }

  render() {
    return T`<section>
      <div class="content">
        <dsb-content-block
          .headline=${this.headline}
          .text=${this.text}
          .trumpet=${this.trumpet}
        >
          <slot></slot>
        </dsb-content-block>
      </div>

      <div class="image">
        <dsb-image-block src=${this.src} alt=${this.alt} gradient="left">
          <slot slot="right" name="badge"></slot>
        </dsb-image-block>
      </div>
    </section>`;
  }
}

customElements.define('dsb-banner', Banner);

export { Banner };
