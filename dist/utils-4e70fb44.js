/**
 * Converts a string to boolean
 * 'false' => false
 * 'true'  => true
 */

/**
 * Returns true if passed dom element is currently in focus
 */
const hasFocus = el => el === document.activeElement;

/**
 * Converts text that contains text and html tags to a document fragment
 */
const stringToDocumentFragment = text => {
    var frag = document.createRange().createContextualFragment(text);
    return frag;
};

export { hasFocus as h, stringToDocumentFragment as s };
