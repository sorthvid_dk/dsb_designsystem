# Developer notes

## Getting started

1. Clone the repo
```
git clone git@bitbucket.org:sorthvid_dk/dsb_designsystem.git
yarn install
```

2. Add a new component
```bash
yarn plop
# Follow the instructions in the the terminal
```

1. Navigate to `/src/components/your-component-name`, where there's a boilerplate component to work from. The `js` file is automatically added to `index.js`.

2. Start the development server
```bash
yarn start:storybook
```

5. Open a browser `http://localhost:6006/` and navigate to your new component in the sidebar menu.


6. Happy coding!

## Git and workflow

TODO(erik): Git-flow, PR, staging, issues etc.

Each new component/feature should be developed on a new branch in the /feature git folder.

```bash
git checkout -b feature/your-component-name
```

When you are done with your feature you push it and create a pull request in bitbucket.

### NPM and releases

The process of publishing a new release to npm is made as automatic as possible.
Whenever the semver-version is bumped, the `postversion`-script in package.json will build the project to `/dist` and create/push a new git branch to `releases/<current npm version>`.

After `npm version patch` is run, it's green light to also publish the project to npm:

TODO(erik): You must be added to @dsb.dk in npm.

```bash
npm version patch
npm publish
```

## `svg`

Whenever a new svg-file is added to `./static/svg`. Run:

```bash
yarn svg
```

to make it importable by the icon component.

## Common tasks

| Command                           | Usage                                                                                                  |
| --------------------------------- | ------------------------------------------------------------------------------------------------------ |
| `yarn start:storybook`            | Starts up a Storybook server with a watch task, used to develop/preview components.                    |
| `yarn plop`                       | Generates template files for getting a component skeleton to work from.                                |
| `yarn start`                      | Development server. Although, usually `yarn storybook` will be used for development.                   |
| `yarn build`                      | Production build. Will generate bundled and browser ready files in the `/dist` folder.                 |
| `yarn build:storybook`            | Builds a static version of the current Storybook. Mostly used by out deploy tools                      |
| `npm version [patch/minor/major]` | [npm](https://docs.npmjs.com/cli/v6/commands/npm-version). Bump the version, before publishing to npm. |
| `npm publish`                     | [npm](https://docs.npmjs.com/cli/v6/commands/npm-publish). Publish a new version to npm.               |

Files merged to `master` is automatically built to [dsb.sorthvid.design](http://dsb.sorthvid.design/).

## Depencencies

- [lit](https://lit.dev)
- [scss](https://sass-lang.com/)
- [Storybook](https://storybook.js.org/)
- [Zeroheight](https://dsb.zeroheight.com/styleguides)

## Author

- [Dept](https://www.deptagency.com/en-dk/office/copenhagen/)

## Gotchas

### "[!] (plugin postcss) Error: no mixin named mq"

Delete the folder `/node-sass` from `/node_modules`