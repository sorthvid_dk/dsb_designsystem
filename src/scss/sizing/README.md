# Sizing
Fixed sizings are defined and used throughout the project.
To create a balanced and structured layout.

When the scales are used exclusively in the project,
it will automatically create a uniform standard and natural balance between page elements.

## Grid?
In cases where a components horizontal spacing should align with the page's grid,
the column function has to be used. (See /scss/grid).

It could look like:

```scss
.component {
  // 32px vertical margin
  // 1 column horizontal margin
  margin: units(4) grid-cols(1);
}
```

## Respect the system
There might be times where a design calls for a specific, odd, padding.
The spacing system is however created to ensure a consistant overall design.

All spacing's scales should therefore be respected.

## Usage
The steps in the scales are not responsive themeself.
But feel free to change step depending on screen size.

The scales can technically be used interchangable with in eachother, in that their output is the same format.
When developing components using the scales you should however try and think of what scale should be used in different aspects of the component.

There are 3 main functions to use,
that each have their own scale and should be applied in their own different context.

These scales are:

### Units
The default scale.

```scss
.component {
  margin-bottom: units(3); // 24px
}
```

Ment to be used for general purpose spacings and sizings.
Use cases include the height of form elements, or the space between a headline and a button.

| Step | Value |
| ---- | ----- |
| 1    | 12px  |
| 2    | 24px  |
| 3    | 36px  |
| 4    | 48px  |
| 5    | 64px  |
| 6    | 80px  |
| 7    | 96px  |
| 8    | 112px |
| 9    | 128px |


### Spacing
The smaller scale.

```scss
.small-component {
  margin-bottom: spacing(3); // 8px
}
```

Ment to be used for adding space between elements in small components.
A use case could be the distance between an input field and it's label.

| Step | Value |
| ---- | ----- |
| 1    | 2px   |
| 2    | 4px   |
| 3    | 8px   |
| 4    | 12px  |
| 5    | 16px  |
| 6    | 24px  |
| 7    | 32px  |
| 8    | 40px  |
| 9    | 48px  |


### Layout
The larger scale.

```scss
.large-component {
  margin-bottom: layout(3); // 32px
}
```

Ment to be used for spacing between larger elements.
A use case could be the distance between two decks.

| Step | Value |
| ---- | ----- |
| 1    | 16px  |
| 2    | 24px  |
| 3    | 32px  |
| 4    | 48px  |
| 5    | 64px  |
| 6    | 96px  |
| 7    | 160px |
| 8    | 224px |
| 9    | 288px |
