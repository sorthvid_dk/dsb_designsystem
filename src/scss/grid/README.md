# Grid
The grid is based on a simple flex-box grid.
It includes a few classes to put on the grid-elements in order to place them correctly.

## Flexibility
There are both SCSS functions and -mixins available.
Use the one that suits the situation best.

## Different aspects
Some of the components require its grid elements to have a specific aspect ratio.
Then the `grid-cols-ar()`-mixin should be used, instead of the `grid-cols()` one.
It calculates the correct `min-height` as well as width, given a column amount.

## Usage
```scss
// src/my-component.scss
@use 'path/to/scss' as *;

.parent {
    @include grid-grid;

    .child {
        // 2 columns wide
        @include grid-cols($cols: 2); // $prop defaults to width

        // 1 column padding-left
        @include grid-cols($prop: padding-left, $cols: 1);

        // push the element 1 column to the right
        @include grid-offset($cols: 1);

        // 6 columns wide, and overwrites default column count
        // i.e. will be 12 'normal' columns
        // [fix this] - is this necessery?
        @include grid-cols($cols: 6, $of: 12);
    }

    .widescreen-child {
      // 12 columns wide, and 16/9-aspect ratio
      // (will expand if content demands it)
      @include grid-cols-ar($cols: 12, $ratio: 16/9);
    }

    .alternative {
        // 2 'columns' wide (always relative to 100vw, doesn't respect page-max-width)
        width: grid-cols(2);
    }
}
```

```html
<!-- src/my-component.html -->
<div class="col-12 col-m-6 col-l-4">
    <!-- 12 columns on mobile -->
    <!-- 6 columns on tablet -->
    <!-- 4 columns on desktop -->
</div>
```
