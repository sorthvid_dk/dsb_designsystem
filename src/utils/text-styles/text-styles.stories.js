import readme from './README.md';
import { html } from 'lit';

export default {
  title: 'Styles/Text Styles',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const TextStyles = () =>
  html`
    <div>
      <!-- headline styles -->
      <p class="ts-headline-1">.ts-headline-1</p>
      <p class="ts-headline-2">.ts-headline-2</p>
      <p class="ts-headline-3">.ts-headline-3</p>
      <p class="ts-headline-4">.ts-headline-4</p>
      <p class="ts-headline-5">.ts-headline-5</p>

      <!-- paragraph styles -->
      <p class="ts-standard">.ts-standard</p>
      <p class="ts-component">.ts-component</p>
      <p class="ts-hero">.ts-hero</p>

      <!-- action styles -->
      <p class="ts-mini">.ts-mini</p>
      <p class="ts-action">.ts-action</p>
      <p class="ts-trumpet">.ts-trumpet</p>
    </div>
  `;
