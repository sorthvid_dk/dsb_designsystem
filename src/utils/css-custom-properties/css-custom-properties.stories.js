import readme from './README.md';
import { html } from 'lit';
import css from './../../variables.scss';

console.log(css);

export default {
  title: 'Styles/CSS Custom Properties',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

// TODO: it's manually copypasted

export const CSSCustomProperties = () =>
  html`
    <div style="display: grid; grid-template-columns: auto 1fr; gap: 6px 0;">
      <code style="color: var(--color-grey-3)">--dsb-breakpoints-s:</code>
      <code>600px;</code>
      <code style="color: var(--color-grey-3)">--dsb-breakpoints-m:</code>
      <code>900px;</code>
      <code style="color: var(--color-grey-3)">--dsb-breakpoints-l:</code>
      <code>1250px;</code>
      <code style="color: var(--color-grey-3)">--dsb-breakpoints-xl:</code>
      <code>1681px;</code>
      <code style="color: var(--color-grey-3)">--dsb-breakpoints-max:</code>
      <code>2320px;</code>
      <code style="color: var(--color-grey-3)">--font-primary:</code>
      <code>"Via Office",serif;</code>
      <code style="color: var(--color-grey-3)">--font-secondary:</code>
      <code>"Helvetica Neue",sans-serif;</code>
      <code style="color: var(--color-grey-3)">--border-radius:</code>
      <code>5px;</code>
      <code style="color: var(--color-grey-3)">--border-width:</code>
      <code>1px;</code>
      <code style="color: var(--color-grey-3)">--page-width:</code>
      <code>1920px;</code>
      <code style="color: var(--color-grey-3)">--ease-in-out:</code>
      <code>cubic-bezier(0.25,0.1,0.25,1);</code>
      <code style="color: var(--color-grey-3)">--units-1:</code>
      <code>12px;</code>
      <code style="color: var(--color-grey-3)">--units-2:</code>
      <code>24px;</code>
      <code style="color: var(--color-grey-3)">--units-3:</code>
      <code>36px;</code>
      <code style="color: var(--color-grey-3)">--units-4:</code>
      <code>48px;</code>
      <code style="color: var(--color-grey-3)">--units-5:</code>
      <code>64px;</code>
      <code style="color: var(--color-grey-3)">--units-6:</code>
      <code>80px;</code>
      <code style="color: var(--color-grey-3)">--units-7:</code>
      <code>96px;</code>
      <code style="color: var(--color-grey-3)">--units-8:</code>
      <code>112px;</code>
      <code style="color: var(--color-grey-3)">--units-9:</code>
      <code>128px;</code>
      <code style="color: var(--color-grey-3)">--spacing-1:</code>
      <code>2px;</code>
      <code style="color: var(--color-grey-3)">--spacing-2:</code>
      <code>4px;</code>
      <code style="color: var(--color-grey-3)">--spacing-3:</code>
      <code>8px;</code>
      <code style="color: var(--color-grey-3)">--spacing-4:</code>
      <code>12px;</code>
      <code style="color: var(--color-grey-3)">--spacing-5:</code>
      <code>16px;</code>
      <code style="color: var(--color-grey-3)">--spacing-6:</code>
      <code>24px;</code>
      <code style="color: var(--color-grey-3)">--spacing-7:</code>
      <code>32px;</code>
      <code style="color: var(--color-grey-3)">--spacing-8:</code>
      <code>40px;</code>
      <code style="color: var(--color-grey-3)">--spacing-9:</code>
      <code>48px;</code>
      <code style="color: var(--color-grey-3)">--layout-1:</code>
      <code>16px;</code>
      <code style="color: var(--color-grey-3)">--layout-2:</code>
      <code>24px;</code>
      <code style="color: var(--color-grey-3)">--layout-3:</code>
      <code>32px;</code>
      <code style="color: var(--color-grey-3)">--layout-4:</code>
      <code>48px;</code>
      <code style="color: var(--color-grey-3)">--layout-5:</code>
      <code>64px;</code>
      <code style="color: var(--color-grey-3)">--layout-6:</code>
      <code>96px;</code>
      <code style="color: var(--color-grey-3)">--layout-7:</code>
      <code>160px;</code>
      <code style="color: var(--color-grey-3)">--layout-8:</code>
      <code>224px;</code>
      <code style="color: var(--color-grey-3)">--layout-9:</code>
      <code>288px;</code>
      <code style="color: var(--color-grey-3)">--color-red:</code>
      <code>#b41730;</code>
      <code style="color: var(--color-grey-3)">--color-dark-red:</code>
      <code>#9d152c;</code>
      <code style="color: var(--color-grey-3)">--color-blue:</code>
      <code>#00233c;</code>
      <code style="color: var(--color-grey-3)">--color-white:</code>
      <code>#fff;</code>
      <code style="color: var(--color-grey-3)">--color-grey-1:</code>
      <code>#eeeff0;</code>
      <code style="color: var(--color-grey-3)">--color-grey-2:</code>
      <code>#ccc;</code>
      <code style="color: var(--color-grey-3)">--color-grey-3:</code>
      <code>#4c4c4c;</code>
      <code style="color: var(--color-grey-3)">--color-black:</code>
      <code>#111;</code>
    </div>
  `;
