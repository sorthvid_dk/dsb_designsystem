import { html } from 'lit';
import readme from './README.md';
import { repeat } from 'Util';

export default {
  title: 'Styles/Grid',
  parameters: {
    readme: {
      sidebar: readme
    }
  }
};

export const Default = () => html`
  <div class="grid">
    <div class="col-6"><span class="placeholder">Column</span></div>
    <div class="col-6"><span class="placeholder">Column</span></div>
    <div class="col-6"><span class="placeholder">Column</span></div>
    <div class="col-6"><span class="placeholder">Column</span></div>
  </div>
`;

export const Responsive = () => html`
  <div class="grid">
    ${repeat(
      12,
      html`
        <div class="col-24 col-s-12 col-m-6 col-l-4">
          <span class="placeholder">Column</span>
        </div>
      `
    )}
  </div>
`;

export const AspectRatio = () => html`
  <div class="grid">
    ${repeat(
      12,
      html`
        <div class="col-ar-24 col-ar-s-12 col-ar-m-6 col-ar-l-4">
          <span class="placeholder">Column</span>
        </div>
      `
    )}
  </div>
`;
