# Spacing

Fixed sizings are defined and used throughout the project.
To create a balanced and structured layout.

When the scales are used exclusively in the project,
it will automatically create a uniform standard and natural balance between page elements.

All the different steps are available as CSS Custom properies.

| Name      | Description       |
| --------- | ----------------- |
| `spacing` | The smaller scale |
| `units`   | The default scale |
| `layout`  | The larger scale  |

## Helper classes

There are helper classes made from the `units`-scale.
Margin and padding in all directions.
They follow the pattern: [margin/padding][t/r/b/l]-[step]

| Class  | Gives                     |
| ------ | ------------------------- |
| `m-2`  | 2 units margin all around |
| `mt-4` | 4 units margin top        |
| `pb-1` | 1 unit padding bottom     |

### Units

The default scale.

```scss
.component {
  margin-bottom: var(--units-3); // 24px
}
```

Ment to be used for general purpose spacings and sizings.
Use cases include the height of form elements, or the space between a headline and a button.

| Step | Value |
| ---- | ----- |
| 1    | 12px  |
| 2    | 24px  |
| 3    | 36px  |
| 4    | 48px  |
| 5    | 64px  |
| 6    | 80px  |
| 7    | 96px  |
| 8    | 112px |
| 9    | 128px |

### Spacing

The smaller scale.

```scss
.small-component {
  margin-bottom: var(--spacing-3); // 8px
}
```

Ment to be used for adding space between elements in small components.
A use case could be the distance between an input field and it's label.

| Step | Value |
| ---- | ----- |
| 1    | 2px   |
| 2    | 4px   |
| 3    | 8px   |
| 4    | 12px  |
| 5    | 16px  |
| 6    | 24px  |
| 7    | 32px  |
| 8    | 40px  |
| 9    | 48px  |

### Layout

The larger scale.

```scss
.large-component {
  margin-bottom: var(--layout-3); // 32px
}
```

Ment to be used for spacing between larger elements.
A use case could be the distance between two decks.

| Step | Value |
| ---- | ----- |
| 1    | 16px  |
| 2    | 24px  |
| 3    | 32px  |
| 4    | 48px  |
| 5    | 64px  |
| 6    | 96px  |
| 7    | 160px |
| 8    | 224px |
| 9    | 288px |
