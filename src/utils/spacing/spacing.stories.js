import { html } from 'lit';
import readme from './README.md';
// import { repeat } from 'Util';

export default {
  title: 'Styles/Spacing',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

const style = (width) => {
  return `display: block; margin-bottom: 1em; height: 20px; background-color: var(--color-blue); width: var(--units-${width});`;
};

export const Spacing = () => html`
  <div>
    ${[...Array(9)].map(
      (_, i) => html` ${i + 1}<span style=${style(i + 1)}></span> `
    )}
  </div>
`;
