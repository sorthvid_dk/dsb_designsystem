# Color

## Full control over light and dark.
The color classes (fex. `.bg--red`) should be used when an html-element that can hold content should have a background color (and preferably not be declared in the components scss), in order to keep control over when the containing text/links should be light and dark. There are some quite handy sass mixins implemented to detect the lightness of any context. So even though a component has a background image, it's good practice to put a `class="bg--class"` on the element in order for the links and icons to render with enough contrast.
