import { html } from 'lit';
import readme from './README.md';

export default {
  title: 'Styles/Color',
  parameters: {
    readme: {
      sidebar: readme
    }
  }
};

export const Default = () => html`
  <div>
    <div class="bg--red">.bg--red</div>
    <div class="bg--blue">.bg--blue</div>
    <div class="bg--grey">.bg--grey</div>
    <div class="bg--darkgrey">.bg--darkgrey</div>
  </div>
`;
