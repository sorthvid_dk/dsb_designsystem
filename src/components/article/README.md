# `<dsb-article>`

Text About the component

## Example

```html
<!-- basic -->
<dsb-article headline="Some headline">
  <p>Some text</p>
</dsb-article>
```

```html
<!-- with background color and subheadline -->
<dsb-article headline="Some headline" background="grey">
  <dsb-button slot="subheadline" label="Click me"></dsb-button>
  <p slot="subheadline">And some text under button</p>
  <p>Some text</p>
</dsb-article>
```

## Api

### Properties/Attributes

| Name         | Type       | default | Description                                         |
| ------------ | ---------- | ------- | :-------------------------------------------------- |
| `headline`   | `string`   | `''`    | Larger text to show in the left column              |
| `background` | `string`\* | `''`    | Adds a baackground color and padding to the element |

\* available background are: `red`|`blue`|`grey`

### Slots

| Name          | Description                         |
| ------------- | :---------------------------------- |
| `sunheadline` | Content to place in the left column |
| _default_     | Content for the right column        |

### CSS Custom Properties

_None_
