import { LitElement, html } from 'lit';
import CSS from './article.scss';

export class Article extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
      background: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.background = '';
  }

  render() {
    return html`
      <div class="content">
        <div class="headline">
          <h2>${this.headline}</h2>
          <slot name="subheadline"></slot>
        </div>
        <slot></slot>
      </div>
    `;
  }
}

customElements.define('dsb-article', Article);
