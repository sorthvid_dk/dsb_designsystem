import './article';
import { html } from 'lit';
import readme from './README.md';
import { random } from '../../../.storybook/utils';

export default {
  title: 'Elements/Article',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Article = (args) =>
  html`<dsb-article headline=${args.headline}>
    <p>${args.text}</p>
  </dsb-article>`;

export const WithSubheadline = (args) =>
  html`<dsb-article headline=${args.headline}>
    <dsb-button slot="subheadline" label="Click me"></dsb-button>
    <p slot="subheadline">And some text.</p>
    <p>${args.text}</p>
  </dsb-article>`;

export const WithBackground = (args) =>
  html`<dsb-article headline=${args.headline} background=${args.background}>
    <dsb-button
      slot="subheadline"
      label="Click me"
      variant="ghost"
    ></dsb-button>

    <p>${args.text}</p>
  </dsb-article>`;

Article.args = WithSubheadline.args = {
  headline: random('headline'),
  text: random('article'),
};

WithBackground.argTypes = {
  background: {
    options: ['red', 'grey', 'blue'],
    control: { type: 'radio' },
  },
};

WithBackground.args = {
  ...Article.args,
  background: 'red',
};
