import { LitElement, html } from 'lit';
import CSS from './accordion-item.scss';
import randomId from './../../js/id';
import { flip } from '../../js/flip';

export class AccordionItem extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
      isOpen: { type: Boolean, reflect: true },
      index: { type: Number },
      id: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.isOpen = false;
    this.index = -1;
    this.id = randomId();
  }

  connectedCallback() {
    super.connectedCallback();
    const items = [
      ...this.parentElement.querySelectorAll('dsb-accordion-item'),
    ];
    this.index = items.findIndex((i) => i === this) + 1;
  }

  toggle() {
    const state = flip.getState(this.shadowRoot.querySelector('.summary'));
    this.isOpen = !this.isOpen;
    flip.expand(state);
  }

  render() {
    return html`
      <button
        class="details"
        aria-controls=${this.id}
        aria-expanded=${this.isOpen}
        @click=${this.toggle}
      >
        <div class="title">
          <span class="index">${this.index}</span>
          <span>${this.headline}</span>
        </div>
        <slot name="headline"></slot>
        <dsb-icon icon="plus"></dsb-icon>
      </button>
      <div
        class="summary"
        id=${this.id}
        aria-hidden=${!this.isOpen}
        ?hidden=${!this.isOpen}
      >
        <div>
          <slot></slot>
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-accordion-item', AccordionItem);
