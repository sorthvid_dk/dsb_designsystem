import { html } from 'lit';
import readme from './README.md';
import './accordion';
import './accordion-item';
import { random } from 'Util';

export default {
  title: 'Elements/Accordion',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Accordion = () =>
  html`<dsb-accordion>
    <h1 slot="headline">This is an Accordion Component</h1>
    <dsb-accordion-item headline="This is an Accordion Item">
      <p>${random('article')}</p>
    </dsb-accordion-item>
    <dsb-accordion-item headline="This is also an Accordion Item">
      <p>${random('article')}</p>
    </dsb-accordion-item>
    <dsb-accordion-item headline="There can be many">
      <p>${random('article')}</p>
    </dsb-accordion-item>
    <dsb-accordion-item headline="Sooooo many">
      <p>${random('article')}</p>
    </dsb-accordion-item>
  </dsb-accordion>`;

export const AccordionItem = () =>
  html`<dsb-accordion-item headline="This is an Accordion Item">
    <p>${random('article')}</p>
  </dsb-accordion-item>`;
// <h1 slot="headline"></h1>
