import { LitElement, html } from 'lit';
import CSS from './accordion.scss';

export class Accordion extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
  }

  render() {
    return html`
      <div class="headline">
        <slot name="headline"></slot>
      </div>
      <slot></slot>
    `;
  }
}

customElements.define('dsb-accordion', Accordion);
