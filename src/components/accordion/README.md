# `<dsb-accordion>` / `<dsb-accordion-item>`

Good for holding a lot of content without taking a lot of space on the page.

## Example

```html
<dsb-accordion>
  <h1 slot="headline">This is an Accordion Component</h1>

  <dsb-accordion-item headline="This is an Accordion Item">
    <p>Hidden content</p>
  </dsb-accordion-item>

  <dsb-accordion-item headline="This is also an Accordion Item">
    <p>Hidden content</p>
  </dsb-accordion-item>

  <dsb-accordion-item headline="There can be many">
    <p>Hidden content</p>
  </dsb-accordion-item>

  <dsb-accordion-item headline="Sooooo many">
    <p>Hidden content</p>
  </dsb-accordion-item>
</dsb-accordion>
```

## Api

### Properties/Attributes

#### `<dsb-accordion>`

_None_

#### `<dsb-accordion-item>`

| Name       | Type     | default | Description             |
| ---------- | -------- | ------- | :---------------------- |
| `headline` | `string` | `''`    | Always visible headline |

### Slots

#### `<dsb-accordion>`

| Name      | Description                           |
| --------- | :------------------------------------ |
| _default_ | Place to put `<dsb-accordion-item>`'s |

#### `<dsb-accordion-item>`

| Name      | Description    |
| --------- | :------------- |
| _default_ | Hidden content |

### CSS Custom Properties

_None_
