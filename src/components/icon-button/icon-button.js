import { LitElement, html } from 'lit';
import CSS from './icon-button.scss';
import './../icon/icon'

export class IconButton extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      icon: { type: String },
      ariaLabel: { type: String },
      disabled: { type: Boolean, reflect: true },
      round: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.icon = '';
    this.ariaLabel = '';
    this.disabled = false;
    this.round = false;
  }

  render() {
    return html`<button
      aria-label="${this.ariaLabel || this.icon}"
      ?disabled="${this.disabled}"
      ?round="${this.round}"
    >
      ${this.icon ? html`<dsb-icon icon=${this.icon}></dsb-icon>` : ''}
      <slot></slot>
    </button>`;
  }
}

customElements.define('dsb-icon-button', IconButton);
