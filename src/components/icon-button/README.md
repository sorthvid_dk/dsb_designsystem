# `<dsb-icon-button>`

Icon Buttons allow users to take actions, and make choices, with a single tap.

## Installation

```bash
yarn install @dsb.dk/designsystem
```

```js
// my-project.js
import '@dsb.dk/designsystem';
```

## Example

Basic usage (recomended)

```html
<dsb-icon-button icon="dsb-logo" ariaLabel="Click me"></dsb-icon-button>
```

With SVG

```html
<dsb-icon-button ariaLabel="Click me">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 449 449">
    <path d="M143 324L36 217 0 252l143 143L449 89l-36-35z" />
  </svg>
</dsb-icon-button>
```

With Icon and text

```html
<dsb-icon-button ariaLabel="Click me">
  Click me
</dsb-icon-button>
```

## Api

### Properties/Attributes

| Name        | Type      | default | Description                                       |
| ----------- | --------- | ------- | :------------------------------------------------ |
| `icon`      | `string`  | `''`    | Name of Icon                                      |
| `ariaLabel` | `string`  | `''`    | The button text, will also render as `aria-label` |
| `disabled`  | `boolean` | `false` | Will render the button disabled                   |

### Slots

| Name      | Description                 |
| --------- | :-------------------------- |
| _default_ | Will render after the icon. |

### CSS Custom Properties

Inherits all CSS Custom Properties from Icon:

| Name           | Default        | Description       |
| -------------- | -------------- | ----------------- |
| `--icon-size`  | `20px`         | Size of the icon  |
| `--icon-color` | `currentColor` | Color of the icon |
