import readme from './README.md';
import './icon-button';
import { html } from 'lit';

export default {
  title: 'Elements/Icon Button',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const IconButton = (args) =>
  html`<dsb-icon-button
    icon=${args.icon}
    ariaLabel=${args.ariaLabel}
    ?disabled=${args.disabled}
    ?round=${args.round}
  ></dsb-icon-button>`;

export const WithBackground = (args) =>
  html` <div>
    <style>
      dsb-icon-button {
        --icon-button-background-color: var(--color-blue);
        --icon-button-padding: var(--units-2);
        --icon-color: var(--color-white);
      }
    </style>
    <dsb-icon-button
      icon=${'play'}
      ariaLabel=${args.ariaLabel}
      ?disabled=${args.disabled}
      ?round=${true}
    ></dsb-icon-button>
  </div>`;

IconButton.args = WithBackground.args = {
  icon: 'dsb-logo',
  ariaLabel: 'Go to homepage',
  disabled: false,
  round: false,
};
