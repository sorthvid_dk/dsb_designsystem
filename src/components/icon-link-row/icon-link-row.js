import { LitElement, html } from 'lit';
import CSS from './icon-link-row.scss';

export class IconLinkRow extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <div class="icon-link-row">
        <div class="deck">
          <div class="deck__inner">
            <div class="deck__content">
              <slot></slot>
            </div>
          </div>
        </div>
      </div>
      `;
  }
}

customElements.define('dsb-icon-link-row', IconLinkRow);
