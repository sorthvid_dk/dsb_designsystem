import readme from './README.md';
import { html } from 'lit';

export default {
  title: 'Patterns/Icon Link Row',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

const items = [
  {
    href: '#',
    icon: 'customerservice',
    text: 'Kundeservice',
  },
  {
    href: '#',
    icon: 'trafficinfo',
    text: 'Trafikinformation',
  },
  {
    href: '#',
    icon: 'ticketservice',
    text: 'Billetter og services',
  },
  {
    href: '#',
    icon: 'dsbplus',
    text: 'DSB Plus',
  },
  {
    href: '#',
    icon: 'user',
    text: 'DSB Orange',
  },
];

export const IconLinkRow = () =>
  html` <dsb-icon-link-row>
          ${items.map(
            (item) => html`<dsb-icon-link
              href="${item.href}"
              icon="${item.icon}"
              text="${item.text}"
            ></dsb-icon-link>`
          )}
        </dsb-icon-link-row>`;
