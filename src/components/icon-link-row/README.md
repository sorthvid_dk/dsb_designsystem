# Icon link row

The Icon-Link-Row is a container of, a row that contains icons as links.
Use the slot to render `<dsb-icon-link>` inside the container.

## Example

```html
<dsb-icon-link-row>
  <dsb-icon-link href="#" icon="customerservice" text="Kundeservice"></dsb-icon-link>
  <dsb-icon-link href="#" icon="trafficinfo" text="Trafikinformation"></dsb-icon-link>
  <dsb-icon-link href="#" icon="ticketservice" text="Billetter og services"></dsb-icon-link>
  <dsb-icon-link href="#" icon="dsbplus" text="DSB Plus"></dsb-icon-link>
</dsb-icon-link-row>;

```

### Slots

| Name         | Description                                                                     |
| ------------ | :------------------------------------------------------------------------------ |
| _default_    | Renders inside the row. Used for placing one or more `<dsb-icon-link>` in a row |


## todo
- introduce hiding of centain links on mobile? See `Kundservice`-link in InVision
- sizing/placement with grid-classes?

### changelog
