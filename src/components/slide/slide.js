import { LitElement, html } from 'lit';
import CSS from './slide.scss';
import '../icon-button/icon-button';

export class Slide extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
      narrow: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.narrow = '';
  }

  get _slottedChildren() {
    const slot = this.shadowRoot.querySelector('slot');
    const childNodes = slot.assignedNodes({ flatten: true });
    return Array.prototype.filter.call(
      childNodes,
      (node) => node.nodeType == Node.ELEMENT_NODE
    );
  }

  slide(next) {
    /**
     * the horizontal left margin
     */
    const { scrollPaddingLeft, paddingLeft } = getComputedStyle(
      this.renderRoot.querySelector('.content')
    );
    const padding = scrollPaddingLeft === 'auto' ? 0 : parseInt(paddingLeft);

    /**
     * returns one item, that is currently at the left border
     * in order to both have the mouse scroll and the buttons keep track of
     * what item is the current 'active' one
     */
    const activeItem = this._slottedChildren.find((item, i) => {
      const { left } = item.getBoundingClientRect();
      const leftPos = left - (i === 0 ? 0 : padding);
      /**
       * return the first item that has a positive left position
       */
      return leftPos >= -10;
    });

    /**
     * Will be the item before/after current one
     */
    const newItem = this._slottedChildren[
      this._slottedChildren.indexOf(activeItem) + (next ? 1 : -1)
    ];

    console.log(newItem);

    /**
     * Do the scrolling
     */
    newItem &&
      newItem.scrollIntoView({
        behavior: 'smooth',
        inline: 'start',
        block: 'nearest',
      });
  }

  render() {
    return html` <section>
      ${this.headline ? html`<h1>${this.headline}</h1>` : ''}
      <div class="content"><slot></slot></div>
      <div class="ui">
        <dsb-icon-button
          icon="arrow--left"
          round
          class="previous"
          @click=${() => this.slide(false)}
        ></dsb-icon-button>
        <dsb-icon-button
          icon="arrow--right"
          round
          class="next"
          @click=${() => this.slide(true)}
        ></dsb-icon-button>
      </div>
    </section>`;
  }
}

customElements.define('dsb-slide', Slide);
