# `<dsb-slide>`

Slideshow component with a basic UI. Is based on the native [`scrollIntoView`](https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollIntoView) API.

## Example

```html
<dsb-slide-show headline="Some headline" narrow>
  <p>1</p>
  <p>2</p>
  <p>3</p>
  <p>4</p>
  <p>5</p>
  <p>6</p>
  <p>7</p>
  <p>8</p>
</dsb-slide-show>
```

## Api

### Properties/Attributes

| Name     | Type      | default | Description                           |
| -------- | --------- | ------- | :------------------------------------ |
| `narrow` | `boolean` | `false` | Limits the max-width of the component |

### Slots

| Name      | Description                                            |
| --------- | :----------------------------------------------------- |
| _default_ | The items that slides. Preferably `<dsb-image-link>`'s |

### CSS Custom Properties

_None_
