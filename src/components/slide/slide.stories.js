import { html } from 'lit';
import './slide.js';
import readme from './README.md';
import { repeat, random } from 'Util';

export default {
  title: 'Elements/Slide',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Slide = (args) => html`<dsb-slide
  headline=${args.headline}
  .narrow=${args.narrow}
>
  <dsb-image-link
    href="#"
    text=${random('paragraph')}
    headline=${random('headline')}
    src=${random('jpg')}
    alt=${'A example'}
  ></dsb-image-link>
  <dsb-image-link
    href="#"
    text=${random('paragraph')}
    headline=${random('headline')}
    src=${random('jpg')}
    alt=${'A example'}
  ></dsb-image-link>
  <dsb-image-link
    href="#"
    text=${random('paragraph')}
    headline=${random('headline')}
    src=${random('jpg')}
    alt=${'A example'}
  ></dsb-image-link>
  <dsb-image-link
    href="#"
    text=${random('paragraph')}
    headline=${random('headline')}
    src=${random('jpg')}
    alt=${'A example'}
  ></dsb-image-link>
  <dsb-image-link
    href="#"
    text=${random('paragraph')}
    headline=${random('headline')}
    src=${random('jpg')}
    alt=${'A example'}
  ></dsb-image-link>
</dsb-slide>`;

export const Narrow = () => html`<dsb-slide narrow>
  ${repeat(100, (_, i) => html`<div>longer item${i}&emsp;</div>`)}
</dsb-slide>`;

Slide.args = Narrow.args = {
  headline: random('paragraph'),
  narrow: false,
};
