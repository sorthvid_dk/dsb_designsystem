import base from '../.base-template';

class SlideShow extends base {
  init() {}

  connectedCallback() {
    /**
     * [required] - the element the items slides within
     */
    this.content = this.querySelector('[data-content]');

    /**
     * [required] - the elements that slides
     */
    this.items = [...this.querySelectorAll('[data-item]')];

    /**
     * [optional] - a button to go to the next item
     */
    this.nextButton = this.querySelector('[data-next]');

    /**
     * [optional] - a button to go to the previous item
     */
    this.prevButton = this.querySelector('[data-prev]');

    /**
     * if there's a next button, listen for clicks
     */
    this.nextButton &&
      this.nextButton.addEventListener(
        'click',
        this.scrollContent.bind(this, { next: true })
      );

    /**
     * if there's a prev button, listen for clicks
     */
    this.prevButton &&
      this.prevButton.addEventListener(
        'click',
        this.scrollContent.bind(this, { next: false })
      );
  }

  scrollContent({ next }) {
    /**
     * the horizontal left margin
     */
    const { scrollPaddingLeft } = window.getComputedStyle(this.content);
    const paddingLeft =
      scrollPaddingLeft === 'auto' ? 0 : parseInt(scrollPaddingLeft);

    /**
     * returns one item, that is currently at the left border
     * in order to both have the mouse scroll and the buttons keep track of
     * what item is the current 'active' one
     */
    const activeItem = this.items.find((item, i) => {
      const { left } = item.getBoundingClientRect();
      const leftPos = left - (i === 0 ? 0 : paddingLeft);
      /**
       * return the first item that has a positive left position
       */
      return leftPos >= -10;
    });

    /**
     * Will be the item before/after current one
     */
    const newItem = this.items[
      this.items.indexOf(activeItem) + (next ? 1 : -1)
    ];

    /**
     * Do the scrolling
     */
    newItem &&
      newItem.scrollIntoView({
        behavior: 'smooth',
        inline: 'start',
        block: 'nearest'
      });
  }
}

export const defineSlideShow = () => customElements.define('slide-show', SlideShow);
