// import { Footer } from './footer.html';
// import { footer } from './footer.state';
import './footer';
import { html } from 'lit';
import readme from './README.md';

export default {
  title: 'Elements/Footer',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => html`<dsb-footer homeUrl="#">
  <li slot="one">One</li>
  <li slot="one">One</li>
  <li slot="one">One</li>

  <li slot="two">Two</li>
  <li slot="two">Two</li>
  <li slot="two">Two</li>
  <li slot="two">Two</li>
  <li slot="two">Two</li>

  <li slot="three">Three</li>

  <li slot="four">Four</li>
  <li slot="four">Four</li>
  <li slot="four">Four</li>
  <li slot="four">Four</li>

  <li slot="bottom">Bottom</li>
  <li slot="bottom">Bottom</li>
  <li slot="bottom">Bottom</li>
</dsb-footer>`;

export const Footer = () => html`<dsb-footer homeUrl="#">
  <li slot="one">
    <dsb-icon-link
      arrow
      href="#"
      label="Kundservice"
      inline=${true}
    ></dsb-icon-link>
  </li>
  <li slot="one">
    <dsb-underline-link
      class="ts-headline-3"
      href="tel:#"
      label="70 13 14 15"
    ></dsb-underline-link>
  </li>
  <li slot="one">
    <p class="ts-trumpet">Vi har åbent 07:00 - 20:00</p>
  </li>

  <li slot="two">
    <dsb-underline-link
      href="#"
      label="Billetter og Service"
    ></dsb-underline-link>
  </li>
  <li slot="two">
    <dsb-underline-link href="#" label="Erhverv"></dsb-underline-link>
  </li>
  <li slot="two">
    <dsb-underline-link href="#" label="DSB Plus"></dsb-underline-link>
  </li>
  <li slot="two">
    <dsb-underline-link href="#" label="Ansvar & miljø"></dsb-underline-link>
  </li>
  <li slot="two">
    <dsb-underline-link href="#" label="Om DSB"></dsb-underline-link>
  </li>

  <li slot="three">
    <dsb-icon-link
      arrow
      href="#"
      label="Trafikinformation"
      inline=${true}
    ></dsb-icon-link>
  </li>
  <li slot="four">
    <a href="#">Afgangstavler</a>
  </li>
  <li slot="four">
    <a href="#">Ændringer i trafikken</a>
  </li>
  <li slot="four">
    <a href="#">Sporarbejde</a>
  </li>
  <li slot="four">
    <a href="#">Køreplaner</a>
  </li>

  <li slot="bottom">
    <dsb-underline-link href="#" label="Cookies på DSB.dk"></dsb-underline-link>
  </li>
  <li slot="bottom">
    <dsb-underline-link href="#" label="Rejseregler"></dsb-underline-link>
  </li>
  <li slot="bottom">
    <dsb-underline-link href="#" label="Persondata"></dsb-underline-link>
  </li>
</dsb-footer>`;
