import { LitElement, html } from 'lit';
import CSS from './footer.scss';

export class Footer extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      homeUrl: { type: String },
    };
  }

  constructor() {
    super();
    this.homeUrl = '';
  }

  render() {
    return html`
      <footer>
        <div class="top">
          <!-- col 1 -->
          <div class="home-link">
            <dsb-icon icon="dsb-logo"></dsb-icon>
          </div>
          <!-- col 2 -->
          <div class="body">
            <ul class="one">
              <slot name="one"></slot>
            </ul>
            <ul class="two">
              <slot name="two"></slot>
            </ul>
            <div>
              <ul class="three">
                <slot name="three"></slot>
              </ul>
              <ul class="four">
                <slot name="four"></slot>
              </ul>
            </div>
          </div>
        </div>

        <div class="bottom">
          <ul class="bottom-row">
            <slot name="bottom"></slot>
          </ul>
          <ul>
            <li class="england">
              <dsb-icon icon="flag--england"></dsb-icon>England
            </li>
            <!-- <li class="denmark">
              <dsb-icon icon="flag--denmark"></dsb-icon>Denmark
            </li> -->
          </ul>
        </div>
      </footer>
    `;
  }
}

customElements.define('dsb-footer', Footer);
