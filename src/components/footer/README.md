# `<dsb-footer>`

Text About the component

## Example

```html
<dsb-footer></dsb-footer>
```

## Api

### Properties/Attributes

| Name      | Type     | default | Description              |
| --------- | -------- | ------- | :----------------------- |
| `homeUrl` | `string` | `''`    | `href` for the logo-link |

### Slots

| Name     | Description         |
| -------- | :------------------ |
| `one`    | Column one          |
| `two`    | Column two          |
| `three`  | Column three top    |
| `four`   | Column three bottom |
| `bottom` | Inline bottom items |

### CSS Custom Properties

_None_
