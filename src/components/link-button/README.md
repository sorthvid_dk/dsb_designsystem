# `<dsb-link-button>`

Anchor tag `<a>`, link styled as a button, used for correct HTML syntax.

## Example

```html
<dsb-link-button
  icon = false,
  url = '/site';
  text = 'Default text',
  variant = 'primary';
></dsb-link-button>
```

## Api

### Properties/Attributes

| Name        | Type       | default   | Description                                       |
| ----------- | ---------- | --------- | :------------------------------------------------ |
| `icon`      | `string`   | `''`      | An optional icon                                  |
| `text`      | `string`   | `''`      | The button text, will also render as `aria-label` |
| `variant`   | `string`\* | `primary` | The style of the button                           |
| `url`       | `string`   | `''`      | Url to redirect to                                |

\* available vaiants are: `primary`|`secondary`|`ghost`|`white`

### Slots

_None_

### CSS Custom Properties

_None_
