import { html } from 'lit';
import readme from './README.md';
import './link-button.js';

export default {
  title: 'Elements/Link Button',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    icon: {
      options: [false, 'dsb-logo--outlined', 'arrow--right', 'trafficinfo'],
      control: { type: 'radio' },
    },
    variant: {
      options: ['primary', 'secondary', 'white', 'ghost'],
      control: { type: 'radio' },
    },
  },
};

export const LinkButton = (args) => html`
  <dsb-link-button
    .icon=${args.icon}
    variant=${args.variant}
    text=${args.text}
    url=${args.url}
    />
`;

LinkButton.args = {
  icon: false,
  text: 'Default text',
  url: '/site',
  variant: 'primary'
};
