import { LitElement, html } from 'lit';
import CSS from './link-button.scss';

export class DSBLinkButton extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      url: { type: String },
      text: { type: String },
      variant: { type: String },
    };
  }

  constructor() {
    super();
    this.icon = false,
    this.url = '';
    this.text = '',
    this.variant = 'primary';
  }

  render() {
    return html`<a href=${this.url} class="${this.variant}">
    ${this.icon ? html`<dsb-icon icon=${this.icon}></dsb-icon>` : ''}
      ${this.text}
      <span class="hover" />
    </a>`;
  }
}

customElements.define('dsb-link-button', DSBLinkButton);
