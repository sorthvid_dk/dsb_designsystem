# `<dsb-flatcard>`

A visible link.

## Example

```html
<!-- default -->
<dsb-flatcard href="#" label="Some text"></dsb-flatcard>
```

```html
<!-- with price and ticket-info -->
<dsb-flatcard
  href="#"
  from="Here"
  to="There"
  prefix="fra"
  price="123,-"
></dsb-flatcard>
```

## Api

### Properties/Attributes

| Name     | Type     | default | Description                                         |
| -------- | -------- | ------- | :-------------------------------------------------- |
| `href`   | `string` | `''`    | The `href` attribute for the `<a>`                  |
| `label`  | `string` | `''`    | Text placed to the left. Overwrites `from` and `to` |
| `from`   | `string` | `''`    | Text placed to the left, before the arrow           |
| `to`     | `string` | `''`    | Text placed to the left, after the arrow            |
| `prefix` | `string` | `''`    | Text placed to the right, before `price`            |
| `price`  | `string` | `''`    | If omitted, an arrow-icon is rendered.              |

### Slots

_None_

### CSS Custom Properties

_None_
