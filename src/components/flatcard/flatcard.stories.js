import readme from './README.md';
import './flatcard';
import { html } from 'lit';

export default {
  title: 'Elements/Flatcard',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Flatcard = (args) =>
  html`<dsb-flatcard href="#" label=${args.label}></dsb-flatcard>`;
export const WithPrice = () =>
  html`<dsb-flatcard
    href="#"
    from="Here"
    to="There"
    price="123,-"
    prefix="fra"
  ></dsb-flatcard>`;

Flatcard.args = {
  label: 'Køb billet nu',
};
