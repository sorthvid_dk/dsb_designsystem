import { LitElement, html } from 'lit';
import CSS from './flatcard.scss';
//import { classMap } from 'lit/directives/class-map.js';

export class Flatcard extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      url: { type: String },
      label: { type: String },
      from: { type: String },
      to: { type: String },
      prefix: { type: String },
      price: { type: String },
    };
  }

  constructor() {
    super();
    this.url = '';
    this.label = '';
    this.from = '';
    this.to = '';
    this.price = '';
    this.prefix = '';
  }

  render() {
    return html`
      <a href=${this.url}>
        <div>
          ${this.label
            ? this.label
            : html` <span>
                ${this.from}
                <dsb-icon icon="arrow--right"></dsb-icon>
                ${this.to}
              </span>`}
        </div>
        <div class="right">
          ${!this.price
            ? html`<dsb-icon icon="arrow--right"></dsb-icon>`
            : html`<span
                ><span class="mini">${this.prefix}</span>
                <span class="price">${this.price}</span></span
              >`}
        </div>
      </a>
    `;
  }
}

customElements.define('dsb-flatcard', Flatcard);
