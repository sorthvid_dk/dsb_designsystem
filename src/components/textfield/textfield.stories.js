import './textfield';
import readme from './README.md';
import { html } from 'lit';

export default {
  title: 'Elements/Textfield',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Textfield = () =>
  html`<dsb-textfield
    value="Some text"
    label="A textfield"
    placeholder="Type something"
  ></dsb-textfield>`;

export const SelectOptions = () =>
  html`<dsb-textfield
    value="Some text"
    label="A textfield"
    placeholder="Type something"
    style="padding: 2em; width: 300px" 
  >
    <option value="hej">Hej</option>
    <option value="hejs">Hej då</option>
    <option value="hejf">Jaha</option>
  </dsb-textfield>`;

export const Prefix = () =>
  html`<dsb-textfield
    label="A textfield"
    placeholder="Type something"
    prefix="Fra"
  ></dsb-textfield>`;

export const HiddenLabel = () =>
  html`<dsb-textfield
    label="A textfield"
    placeholder="Type something"
    prefix="Fra"
    .hiddenLabel=${true}
  ></dsb-textfield>`;
