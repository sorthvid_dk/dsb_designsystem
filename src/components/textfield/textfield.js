import { LitElement, html } from 'lit';
import CSS from './textfield.scss';
import { live } from 'lit/directives/live.js';
import { hasFocus } from '../../js/utils';
// import { repeat } from 'lit/directives/repeat';
// import { classMap } from 'lit/directives/class-map.js';

// TODO(erik): value isn't reflected in the DOM

export class Textfield extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      value: { attribute: true, reflect: true },
      label: { type: String },
      type: { type: String },
      prefix: { type: String },
      placeholder: { type: String },
      disabled: { type: Boolean, reflect: true },
      hiddenLabel: { type: Boolean },

      isOpen: { state: true },
      activeItem: { type: Number, state: true },
    };
  }

  constructor() {
    super();
    this.value = '';
    this.label = '';
    this.type = 'text';
    this.prefix = '';
    this.placeholder = '';
    this.disabled = false;
    this.required = false;
    this.hiddenLabel = false;

    this.isOpen = false;
    this.activeItem = -1;
  }

  connectedCallback() {
    super.connectedCallback();
    this.addEventListener('click', this.onFocus.bind(this));
    window.addEventListener('keydown', this.changeOption.bind(this));

    // connectedCallback() {
    //   const input = this.querySelector('input');
    //   const options = this.querySelector('[data-options]');

    //   [...options.children].forEach(child => child.setAttribute('tabindex', '-1'))

    //   // first one to connect creates a keydown event
    //   if (!getSubscription('keydown'))
    //     document.body.addEventListener('keydown', e => publish('keydown', e));

    //   subscribe('keydown', dropdownControl({ input, options }));
    // }
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('click', this.onFocus.bind(this));
    window.removeEventListener('keydown', this.changeOption.bind(this));
  }

  onFocus() {
    const input = this.renderRoot.querySelector('input');

    if (document.activeElement === input) {
      return;
    }

    if(input) input.focus();
  }

  toggleSelectOptions(shouldOpen) {
    this.isOpen = shouldOpen;
  }

  changeOption(event) {
    if (
      (event.key !== 'ArrowUp' && event.key !== 'ArrowDown') ||
      !hasFocus(this)
    ) {
      return;
    }

    this.activeItem += event.key === 'ArrowUp' ? -1 : 1;

    // const newActiveItem = this.renderRoot.querySelector('slot')
  }

  render() {
    return html`
      <span class="container">
        <label .hidden=${this.hiddenLabel || !this.label}>
          ${this.label}
        </label>

        <span class="input">
          ${this.prefix ? html`<span class="prefix">${this.prefix}</span>` : ''}

          <input
            aria-labelledby="label"
            type=${this.type}
            .value="${live(this.value)}"
            ?disabled="${this.disabled}"
            placeholder="${this.placeholder}"
            ?required="${this.required}"
            ?readonly="${this.readOnly}"
            @focus=${() => this.toggleSelectOptions(true)}
            @blur=${() => this.toggleSelectOptions(false)}
          />
        </span>

        <div class="select">
          <slot></slot>
        </div>
      </span>
    `;
  }
}

//  .hidden=${!this.isOpen}

// <div class="input-text__placeholder">
//   ${Text(mini({ text: placeholder }))}
// </div>

// ${maybe({
//   component: InputDropdown,
//   if: dropdown,
// })}

customElements.define('dsb-textfield', Textfield);
