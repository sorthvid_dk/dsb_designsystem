import { LitElement, html } from 'lit';
import CSS from './centered-grid.scss';

export class CenteredGrid extends LitElement {
  static get styles() {
    return [CSS];
  }

  render() {
    return html` <slot></slot> `;
  }
}

customElements.define('dsb-centered-grid', CenteredGrid);
