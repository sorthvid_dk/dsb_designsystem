# `<dsb-centered-grid>`

Layout component to create a simple grid. To be filled with f.ex. `<dsb-card>`

## Example

```html
<dsb-centered-grid>
  <p>1</p>
  <p>2</p>
  <p>3</p>
  <p>4</p>
  <p>5</p>
  <p>6</p>
  <p>7</p>
  <p>8</p>
  <p>9</p>
</dsb-centered-grid>
```

## Api

### Properties/Attributes

_None_

### Slots

| Name      | Description                   |
| --------- | :---------------------------- |
| _default_ | Elements to place in the grid |

### CSS Custom Properties

_None_
