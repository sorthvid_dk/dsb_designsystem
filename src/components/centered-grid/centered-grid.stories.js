import { html } from 'lit';
import './centered-grid';
import readme from './README.md';
import { random } from 'Util';

export default {
  title: 'Elements/Centered Grid',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const CenteredGrid = () => html`<dsb-centered-grid>
  <p>1</p>
  <p>2</p>
  <p>3</p>
  <p>4</p>
  <p>5</p>
  <p>6</p>
  <p>7</p>
  <p>8</p>
  <p>9</p>
</dsb-centered-grid>`;

export const WithCards = () => html`<dsb-centered-grid>
  <dsb-card
    label="${random('cta')}"
    headline="${random('cta')}"
    text="${random('sentence')}"
  ></dsb-card>
  <dsb-card
    label="${random('cta')}"
    headline="${random('cta')}"
    text="${random('sentence')}"
  ></dsb-card>
  <dsb-card
    label="${random('cta')}"
    headline="${random('cta')}"
    text="${random('sentence')}"
  ></dsb-card>
  <dsb-card
    label="${random('cta')}"
    headline="${random('cta')}"
    text="${random('sentence')}"
  ></dsb-card>
  <dsb-card
    label="${random('cta')}"
    headline="${random('cta')}"
    text="${random('sentence')}"
  ></dsb-card>
  <dsb-card
    label="${random('cta')}"
    headline="${random('cta')}"
    text="${random('sentence')}"
  ></dsb-card>
</dsb-centered-grid>`;
