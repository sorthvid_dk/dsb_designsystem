import { html } from 'lit';
import './badge';
import readme from './README.md';

export default {
  title: 'Elements/Badge',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    label: { control: 'text' },
    text: { control: 'text' },
    placement: {
      options: ['left', 'right'],
      control: { type: 'radio' },
    },
    color: {
      options: ['red', 'white'],
      control: { type: 'radio' },
    },
  },
};

export const Badge = (args) =>
  html`<dsb-badge
    label=${args.label}
    text=${args.text}
    placement=${args.placement}
    color=${args.color}
  ></dsb-badge>`;

Badge.args = {
  label: 'Label',
  text: 'Text',
  placement: 'left',
  color: 'red',
};
