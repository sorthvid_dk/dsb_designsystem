import { LitElement, html } from 'lit';
import CSS from './badge.scss';

// @customElement('dsb-button')
export class Badge extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      placement: { type: String },
      label: { type: String },
      text: { type: String },
      color: { type: String },
    };
  }

  constructor() {
    super();
    this.placement = 'left';
    this.label = '';
    this.text = '';
    this.color = '';
  }

  render() {
    return html`
      <div class="badge ${this.placement} ${this.color}">
        <div class="dot">
          <div class="inner">
            <div class="content">
              <span class="label">${this.label}</span>
              <span class="text">${this.text}</span>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-badge', Badge);
