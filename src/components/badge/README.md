# `<dsb-badge>`

Gives extra info about f.ex. a price.

## Example

```html
<dsb-badge
  label="Small text"
  text="Large text"
  placement="Left"
  color="red"
></dsb-badge>
```

## Api

### Properties/Attributes

| Name        | Type       | default | Description                                      |
| ----------- | ---------- | ------- | :----------------------------------------------- |
| `label`     | `string`   | `''`    | Small text in top row                            |
| `text`      | `string`   | `''`    | Larger text in bottom row                        |
| `placement` | `string`   | `''`    | **Not yet implemented**                          |
| `color`     | `string`\* | `''`    | Changes color of the badge, default badge is red |

\* available vaiants are: `white`

### Slots

_None_

### CSS Custom Properties

| Name                       | Default              | Description      |
| -------------------------- | -------------------- | ---------------- |
| `--badge-background-color` | `var(--color-red)`   | Background color |
| `--badge-color`            | `var(--color-white)` | Text color       |
