import readme from './README.md';
import { html } from 'lit';
import './header';
import './../icon/icon';
import './../icon-button/icon-button';

export default {
  title: 'Elements/Header',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => html`<dsb-header>
  <span slot="topbarLeft">Left</span>
  <span slot="topbarMiddle">Middle</span>
  <span slot="topbarRight">Right</span>
  <span slot="topbarRight">Right</span>
  <span slot="topbarRight">Right</span>
</dsb-header>`;

export const Basic = () => html`<dsb-header>
  <dsb-icon slot="topbarLeft" icon="dsb-logo--outlined"></dsb-icon>

  <!-- Content can be any html -->
  <span slot="topbarRight" class="flex flex-column align-end">
    <div class="ts-mini">Flemming Jensen</div>
    <div class="ts-mini ts-bold">1234 point</div>
  </span>

  <!-- Multiple elements in topbarRight will lay them out in a grid -->
  <dsb-icon slot="topbarRight" icon="burger"></dsb-icon>
</dsb-header>`;

export const WithAnInput = () => html`<dsb-header>
  <dsb-icon slot="topbarLeft" icon="dsb-logo--outlined"></dsb-icon>

  <dsb-textfield
    slot="topbarMiddle"
    prefix="Fra"
    label="Example"
    hiddenLabel
  ></dsb-textfield>

  <!-- Content can be any html -->
  <span slot="topbarRight" class="flex flex-column align-end">
    <div class="ts-mini">Flemming Jensen</div>
    <div class="ts-mini ts-bold">1234 point</div>
  </span>

  <!-- Multiple elements in topbarRight will lay them out in a grid -->
  <dsb-icon slot="topbarRight" icon="burger"></dsb-icon>
</dsb-header>`;

export const Header = () => html`<div style="height: 300vh">
  <dsb-header contentRoot=".page-content">
    <a slot="topbarLeft" href="#">
      <dsb-icon icon="dsb-logo--outlined"></dsb-icon>
    </a>

    <div slot="topbarMiddle">
      <dsb-header-form headline="Køb rejse" showHeadline>
        <dsb-textfield
          slot="one"
          label="From"
          hiddenLabel
          prefix="Fra"
          placeholder="Station, Stoppested, Vej, By, Lokalitet"
        ></dsb-textfield>
        <dsb-textfield
          slot="two"
          label="To"
          hiddenLabel
          prefix="Til"
          placeholder="Station, Stoppested, Vej, By, Lokalitet"
        ></dsb-textfield>
        <dsb-textfield
          slot="three"
          label="Udrejse"
          placeholder="12 / 3 2021"
        ></dsb-textfield>
        <dsb-textfield
          slot="four"
          label="Antal rejsende"
          placeholder="1 Vuxen"
        ></dsb-textfield>
        <dsb-textfield
          slot="five"
          label="Pladsbilleter"
          placeholder="0 Pladser"
        ></dsb-textfield>
        <dsb-icon-link
          slot="six"
          icon="world"
          label="Rejse til udlandet"
          mini
          inline
        ></dsb-icon-link>
        <dsb-button
          slot="seven"
          label="Søg rejse"
          variant="ghost"
          fullwidth="true"
        ></dsb-button>
      </dsb-header-form>
    </div>

    <!-- 
        DEV
       -->
    <dsb-flyout slot="topbarRight">
      <dsb-icon-button slot="openButton" icon="user"> </dsb-icon-button>
      <div>CONTENT</div>
      <div style="height: 50vh">CONTENT</div>
      <div>CONTENT</div>
      <div>CONTENT</div>
      <div>CONTENT</div>
    </dsb-flyout>

    <dsb-flyout slot="topbarRight">
      <dsb-icon-button slot="openButton" icon="magnifying-glass">
      </dsb-icon-button>
      <dsb-header-search placeholder="Hvad søger du?"></dsb-header-search>
    </dsb-flyout>

    <dsb-flyout slot="topbarRight">
      <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
      <dsb-header-menu>
        <dsb-underline-link slot="left" href="#">DSB Plus</dsb-underline-link>
        <dsb-underline-link slot="left" href="#"
          >Ansvar & Miljø</dsb-underline-link
        >
        <dsb-underline-link slot="left" href="#">Om DSB</dsb-underline-link>
        <dsb-underline-link href="#">Trafikinformation</dsb-underline-link>
        <dsb-underline-link href="#">Billetter og Service</dsb-underline-link>
        <dsb-underline-link href="#">Erhverv</dsb-underline-link>
        <dsb-underline-link href="#">Kundeservice & kontakt</dsb-underline-link>
      </dsb-header-menu>
    </dsb-flyout>
  </dsb-header>

  <style>
    .page-content {
      transition: var(--flyout-transition);
      padding-top: calc(var(--dsb-header-height) * 2);
    }
  </style>

  <div class="page-content">
    <h1>App content</h1>
  </div>
</div>`;
