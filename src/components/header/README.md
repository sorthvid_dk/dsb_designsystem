# `<dsb-header>`

The header is a fixed element in the top on each page.

It contains of multiple components, for example a form, menus and login.
Note that it doesn't make so much sense to use the component all by itself, without filling up its slots.

## Example

```html
<dsb-header>
  <span slot="topbarLeft">Left</span>
  <span slot="topbarMiddle">Middle</span>
  <span slot="topbarRight">Right</span>
  <span slot="topbarRight">Right</span>
  <span slot="topbarRight">Right</span>
</dsb-header>
```

```html
<dsb-header contentRoot=".app-content">
  <!-- ... -->

  <!-- Will move app-content on opening/closing -->
  <dsb-flyout>
    <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
    <div>Flyout content</div>
  </dsb-flyout>

  <!-- ... -->
</dsb-header>

<div class="app-content">App content</div>
```

## Api

### Properties/Attributes

| Name          | Type       | default | Description                                                    |
| ------------- | ---------- | ------- | :------------------------------------------------------------- |
| `contentRoot` | `string`\* | `''`    | When the flyouts are opened. This element will translate down. |

\* Has to be a CSS-selector: `.class-name`|`#id`

### Slots

| Name           | Description                              |
| -------------- | :--------------------------------------- |
| `topbarLeft`   | Place to put home link                   |
| `topbarModdle` | Place to put form                        |
| `topbarRight`  | Place to put icons containing menus etc. |

### CSS Custom Properties

_None_
