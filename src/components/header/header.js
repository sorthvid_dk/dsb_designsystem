import { LitElement, html } from 'lit';
import CSS from './header.scss';
import './../flyout/flyout';
import { FLYOUT_WAS_TOGGLED } from './../flyout/flyout';
import { HEADER_FORM_WAS_TOGGLED } from './../header-form/header-form';

export class Header extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      flyoutIsOpen: { state: true },
      contentRoot: { type: String },
      translateTargetElement: { state: true },
      // formIsOpen: { state: true },
    };
  }

  constructor() {
    super();
    this.flyoutIsOpen = false;
    this.contentRoot = '';
    this.translateTargetElement = null;
    // this.formIsOpen = false;
  }

  connectedCallback() {
    super.connectedCallback();

    window.addEventListener(
      FLYOUT_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );

    window.addEventListener(
      HEADER_FORM_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );
  }

  disconnectedCallback() {
    super.disconnectedCallback();

    window.removeEventListener(
      FLYOUT_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );

    window.removeEventListener(
      HEADER_FORM_WAS_TOGGLED,
      this.translateAppContent.bind(this)
    );
  }

  translateAppContent(event) {
    // this.formIsOpen = event.detail.formIsToggled && event.detail.isOpen;

    if (!this.contentRoot) return;

    this.flyoutIsOpen = event.detail.isOpen;
    this.translateValue = event.detail.contentSize.height;

    this.translateTargetElement = document.body.querySelector(this.contentRoot);
    const headerHeight = this.getBoundingClientRect().height;

    this.translateTargetElement.style.transform = this.flyoutIsOpen
      ? `translateY(${this.translateValue - headerHeight}px)`
      : '';
  }

  render() {
    return html`
      <slot class="left" name="topbarLeft"></slot>
      <div class="hide-on-mobile">
        <slot class="middle" name="topbarMiddle"></slot>
      </div>
      <slot class="right" name="topbarRight"></slot>
    `;
  }
}

customElements.define('dsb-header', Header);
