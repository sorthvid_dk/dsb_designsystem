import { LitElement, html } from 'lit';
import CSS from './content-block.scss';
import { stringToDocumentFragment } from '../../js/utils';

export class ContentBlock extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      trumpet: { type: String },
      headline: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.trumpet = ''
    this.headline = ''
    this.text = ''
  }

  getTextWithoutParagraph(text) {
    const includesParagraphTag = text && text.startsWith('<p>') && text.endsWith('</p>');
    if (includesParagraphTag) {
      return text = text.substring(3, text.lastIndexOf('</p>'));
    }
    return text;
  }

  render() {
    const textWithoutParagraph = this.getTextWithoutParagraph(this.text);

    return html`<div>
      ${this.trumpet ? html`<span class="trumpet">${this.trumpet}</span>` : ''}
      ${this.headline ? html`<h1>${this.headline}</h1>` : ''}
      ${this.text ? html`<p>${stringToDocumentFragment(textWithoutParagraph)}</p>` : ''}
      <slot></slot>
    </div>`;
  }
}

customElements.define('dsb-content-block', ContentBlock);
