import './content-block';
import readme from './README.md';
import { html } from 'lit';
import { random } from 'Util';

export default {
  title: 'Elements/Content Block',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

const trumpet = random('cta');
const headline = random('headline');
const htmlText = `<p>Når du er ung eller studerende, kan du få rabat på alle dine togrejser,
rabatkort og lign. <a href="#">Besparelsen afhænger af</a>, hvor ofte du
rejser med toget, og hvor din rejse går hen.</p>`;
const buttonLabel = random('cta');

export const ContentBlock = (args) =>
  html`<dsb-content-block
        trumpet=${args.trumpet}
        headline=${args.headline}
        text=${args.text}
       />`
ContentBlock.args = {
  trumpet: trumpet,
  headline: headline,
  text: htmlText
}

export const WithStyling = (args) =>
  html`<dsb-content-block style=${args.style}
        trumpet=${args.trumpet}
        headline=${args.headline}
        text=${args.text}
       />`
WithStyling.args = {
  style: "--link-color: var(--color-red); color: var(--color-red);",
  trumpet: trumpet,
  headline: headline,
  text: htmlText
}

export const WithButton = () =>
html`<dsb-content-block
      trumpet=${trumpet}
      headline=${headline}
      text=${htmlText}>
      <dsb-button label=${buttonLabel} />
     </dsb-content-block>`

export const WithContentInSlot = () =>
  html`<dsb-content-block>
    <span class="trumpet">${trumpet}</span>
    <h1>${headline}</h1>
    <p class="mb-2">
      Når du er ung eller studerende, kan du få rabat på alle dine togrejser,
      rabatkort og lign. <a href="#">Besparelsen afhænger af</a>, hvor ofte du
      rejser med toget, og hvor din rejse går hen.
    </p>
    <dsb-button label=${buttonLabel} />
  </dsb-content-block>`;