# `<dsb-content-block>`

Convenient way of placing some text content. Primarly used internally in other designsystem-elements.

## Example

```html
<!-- using attributes -->
<dsb-content-block
  trumpet="Some trumpet content"
  headline="Some headline"
  text="Some more content - can include html tags, for example <strong>some text</strong>"
>
  <dsb-button label="Some button"></dsb-button>
</dsb-content-block>
```

```html
<!-- using slots -->
<dsb-content-block>
  <span class="trumpet">Some trumpet content</span>
  <h1>Some content</h1>
  <p>Some more content</p>
  <dsb-button label="Some button"></dsb-button>
</dsb-content-block>
```

## Api

### Properties/Attributes

| Name       | Type     | default | Description                                                    |
| ---------- | -------- | ------- | :------------------------------------------------------------- |
| `trumpet`  | `string` | `''`    | Renders above the headline                                     |
| `headline` | `string` | `''`    | The main headline                                              |
| `text`     | `string` | `''`    | The paragraph text. Can include `<html>` tags. |

### Slots

| Name      | Description                                                                |
| --------- | :------------------------------------------------------------------------- |
| _default_ | Renders after the attributes. Can be used interchangebly with `attributes` |

### CSS Custom Properties

| Name                       | Default              | Description                                 |
| -------------------------- | -------------------- | ------------------------------------------- |
| `--link-color`             | `var(--color-red)`   | Color for anchor tags inside paragraph text |
