import { LitElement, html } from 'lit';
import CSS from './li.scss';
import ULOLCSS from './ulol.scss';

export class Li extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      variant: { type: String },
      number: { type: Number },
    };
  }

  constructor() {
    super();
    this.variant = '';
    this.number = 0;
  }

  connectedCallback() {
    super.connectedCallback();
    const items = [...this.parentElement.querySelectorAll('dsb-li')];
    this.number = items.findIndex((i) => i === this) + 1;
  }

  render() {
    return html`
      <li class=${this.variant}>
        <span>
          ${this.variant === 'check'
            ? html`<dsb-icon icon="check"></dsb-icon>`
            : ''}
          ${this.variant === 'number'
            ? html`<span class="index">${this.number}</span>`
            : ''}

          <slot></slot>
        </span>
      </li>
    `;
  }
}

customElements.define('dsb-li', Li);

/**
 * OL / UL
 */
export class Ol extends LitElement {
  static get styles() {
    return [ULOLCSS];
  }

  render() {
    return html`
      <ol>
        <slot></slot>
      </ol>
    `;
  }
}

customElements.define('dsb-ol', Ol);

export class Ul extends LitElement {
  static get styles() {
    return [ULOLCSS];
  }

  render() {
    return html`<ul>
      <slot></slot>
    </ul>`;
  }
}

customElements.define('dsb-ul', Ul);
