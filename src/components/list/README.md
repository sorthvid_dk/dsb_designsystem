# `<dsb-ul>` / `<dsb-ol>` / `<dsb-li>`

Text About the component

## Example

```html
<dsb-ul>
  <dsb-li variant="number">Item one</dsb-li>
  <dsb-li variant="number">Item two</dsb-li>
  <dsb-li variant="number">Item three</dsb-li>
  <dsb-li variant="number">Item four</dsb-li>
  <dsb-li variant="number">Item five</dsb-li>
</dsb-ul>
```

## Api

### Properties/Attributes

#### `<dsb-ul>` / `<dsb-ol>`

_None_

#### `<dsb-li>`

| Name      | Type       | default | Description                              |
| --------- | ---------- | ------- | :--------------------------------------- |
| `variant` | `string`\* | `''`    | Optional list-style bullet for the items |

\* available variants are: `bullet`|`check`|`number`

### Slots

#### `<dsb-ul>` / `<dsb-ol>`

| Name      | Description                     |
| --------- | ------------------------------- |
| _default_ | The place to put the `<dsb-li>` |

#### `<dsb-li>`

_None_

### CSS Custom Properties

_None_
