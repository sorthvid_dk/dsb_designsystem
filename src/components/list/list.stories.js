import { html } from 'lit';
import readme from './README.md';
import './list';

export default {
  title: 'Elements/List',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    variant: {
      options: ['bullet', 'check', 'number'],
      control: { type: 'radio' },
    },
  },
};

export const List = (args) =>
  html`<dsb-ul>
    <dsb-li variant=${args.variant}>Item one</dsb-li>
    <dsb-li variant=${args.variant}>Item two</dsb-li>
    <dsb-li variant=${args.variant}>Item three</dsb-li>
    <dsb-li variant=${args.variant}>Item four</dsb-li>
    <dsb-li variant=${args.variant}>Item five</dsb-li>
  </dsb-ul>`;

List.args = {
  variant: '',
};
