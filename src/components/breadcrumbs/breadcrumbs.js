import { LitElement, html } from 'lit';
import CSS from './breadcrumbs.scss';
import './breadcrumbs-item';

export class Breadcrumbs extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      items: { type: Array}
    };
  }

  constructor() {
    super();
    this.items = [];
  }

  render() {
    return this.items.length > 1 ?
      html`
        <ul>
          ${this.items.map(item => html`
            <dsb-breadcrumbs-item label=${item.text} href=${item.url} />
          `)}
        </ul>
      ` : '';
  }
}

customElements.define('dsb-breadcrumbs', Breadcrumbs);
