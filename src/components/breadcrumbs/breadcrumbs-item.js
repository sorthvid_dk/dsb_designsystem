import { LitElement, html } from 'lit';
import CSS from './breadcrumbs-item.scss';

export class BreadcrumbsItem extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      href: { type: String },
      label: { type: String },
    };
  }

  constructor() {
    super();
    this.href = '';
    this.label = '';
  }

  render() {
    return html`
      <li>
        ${this.href
          ? html`<a href=${this.href}>${this.label}</a><span>&gt;</span>`
          : html`<span class="dimmed">${this.label}</span>`}
      </li>
    `;
  }
}

customElements.define('dsb-breadcrumbs-item', BreadcrumbsItem);
