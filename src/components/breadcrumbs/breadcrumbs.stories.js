import { html } from 'lit';
import readme from './README.md';
import './breadcrumbs';
import './breadcrumbs-item';
import { items } from '~/breadcrumbs/data';

export default {
  title: 'Elements/Breadcrumbs',
  component: Breadcrumbs,
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    items: {
      options: items,
      control: 'object',
    },
  }
};

export const Breadcrumbs = (args) => html`
  <dsb-breadcrumbs .items=${args.items} />
`;

Breadcrumbs.args = {
  items: items,
};
