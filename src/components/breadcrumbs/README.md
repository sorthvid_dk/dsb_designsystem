# `<dsb-breadcrumbs>`

Holds breadcrumbs-items. `<dsb-breadcrumbs-item>`

## Note
`<dsb-breadcrumbs>` Will only be rendered if the items list added, contains more than 1 element.

## Example

```html
<!-- Usage -->
<dsb-breadcrumbs .items={listOfItemsToDisplay} />

<!-- Will render -->

<dsb-breadcrumbs>

  <dsb-breadcrumbs-item label="Forside" href="#"></dsb-breadcrumbs-item>
  <dsb-breadcrumbs-item label="Kundservice" href="#"></dsb-breadcrumbs-item>
  <dsb-breadcrumbs-item label="Spørgsmål" href="#"></dsb-breadcrumbs-item>
  <dsb-breadcrumbs-item label="Billetter & services"></dsb-breadcrumbs-item>

</dsb-breadcrumbs>

```

## Api

### Properties/Attributes

#### `<dsb-breadcrumbs>`

| Name     | Type    | default | Description                                    |
| -------- | ------- | ------- | :--------------------------------------------- |
| `.items` | `array` | `[]`    | Array contain links with both `text` and `url`. Last item should not have a url, then it will be dimmed |


### Slots

#### `<dsb-breadcrumbs>`

_None_

#### `<dsb-breadcrumbs-item>`

_None_

### CSS Custom Properties

_None_
