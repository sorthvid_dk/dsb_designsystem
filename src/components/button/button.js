import { LitElement, html } from 'lit';
import CSS from './button.scss';
//import { classMap } from 'lit/directives/class-map.js';

export class Button extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      icon: { type: String },
      label: { type: String },
      variant: { type: String },
      big: { type: Boolean },
      disabled: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.icon = '';
    this.label = '';
    this.variant = 'primary';
    this.big = false;
    this.disabled = false;
    this.fullwidth = false;
  }

  render() {
    return html`<button
      ?disabled="${this.disabled}"
      ?fullwidth="${this.fullwidth}"
      aria-label="${this.label || this.icon}"
      class="${this.variant} ${this.big ? 'big' : ''}"
    >
      ${this.icon ? html`<dsb-icon icon=${this.icon}></dsb-icon>` : ''}
      <span>${this.label}</span>
      <slot></slot>
      <span class="hover" />
    </button>`;
  }

  // createRenderRoot() {
  //   return this.attachShadow({ mode: 'open', delegatesFocus: true });
  // }
}

customElements.define('dsb-button', Button);
