import readme from './README.md';
import './button';
import { html } from 'lit';

export default {
  title: 'Elements/Button',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    label: { control: 'text' },
    icon: {
      options: [false, 'dsb-logo--outlined', 'arrow--right', 'trafficinfo'],
      control: { type: 'radio' },
    },
    variant: {
      options: ['primary', 'secondary', 'white', 'ghost'],
      control: { type: 'radio' },
    },
    big: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
  },
};

export const Button = (args) =>
  html`<dsb-button
    .icon=${args.icon}
    label=${args.label}
    variant=${args.variant}
    big=${args.big}
    ?disabled=${args.disabled}
    ?fullwidth=${args.fullwidth}
  ></dsb-button>`;

Button.args = {
  icon: false,
  label: 'Button',
  variant: 'primary',
  big: false,
  disabled: false,
  fullwidth: false,
};
