# `<dsb-button>`

Buttons allow users to take actions, and make choices, with a single tap.

## Installation

```bash
yarn install @dsb.dk/designsystem
```

```js
// my-project.js
import '@dsb.dk/designsystem';
```

## Example

```html
<dsb-button icon="cross" label="Click me" variant="primary"></dsb-button>
```

## Api

### Properties/Attributes

| Name        | Type       | default   | Description                                       |
| ----------- | ---------- | --------- | :------------------------------------------------ |
| `icon`      | `string`   | `''`      | An optional icon                                  |
| `label`     | `string`   | `''`      | The button text, will also render as `aria-label` |
| `variant`   | `string`\* | `primary` | The style of the button                           |
| `big`       | `boolean`  | `false`   | A bigger button, on large screens                 |
| `disabled`  | `boolean`  | `false`   | Will render the button disabled                   |
| `fullwidth` | `boolean`  | `false`   | Fullwidth button                                  |

\* available vaiants are: `primary`|`secondary`|`ghost`|`white`

### Slots

| Name      | Description                                                                                                 |
| --------- | :---------------------------------------------------------------------------------------------------------- |
| _default_ | Will render where the label is. It's **discouraged** to use the slot, as it doesn't render the `aria-label` |

### CSS Custom Properties

Inherits all CSS Custom Properties from Icon. As well as its own:

| Name                  | Default              | Description      |
| --------------------- | -------------------- | ---------------- |
| `--button-background` | `var(--color-red)`   | Background color |
| `--button-color`      | `var(--color-white)` | Text color       |
