# `<dsb-header-form>`

Form that is placed in the `<dsb-header>`

## Example

```html
<dsb-header-form>TODO</dsb-header-form>
```

## Api

### Properties/Attributes

| Name       | Type      | default | Description                              |
| ---------- | --------- | ------- | :--------------------------------------- |
| `headline` | `String`  | `''`    | Text above the form                      |
| `expanded` | `Boolean` | `false` | If the headline should be always visible |

### Slots

_**TODO**_

| Name      | Description |
| --------- | :---------- |
| `named`   | About       |
| _default_ | About       |

### CSS Custom Properties

_None_
