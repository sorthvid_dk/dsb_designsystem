import { html } from 'lit';
import readme from './README.md';
import './header-form';

export default {
  title: 'Elements/Header Form',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const HeaderForm = (args) => html`<dsb-header-form style="position: relative;" ?show-headline=${args.showHeadline} headline="Køb rejse">
  <dsb-textfield
    slot="one"
    label="From"
    hiddenLabel
    prefix="Fra"
    placeholder="Station, Stoppested, Vej, By, Lokalitet"
  ></dsb-textfield>
  <dsb-textfield
    slot="two"
    label="To"
    hiddenLabel
    prefix="Til"
    placeholder="Station, Stoppested, Vej, By, Lokalitet"
  ></dsb-textfield>
  <dsb-textfield
    slot="three"
    label="Udrejse"
    placeholder="12 / 3 2021"
  ></dsb-textfield>
  <dsb-textfield
    slot="four"
    label="Antal rejsende"
    placeholder="1 Vuxen"
  ></dsb-textfield>
  <dsb-textfield
    slot="five"
    label="Pladsbilleter"
    placeholder="0 Pladser"
  ></dsb-textfield>
  <dsb-icon-link
    slot="six"
    icon="world"
    label="Rejse til udlandet"
    mini
    inline
  ></dsb-icon-link>
  <dsb-button slot="seven" label="Søg rejse" fullwidth="true"></dsb-button>
</dsb-header-form>`;

HeaderForm.args = {
  showHeadline: true
}