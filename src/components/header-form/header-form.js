import { LitElement, html } from 'lit';
import CSS from './header-form.scss';
import './../textfield/textfield';
import { flip } from '../../js/flip';

const clamp = (min, val, max) => Math.max(min, Math.min(max, val));

const MOBILE_BREAKPOINT = 600;

export const HEADER_FORM_WAS_TOGGLED = 'header-form-was-toggled';

export class HeaderForm extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
      isOpen: { attribute: true, reflect: true },
      currentScroll: { type: Number, state: true },
      isMobile: { state: true },
      isOpening: { state: true },
      showHeadline: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.isOpen = false;
    this.isMobile = null;
    this.lastTopMargin = 0;
    this.headlineHeight = 0;
    this.firstFormFieldHeight = 0;
    this.inputElements = null;
    this.isOpening = false;
    this.showHeadline = false;
    this.backdrop = null;
  }

  connectedCallback() {
    super.connectedCallback();

    setTimeout(() => {
      this.setMargin();
      this.resizeHandler();

      this.inputElements = [...this.querySelectorAll('dsb-textfield')];
      this.inputElements.forEach((input) => {
        input.addEventListener('focus', this.setOpen.bind(this));
        // input.addEventListener('blur', this.setOpen.bind(this));
      });

      this.backdrop = this.shadowRoot.querySelector('.backdrop');
      this.backdrop &&
        this.backdrop.addEventListener('click', this.setClose.bind(this));
    }, 10);

    window.addEventListener('keydown', this.setClose.bind(this));
    window.addEventListener('wheel', this.setMargin.bind(this));
    window.addEventListener('resize', this.resizeHandler.bind(this));
  }

  disconnectedCallback() {
    super.disconnectedCallback();

    window.removeEventListener('keydown', this.setClose.bind(this));
    window.removeEventListener('wheel', this.setMargin.bind(this));
    window.removeEventListener('resize', this.resizeHandler.bind(this));
    this.inputElements.forEach((input) => {
      input.removeEventListener('focus', this.setOpen.bind(this));
      // input.removeEventListener('blur', this.setOpen.bind(this));
    });
    this.backdrop.removeEventListener('click', this.setClose.bind(this));
  }

  setClose(event) {
    if (!this.isOpen || (event.key && event.key !== 'Escape')) return;

    this.setOpen(null, false);
    this.inputElements.forEach((input) => input.blur());
  }

  setOpen(event, overwrite) {
    this.isOpening = true;
    this.classList.toggle('is-opening');

    const state = flip.getState(
      this.shadowRoot.querySelector('.curtain-content')
    );

    this.isOpen = overwrite !== undefined ? overwrite : true;

    flip.expand(state, () => {
      this.classList.toggle('is-opening');
      this.isOpening = false;
    });

    this.setMargin();

    setTimeout(() => {
      this.dispatchEvent(
        new CustomEvent(HEADER_FORM_WAS_TOGGLED, {
          bubbles: true,
          detail: {
            message: `<dsb-header-form> was toggled`,
            isOpen: this.isOpen,
            contentSize: this.getBoundingClientRect(),
            formIsToggled: true,
          },
        })
      );
    }, 1);
  }

  getHeights() {
    const headline = this.shadowRoot.querySelector('.headline');
    this.headlineHeight = headline
      ? headline.getBoundingClientRect().height
      : 0;
    this.height = this.getBoundingClientRect().height;
  }

  /**
   * setMargin()
   * makes the form slide up and down on scroll.
   * behaves differently on mobile/desktop
   */
  setMargin({ deltaY = 0 } = {}) {
    const oldMarginValue = parseInt(this.style.marginTop) || 0;
    const newMarginValue = oldMarginValue - deltaY;

    
    if (this.isOpen) {
      this.style.marginTop = 0 + 'px';
      return;
    }
    
    if (!this.showHeadline && !this.isMobile) {
      console.log(this.isMobile, this.headlineHeight)
      this.style.marginTop = -this.headlineHeight + 'px';
      return;
    }

    if (this.isMobile) {
      const minValue = this.getBoundingClientRect().height * -1;

      console.log(minValue);

      this.style.marginTop =
        window.pageYOffset > this.headlineHeight || !this.showHeadline
          ? clamp(minValue, newMarginValue, -this.headlineHeight) + 'px'
          : clamp(minValue, newMarginValue, 0) + 'px';

      return;
    }

    /**
     * is desktop
     */
    this.style.marginTop =
      window.pageYOffset > this.headlineHeight
        ? -this.headlineHeight + 'px'
        : clamp(-this.headlineHeight, newMarginValue, 0) + 'px';
  }

  resizeHandler() {
    this.isMobile = window.innerWidth < MOBILE_BREAKPOINT;
    this.getHeights();
    const curtain = this.shadowRoot.querySelector('.curtain-content');
    curtain.removeAttribute('style');
  }

  render() {
    return html`
      ${this.headline ? html`<h1 class="headline">${this.headline}</h1>` : ''}

      <slot name="one"></slot>

      ${!this.isMobile ? html`<slot name="two"></slot>` : ''}

      <div class="curtain-content" ?hidden=${!this.isOpen}>
        ${this.isMobile ? html`<slot name="two"></slot>` : ''}
        <slot name="three"></slot>
        <slot name="four"></slot>
        <slot name="five"></slot>
        <slot name="six"></slot>
        <slot name="seven"></slot>
        <!-- <slot></slot> -->
        <hr />
      </div>

      <div class="backdrop"></div>
    `;
  }
}

customElements.define('dsb-header-form', HeaderForm);
