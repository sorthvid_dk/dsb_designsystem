# `<dsb-cta>`

Gives info or the ability for a user to make an action.

## Example

```html
<!-- with info text -->
<dsb-cta label="You can buy this" sublabel="Somewhere" prefix="Fra" info="80,-">
</dsb-cta>
```

```html
<!-- with button -->
<dsb-cta prefix="Fra" info="149,-">
  <dsb-button label="Køb nu" icon="trafficinfo"></dsb-button>
</dsb-cta>
```

## Api

### Properties/Attributes

| Name       | Type     | default | Description          |
| ---------- | -------- | ------- | :------------------- |
| `label`    | `string` | `''`    | Bold text            |
| `sublabel` | `string` | `''`    | Text under bold text |
| `prefix`   | `string` | `''`    | Text before `info`   |
| `info`     | `string` | `''`    | Usually a price      |

### Slots

| Name      | Description                                              |
| --------- | :------------------------------------------------------- |
| _default_ | Renders where `label` and `sublabel` are. Use either or. |

### CSS Custom Properties

_None_
