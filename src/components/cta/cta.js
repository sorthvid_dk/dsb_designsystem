import { LitElement, html } from 'lit';
import CSS from './cta.scss';
import './../button/button';

export class Cta extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      label: { type: String },
      sublabel: { type: String },
      prefix: { type: String },
      info: { type: String },
    };
  }

  constructor() {
    super();
    this.label = '';
    this.sublabel = '';
    this.prefix = '';
    this.info = '';
  }

  render() {
    return html`
      <div class="cta">
        <div class="main">
          <slot></slot>
          ${this.label
            ? html`
                <div class="label">
                  <p class="action">${this.label}</p>
                  <p class="mini">${this.sublabel}</p>
                </div>
              `
            : ''}
        </div>
        ${this.info
          ? html`
              <div class="info-container">
                <p class="mini">${this.prefix}</p>
                <p class="info">${this.info}</p>
              </div>
            `
          : ''}
      </div>
    `;
  }
}

customElements.define('dsb-cta', Cta);
