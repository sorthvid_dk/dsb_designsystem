// import { CTA } from './cta.html';
import readme from './README.md';
import { html } from 'lit';
import './cta';
import './../button/button';

export default {
  title: 'Elements/Cta',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Cta = (args) =>
  html`<dsb-cta
    .label=${args.label}
    .sublabel=${args.sublabel}
    .prefix=${args.prefix}
    .info=${args.info}
  >
  </dsb-cta>`;

export const WithButton = (args) =>
  html`<dsb-cta prefix=${args.prefix} info=${args.info}>
    <dsb-button label="Køb nu" icon="trafficinfo"></dsb-button>
  </dsb-cta>`;

Cta.args = WithButton.args = {
  label: 'Køb på stationen',
  sublabel: ' Salg & Service / 7-Eleven',
  prefix: 'Fra',
  info: '149,-',
};
