import { html } from 'lit';
import readme from './README.md';
import { random } from 'Util';
import './../badge/badge';
import './image-block';

export default {
  title: 'Elements/Image Block',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    gradient: {
      control: { type: 'radio' },
      options: ['left', 'right', 'top', 'bottom'],
    },
  },
};

const image = random('jpg');

export const ImageBlock = () =>
  html`<dsb-image-block src=${image} alt="An example"></dsb-image-block>`;
export const WithBadge = () =>
  html`<dsb-image-block src=${image} alt="An example">
    <dsb-badge text="Left"></dsb-badge>
    <dsb-badge slot="right" text="Right"></dsb-badge>
  </dsb-image-block>`;
export const AspectRatio = () =>
  html`<dsb-image-block
    aspect="16/9"
    src=${image}
    alt="An example"
  ></dsb-image-block>`;
export const CSSBackground = () =>
  html`<dsb-image-block bg=${image}></dsb-image-block>`;
export const Gradient = (args) =>
  html`<dsb-image-block
    src=${image}
    alt="An example"
    gradient=${args.gradient}
  ></dsb-image-block>`;

Gradient.args = {
  gradient: 'left',
};
