# `<dsb-image-block>`

Shows either an `<img>` or a `<div>` with a background image. Can hold a Badge in one of the top corners.
If the `aspect` attribute is added, the image will have a fixed aspect ratio.

## Installation

```bash
yarn install @dsb.dk/designsystem
```

```js
// my-project.js
import '@dsb.dk/designsystem';
```

## Example

```html
<dsb-image-block
  aspect="16/9"
  alt="An example"
  src="path/to/image.jpg"
></dsb-image-block>
```

## Api

### Properties/Attributes

| Name       | Type         | Description                                                             |
| ---------- | ------------ | :---------------------------------------------------------------------- |
| `aspect`   | `string`\*   | Will crop the image in given format.                                    |
| `src`      | `string`     | `src` attribute for the `<img>`                                         |
| `alt`      | `string`     | `alt` attribute for the `<img>`                                         |
| `bg`       | `string`     | Instead of `src`. Will render given url as a CSS Background             |
| `gradient` | `string`\*\* | Adds a transparent gradient. Good for ensuring overlaying text's contrast |

\* Values should be seperated with a slash: `x/y` `4/3` `16/9`\
\*\* Available value are `left`|`right`|`top`|`bottom`

### Slots

Should only be used with `<dsb-badge>`

| Name      | Description                              |
| --------- | :--------------------------------------- |
| _default_ | Content to show in the top left corner.  |
| `right`   | Content to show in the top right corner. |

### CSS Custom Properties

| Name           | Default | Description                                  |
| -------------- | ------- | -------------------------------------------- |
| `--object-fit` | `cover` | Value for CSS `object-fit`/`background-size` |
