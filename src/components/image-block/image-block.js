import { LitElement, html } from 'lit';
import { classMap } from 'lit/directives/class-map.js';
import CSS from './image-block.scss';
// import { ifDefined } from 'lit/directives/if-defined.js';

export class ImageBlock extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      bg: { type: String },
      src: { type: String },
      alt: { type: String },
      aspect: { type: String },
      gradient: { type: String },
    };
  }

  constructor() {
    super();
    this.bg = '';
    this.src = '';
    this.alt = '';
    this.aspect = '';
    this.gradient = '';
  }

  getAspect() {
    return this.aspect
      .split('/')
      .map((a, i) => `--aspect-${i}: ${a}`)
      .join('; ') + ';';
  }

  // ?data-passepartout="${attributes.includes('passepartout')}"

  render() {
    return html`
      <div class="container">
        <slot></slot>
        <slot name="right"></slot>

        ${this.gradient
          ? html` <div class="gradient ${this.gradient}"></div> `
          : ''}

        <div
          class="${classMap({ box: true, aspect: this.aspect || this.bg })}"
          style=${this.aspect ? this.getAspect() : undefined}
        >
          ${this.bg
            ? html`<div
                class="image"
                style="background-image: url(${this.bg});"
              ></div>`
            : html`<img src=${this.src} alt=${this.alt} />`}
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-image-block', ImageBlock);
