# `<dsb-XXX>`

Text About the component

## Example

```html
<dsb-XXX></dsb-XXX>
```

## Api

### Properties/Attributes

| Name   | Type     | default | Description |
| ------ | -------- | ------- | :---------- |
| `attr` | `string` | `''`    | About       |

\* available vaiants are: `one`|`two`

### Slots

| Name      | Description |
| --------- | :---------- |
| `named`   | About       |
| _default_ | About       |

### CSS Custom Properties

| Name     | Default | Description |
| -------- | ------- | ----------- |
| `--prop` | `none`  | About       |
