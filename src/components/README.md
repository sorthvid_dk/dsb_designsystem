# Structure
- [Plop](https://plopjs.com/)

The components all have the same hierarchy, which means they're all located in the root of the components folder.\
All the files that makes up a certain component are places next to each other.\

An imported components markup is named in PascalCase, and the state is in camelCase.\
```js
import Component from './component.html'
import component from './component.state'

// Markup will render using the corresponding state
Component(component())
```

### How does it work?
- Markup is writtenand and exported as a lit function.
- Storybook imports the markup and feeds it a state.
- The styling is imported and applied by the global `src/index.scss`.
- Javascript is exported and handled by webpack.
- The result is outputted in the Storybook UI.


## HTML
- [lit](https://lit.polymer-project.org/)

The markup is written as a javascript function that is being consumed by [lit](https://lit.polymer-project.org/).
This makes it able to insert variables and data into the template layer, that is being translated at runtime.

Every component that uses another component should import the child, to keep the markup DRY.

## SCSS
- [scss](https://sass-lang.com/)
- [BEM](http://getbem.com/)

Be sure to import/use/forward the component's scss file in the main `src/index.scss`.
The project aims to keep a BEM structure to facilitate namespacing and code structure.

By the time this is written (april 2020) the only sass compiler that handles the latest sass-syntax is dart-sass.\
Be sure to `@use` and `@forward` your files.

## State

The state is all the dynamic data and content that a component uses.

The state is being fed into the markup when the component becomes a Storybook story

## JS
- [Babel](https://babeljs.io/)

There is no specific javascript framework or templating tool in use.

One of the primary goals of the whole project is to make as generic code as possible. That should be able to be used in most frameworks and environments. This puts some pressure on how the javascript should be written. There are many frameworks around and most of them have their own specific way of handling their code, especially the javascript.

Be sure to write all javascript to be as non-specific as it can be.

All javascript should be exported from the `src/index.js`, so that it's being build into the distribution package.

## Stories
- [Storybook](https://storybook.js.org/)

Storybook is used to show the result and outcome of each component. It compiles all code at runtime, and uses [webpack](https://webpack.js.org/)'s HMR to watch for changes in the code while developing. This together with the Storybook addons makes it a fast and effective setup for developing components.


# General
## Color
The color classes should be used in order to keep control over when the text/links should be light and dark. Even though a component has a background image, it's good practice to put a .bg--class in order for the links and icons to render with enough contrast.

# What, when and how?

The project is designed to separate and contain different aspects and functionality into each own folder. Although there might be minor exeptions. It can be seen as a trickle down design. Files in the `/js`-folder are used by the files in the `/custom-elements`-folder, that is in turn used by the accual components, that are in the `/components`-folder.

## The 'How?'

If the files in the /js-folder knows 'what' to do with an element, and the files in the /custom-elements-folder knows 'when' to do something. These files knows 'how' to use them. How to lay them out, how to combine them together and how to style them.
