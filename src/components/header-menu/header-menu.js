import { LitElement, html } from 'lit';
import CSS from './header-menu.scss';

export class HeaderMenu extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <div class="left">
        <slot name="left"></slot>
      </div>
      <div class="right">
        <slot></slot>
      </div>
    `;
  }
}

customElements.define('dsb-header-menu', HeaderMenu);
