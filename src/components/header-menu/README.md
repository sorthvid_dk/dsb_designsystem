# `<dsb-header-menu>`

Lays out a number of `<slot>`:ed `<dsb-underline-link>` in two columns.

## Example

```html
<dsb-header-menu>
  <dsb-underline-link slot="left" href="#">
    Small link to the left
  </dsb-underline-link>

  <dsb-underline-link href="#">
    Big link to the right
  </dsb-underline-link>

</dsb-header-menu>
```

## Api

### Properties/Attributes

_None_

### Slots

| Name         | Description                   |
| ------------ | :---------------------------- |
| `left`\*     | Smaller links to the the left |
| _default_ \* | Bigger links to the right     |

_only fill the slot with `<dsb-underline-link>`_, see example.

### CSS Custom Properties

_None_
