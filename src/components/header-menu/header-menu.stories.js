import { html } from 'lit';
import './header-menu';
import readme from './README.md';

export default {
  title: 'Elements/Header Menu',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const HeaderMenu = () => html`<dsb-header-menu>
  <dsb-underline-link slot="left" href="#">DSB Plus</dsb-underline-link>
  <dsb-underline-link slot="left" href="#">Ansvar & Miljø</dsb-underline-link>
  <dsb-underline-link slot="left" href="#">Om DSB</dsb-underline-link>
  <dsb-underline-link href="#">Trafikinformation</dsb-underline-link>
  <dsb-underline-link href="#">Billetter og Service</dsb-underline-link>
  <dsb-underline-link href="#">Erhverv</dsb-underline-link>
  <dsb-underline-link href="#">Kundeservice & kontakt</dsb-underline-link>
</dsb-header-menu>`;
