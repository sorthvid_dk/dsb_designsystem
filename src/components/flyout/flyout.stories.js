import readme from './README.md';
import { html } from 'lit';
import '../icon-button/icon-button';

export default {
  title: 'Elements/Flyout',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Flyout = () => html`<dsb-flyout>
  <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
  <div>Flyout content</div>
</dsb-flyout>`;