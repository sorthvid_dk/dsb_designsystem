`<dsb-flyout>`

A flyout consists of an always visible opening-button and the content that "flys in".

## Example

```html
<dsb-flyout>
  <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
  <div>Flyout content</div>
</dsb-flyout>
```

## Api

### Properties/Attributes

_None_

### Slots

| Name         | Description                                                |
| ------------ | :--------------------------------------------------------- |
| `openButton` | The element that will be on the page, toggling the content |
| _default_    | Content that will fly in                                   |

### Events

| Name             | Description                                |
| ---------------- | :----------------------------------------- |
| `flyout-toggled` | The content within the flyout was changed. |

### CSS Custom Properties

_None_
