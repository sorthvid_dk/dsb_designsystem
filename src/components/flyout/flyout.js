import { LitElement, html } from 'lit';
import CSS from './flyout.scss';
import randomId from './../../js/id';
import { flip } from '../../js/flip';

export const FLYOUT_WAS_TOGGLED = 'flyout-toggled';
export const FLYOUT_DOM_TREE_CHANGED = 'flyout-changed';

export class Flyout extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      isOpen: { state: true },
      id: { state: true },
    };
  }

  constructor() {
    super();
    this.isOpen = false;
    this.id = randomId();
    this.activeElementBeforeOpening = null;
    this.observer = null;
    this.contentSize = null;
  }

  connectedCallback() {
    super.connectedCallback();

    // TODO: can't find element directly? why?
    setTimeout(() => {
      this.content = this.renderRoot.querySelector('aside');
      this.initMutationObserver();
      this.contentSize = flip.getState(this.content);
      window.addEventListener('resize', this.animateSize.bind(this));
    }, 10);

    window.addEventListener('keydown', this.closeOnEsc.bind(this));
    window.addEventListener('click', this.closeOnBodyClick.bind(this));
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    window.removeEventListener('keydown', this.closeOnEsc.bind(this));
    window.removeEventListener('resize', this.animateSize.bind(this));
    window.removeEventListener('click', this.closeOnBodyClick.bind(this));
    this.observer && this.observer.disconnect();
  }

  closeOnEsc(event) {
    if (!this.isOpen) return;
    if (event.key === 'Escape') this.toggle(false);
  }

  closeOnBodyClick(event) {
    if (!this.isOpen) return;

    if (event.clientY > this.content.getBoundingClientRect().height) {
      this.toggle(false);
    }
  }

  toggle(shouldOpen) {
    this.isOpen = shouldOpen !== undefined ? shouldOpen : !this.isOpen;

    if (this.isOpen && document.activeElement) {
      this.activeElementBeforeOpening = document.activeElement;
    }

    if (!this.isOpen && this.activeElementBeforeOpening) {
      this.activeElementBeforeOpening.focus();
    }

    this.fireCustomEvent();
  }

  animateSize() {
    flip.expand(this.contentSize);

    this.contentSize = flip.getState(this.content);
  }

  initMutationObserver() {
    const config = { attributes: true, childList: true, subtree: true };

    const callback = (mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList') {
          this.fireCustomEvent();
          this.animateSize();
        }
      }
    };

    this.observer = new MutationObserver(callback);
    this.observer.observe(this, config);
  }

  fireCustomEvent() {
    this.dispatchEvent(
      new CustomEvent(FLYOUT_WAS_TOGGLED, {
        bubbles: true,
        detail: {
          message: `<dsb-flyout> with id ${this.id} was toggled`,
          isOpen: this.isOpen,
          contentSize: this.content.getBoundingClientRect(),
        },
      })
    );
  }

  render() {
    return html`
      <slot
        name="openButton"
        @click=${() => this.toggle()}
        aria-expanded=${this.isOpen}
        aria-controls=${this.id}
      ></slot>

      <aside
        class="content ${this.isOpen ? 'is-open' : ''}"
        .hidden=${!this.isOpen}
        aria-hidden=${!this.isOpen}
        id=${this.id}
        role="dialog"
      >
        <div class="close-button">
          <dsb-icon-button
            icon="cross"
            @click=${() => this.toggle(false)}
            aria-expanded=${this.isOpen}
            aria-controls=${this.id}
          ></dsb-icon-button>
        </div>

        <slot></slot>
      </aside>

      <div class="backdrop"></div>
    `;
  }
}

customElements.define('dsb-flyout', Flyout);
