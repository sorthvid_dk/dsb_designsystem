// import { Media } from './media.html';
import './media';
import readme from './README.md';
import { html } from 'lit';
import { random } from 'Util';

export default {
  title: 'Elements/Media',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Media = () =>
  html`<dsb-media
    headline=${random('paragraph')}
    src=${random('video')}
  ></dsb-media>`;
