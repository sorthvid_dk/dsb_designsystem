# `<dsb-media>`

For showing a video or image in full page width.

## Example

```html
<dsb-media
  headline="Some headline"
  src="path/to/my/video.mp4"
  type="video/mp4"
></dsb-media>
```

## Api

### Properties/Attributes

| Name       | Type     | default | Description                       |
| ---------- | -------- | ------- | :-------------------------------- |
| `headline` | `string` | `''`    | Centered headline above the media |
| `src`      | `string` | `''`    | `src` for the `<video>`           |
| `type`     | `string` | `''`    | `type` for the `<video>`          |

### Slots

_None_

### CSS Custom Properties

_None_
