import { LitElement, html } from 'lit';
import CSS from './media.scss';

export class Media extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
      src: { type: String },
      type: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.src = '';
    this.type = '';
  }

  render() {
    return html` <section>
      ${this.headline ? html`<h1>${this.headline}</h1>` : ''}
      <dsb-video toggle src=${this.src} type=${this.type}></dsb-video>
    </section>`;
  }
}

customElements.define('dsb-media', Media);
