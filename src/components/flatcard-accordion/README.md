# `<dsb-flatcard-accordion>`

## Example

```html
<dsb-flatcard-accordion label="Are trains the best?">
  <p class="ts-component">Some content that is hidden by default</p>
</dsb-flatcard-accordion>
```

## Api

### Properties/Attributes

| Name    | Type     | default | Description                 |
| ------- | -------- | ------- | :-------------------------- |
| `label` | `string` | `''`    | The always visible headline |

### Slots

| Name      | Description                             |
| --------- | :-------------------------------------- |
| _default_ | Content to show when clicking the label |

### CSS Custom Properties

_None_
