import { LitElement, html } from 'lit';
import CSS from './flatcard-accordion.scss';
import randomId from '../../js/id';
import { flip } from './../../js/flip';

export class FlatcardAccordion extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      label: { type: String },
      isOpen: { type: Boolean, reflect: true },
      id: { type: String },
    };
  }

  constructor() {
    super();
    this.label = '';
    this.isOpen = false;
    this.id = randomId();
  }

  connectedCallback() {
    super.connectedCallback();
    // TODO: can't find element directly
    setTimeout(() => {
      this.button = this.renderRoot.querySelector('button');
      this.content = this.renderRoot.querySelector('.content');
      this.button.addEventListener('click', this.toggle.bind(this));
    }, 10);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.button.removeEventListener('click', this.toggle.bind(this));
  }

  toggle() {
    const state = flip.getState(this.content);
    console.log(state)
    this.isOpen = !this.isOpen;
    flip.expand(state);
  }

  render() {
    return html`
      <div class="details">
        <button aria-expanded=${this.isOpen} aria-controls=${this.id}>
          <div>${this.label}</div>
          <dsb-icon class="plus" icon="plus"></dsb-icon>
        </button>
        <div
          class="content"
          ?hidden=${!this.isOpen}
          aria-hidden=${!this.isOpen}
          id=${this.id}
        >
          <slot></slot>
        </div>
      </div>
    `;
  }
}

customElements.define('dsb-flatcard-accordion', FlatcardAccordion);
