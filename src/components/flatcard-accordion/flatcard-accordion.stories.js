// import { FlatCardAccordion } from './flat-card-accordion.html';
import readme from './README.md';
import './flatcard-accordion';
import { html } from 'lit';

export default {
  title: 'Elements/Flatcard Accordion',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const FlatcardAccordion = () =>
  html`<dsb-flatcard-accordion label="Kan jeg tilkøbe tillæg til  1' med min Orange Fri-billet?">
    <p class="ts-component">
      Consequat elit exercitation pariatur aliqua elit officia exercitation
      dolor nulla minim tempor sit. Elit tempor veniam magna ipsum et nostrud
      veniam aliqua in consectetur duis magna Lorem ex. Voluptate est incididunt
      occaecat esse non anim quis elit irure sint sit exercitation.
    </p>
  </dsb-flatcard-accordion>`;
