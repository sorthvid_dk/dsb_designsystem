# `<dsb-video>`

Combination of a `<video>` and a UI

## Example

```html
<dsb-video src="path/to/my/video.mp4" type="video/mp4"></dsb-video>
```

```html
<dsb-video toggle src="path/to/my/video.mp4" type="video/mp4"></dsb-video>
```

## Api

### Properties/Attributes

| Name     | Type      | default | Description                                                         |
| -------- | --------- | ------- | :------------------------------------------------------------------ |
| `toggle` | `boolean` | `false` | Makes the UI a single button, that changes icon based on play state |
| `src`    | `string`  | `''`    | `src` for the `<video>`                                             |
| `type`   | `string`  | `''`    | `type` for the `<video>`                                            |

### Slots

_None_

### CSS Custom Properties

_None_
