import { LitElement, html } from 'lit';
import '../icon/icon';
import CSS from './video.scss';

// @customElement('dsb-button')
export class Video extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      src: { type: String },
      type: { type: String },
      toggle: { type: Boolean, reflect: true },
      playing: { type: Boolean, state: true },
    };
  }

  constructor() {
    super();
    this.src = '';
    this.type = '';
    this.toggle = false;
    this.playing = false;
  }

  togglePlay(shouldPlay) {
    if (shouldPlay === undefined) {
      this.playing = !this.playing;
      if (this.playing) {
        this.renderRoot.querySelector('video').play();
      } else {
        this.renderRoot.querySelector('video').pause();
      }

      return;
    }

    if (shouldPlay) {
      this.playing = true;
      this.renderRoot.querySelector('video').play();
    } else {
      this.playing = false;
      this.renderRoot.querySelector('video').pause();
    }
  }

  render() {
    return html`
      <video>
        <source src="${this.src}" type=${this.type} />

        Sorry, your browser doesn't support embedded videos.
      </video>

      <div class="ui">
        ${this.toggle
          ? html`<dsb-icon-button
              icon=${this.playing ? 'pause' : 'play'}
              round
              @click=${() => this.togglePlay()}
            ></dsb-icon-button>`
          : html`
              <dsb-icon-button
                icon="play"
                round
                @click=${() => this.togglePlay(true)}
              ></dsb-icon-button>
              <dsb-icon-button
                icon="pause"
                round
                @click=${() => this.togglePlay(false)}
              ></dsb-icon-button>
            `}
      </div>
    `;
  }
}

customElements.define('dsb-video', Video);
