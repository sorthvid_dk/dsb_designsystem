import { html } from 'lit';
import './video';
import readme from './README.md';
import { random } from 'Util';

export default {
  title: 'Elements/Video',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Video = () =>
  html`<dsb-video src=${random('video')} type="video/mp4"></dsb-video>`;
export const WithToggleButton = () =>
  html`<dsb-video toggle src=${random('video')} type="video/mp4"></dsb-video>`;
