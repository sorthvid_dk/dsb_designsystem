# `<dsb-banner>`

A fullwidth image banner with overlaying content.

Can be used both with slotted content, and with attributes. Depending on usecase. **Note** that when content comes through slots, the `<h*>` and `<p>` has to be a first level children. See: [Lit Docs ::slotted](https://lit.dev/docs/components/styles/#slotted). Slots/attributes can be used interchangeably, the headline may be an attribute together with a slotted paragraph f.ex.

## Installation

```bash
yarn install @dsb.dk/designsystem
```

```js
// my-project.js
import '@dsb.dk/designsystem';
```

## Example

```html
<dsb-banner
  src="path/to/image.jpg"
  alt="My image alt text"
  headline="My headline"
  text="My paragraph text"
>
</dsb-banner>
```

Using slots:

```html
<dsb-banner src="path/to/image.jpg" alt="My image alt text">
  <dsb-badge slot="badge" label="badge" text="badge"></dsb-badge>
  <h1>My headline</h1>
  <p>HTML content, that may include nested <a href="#">links</a> etc.</p>
  <dsb-button label="Slotted button"></dsb-button>
</dsb-banner>
```

## Api

### Properties/Attributes

| Name       | Type     | Description                     |
| ---------- | -------- | :------------------------------ |
| `headline` | `string` | The component's headline        |
| `text`     | `string` | The component's paragraph text  |
| `src`      | `string` | `src` attribute for the `<img>` |
| `alt`      | `string` | `alt` attribute for the `<img>` |

### Slots

| Name      | Description                                                  |
| --------- | :----------------------------------------------------------- |
| _default_ | Renders below the headline and paragraph, if they're defined |
| `badge`   | An optional badge in the top right corner                    |

### CSS Custom Properties

_None_
