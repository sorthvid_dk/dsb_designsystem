import { LitElement, html } from 'lit';
import CSS from './banner.scss';
import './../content-block/content-block';

export class Banner extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      trumpet: { type: String },
      headline: { type: String },
      text: { type: String },
      src: { type: String },
      alt: { type: String },
    };
  }

  constructor() {
    super();
    this.trumpet = '';
    this.headline = '';
    this.text = '';
    this.src = '';
    this.alt = '';
  }

  render() {
    return html`<section>
      <div class="content">
        <dsb-content-block
          .headline=${this.headline}
          .text=${this.text}
          .trumpet=${this.trumpet}
        >
          <slot></slot>
        </dsb-content-block>
      </div>

      <div class="image">
        <dsb-image-block src=${this.src} alt=${this.alt} gradient="left">
          <slot slot="right" name="badge"></slot>
        </dsb-image-block>
      </div>
    </section>`;
  }
}

customElements.define('dsb-banner', Banner);
