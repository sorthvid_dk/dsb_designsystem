// import { Banner } from './banner.html';
// import { banner } from './banner.state';
import './banner';
import { html } from 'lit';
import { random } from 'Util';
import readme from './README.md';

import './../button/button';

export default {
  title: 'Elements/Banner',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

// export const Default = () => Banner(banner());
// export const CSSBackground = () => markup(state({ cssbg: true }));

const headline = random('headline');
const paragraph = random('paragraph');

export const Banner = (args) =>
  html`<dsb-banner
    src=${args.src}
    alt=${args.alt}
    trumpet=${args.trumpet}
    headline=${args.headline}
    text=${args.text}
  >
  </dsb-banner>`;

export const UsingSlots = (args) =>
  html`<dsb-banner src=${args.src} alt=${args.alt} trumpet=${args.trumpet}>
    <dsb-badge slot="badge" label="badge" text="badge"></dsb-badge>
    <h1>${headline}</h1>
    <p>${paragraph}</p>
    <dsb-button label="Slotted button"></dsb-button>
  </dsb-banner>`;

Banner.args = {
  trumpet: random('cta'),
  headline,
  text: paragraph,
  src: random('jpg'),
  alt: 'An example image',
};

UsingSlots.args = {
  trumpet: random('cta'),
  src: random('jpg'),
  alt: 'An example image',
};
