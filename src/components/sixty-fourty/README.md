# `<dsb-sixty-fourty>`

60/40 layout used to display 2 image-link components
Use slots if you want badges on image-links

## Example

```html
<dsb-sixty-fourty
    sixtyHref='#'
    sixtyHeadline='Sixty headline'
    sixtySrc='src to image'
    sixtyText='Sixty body text'
    .sixtyOverlap='sixty true or false'
    sixtyAlt=${args.sixtyAlt}
    fourtyHref=${args.fourtyHref}
    fourtyHeadline=${args.fourtyHeadline}
    fourtySrc=${args.fourtySrc}
    fourtyText=${args.fourtyText}
    .fourtyOverlap=${args.fourtyOverlap}
    fourtyAlt=${args.fourtyAlt}>
></dsb-sixty-fourty>
```

## Api

### Properties/Attributes

| Name    | Type      | default | Description     |
| ------- | --------- | ------- | --------------- |
| `SixtyOverlap` | `boolean` | `true` | Text is outside of image |
| `fourtyOverlap` | `boolean` | `false` | Text is inside of image |

### Slots

| Name      | Description |
| --------- | :---------- |
| `sixtybadge`   | Used for placing a `<dsb-badge>` in the corner of the sixty image                                         |
| `fourtybadge`   | Used for placing a `<dsb-badge>` in the corner of the fourty image                                         |
| _default_ | About       |

### CSS Custom Properties

| Name     | Default | Description |
| -------- | ------- | ----------- |
| `--prop` | `none`  | About       |
