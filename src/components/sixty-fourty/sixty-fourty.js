import { LitElement, html } from 'lit';
import CSS from './sixty-fourty.scss';
import '../image-link/image-link';

export class DSBSixtyFourty extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      sixtyHref: { type: String },
      sixtyText: { type: String },
      sixtyHeadline: { type: String },
      sixtySrc: { type: String },
      sixtyOverlap: { type: Boolean },
      sixtyAlt: { type: String },
      fourtyHref: { type: String },
      fourtyText: { type: String },
      fourtyHeadline: { type: String },
      fourtySrc: { type: String },
      fourtyOverlap: { type: Boolean },
      fourtyAlt: { type: String },
    }
  }

  constructor() {
    super();
    this.sixtyHref = '#';
    this.sixtyText = 'Sixty Description...';
    this.sixtyHeadline = 'Sixty headline';
    this.sixtySrc = 'https://i.ibb.co/M2K3DSN/boats.jpg';
    this.sixtyAlt = 'sixty alt text';
    this.sixtyOverlap = false;
    this.fourtyHref = '#';
    this.fourtyText = 'Fourty description...';
    this.fourtyHeadline = 'Fourty headline';
    this.fourtySrc = 'https://i.ibb.co/pd5X6kx/image-dsb.jpg';
    this.fourtyOverlap = false;
    this.fourtyAlt = 'fourty alt text';
  }

  render() {
    return html`
    <div class="sixtyfourty">

      <div class="sixty">
          <dsb-image-link
            href=${this.sixtyHref}
            headline=${this.sixtyHeadline}
            src=${this.sixtySrc}
            text=${this.sixtyText}
            .overlap=${this.sixtyOverlap}
            alt=${this.sixtyAlt}>
            <slot name="sixtybadge"></slot>
          </dsb-image-link>
      </div>

      <div class="fourty">
          <dsb-image-link
            href=${this.fourtyHref}
            headline=${this.fourtyHeadline}
            src=${this.fourtySrc}
            text=${this.fourtyText}
            .overlap=${this.fourtyOverlap}
            alt=${this.fourtyAlt}>
            <slot name="fourtybadge"></slot>
          </dsb-image-link>
      </div>

    </div>`;
  }
}

customElements.define('dsb-sixty-fourty', DSBSixtyFourty);
