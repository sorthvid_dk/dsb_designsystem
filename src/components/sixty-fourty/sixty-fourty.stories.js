import { html } from 'lit';
import readme from './README.md';
import './sixty-fourty.js';
import { random } from 'Util';


export default {
  title: 'Elements/Sixty Fourty',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    sixtyOverlap: { type: 'boolean' },
    fourtyOverlap: { type: 'boolean' },
  },
};

const args = {
  sixtyOverlap: true,
  sixtyHref: '#',
  sixtyHeadline: random('headline'),
  sixtyText: random('sentence'),
  sixtySrc: random('jpg'),
  sixtyAlt: 'This is an example alt text',
  fourtyOverlap: false,
  fourtyHref: '#',
  fourtyHeadline: random('headline'),
  fourtyText: random('sentence'),
  fourtySrc: random('jpg'),
  fourtyAlt: 'This is an example alt text',
};

export const SixtyFourty = (args) => html`
  <dsb-sixty-fourty
    sixtyHref=${args.sixtyHref}
    sixtyHeadline=${args.sixtyHeadline}
    sixtySrc=${args.sixtySrc}
    sixtyText=${args.sixtyText}
    .sixtyOverlap=${args.sixtyOverlap}
    sixtyAlt=${args.sixtyAlt}
    fourtyHref=${args.fourtyHref}
    fourtyHeadline=${args.fourtyHeadline}
    fourtySrc=${args.fourtySrc}
    fourtyText=${args.fourtyText}
    .fourtyOverlap=${args.fourtyOverlap}
    fourtyAlt=${args.fourtyAlt}>
  </dsb-sixty-fourty>
`;

export const SixtyFourtyWithBadges = (args) => html`
  <dsb-sixty-fourty
    sixtyHref=${args.sixtyHref}
    sixtyHeadline=${args.sixtyHeadline}
    sixtySrc=${args.sixtySrc}
    sixtyText=${args.sixtyText}
    .sixtyOverlap=${args.sixtyOverlap}
    sixtyAlt=${args.sixtyAlt}
    fourtyHref=${args.fourtyHref}
    fourtyHeadline=${args.fourtyHeadline}
    fourtySrc=${args.fourtySrc}
    fourtyText=${args.fourtyText}
    .fourtyOverlap=${args.fourtyOverlap}
    fourtyAlt=${args.fourtyAlt}>
    <dsb-badge slot="sixtybadge" label="Fra" text="149,-" placement="left"></dsb-badge>
    <dsb-badge slot="fourtybadge" label="Fra" text="149,-" placement="left"></dsb-badge>
  </dsb-sixty-fourty>
`;



SixtyFourty.args = args;
SixtyFourtyWithBadges.args = args;
