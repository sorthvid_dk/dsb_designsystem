import readme from './README.md';
import { html } from 'lit';
import './card';
import { random } from 'Util';

export default {
  title: 'Elements/Card',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

const demo = {
  label: random('cta'),
  headline: random('cta'),
  text: random('sentence'),
};

export const Card = () =>
  html`
    <dsb-card
      label="${demo.label}"
      headline="${demo.headline}"
      text="${demo.text}"
    ></dsb-card>
  `;
