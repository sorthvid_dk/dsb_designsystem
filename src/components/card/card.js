import { LitElement, html } from 'lit';
import CSS from './card.scss';
import './../icon/icon';

export class Card extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      active: { type: Boolean, state: true },
      headline: { type: String },
      label: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.active = false;
    this.headline = '';
    this.label = '';
    this.text = '';
  }

  render() {
    return html` <div class="${this.active ? '-active' : ''}">
      <span class="label">${this.label}</span>
      <p class="title">
        ${this.headline}
        <dsb-icon icon="arrow--right" />
      </p>
      <p class="text">${this.text}<slot></slot></p>
    </div>`;
  }
}

customElements.define('dsb-card', Card);
