# `<dsb-card>`

Info block. Place inside an `<a>` to make it a link.

## Example

```html
<dsb-card
  label="Red mini text"
  headline="Bold text"
  text="Paragraph text"
></dsb-card>
```

## Api

### Properties/Attributes

| Name       | Type     | default | Description                              |
| ---------- | -------- | ------- | :--------------------------------------- |
| `label`    | `string` | `''`    | Trumpet text in top of component         |
| `headline` | `string` | `''`    | Will automatically get an arrow after it |
| `text`     | `string` | `''`    | Longer paragrapg text                    |

### Slots

| Name      | Description                   |
| --------- | :---------------------------- |
| _default_ | Renders just after the `text` |

### CSS Custom Properties

_None_
