# `<dsb-image-link>`

The Image Link is an `<a>` that includes an image and some text.\
Comes in two variants. A default that has the text under the image,\
and one with the text overlapping the image.

## Example

```html
<dsb-image-link
  overlap
  href="#"
  headline="headline text"
  text="This is the copytext under the headline"
  src="path/to/image"
  alt="An alt text for the image"
>
  <dsb-badge label="Fra" text="149,-" placement="left"></dsb-badge>
</dsb-image-link>
```

## Api

### Properties/Attributes

| Name       | Type      | Description                                                |
| ---------- | --------- | :--------------------------------------------------------- |
| `overlap`  | `boolean` | Changes the style to have the text on top of the image     |
| `href`     | `string`  | The `href` attribute for the `<a>`                         |
| `headline` | `string`  | The headline text, will always have an arrow icon after it |
| `text`     | `string`  | Body text                                                  |
| `src`      | `string`  | The `src` attribute for the `<img>`                        |
| `alt`      | `string`  | The `alt` attribute for the `<img>`                        |

### Slots

| Name      | Description                                                                       |
| --------- | :-------------------------------------------------------------------------------- |
| _default_ | Will render it's content in the top left/right corner. Used for including a badge |
