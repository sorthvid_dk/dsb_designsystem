import { LitElement, html } from 'lit';
import './../icon/icon';
import './../image-block/image-block';
import CSS from './image-link.scss';
import { classMap } from 'lit/directives/class-map.js';

// @customElement('dsb-button')
export class ImageLink extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      overlap: { type: Boolean },
      href: { type: String },
      src: { type: String },
      alt: { type: String },
      headline: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.overlap = false;
    this.href = '';
    this.src = '';
    this.alt = '';
    this.headline = '';
    this.text = '';
  }

  // <slot></slot>
  // <slot name="right"></slot>

  // <div class="image">
  //   <div class="boundaries">
  //     <img src=${this.src} alt=${this.alt} />
  //   </div>
  // </div>
  render() {
    return html`
      <a href=${this.href} class="${classMap({ overlap: this.overlap })}">
        <dsb-image-block src=${this.src} alt=${this.alt} aspect="16/9">
          <slot></slot>
          <slot name="right"></slot>
        </dsb-image-block>
        <div class="content">
          <div class="headline">
            ${this.headline} <dsb-icon icon="arrow--right"></dsb-icon>
          </div>
          <div class="text">${this.text}</div>
        </div>
      </a>
    `;
  }
}

customElements.define('dsb-image-link', ImageLink);
