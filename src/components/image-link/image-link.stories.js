import { random } from 'Util';
import { html } from 'lit';
import readme from './README.md';
import './image-link';
import '~/badge/badge';

export default {
  title: 'Elements/Image Link',
  parameters: {
    readme: {
      sidebar: readme,
      
    },
  },
  argTypes: {
    overlap: { type: 'boolean' },
  },
};

const args = {
  overlap: false,
  href: '#',
  headline: random('headline'),
  text: random('sentence'),
  src: random('jpg'),
  alt: 'This is an example alt text',
};

export const ImageLink = (args) =>
  html`<dsb-image-link
    .overlap=${args.overlap}
    href=${args.href}
    text=${args.text}
    headline=${args.headline}
    src=${args.src}
    alt=${args.alt}
  ></dsb-image-link>`;

export const ImageLinkWithBadge = (args) =>
  html` <dsb-image-link
    .overlap=${args.overlap}
    href=${args.href}
    text=${args.text}
    headline=${args.headline}
    src=${args.src}
    alt=${args.alt}
  >
    <dsb-badge .label=${'Fra'} .text=${'149,-'} placement="left"></dsb-badge>
  </dsb-image-link>`;

ImageLink.args = args;
ImageLinkWithBadge.args = args;
