# `<dsb-icon>`

Icon displays a `<svg>` icon from the available icons in the designsystem.

## Example

```html
<dsb-icon icon="dsb-logo"></dsb-icon>
```

```html
<!-- fancy icon -->
<style>
  .fancy {
    --icon-size: 135px;
    --icon-color: cornflowerblue;
  }
</style>
<dsb-icon class="fancy" icon="dsb-logo"></dsb-icon>
```

## Api

### Properties/Attributes

| Name   | Type     | Description          |
| ------ | -------- | :------------------- |
| `icon` | `string` | The name of the icon |

### Slots

_None_

### CSS Custom Properties

| Name           | Default        | Description       |
| -------------- | -------------- | ----------------- |
| `--icon-size`  | `20px`         | Size of the icon  |
| `--icon-color` | `currentColor` | Color of the icon |

### Storybook development

To generate icons you:

- Add a new .svg to the `/static/svg`.
- Run `yarn svg` from `/root`. This will take every file and make then importable by `<dsb-icon>`.
- The name is generated from its corresponding filename.
- Use the components like: `<dsb-icon icon="my-icon">`
