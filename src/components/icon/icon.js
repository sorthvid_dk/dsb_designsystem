import { LitElement, svg } from 'lit';
import { getIcon } from './getIcon.js';
import CSS from './icon.scss';

export class Icon extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      icon: { type: String },
    };
  }

  constructor() {
    super();
    this.icon = '';
  }

  render() {
    return svg`${getIcon(this.icon)}`;
  }
}

customElements.define('dsb-icon', Icon);
