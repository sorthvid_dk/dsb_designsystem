import readme from './README.md';
import './icon';
import { html } from 'lit';
import { allIcons } from './getIcon.js';

export default {
  title: 'Elements/Icon',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Icon = () => html`<dsb-icon icon="dsb-logo"></dsb-icon>`;

export const Fancy = (args) => html`
  <span>
    <style>
      .fancy {
        --icon-size: ${args.size}px;
        --icon-color: cornflowerblue;
      }
    </style>
    <dsb-icon class="fancy" icon="dsb-logo"></dsb-icon>
  </span>
`;

Fancy.args = {
  size: 135
}

export const AllIcons = () =>
  html`<div class="m-2" style="--icon-size: 3em">
    ${allIcons.map(
      (icon) =>
        html`
          <p class="ts-mini mt-4">${icon}</p>
          <dsb-icon icon=${icon}></dsb-icon>
        `
    )}
  </div>`;
