import { LitElement, html } from 'lit';
import CSS from './hero.scss';
import { stringToDocumentFragment } from '../../js/utils';

export class Hero extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      src: { type: String },
      alt: { type: String },
      headline: { type: String },
      trumpet: { type: String },
      variant: { type: String, reflect: true },
      text: { type: String },
      ctaPrefix: { type: String },
      ctaInfo: { type: String },
      buttonLabel: { type: String },
      buttonIcon:  { type: String },
    };
  }

  constructor() {
    super();
    this.src = '';
    this.alt = '';
    this.text = '';
    this.trumpet = '';
    this.headline = '';
    this.variant = 'full';
    this.ctaPrefix = '';
    this.ctaInfo = '';
    this.buttonLabel = '';
    this.buttonIcon = '';
  }

  getButtonColorForHero() {
    let result = '';

    switch (this.variant) {
      case 'full':
        result = 'red'
        break;
      case 'sub':
        result = this.src ? 'red' : 'ghost'
        break;
      default:
        result = 'ghost'
        break;
    }
    return result;
  }

  imageBlock() {
    if (!this.src) {
      return '';
    }

    return html`
      <div class="image">
        <div>
          <img src=${this.src} alt=${this.alt} />
        </div>
      </div>
    `;
  }

  badgeSlot() {
    if (this.variant !== 'sub' || (this.variant === 'sub' && this.src)) {
      return html`
        <div class="badge"><slot name="badge"></slot></div>
      `;
    } else {
      return '';
    }
  }

  render() {
    return html`
      <section>
        <div class="deck">
          <span class="background">
            ${this.variant === 'sub' ? this.imageBlock() : ''}
          </span>
          <div class="breadcrumbs"><slot name="breadcrumbs"></slot></div>
          ${this.badgeSlot()}

          <div class="content">
            <div class="text-container">
              ${this.trumpet
                ? html`<p class="trumpet">${this.trumpet}</p>`
                : ''}

              <h1 class="headline">${this.headline}</h1>

              <div class="text">${stringToDocumentFragment(this.text)}</div>

              <div class="cta">
                <dsb-cta
                  .prefix=${this.ctaPrefix}
                  .info=${this.ctaInfo}
                >
                  ${this.buttonLabel
                    ? html`<dsb-button
                        label=${this.buttonLabel}
                        icon=${this.buttonIcon}
                        variant=${this.getButtonColorForHero()}
                      ></dsb-button>`
                    : ''}
                </dsb-cta>

              </div>

            </div>

            ${this.variant !== 'sub' ? this.imageBlock() : ''}
          </div>
        </div>
      </section>
    `;
  }
}

customElements.define('dsb-hero', Hero);
