# `<dsb-hero>`

The Hero is always placed in the top of a page.

## Example

```html
<dsb-hero
  variant="split"
  src="path/to/image.jpg"
  alt="An image"
  trumpet="Optional trumpet text"
  headline="The hero headline"
  text="<p>text content and here we add a link <a href='#'>link description</a>.</p>"
  ctaPrefix = "Prefix for cta ex 'from'"
  ctaInfo = "Info/Value for cta ex '149,-'"
  buttonLabel = "Button text"
  buttonIcon = "Button icon"
>
  <dsb-breadcrumbs slot="breadcrumbs" .items=${items}></dsb-breadcrumbs>
  <dsb-badge slot="badge" .label="From" .text="149,-" placement="left"></dsb-badge>
</dsb-hero>
```

## Api

### Properties/Attributes

| Name         | Type     | Description                                            |
| ------------ | -------- | :----------------------------------------------------- |
| `variant`    | `string` | The variant of Hero component: 'split', 'full' or 'sub'|
| `src`        | `string` | The `src` attribute for the `<img>`                    |
| `alt`        | `string` | The `alt` attribute for the `<img>`                    |
| `trumpet`    | `string` | An optional trumpet text, shown above the headline     |
| `headline`   | `string` | The headline text                                      |
| `text`       | `string` | The body text from formatted from backend as html      |
| `ctaPrefix`  | `string` | Prefix for the call to action                          |
| `ctaInfo`    | `string` | The info/value to display after cta prefix             |
| `buttonLabel`| `string` | The button text                                        |
| `buttonIcon` | `string` | The icon displayed in button before button text        |


### Slots

| Name          | Description                                                                  |
| ------------- | :--------------------------------------------------------------------------- |
| `breadcrumbs` | Used to display `<dsb-breadcrumbs>` in sub pages                             |
| `badge`       | Used for placing a `<dsb-badge>` in the corner of the image                  |

#### Todo

- Fix breadcrumbs.
