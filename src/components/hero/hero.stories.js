import { html } from 'lit';
import readme from './README.md';
import './hero';
import '~/breadcrumbs/breadcrumbs';
import { items } from '~/breadcrumbs/data';
import '~/button/button';
import '~/cta/cta';
import { random } from 'Util';

export default {
  title: 'Elements/Hero',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
  argTypes: {
    variant: {
      options: ['split', 'full', 'sub'],
      control: { type: 'radio' },
    },
    buttonIcon: {
      options: ['trafficinfo', 'dsb-logo--outlined', 'arrow--right'],
      control: { type: 'radio' },
    }
  }
};

export const Hero = (args) => html`
  <dsb-hero
    src=${args.src}
    alt=${args.alt}
    trumpet=${args.trumpet}
    headline=${args.title}
    variant=${args.variant}
    text=${args.text}
    ctaPrefix = ${args.ctaPrefix}
    ctaInfo = ${args.ctaInfo}
    buttonLabel = ${args.buttonLabel}
    buttonIcon = ${args.buttonIcon}
  >
    <dsb-breadcrumbs slot="breadcrumbs" .items=${args.breadcrumbs}></dsb-breadcrumbs>
    <dsb-badge slot="badge" .label=${args.badgeLabel} .text=${args.badgeText} placement="left"></dsb-badge>
  </dsb-hero>
`;

Hero.args = {
  variant: 'sub',
  trumpet: random('cta'),
  title: random('headline'),
  text: `<p>
    Når du er ung eller studerende, kan du få rabat på alle dine togrejser,
    rabatkort og lign. <a href="#">Besparelsen afhænger af</a>, hvor ofte du
    rejser med toget, og hvor din rejse går hen.
  </p>`,
  src: random('jpg'),
  alt: 'Image alt text',
  ctaPrefix: 'Fra',
  ctaInfo: '80,-',
  buttonLabel: random('cta'),
  buttonIcon: 'trafficinfo',

  badgeLabel: 'Fra',
  badgeText: '149,-',
  breadcrumbs: items,
};
