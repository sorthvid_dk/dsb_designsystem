# `<dsb-underline-link>`

Simple link element with an underline hover effect.

## Example

```html
<dsb-underline-link href="#" label="Click me"></dsb-underline-link>
```

## Api

### Properties/Attributes

| Name    | Type     | default | Description                              |
| ------- | -------- | ------- | :--------------------------------------- |
| `href`  | `string` | `''`    | `href` for the `<a>`                     |
| `label` | `string` | `''`    | Visible link text, also the `aria-label` |

### Slots

| Name      | Description                                                          |
| --------- | :------------------------------------------------------------------- |
| _default_ | Can be used instead of `label`, but will not render any `aria-label` |

### CSS Custom Properties

_None_
