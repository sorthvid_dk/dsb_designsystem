import './underline-link.js';
import { html } from 'lit';
import readme from './README.md';

export default {
  title: 'Elements/Underline Link',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const UnderlineLink = () =>
  html`<dsb-underline-link href="#" label="Underline link"></dsb-underline-link>`;
