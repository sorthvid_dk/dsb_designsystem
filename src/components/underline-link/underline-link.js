import { LitElement, html } from 'lit';
import CSS from './underline-link.scss';

export class UnderlineLink extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      href: { type: String },
      label: { type: String },
    };
  }

  constructor() {
    super();
    this.href = '';
    this.label = '';
  }

  render() {
    return html` <a href=${this.href}>${this.label}<slot></slot></a> `;
  }
}

customElements.define('dsb-underline-link', UnderlineLink);
