# `<dsb-test>`

Text about the component

## Example

```html
<dsb-test></dsb-test>
```

## Api

### Properties/Attributes

| Name    | Type      | default | Description     |
| ------- | --------- | ------- | --------------- |
| `fancy` | `boolean` | `false` | Makes it cooler |

### Slots

| Name      | Description |
| --------- | :---------- |
| `named`   | About       |
| _default_ | About       |

### CSS Custom Properties

| Name     | Default | Description |
| -------- | ------- | ----------- |
| `--prop` | `none`  | About       |
