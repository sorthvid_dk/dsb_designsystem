import { html } from 'lit';
import readme from './README.md';
import './test.js';

export default {
  title: 'Elements/Test',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Test = (args) => html`
  <dsb-test .fancy=${args.fancy}>Slot content</dsb-test>
`;

Test.args = {
  fancy: false,
};
