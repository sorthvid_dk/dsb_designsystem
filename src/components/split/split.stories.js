import { html } from 'lit';
import './split';
import readme from './README.md';
import { random } from 'Util';

export default {
  title: 'Elements/Split',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Split = (args) =>
  html`<dsb-split
    ?inverted=${args.inverted}
    src=${args.src}
    alt=${args.alt}
    trumpet=${args.trumpet}
    headline=${args.headline}
    text=${args.text}
  ></dsb-split>`;

export const UsingSlots = (args) =>
  html`<dsb-split ?inverted=${args.inverted} src=${args.src} alt=${args.alt}>
    <dsb-badge slot="badge" text="Badge"></dsb-badge>

    <h2>Nem planlægning med mere end 30 daglige afgange</h2>

    <p>
      City Pass giver mulighed for at rejse ubegrænset med bus, tog og metro, så
      du kan rejse ubekymret rundt i byen uden at skulle tænke på at købe ny
      billet hver gang du skal videre.
    </p>

    <dsb-button icon="arrow--right" label="Click on me"></dsb-button
  ></dsb-split>`;

Split.args = {
  inverted: false,
  src: random('jpg'),
  alt: 'Example image alt text',
  trumpet: random('city'),
  headline: random('headline'),
  text: random('paragraph'),
};

UsingSlots.args = {
  inverted: true,
  src: random('jpg'),
  alt: 'Example image alt text',
};
