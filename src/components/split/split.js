import { LitElement, html } from 'lit';
import CSS from './split.scss';
import '../image-block/image-block';
import '../content-block/content-block';

export class Split extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      inverted: { type: Boolean, reflect: true },
      src: { type: String },
      alt: { type: String },
      trumpet: { type: String },
      headline: { type: String },
      text: { type: String },
    };
  }

  constructor() {
    super();
    this.inverted = false;
    this.src = '';
    this.alt = '';
    this.trumpet = '';
    this.headline = '';
    this.text = '';
  }

  render() {
    return html` <div class="split">
      <div class="image">
        <dsb-image-block aspect="1/1" src=${this.src} alt=${this.alt}>
          <slot name="badge"></slot>
          <slot name="right" slot="right"></slot>
        </dsb-image-block>
      </div>
      <div class="content">
        <dsb-content-block
          trumpet=${this.trumpet}
          headline=${this.headline}
          text=${this.text}
        >
          <slot></slot>
        </dsb-content-block>
      </div>
    </div>`;
  }
}

customElements.define('dsb-split', Split);
