# `<dsb-split>`

A 50/50 layout of image and text

## Example

```html
<!-- using attributes -->
<dsb-split
  inverted
  src="path/to/my/image.jpg"
  alt="An alt text"
  trumpet="Some trumpet"
  headline="Some headline"
  text="Some text"
>
  <dsb-button label="Click me"></dsb-button>
</dsb-split>
```

```html
<!-- using slots -->
<dsb-split inverted src="path/to/my/image.jpg" alt="An alt text">
  <dsb-badge slot="badge" text="Badge"></dsb-badge>
  <h2>Nem planlægning med mere end 30 daglige afgange</h2>
  <p>
    City Pass giver mulighed for at rejse ubegrænset med bus, tog og metro, så
    du kan rejse ubekymret rundt i byen uden at skulle tænke på at købe ny
    billet hver gang du skal videre.
  </p>

  <dsb-button label="Click me"></dsb-button>
</dsb-split>
```

## Api

### Properties/Attributes

| Name       | Type      | default | Description                             |
| ---------- | --------- | ------- | :-------------------------------------- |
| `inverted` | `boolean` | `false` | Flips the image to the right side       |
| `src`      | `string`  | `''`    | `src` for the `<img>`                   |
| `alt`      | `string`  | `''`    | `alt` for the `<img>`                   |
| `trumpet`  | `string`  | `''`    | Text above the headline                 |
| `headline` | `string`  | `''`    | Main headline                           |
| `text`     | `string`  | `''`    | A longer parapgraph text                |
| `contain`  | `boolean` | `false` | Adds "object-fit: contain" to the image |

### Slots

| Name      | Description                                                                                         |
| --------- | :-------------------------------------------------------------------------------------------------- |
| `badge`   | Used for placing a `<dsb-badge>` in the corner of the image                                         |
| _default_ | Renders after the `attributes`. Used when the text contains `html`, and/or there's a `<dsb-button>` |

### CSS Custom Properties

_None_
