import { LitElement, html } from 'lit';
import CSS from './header-search.scss';

export class HeaderSearch extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      headline: { type: String },
      placeholder: { type: String },
      ariaLabel: { type: String },
    };
  }

  constructor() {
    super();
    this.headline = '';
    this.placeholder = '';
    this.ariaLabel = '';
  }

  //   connectedCallback() {
  //     super.connectedCallback();
  //     this.addEventListener('click', this.expand.bind(this));
  //   }

  //   disconnectedCallback() {
  //     super.disconnectedCallback();
  //     this.removeEventListener('click', this.expand.bind(this));
  //   }

  render() {
    return html`<section>
      <div class="input">
        <dsb-icon icon="magnifying-glass"></dsb-icon>
        <input
          type="search"
          id="site-search"
          name="q"
          aria-label=${this.ariaLabel}
          .placeholder=${this.placeholder}
        />
      </div>
      <slot></slot>
    </section>`;
  }
}

customElements.define('dsb-header-search', HeaderSearch);
