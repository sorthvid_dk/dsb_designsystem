import { html } from 'lit';
import './header-search'
import readme from './README.md';

export default {
  title: 'Elements/Header Search',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const HeaderSearch = () => html`<dsb-header-search placeholder="Hvad søger du?"></dsb-header-search>`;
