import { LitElement, html } from 'lit';
import './../icon/icon.js';
import CSS from './icon-link.scss';

export class IconLink extends LitElement {
  static get styles() {
    return [CSS];
  }

  static get properties() {
    return {
      href: { type: String },
      icon: { type: String },
      trailingIcon: { type: String },
      label: { type: String },
      mini: { type: Boolean, reflect: true },
      inline: { type: Boolean, reflect: true },
      arrow: { type: Boolean, reflect: true },
    };
  }

  constructor() {
    super();
    this.href = '';
    this.icon = '';
    this.trailingIcon = '';
    this.label = '';
    this.mini = false;
    this.inline = false;
    this.arrow = false;
  }

  render() {
    return html`
      <a href=${this.href} aria-label=${this.icon}>
        <div class="content">
          ${this.icon ? html`<dsb-icon icon=${this.icon}></dsb-icon>` : ''}
          <span>${this.label}</span>
          ${this.arrow ? html`<dsb-icon icon="arrow--right"></dsb-icon>` : ''}
          ${this.trailingIcon
            ? html`<dsb-icon icon=${this.trailingIcon}></dsb-icon>`
            : ''}
        </div>
      </a>
    `;
  }
}

customElements.define('dsb-icon-link', IconLink);
