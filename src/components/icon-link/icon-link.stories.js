import './icon-link.js';
import { html } from 'lit';
import readme from './README.md';

export default {
  title: 'Elements/Icon Link',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const IconLink = () =>
  html`<dsb-icon-link href="#" icon="cross" label="Icon link"></dsb-icon-link>`;

export const WithTrailingIcon = () =>
  html`<dsb-icon-link
    href="#"
    trailingIcon="cross"
    label="Icon link"
  ></dsb-icon-link>`;

export const ArrowLink = () =>
  html`<dsb-icon-link arrow href="#" label="Arrow link"></dsb-icon-link>`;

export const WithoutIcon = () =>
  html`<dsb-icon-link href="#" label="Arrow link"></dsb-icon-link>`;

export const Fullsize = () =>
  html`<dsb-icon-link fill-space href="#" label="Arrow link"></dsb-icon-link>`;
