import { html } from "lit";
import { getIcon, repeat, random } from "Util";

import pin from "Svg/pin.svg";

import { inputDropdown } from "./input-dropdown.state";

export const InputDropdown = ({ value } = inputDropdown()) => html`
  <ul class="input-dropdown" data-options>
    ${repeat(
      6,
      (x, i, city = random("city") || value) =>
        html`
          <li class="input-dropdown__item" tabindex="-1" value="${city}">
            ${getIcon(pin)} ${city}
            <!--<span class="black">Dropd</span>own item-->
          </li>
        `
    )}
  </ul>
`;
