import { InputDropdown } from './input-dropdown.html';
import { inputDropdown } from './input-dropdown.state';
import readme from './README.md';

export default {
  title: 'Components/Input Dropdown',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => InputDropdown(inputDropdown());
