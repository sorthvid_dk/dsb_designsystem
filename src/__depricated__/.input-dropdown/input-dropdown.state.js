export const inputDropdown = ({ id = 'Input Dropdown' } = {}) => ({
  items: [
    'Item number one',
    'Item number two',
    'Item number three',
    'Item number four',
    'Item number five',
    'Item number six',
  ],
  id,
});
