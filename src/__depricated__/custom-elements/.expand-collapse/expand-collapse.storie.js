import {
  default as markup,
  multiple,
  nonContained,
  shy
} from './expand-collapse.html';
import readme from './README.md';

export default {
  title: 'Custom Elements/<expand-collapse>',
  parameters: {
    readme: {
      sidebar: readme
    }
  }
};

export const Default = () => markup();

export const NonContained = () => nonContained();

export const Multiple = () => multiple();

export const Shy = () => shy();
