import { html } from "lit";

export default () => html`
  <expand-collapse>
    <div data-trigger>Trigger</div>
    <div data-content>
      <button data-close>&times;</button>
      Content
    </div>
  </expand-collapse>
`;

export const nonContained = () => html`
  <div>
    <div data-trigger="one">Trigger One</div>
    <div data-trigger="two">Trigger Two</div>

    <expand-collapse>
      <div data-content="one">
        <button data-close>&times;</button>
        Content One
      </div>
    </expand-collapse>

    <expand-collapse data-content="two">
      <button data-close>&times;</button>
      Content Two
    </expand-collapse>
  </div>
`;

export const multiple = () => html`
  <expand-collapse>
    <div data-trigger="one">Trigger One</div>
    <div data-content="one">
      <button data-close>&times;</button>
      Content One
    </div>

    <div data-trigger="two">Trigger Two</div>
    <div data-content="two">
      <button data-close>&times;</button>
      Content Two
    </div>

    <div data-trigger="three">Trigger Three</div>
    <div data-content="three">
      <button data-close>&times;</button>
      Content Three
    </div>

    <div data-trigger="four">Trigger Four</div>
    <div data-content="four">
      <button data-close>&times;</button>
      Content Four
    </div>
  </expand-collapse>
`;

export const shy = () => html`
  <expand-collapse data-shy="true">
    <div data-trigger="one">Trigger One</div>
    <div data-content="one">
      <button data-close>&times;</button>
      Content One
    </div>

    <div data-trigger="two">Trigger Two</div>
    <div data-content="two">
      <button data-close>&times;</button>
      Content Two
    </div>

    <div data-trigger="three">Trigger Three</div>
    <div data-content="three">
      <button data-close>&times;</button>
      Content Three
    </div>

    <div data-trigger="four">Trigger Four</div>
    <div data-content="four">
      <button data-close>&times;</button>
      Content Four
    </div>
  </expand-collapse>
`;
