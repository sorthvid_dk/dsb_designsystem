# `<expand-collapse>`

```js
import { defineExpandCollapse } from '@dsb.dk/designsystem/dist/js/dsb.esm.js'
```

Is in fact an accordion item.\
Can only be used as a contained expand-item, \
where the trigger and the content is both in the same parent container (like a single accordion item).

## Usage

Inside of the expand-collapse-element:\
`data-trigger`: is the element that toggles open/close state.\
`data-content`: is what is expanded.\
`data-close`: <i>optional</i> can be placed inside `data-content` and will close the content on click.

## Accessibility

The element will handle the aria- and other attributes needed for making the element accessible!

## Gotcha

Max-height is used to animate the content in and out.
If the element with data-content has padding or margin, it will appear half closed.
So apply any padding/margin to inner elements, inside data-content.
