import base from '../.base-template';
import expandable from './../../js/expandable';
import { parseBool } from '../../../js/utils';
import { flip } from '../../../js/flip';

class ExpandCollapse extends base {
  connectedCallback() {
    const {
      toggleClass = 'is-open',
      closeOnBodyClick = false,
      disableScroll = false,
      trapFocus = false,
      shy = false,
    } = this.dataset;

    const multiple = this.querySelectorAll('[data-trigger]').length;
    const contained = this.querySelector('[data-trigger]');

    const options = {
      closeOnBodyClick: parseBool(closeOnBodyClick),
      disableScroll: parseBool(disableScroll),
      trapFocus: parseBool(trapFocus),
      toggleClass,
      onOpen: (element) => flip.on(element),
      onClose: (element) => flip.off(element),
    };

    if (multiple) {
      const triggers = [...this.querySelectorAll(`[data-trigger]`)];

      const instances = triggers.map((trigger) => {
        const currentId = trigger.dataset.trigger;
        const currentEl = this.querySelector(`[data-content="${currentId}"]`);

        return expandable({
          ...options,
          el: currentEl,
          contained: false,
          trigger,
          content: currentEl,
          closeButton: currentEl.querySelector('[data-close]'),
        });
      });

      /**
       * Closes all other when a new is opened
       */
      if (shy) {
        triggers.forEach((trigger) =>
          trigger.addEventListener(
            'click',
            closeOthers.bind(trigger, instances)
          )
        );
      }
    } else if (!contained) {
      const content = this.querySelector('[data-content]') || this;
      const id = content.dataset.content;

      expandable({
        ...options,
        id,
        el: this,
        contained: parseBool(contained),
        trigger: false,
        content,
        closeButton: this.querySelector('[data-close]'),
      });
    } else {
      expandable({
        ...options,
        el: this,
        contained: parseBool(contained),
        trigger: this.querySelector('[data-trigger]'),
        content: this.querySelector('[data-content]'),
        closeButton: this.querySelector('[data-close]'),
      });
    }
  }
}

export const defineExpandCollapse = () =>
  customElements.define('expand-collapse', ExpandCollapse);

function closeOthers(instances) {
  instances.forEach((instance) => {
    if (instance.trigger !== this) {
      instance.close();
    }
  });
}
