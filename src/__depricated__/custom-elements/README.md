# Custom elements

Most of the logic that is tied to the different components are contained in a Custom element.

The huge benefit of containing our logic in a custom element is that we gets access to the elements lifecycle hooks,\
which means all the instantiation can happen more or less by itself. Without the need of keeping track of page loads and init-funcitons.

## When?

The custom elements should use scripts from the js folder, which knows 'what' to do, and keep control over when to do them. It could be when the element is in the dom, when a key is pressed or when the page is loaded.
