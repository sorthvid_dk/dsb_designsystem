import base from '../.base-template';
import expandable from './../../js/expandable';
import { parseBool } from './../../js/utils';

class FlyOut extends base {
  connectedCallback() {
    const {
      moveBody = false,
      toggleClass = 'is-open',
      closeOnBodyClick = false,
      disableScroll = false,
      trapFocus = false,
      shy = false,
    } = this.dataset;

    const multiple = this.querySelectorAll('[data-trigger]').length;
    const contained = this.querySelector('[data-trigger]');

    const options = {
      closeOnBodyClick: parseBool(closeOnBodyClick),
      disableScroll: parseBool(disableScroll),
      trapFocus: parseBool(trapFocus),
      toggleClass,
      onOpen: (element) => {
        if (parseBool(moveBody)) {
          const { height, transitionDuration } = getComputedStyle(element);

          document.body.style.transition = `margin-top ${transitionDuration}`;
          document.body.style.marginTop = height;
          document.body.classList.add('is-fixed', 'is-dimmed');
        }
      },
      onClose: () => {
        if (moveBody) {
          document.body.style.removeProperty('margin-top');
          document.body.classList.remove('is-fixed', 'is-dimmed');
          document.body.addEventListener(
            'transitionend',
            () => document.body.style.removeProperty('transition'),
            { once: true }
          );
        }
      },
    };

    if (multiple) {
      const triggers = [...this.querySelectorAll(`[data-trigger]`)];

      const instances = triggers.map((trigger) => {
        const currentId = trigger.dataset.trigger;
        const currentEl = this.querySelector(`[data-content="${currentId}"]`);

        return expandable({
          ...options,
          el: currentEl,
          contained: false,
          trigger,
          content: currentEl,
          closeButton: currentEl.querySelector('[data-close]'),
        });
      });

      /**
       * Closes all other when a new is opened
       */
      if (shy) {
        triggers.forEach((trigger) =>
          trigger.addEventListener(
            'click',
            closeOthers.bind(trigger, instances)
          )
        );
      }
    } else if (!contained) {
      const content = this.querySelector('[data-content]') || this;
      const id = content.dataset.content;

      expandable({
        ...options,
        id,
        el: this,
        contained: parseBool(contained),
        trigger: false,
        content,
        closeButton: this.querySelector('[data-close]'),
      });
    } else {
      expandable({
        ...options,
        el: this,
        contained: parseBool(contained),
        trigger: this.querySelector('[data-trigger]'),
        content: this.querySelector('[data-content]'),
        closeButton: this.querySelector('[data-close]'),
      });
    }
  }
}

export const defineFlyOut = () => customElements.define('fly-out', FlyOut);

function closeOthers(instances) {
  instances.forEach((instance) => {
    if (instance.trigger !== this) {
      instance.close();
    }
  });
}
