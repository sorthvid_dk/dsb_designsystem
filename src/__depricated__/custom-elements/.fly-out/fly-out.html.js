import { html } from "lit";

export default () => html`
  <fly-out>
    <div data-trigger>Trigger</div>
    <div data-content>
      <button data-close>&times;</button>
      Content
    </div>
  </fly-out>
`;

export const moveBody = () => html`
  <fly-out data-move-body="true" data-close-on-body-click="true">
    <div data-trigger>Trigger</div>
    <div data-content>
      <button data-close>&times;</button>
      Content
    </div>
  </fly-out>
`;
