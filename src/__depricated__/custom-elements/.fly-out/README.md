# `<fly-out>`

```js
import { defineFlyOut } from '@dsb.dk/designsystem/dist/js/dsb.esm.js'
```

Inherits most of its functionality from `<expand-collapse>`. But instead of having the height changing on opening/closing, the element is translated.

## Usage

Inside of the expand-collapse-element:\
`data-trigger`: is the element that toggles open/close state.\
`data-content`: is what is expanded.\
`data-close`: <i>optional</i> can be placed inside `data-content` and will close the content on click.

## Accessibility

The element will handle the aria- and other attributes needed for making the element accessible!
