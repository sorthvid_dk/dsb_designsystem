import { html } from "lit";

export default () => html`
  <div>
    <mobile-hide>
      foobar
    </mobile-hide>
    <div style="height: 150vh"></div>
  </div>
`;
