# `<mobile-hide>`

```js
import { defineMobileHide } from '@dsb.dk/designsystem/dist/js/dsb.esm.js'
```

Currently used and tested for the header-form.\
In order to control the opening and closing of the form.\
This element also moves on mobile, up and down, together with the scroll.

## Usage

