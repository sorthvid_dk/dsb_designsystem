import ally from 'ally.js';
import base from '../.base-template';

const clamp = ({ value, min, max }) => Math.min(max, Math.max(value, min));
const screenBiggerThan = (width) => window.innerWidth >= width;

class MobileHide extends base {
  init() {}

  connectedCallback() {
    let lastScroll = 0;
    this.element = this;
    this.formElement = this.querySelector('.header-form');
    this.closeButton = this.querySelector('[data-close]');
    const inputElements = [...this.element.querySelectorAll('input')];

    const updateElementTopPosition = (el) => {
      /**
       * Only run on smaller screens,
       * where the form is below the header icons
       */
      if (screenBiggerThan(900)) {
        el.style.removeProperty('top');
        return;
      }

      /**
       * Save last value, to know what to increment from
       */
      const oldValue = parseInt(el.style.top) || 0;

      /**
       * The change in page scroll, will be +/-1
       */
      const scrollChange =
        window.pageYOffset > -1 ? lastScroll - window.pageYOffset : 0;

      /**
       * Make the change
       */
      el.style.top =
        clamp({
          /**
           * If new 'value' is between min-max, be 'value'
           */
          value: oldValue + scrollChange,
          /**
           * Never be less than...
           */
          min: screenBiggerThan(600) ? -48 : -44,
          /**
           * ... or more than
           */
          max: screenBiggerThan(600) ? 18 : 16,
        }) + 'px';

      /**
       * The new scroll position will be the old in the next call
       */
      lastScroll = window.pageYOffset;
    };

    /**
     * On small screens the form should slide up and down
     */
    window.addEventListener(
      'scroll',
      updateElementTopPosition.bind(this, this.element)
    );
    /**
     * For having transition on the form when opening/closing.
     * Bun no transition when the page is scrolled.
     */

    this.addEventListener('click', (event) => {
      if (event.target.closest('.header-form__close-button')) return;

      this.transitionClassAdd();
    });

    inputElements.forEach((input) =>
      input.addEventListener('focus', this.transitionClassAdd.bind(this))
    );
    inputElements.forEach((input) =>
      input.addEventListener('blur', this.transitionClassRemove.bind(this))
    );

    /**
     * b/c the form is opened by :focus-within,
     * A simple blur closes it
     */
    this.closeButton &&
      this.closeButton.addEventListener('click', () => {
        document.body.classList.remove('is-fixed', 'is-dimmed');
        ally.element.blur(this.formElement);
      });

    document.body.addEventListener('click', (e) => {
      if (e.clientY >= this.getBoundingClientRect().bottom) {
        document.body.classList.remove('is-fixed', 'is-dimmed');
        ally.element.blur(this);
      }
    });
  }

  transitionClassAdd() {
    /**
     * Gives us control to add CSS transition when the form gets expanded,
     * while not having any transition when page is scrolled
     */
    document.body.classList.add('is-fixed', 'is-dimmed');
    this.classList.add('is-smooth');
  }

  transitionClassRemove() {
    /**
     * After opening/closing is done, remove the transition class
     */
    const removeClass = () => {
      this.classList.remove('is-smooth');
    };
    this.addEventListener('transitionend', removeClass, {
      once: true,
    });
  }
}

export const defineMobileHide = () =>
  customElements.define('mobile-hide', MobileHide);
