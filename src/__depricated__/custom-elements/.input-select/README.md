# `<input-select>`

```js
import { defineInputSelect } from '@dsb.dk/designsystem/dist/js/dsb.esm.js'
```

A minimal, but styleable, combination of an html `<select>` and an `<input type="text">`

## Usage

Inside of the video-player-element:\
- `<input>`: A normal html input element\
- `data-options`: This elements children acts as the choosable options. Value is determained from the childs value-attr, or its innerHTML
