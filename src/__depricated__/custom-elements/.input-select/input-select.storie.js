import { html } from 'lit';

import readme from './README.md';

export default {
  title: 'Custom Elements/<input-select>',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => html`
  <input-select>
    <input type="text" />

    <ul data-options>
      <li value="option 1">Options 1</li>
      <li value="option 2">Options 2</li>
      <li>Fallback to innerHTML</li>
      <li value="option 4">Options 4</li>
      <li value="option 5">Options 5</li>
      <li value="option 6">Options 6</li>
    </ul>
  </input-select>
`;
