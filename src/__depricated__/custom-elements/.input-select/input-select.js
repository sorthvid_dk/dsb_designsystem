import base from '../.base-template';
import { publish, subscribe, getSubscription } from '../../js/pub-sub';
import dropdownControl from '../../js/dropdown-control';

class InputSelect extends base {
  init() {}

  connectedCallback() {
    const input = this.querySelector('input');
    const options = this.querySelector('[data-options]');

    [...options.children].forEach(child => child.setAttribute('tabindex', '-1'))

    // first one to connect creates a keydown event
    if (!getSubscription('keydown'))
      document.body.addEventListener('keydown', e => publish('keydown', e));

    subscribe('keydown', dropdownControl({ input, options }));
  }
}

export const defineInputSelect = () => customElements.define('input-select', InputSelect);
