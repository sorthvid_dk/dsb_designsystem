import base from '../.base-template';

/**
 * data-toggle: element that will toggle pause/play
 * data-play: element that will play video
 * data-pause: element that will pause video
 */

class VideoPlayer extends base {
  init() {}

  connectedCallback() {
    this.video = this.querySelector('video');
    this.toggleButton = this.querySelector('[data-toggle]');
    this.playButton = this.querySelector('[data-play]');
    this.pauseButton = this.querySelector('[data-pause]');

    this.toggleButton &&
      this.toggleButton.addEventListener('click', this.togglePlay.bind(this));

    this.playButton &&
      this.playButton.addEventListener('click', () => this.video.play());

    this.pauseButton &&
      this.pauseButton.addEventListener('click', () => this.video.pause());

    this.video.addEventListener('play', this.toggleAttribute.bind(this));
    this.video.addEventListener('pause', this.toggleAttribute.bind(this));

    this.toggleAttribute();
  }

  toggleAttribute() {
    this.dataset.status = this.video.paused ? 'paused' : 'playing';
  }

  togglePlay() {
    if (this.video.paused) {
      this.video.play();
    } else {
      this.video.pause();
    }
  }
}

export const defineVideoPlayer = () => customElements.define('video-player', VideoPlayer);
