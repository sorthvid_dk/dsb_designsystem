import { html } from "lit";
import { random } from "Util";

import readme from './README.md';

export default {
  title: 'Custom Elements/<video-player>',
  parameters: {
    readme: {
      sidebar: readme
    }
  }
};

export const Default = () => html`
  <video-player>
    <video>
      <source src="${random("video")}" type="video/mp4" />

      Sorry, your browser doesn't support embedded videos.
    </video>

    <button data-toggle>⏯</button>
    <button data-play>▶️</button>
    <button data-pause>⏸</button>
  </video-player>
`
