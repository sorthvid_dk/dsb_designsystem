# `<video-player>`

```js
import { defineVideoPlayer } from '@dsb.dk/designsystem/dist/js/dsb.esm.js'
```

A container element for playing videos. Adds functionality for toggling play state on and of. Takes a `<video>` element and optional play pause elements.

## Usage

Inside of the video-player-element:\
- `<video>`: A normal html video element\
- `data-toggle`: Will toggle play/pause of the video\
- `data-play`: Plays the video. Element is hidden when video is playing\
- `data-pause`: Pauses the video. Element is hidden when video is paused\
