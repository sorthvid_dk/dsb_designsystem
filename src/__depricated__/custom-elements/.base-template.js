/**
 * Used to extend all other custom elements in the project
 */

export default class HTMLCustomElement extends HTMLElement {
  constructor(...args) {
    const self = super(...args);
    self.init();
    return self;
  }
  init() {
    return true;
  }
}
