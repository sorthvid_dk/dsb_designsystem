import { boolean, text } from '@storybook/addon-knobs';

export const inputText = ({
  id = 'Input Text',
  inputId,
  label,
  placeholder,
} = {}) => ({
  modifiers: '',
  inputId: inputId || 'foo',
  label:
    label !== undefined
      ? label
      : text('Input label', 'Text input with label', id),
  text: text('Input text', '', id),
  placeholder:
    placeholder !== undefined
      ? placeholder
      : text('Input placeholder', 'Write something please', id),
  dropdown: boolean('Dropdown', true, id),
});
