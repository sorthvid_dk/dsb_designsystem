import { InputText } from './input-text.html';
import { inputText } from './input-text.state';
import readme from './README.md';

export default {
  title: 'Components/Input Text',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => InputText(inputText());
