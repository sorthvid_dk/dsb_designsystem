import { html } from "lit";
import { randomId, maybe } from "Util";

import { InputDropdown } from "../.input-dropdown/input-dropdown.html";
import { InputLabel } from "./../.input-label/input-label.html";
import { Text } from "./../.typography/typography.html";
import { mini } from "./../.typography/typography.state";

import { inputText } from "./input-text.state";

const id = randomId();

export const InputText = (
  { modifiers = "", text, label, placeholder, dropdown } = inputText()
) => html`
  <input-select class="input-text ${modifiers}">
    <div class="input-text__inner">
      <!-- note - the empty placeholder is to make targetable by the scss -->
      <input
        type="text"
        id="${id}"
        class="input-text__input"
        value="${text || ""}"
        placeholder=" "
      />
      ${maybe({
        component: InputLabel,
        if: label,
        content: { for: id, text: label },
      })}

      <div class="input-text__placeholder">
        ${Text(mini({ text: placeholder }))}
      </div>

      ${maybe({
        component: InputDropdown,
        if: dropdown,
      })}
    </div>
  </input-select>
`;
