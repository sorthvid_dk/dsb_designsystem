import { html } from 'lit';
import { random } from 'Util'

import { backgroundImage } from "./background-image.state";

export const BackgroundImage = (state = backgroundImage()) => state.padding
? html`<div class="background-image background-image--padding" style="background-image: url(${random('png')});"></div>`
: html`<div class="background-image" style="background-image: url(${random('jpg')});"></div>`;
