import { BackgroundImage as markup } from './background-image.html';
import readme from './README.md';

export default {
  title: 'Components/Background Image',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const BackgroundImage = () => markup();
