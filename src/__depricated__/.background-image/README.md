# Background image

A way of showing an image by defining it as an CSS background image.


## Options
- *Padding* - Adds an margin around the image, and makes it non-cropped.
  - Adds `.background-image--padding` to the `<div>` holding the image.


## todo
- add placement controls - `.ne`, `.nw`, `.se`, `.sw`?
- 'merge' this component with 'Image' - maybe


### changelog
