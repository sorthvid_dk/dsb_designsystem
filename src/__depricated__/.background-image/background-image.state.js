import { boolean } from '@storybook/addon-knobs'

export const backgroundImage = ({ id = 'Background image', padding } = {}) => ({
  padding:
    padding !== undefined ? padding : boolean(`BG Image padding`, false, id)
});
