import { article } from '../article/article.state';

export const collapseGrid = ({ id = 'Collapse Grid' } = {}) => ({
  modifiers: '',
  contentWidth: 'col-24 col-s-22 col-m-20',
  article: article({
    id,
    modifiers: 'collapse-grid__expandable',
    deckModifiers: 'deck--collapse bg--grey',
  }),
});
