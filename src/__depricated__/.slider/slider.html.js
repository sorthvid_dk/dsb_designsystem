import { html } from "lit";
import { getIcon, repeat } from "Util";

import { Deck } from "../.deck/deck.html";
import { CenterHeadline } from "../center-headline/center-headline.html";
import "../image-link/image-link";
import { Link } from "../.link/link.html";

import arrow from "Svg/arrow--right.svg";

import { slider } from "./slider.state";

export const Slider = ({ modifiers = "", deck } = slider()) => html`
  ${Deck({
    state: deck,
    content: html`
      <slide-show class="slider ${modifiers}" data-narrow>
        ${CenterHeadline()}
        <div class="slider__content" data-content>
          ${repeat(
            8,
            () => html` <div data-item><dsb-image-link></dsb-image-link></div> `
          )}
        </div>
        <div class="slider__ui" data-ui>
          <div class="slider__buttons">
            <button class="supportive bg--blue" data-prev>
              ${getIcon(arrow)}
            </button>
            <button class="supportive bg--blue" data-next>
              ${getIcon(arrow)}
            </button>
          </div>
          <div class="slider__link">${Link()}</div>
        </div>
      </slide-show>
    `,
  })}
`;
