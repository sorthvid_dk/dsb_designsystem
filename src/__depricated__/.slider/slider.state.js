// import { boolean } from '@storybook/addon-knobs';
import { deck } from '../.deck/deck.state';
import { imageLink } from '../image-link/image-link.state';

export const slider = ({ id = 'Slider' } = {}) => ({
  modifiers: '',
  deck: deck({ id, modifiers: 'deck--fullscreen', contentWidth: ' ' }),
  imageLink: () => imageLink({ id, badge: false }),
});
