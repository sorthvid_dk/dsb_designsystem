import { Slider } from './slider.html';
import readme from './README.md';

export default {
  title: 'Components/Slider',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => Slider();
