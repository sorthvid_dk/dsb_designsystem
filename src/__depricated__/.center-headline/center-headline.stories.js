import { CenterHeadline } from './center-headline.html';
import readme from './README.md';

export default {
  title: 'Components/Center Headline',
  parameters: {
    readme: {
      sidebar: readme,
    },
  },
};

export const Default = () => CenterHeadline();
