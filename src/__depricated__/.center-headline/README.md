# Center Headline

[about this component]


## Composed of:
- [child components]


## Options
- *[option name]* - [option description]
  - [change in markup]


### changelog
- **[date]**
  - [change made]
