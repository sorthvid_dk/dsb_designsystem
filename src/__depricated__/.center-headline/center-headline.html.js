import { html } from "lit";
import { random } from "Util";

import { Text } from "../.typography/typography.html";

// import state from './center-headline.state'

export const CenterHeadline = () => html`
  <div class="center-headline">
    ${Text({ type: "headline-5", text: random("paragraph") })}
  </div>
`;
