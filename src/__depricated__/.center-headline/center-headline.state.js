import { boolean } from '@storybook/addon-knobs';

export const centerHeadline = ({ id = 'Center Headline', overwrite /* ev. overwrites */ } = {}) => ({
  modifiers: '',
  knob: (overwrite !== undefined) ? overwrite : boolean('Knob', true, id)
});
