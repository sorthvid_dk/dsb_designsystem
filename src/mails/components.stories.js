import layout from './components/layout.html';
import FullTextHtml from './components/fulltext.html';
import SplitTextRightHtml from './components/split-text-right.html';
import SplitTextLeftHtml from './components/split-text-left.html';
import ArticleHtml from './components/article.html';
import ArticleListHtml from './components/article-list.html';
import BannerHtml from './components/banner.html';
import MediaHtml from './components/media.html';
import VideoHtml from './components/video.html';
import CardListHtml from './components/card-list.html';
import UspGridHtml from './components/usp-grid.html';
import TeaserGridHtml from './components/teaser-grid.html';
import FooterHtml from './components/footer.html';
import defaultState from './state';
import readme from './README.md';
// import './misc/mail-styles.css';
export default {
  title: 'Mails/Components',
  parameters: {
    knobs: {
      timestamps: true,
      escapeHTML: false,
    },
    readme: {
      sidebar: readme,
    }

  }

};
const standardState = ()=>defaultState();

export const DefaultHeader = () => layout(
  [],
  standardState()
);

export const ServiceHeader = () => layout(
  [],
  standardState()
);
export const FullText = () => layout(
  [FullTextHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const SplitTextRight = () => layout(
  [SplitTextRightHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const SplitTextLeft = () => layout(
  [SplitTextLeftHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const Article = () => layout(
  [ArticleHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const ArticleList = () => layout(
  [ArticleListHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const Banner = () => layout(
  [BannerHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const Media = () => layout(
  [MediaHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const Video = () => layout(
  [VideoHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const CardList = () => layout(
  [CardListHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const UspGrid = () => layout(
  [UspGridHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const TeaserGrid = () => layout(
  [TeaserGridHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
export const Footer = () => layout(
  [FooterHtml],
  {...standardState(), showHeader: false, showFooter: false}
);
