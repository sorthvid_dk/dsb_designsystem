# Emails

## Headers
Divider should be added to headers with hero that have colored backgrounds.

## Images
All images should be served from DSB's own CDN therefore all image src starting with https://cdn.sorthvid.design or https://i.imgur.com should be changed.
