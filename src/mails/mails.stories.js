import layout from './components/layout.html';
import defaultState, { TYPE_SERVICE, TYPE_STANDARD, TYPE_TRAFFIC } from './state';
import FullTextHtml from './components/fulltext.html';
import SplitTextLeftHtml from './components/split-text-left.html';
import ArticleHtml from './components/article.html';
import VideoHtml from './components/video.html';
import readme from './README.md';
// import './misc/mail-styles.css';

const colors = {
  white: '#fff',
  blue: '#041231',
  red: '#b41730',
}


export default {
  title: 'Mails/Emails',
  parameters: {
    readme: {
      sidebar: readme,
    },
  }
};
const standardState = ()=>({...defaultState({
  fullText: {
    type: 'H2'
  },
  article: {
    bg: colors.red,
    ctaType: 'link',
  }
}), type: TYPE_STANDARD});
export const Standard = () => layout(
  [
    FullTextHtml,
    VideoHtml,
    SplitTextLeftHtml,
    ArticleHtml
  ],
  {...standardState(), showHeader: true, showFooter: true},
);


const trafficState = ()=>({...defaultState(), type: TYPE_TRAFFIC});
export const Traffic = () => layout(
  [

  ],
  {...trafficState(), showHeader: true},
  );

const serviceState = ()=>({...defaultState(), type: TYPE_SERVICE});
export const Service = () => layout(
  [

  ],
  {...serviceState(), showHeader: true},
);
