import { html } from 'lit';

import { unsafeHTML } from 'lit/directives/unsafe-html';

/**
 * @param text {string} - knobs text
 * @param iconSrc {string} - absolute src to icon
 * @param color {string} - text color
 */

export default (text = 'Køb billet', iconSrc, color='#000') => html`
  <tr>
    <td class="h-u" style="font:700 14px/27px Helvetica Neue, Helvetica, Arial, sans-serif; color:${color};">
      <a style="color:${color}; text-decoration:none;" href="http://link">
        ${unsafeHTML(text)}
        <span style="display:inline-block; vertical-align:-1px;">
          <img src="${iconSrc}" width="24" style="width:24px; display:block; vertical-align:top;" alt="&rarr;" />
        </span>
      </a>
    </td>
  </tr>`
