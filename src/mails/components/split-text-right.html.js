import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import Button from './button.html';
import ButtonLink from './button-link.html';
import defaultState from '../state';

export default (state = defaultState()) => html`
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td style=${styleMap({background: state.splitText.backgroundColor})}>
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <th class="tflex" width="300" align="left">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="wi-100p" align="center">
                    <img src="/images/mails/img-01.jpg" width="300" style="width:300px; vertical-align:top;" alt="" />
                  </td>
                </tr>
              </table>
            </th>
            <th class="tflex" align="left">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="pt-26 plr-20 pb-34" style="padding:20px 30px;">
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="h-u pb-9" style="padding:0 0 12px; font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                          <a style="text-decoration:none; color:#b41730;" href="http://link">Tilbud til dig</a> i 7-Eleven på DSB's stationer
                        </td>
                      </tr>
                      <tr>
                        <td class="pb-13" style="padding:0 0 17px; font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                          Frem til 11. oktober kan alle med DSB Plus købe en lille kop filterkaffe til kun 10 kr. (normalpris 15&nbsp;kr.).
                        </td>
                      </tr>
                      ${state.splitText.cta === 'button' ? Button('solid', state.splitText.linkText, 'btn-03') : ''}
                      ${state.splitText.cta === 'link' ? ButtonLink(state.splitText.linkText, state.icons.arrow[state.type]) : ''}
                    </table>
                  </td>
                </tr>
              </table>
            </th>
          </tr>
        </table>
      </td>
    </tr>
  </table>
`;
