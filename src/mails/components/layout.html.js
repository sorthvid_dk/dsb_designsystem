import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import StandardHead from './mail-header.html';
import ServiceHead from './service-header.html';
import Footer from './footer.html';
import { TYPE_SERVICE, TYPE_TRAFFIC, TYPE_STANDARD } from '../state';
// import '../misc/mail-styles.css';

export const eachComponent = (components, state)=>{
  if(components.length===0) return '';
  return components.map(c=>c(state));
}

function renderHeader(state) {
  if (state.type === TYPE_STANDARD || state.type === TYPE_TRAFFIC) {
    return StandardHead(state);
  }
  if (state.type === TYPE_SERVICE) {
    return ServiceHead(state);
  }
}

export default (children=[], state={}) => html`
  <div style=${styleMap({...state.bodyStyle, background: state.bodyBackground})}>
    <table width="100%" style=${styleMap({ minWidth:'320px', background: state.bodyBackground })} cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table class="w-100p" width="600" align="center" style="background:#fffffe; max-width:600px;" cellpadding="0" cellspacing="0">
						<tr>
							<td>

                ${state.showHeader ? renderHeader(state) : ''}
                ${eachComponent(children, state)}
                ${state.showFooter ? Footer(state) : ''}
              </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="hm" style="line-height:0;"><div style="white-space:nowrap; font:15px/0 courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
			</tr>
		</table>
  </div>
`;
