import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
// import Button from './button.html';
//import ButtonLink from './button-link.html';
import defaultState from '../state';
import { TYPE_TRAFFIC, TYPE_STANDARD, CHECKMARKS, BULLITS, ORDERED } from '../state';

const bullit = (text, color) => html`<tr>
  <td style="padding:0 0 10px;">
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" width="24" style="font:18px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
          &bull;
        </td>
        <td style="font:300 14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:${color};">
          ${text}
        </td>
      </tr>
    </table>
  </td>
</tr>`;
const checkmark = (text, color) => html`<tr>
<td style="padding:0 0 10px;">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="top" width="32">
        <img src="https://cdn.sorthvid.design/images/ico-02.png" width="22" style="width:22px; vertical-align:top;" alt="" />
      </td>
      <td style="font:300 14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:${color};">
        ${text}
      </td>
    </tr>
  </table>
</td>
</tr>`;
const ordered = (text, color, index) => html`<tr>
<td style="padding:0 0 14px;">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="top" width="42">
        <div style="margin:2px 0 0;">
          <img src="https://cdn.sorthvid.design/images/ico-0${index+3}.png" width="22" style="width:22px; font:20px/22px Arial, Helvetica, sans-serif; color:#b41730; vertical-align:top;" alt="${index}" />
        </div>
      </td>
      <td style="font:300 14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:${color};">
        ${text}
      </td>
    </tr>
  </table>
</td>
</tr>`;

export default (state = defaultState()) => {
  const style = {
    padding: '35px 30px',
    background: state.articleList.background,
  };

  const textColorType = state.articleList.background === '#fff' ? TYPE_STANDARD : TYPE_TRAFFIC;
  return html`
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="plr-20 pb-25 ${textColorType === TYPE_TRAFFIC ? 'l-fff' : ''}" style=${styleMap(style)}>
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <th class="tflex" width="180" align="left" style="vertical-align:top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="font:700 17px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:${state.colors[textColorType].text};">
                      ${state.articleList.title}
                    </td>
                  </tr>
                </table>
              </th>
              <th class="tflex" width="30" height="10"></th>
              <th class="tflex" align="left" style="vertical-align:top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  ${state.articleList.items.split(',').map((item, index) => {
                    if (state.articleList.type === BULLITS) {
                      return bullit(item, '#fff')
                    }
                    if (state.articleList.type === CHECKMARKS) {
                      return checkmark(item, '#232323')
                    }
                    if (state.articleList.type === ORDERED) {
                      return ordered(item, '#232323', index)
                    }

                  })}
                </table>
              </th>
            </tr>
          </table>
        </td>
      </tr>
    </table>`;
}
