import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import defaultState from '../state';


export default (state = defaultState()) => {
  const isGrey = state.cardList.background === 'grey';
  const style = {
    padding: '35px 30px',
    background: isGrey ? '#eeeff0' : '',
  };
  const lineItems = state.cardList.items.split(',').map(item=>({
    from: item.split(':')[0],
    to: item.split(':')[1],
    price: item.split(':')[2],
  }));
  return html`
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="plr-20" style=${styleMap(style)}>
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="plr-20 pb-9" align="center" style="padding:0 30px 6px; font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                Tag afsted til julemarkeder i&nbsp;tyskland
              </td>
            </tr>
            <tr>
              <td class="plr-20 pb-18" align="center" style="padding:0 30px 32px; font:300 15px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                Vælg den rejseperiode, der passer dig bedst og book online&nbsp;til:
              </td>
            </tr>
            ${lineItems.map(item => html`
              <tr>
                <td style="padding:0 0 10px;">
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="pt-15 plr-20 pb-18" style="background:${isGrey ? '#fffffe' : '#f0f1f2'}; padding:21px 30px; border-radius:5px;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <table cellpadding="0" cellspacing="0">
                                <tr>
                                  <td style="font:700 17px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
                                    <a style="color:#000; text-decoration:none; display:block;" href="http://link">
                                      ${item.from} <span class="d-b"><span class="hm">&nbsp;</span><span style="display:inline-block; vertical-align:1px;"><img src="/images/mails/ico-10.png" width="20" style="width:20px; display:block; vertical-align:top;" alt="" /></span>&nbsp; ${item.to}</span>
                                    </a>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td class="w-10" width="15"></td>
                            <td align="right">
                              <table cellpadding="0" cellspacing="0">
                                <tr>
                                  <td style="padding:1px 0 0;">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td align="right" style="font:13px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
                                          Fra
                                        </td>
                                        <td width="16"></td>
                                        <td align="right" style="font:700 22px/27px Helvetica Neue, Helvetica, Arial, sans-serif; color:#b41730;">
                                          ${item.price},-
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>`
            )}

            <tr>
              <td style="padding:20px 0 5px;">
                <table align="center" style="border-radius:5px; overflow:hidden;" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="active-t btn-03" align="center" style="background:#b41730; mso-padding-alt:5px 36px 11px; font:700 14px/27px Helvetica Neue, Helvetica, Arial, sans-serif;">
                      <a style="color:#fff; text-decoration:none; display:block; padding:6px 36px;" href="http://link">Køb billet</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  `;
}
