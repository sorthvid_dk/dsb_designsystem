import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import Button from './button.html';
import ButtonLink from './button-link.html';
import defaultState, { TYPE_TRAFFIC, TYPE_STANDARD } from '../state';

export default (state = defaultState()) => {
  const style = {
    padding: '35px 30px',
    background: state.article.background,
  };

  const textColorType = state.article.background === '#fff' ? TYPE_STANDARD : TYPE_TRAFFIC;
  return html`
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="plr-20 ${textColorType === TYPE_TRAFFIC ? 'l-fff' : ''}" style=${styleMap(style)}>
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <th class="tflex" width="180" align="left" style="vertical-align:top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="font:700 17px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:${state.colors[textColorType].text};">
                      ${state.article.title}
                    </td>
                  </tr>
                </table>
              </th>
              <th class="tflex" width="30" height="10"></th>
              <th class="tflex" align="left" style="vertical-align:top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="pb-14" style="padding:0 0 20px; font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:${state.colors[textColorType].textSecondary};">
                      Vi sender denne mail, fordi du har købt billetter til en rejse, som kan blive berørt af sporarbejdet. På din DSB Plus profil har du mulighed for at oplyse en startstop-station og modtage information, når der er planlagte ændringer på&nbsp;din&nbsp;rejse.
                    </td>
                  </tr>
                  ${state.article.ctaType === 'button' ? Button('', state.article.linkText, 'btn-01') : ''}
                  ${state.article.ctaType === 'link' ? ButtonLink(state.article.linkText, state.icons.arrow[textColorType === TYPE_STANDARD ? TYPE_STANDARD : 'white'], textColorType === TYPE_STANDARD ? '#000' : '#fff') : ''}

                </table>
              </th>
            </tr>
          </table>
        </td>
      </tr>
    </table>`;
}
