import { html } from "lit";
import Hero from "./hero.html";
// import { eachComponent } from './layout.html';

// import Deck from './../.deck/deck.html'
// import Headline from './../typography/headline-5/headline-5.html'
// import List from './../list/list.html'

import defaultState, { TYPE_STANDARD } from '../state'

export default (state = defaultState()) => {
  return html`
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="l-fff plr-15" style="background:${state.colors[state.type].primary}; padding:6px 20px 7px 15px;">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <div style="margin:2px 0 0;">
                <a style="text-decoration:none;" href="${state.header.logoHref}">
                  <img src="${state.images[state.type].logo}" width="38" style="width:38px; font:20px/22px Arial, Helvetica, sans-serif; color:#fff; vertical-align:top;" alt="DSB" />
                </a>
              </div>
            </td>
            <td width="20"></td>
            <td>
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="right" style="font:11px/13px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                    ${state.user}
                  </td>
                </tr>
                <tr>
                  <td align="right" style="font:700 11px/13px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                    ${state.header.points}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    ${state.header.divider
      ?
      html`<tr>
          <td class="wi-100p" style="line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
            <img src="${state.type==TYPE_STANDARD ? '/images/mails/divider-1.png' : '/images/mails/divider-2.png'}" width="600" style="width:600px; vertical-align:top;" alt="" />
          </td>
        </tr>`
      :
      ''
    }
    ${state.header.hero
      ?
      Hero()
      :
      ''
    }
  </table>
`;
  }
