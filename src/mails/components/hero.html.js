import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import Button from './button.html'
import defaultState from '../state';

const images = {
  'small': {
    img: 'https://i.imgur.com/dRAlzpB.png',
    height: '226',
    pClass: 'pb-18p',
    className: 'bg-01-m',
  },
  'smallsize': {
    img: 'https://via.placeholder.com/600x226',
    height: '226',
    pClass: 'pb-18p',
    className: 'bg-01-m',
  },
  'large': {
    img: 'https://i.imgur.com/fmYWUAg.png',
    height: '285',
    pClass: 'pb-40',
    className: 'bg-02-m',
  },
  'largesize': {
    img: 'https://via.placeholder.com/600x285',
    height: '285',
    pClass: 'pb-40',
    className: 'bg-02-m',
  },
  'redsolidsmall': {
    img: 'https://i.imgur.com/2U2TiUO.png',
    height: '233',
    pClass: 'pb-10p l-fff',
    className: 'bg-03-m',
  },
  'redsolidlarge': {
    img: 'https://i.imgur.com/Gd4yye4.png',
    height: '306',
    pClass: 'pb-10p l-fff',
    className: 'bg-04-m',
  },
  'bluesolidsmall': {
    img: 'https://i.imgur.com/vz6crN5.png',
    height: '202',
    pClass: 'plr-20 pb-10p',
    className: 'bg-05-m',
  },
};
const titleStyle = (state) => {
  return {
    padding: state.hero.trumpet === "None" ? '33px 0 8px' : '0 0 8px',
    font: '700 28px/33px Helvetica Neue, Helvetica, Arial, sans-serif',
    color: '#fff'
  };
}
export default (state = defaultState()) => html`
  <tr data-dimentions="{height: ${images[state.hero.backgroundImage.toLowerCase()].height}}">
    <td class="${images[state.hero.backgroundImage.toLowerCase()].className}" bgcolor="#fffffe" background="${images[state.hero.backgroundImage.toLowerCase().img]}" style="background-image:url(${images[state.hero.backgroundImage.toLowerCase()].img}); background-repeat:no-repeat; background-position:center top;">
      <!--[if gte mso 9]>
        <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="display:inline-block; width:600px; height:226px;" src="${images[state.hero.backgroundImage.toLowerCase()].img}" />
          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="display:inline-block; position:absolute; width:600px; height:226px;">
            <v:fill opacity="0%" color="#fffffe" />
            <v:textbox style="padding-top:0; padding-right:0; padding-bottom:0; padding-left:0;">
              <![endif]-->
              <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="hm" width="40" height="${images[state.hero.backgroundImage.toLowerCase()].height}"></td>
                    <td class="plr-20 ${images[state.hero.backgroundImage.toLowerCase()].pClass}" valign="top">
                      <table class="maxw-225" width="280" cellpadding="0" cellspacing="0">
                      ${state.hero.trumpet && state.hero.trumpet.label === 'DSB Plus'
                      ?
                      html`
                        <tr>
                          <td style="padding:28px 0 3px; line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                            <img src="https://cdn.sorthvid.design/images/text-01.png" width="50" style="width:50px; font:12px/14px Arial, Helvetica, sans-serif; color:#fff; vertical-align:top;" alt="DSB Plus" />
                          </td>
                        </tr>`
                      :
                      ''}
                      ${state.hero.trumpet && state.hero.trumpet.label === 'DSB Trafficinfo'
                      ?
                      html`
                        <tr>
                          <td style="padding:28px 0 3px; line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                            <img src="https://cdn.sorthvid.design/images/text-02.png" width="99" style="width:99px; font:12px/14px Arial, Helvetica, sans-serif; color:#fff; vertical-align:top;" alt="Trafikinformation" />
                          </td>
                        </tr>`
                      :
                      ''}
                      ${state.hero.title
                      ?
                      html`
                        <tr>
                          <td class="pt-14p" style="${styleMap(titleStyle(state))}">
                            ${state.hero.title}
                          </td>
                        </tr>`
                      : ''}
                      ${state.hero.manchet
                      ?
                      html`
                        <tr>
                          <td class="pb-29" style="padding:0 0 33px; font:300 15px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                            Rejs lørdag og søndag i hele landet for&nbsp;max.&nbsp;99,-
                          </td>
                        </tr>`
                        :
                        ''}
                        ${state.hero.button.length
                        ?
                          Button(state.hero.buttonOpaque || state.hero.backgroundImage.toLowerCase().indexOf('solid')===-1 ? 'solid' : '')
                        :
                          ''}
                      </table>
                    </td>
                    <td class="hm" width="40"></td>
                  </tr>
                </table>
              <!--[if gte mso 9]>
            </v:textbox>
          </v:rect>
        </v:image>
      <![endif]-->
    </td>
  </tr>
`;
