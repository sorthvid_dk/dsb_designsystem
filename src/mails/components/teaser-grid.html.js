import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import ButtonLink from './button-link.html';
import defaultState from '../state';


export default (state = defaultState()) => {
  const style = {
    padding:'35px 30px 42px',
    background: state.teaserGrid.background === 'grey' ? '#eeeff0' : '',
  };
  return html`
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="plr-20" style=${styleMap(style)}>
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="plr-20 pb-32" align="center" style="padding:0 30px 37px; font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                Tag på Interrail og oplev&nbsp;verden
              </td>
            </tr>
            <tr>
              <td class="pb-37" style="padding:0 0 39px;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <th class="tflex" width="255" align="left" style="vertical-align:top;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="wi-100p pb-11" align="center" style="padding:0 0 10px;">
                            <img src="https://cdn.sorthvid.design/images/img-04.jpg" width="255" style="width:255px; vertical-align:top;" alt="" />
                          </td>
                        </tr>
                        ${ButtonLink(state.teaserGrid.ctaText, 'https://cdn.sorthvid.design/images/ico-01.png')}
                        ${state.teaserGrid.bodyText
                          ?
                          html`<tr>
                          <td style="font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                            ${state.teaserGrid.bodyText}
                          </td>
                        </tr>`
                          :
                          ''
                        }
                      </table>
                    </th>
                    <th class="tflex" width="30" height="37"></th>
                    <th class="tflex" align="left" style="vertical-align:top;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="wi-100p pb-11" align="center" style="padding:0 0 10px;">
                            <img src="https://cdn.sorthvid.design/images/img-05.jpg" width="255" style="width:255px; vertical-align:top;" alt="" />
                          </td>
                        </tr>
                        ${ButtonLink(state.teaserGrid.ctaText, 'https://cdn.sorthvid.design/images/ico-01.png')}
                        ${state.teaserGrid.bodyText
                          ?
                          html`<tr>
                          <td style="font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                            ${state.teaserGrid.bodyText}
                          </td>
                        </tr>`
                          :
                          ''
                        }
                      </table>
                    </th>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="pb-37" style="padding:0 0 39px;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <th class="tflex" width="255" align="left" style="vertical-align:top;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="wi-100p pb-11" align="center" style="padding:0 0 10px;">
                            <img src="https://cdn.sorthvid.design/images/img-06.jpg" width="255" style="width:255px; vertical-align:top;" alt="" />
                          </td>
                        </tr>
                        ${ButtonLink(state.teaserGrid.ctaText, 'https://cdn.sorthvid.design/images/ico-01.png')}
                        ${state.teaserGrid.bodyText
                          ?
                          html`<tr>
                          <td style="font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                            ${state.teaserGrid.bodyText}
                          </td>
                        </tr>`
                          :
                          ''
                        }
                      </table>
                    </th>
                    <th class="tflex" width="30" height="37"></th>
                    <th class="tflex" align="left" style="vertical-align:top;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="wi-100p pb-11" align="center" style="padding:0 0 10px;">
                            <img src="https://cdn.sorthvid.design/images/img-07.jpg" width="255" style="width:255px; vertical-align:top;" alt="" />
                          </td>
                        </tr>
                        ${ButtonLink(state.teaserGrid.ctaText, 'https://cdn.sorthvid.design/images/ico-01.png')}
                        ${state.teaserGrid.bodyText
                          ?
                          html`<tr>
                          <td style="font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                            ${state.teaserGrid.bodyText}
                          </td>
                        </tr>`
                          :
                          ''
                        }
                      </table>
                    </th>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="pt-8" style="padding:6px 0 0;">
                <table align="center" style="border-radius:5px; overflow:hidden;" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="active-t btn-01" align="center" style="background:#b41730; mso-padding-alt:6px 26px 12px; font:700 14px/27px Helvetica Neue, Helvetica, Arial, sans-serif;">
                      <a style="color:#fff; text-decoration:none; display:block; padding:7px 26px;" href="http://link">${state.teaserGrid.buttonText}</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>`;
}
