import { html } from 'lit';
import defaultState from '../state';
import Button from './button.html'
import ButtonLink from './button-link.html'

const H1 = (text='Lorem ipsum dorum lut')=>html`<td class="pr-30" style="padding:0 90px 15px 0; font:700 28px/33px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
  ${text}
</td>`;
const H2 = (text='Lorem ipsum dorum lut')=>html`<td class="pr-30" style="padding:0 0 15px; font:700 17px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
  ${text}
</td>`;

export default (state = defaultState()) => html`
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="plr-20" style="padding:35px 80px;">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            ${state.fullText.type === 'H1'
              ?
              H1(state.fullText.title)
              :
              H2(state.fullText.title)
            }

          </tr>
          <tr>
            <td style="padding:0 0 18px; font:300 15px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit. <a style="text-decoration:underline; color:#b41730;" href="http://link">Få overblik over ændringerne</a>
            </td>
          </tr>
          ${state.fullText.cta === 'button' ? Button('solid', state.fullText.linkText, 'btn-03') : ''}
          ${state.fullText.cta === 'link' ? ButtonLink(state.fullText.linkText, state.icons.arrow[state.type]) : ''}
        </table>
      </td>
    </tr>
  </table>
`;
