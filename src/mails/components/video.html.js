import { html } from 'lit';
import defaultState from '../state';

export default (state = defaultState()) => html`
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="pt-35 plr-20 pb-40" style="background:#eeeff0; padding:45px 30px 40px;">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="plr-20 pb-13" align="center" style="padding:0 30px 11px; font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
              ${state.video.title}
            </td>
          </tr>
          <tr>
            <td class="plr-0 pb-33" align="center" style="padding:0 30px 30px; font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
            ${state.video.text}
            </td>
          </tr>
          <tr>
            <td class="wi-100p active-i" align="center">
              <a style="text-decoration:none;" href="http://link">
                <img src="/images/mails/img-03.jpg" width="540" style="width:540px; font:20px/22px Arial, Helvetica, sans-serif; color:#000; vertical-align:top;" alt="PLAY" />
              </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>`;
