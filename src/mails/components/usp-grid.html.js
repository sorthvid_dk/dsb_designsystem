import { html } from 'lit';
import { styleMap } from 'lit/directives/style-map';
import defaultState from '../state';


export default (state = defaultState()) => {
  const style = {
    padding: '35px 0 0',
    background: state.uspGrid.background === 'grey' ? '#eeeff0' : '',
  };
  return html`
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td style=${styleMap(style)}>
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="plr-20 pb-9" align="center" style="padding:0 30px 9px; font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                Fordele med DSB Plus
              </td>
            </tr>
            <tr>
              <td class="plr-20 pb-30" align="center" style="padding:0 80px 28px; font:300 14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
              </td>
            </tr>
            <tr>
              <td style="border-top:1px solid #d8d8d8; border-bottom:1px solid #d8d8d8;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="w-50p" valign="top" width="299" style="border-right:1px solid #d8d8d8;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="ptb-37 plr-20" style="padding:39px 35px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="pb-15" style="padding:0 0 8px;">
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="center" height="50">
                                        <img src="https://cdn.sorthvid.design/images/ico-11.png" width="50" style="width:50px; vertical-align:top;" alt="" />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td class="h-u" align="center" style="font:14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
                                  <a style="text-decoration:none; color:#000;" href="http://link">Undgå kontrolafgiften</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td valign="top">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="ptb-37 plr-20" style="padding:39px 35px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="pb-15" style="padding:0 0 8px;">
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="center" height="50">
                                        <img src="https://cdn.sorthvid.design/images/ico-12.png" width="50" style="width:50px; vertical-align:top;" alt="" />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td class="h-u" align="center" style="font:14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
                                  <a style="text-decoration:none; color:#000;" href="http://link">Først besked om DSB Orange udsalg</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="border-bottom:1px solid #d8d8d8;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="w-50p" valign="top" width="299" style="border-right:1px solid #d8d8d8;">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="ptb-37 plr-20" style="padding:39px 35px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="pb-15" style="padding:0 0 8px;">
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="center" height="50">
                                        <img src="https://cdn.sorthvid.design/images/ico-13.png" width="50" style="width:50px; vertical-align:top;" alt="" />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td class="h-u" align="center" style="font:14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
                                  <a style="text-decoration:none; color:#000;" href="http://link">Trafikinformation ved ændringer på din&nbsp;rejse</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td valign="top">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="ptb-37 plr-20" style="padding:39px 35px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="pb-15" style="padding:0 0 8px;">
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="center" height="50">
                                        <img src="https://cdn.sorthvid.design/images/ico-14.png" width="50" style="width:50px; vertical-align:top;" alt="" />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td class="h-u" align="center" style="font:14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#000;">
                                  <a style="text-decoration:none; color:#000;" href="http://link">Få 20% rabat i 7&#65279;-&#65279;Eleven</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  `;
}
