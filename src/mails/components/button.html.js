import { html } from 'lit';

/**
 * @param state {string} - solid will give a red button
 * @param text {string} - text from knobs
 * @param cssClass {string} - button css class
 */

export default (state = 'solid', text = 'Køb billet', cssClass=false) => html`
  <tr>
    <td>
      ${state === 'solid'
        ?
          html`<table style="border-radius:5px; overflow:hidden;" cellpadding="0" cellspacing="0">
            <tr>
              <td class="active-t ${cssClass || 'btn-01'}" align="center" style="background:#b41730; mso-padding-alt:6px 26px 12px; font:700 14px/27px Helvetica Neue, Helvetica, Arial, sans-serif;">
                <a style="color:#fff; text-decoration:none; display:block; padding:7px 26px;" href="http://link">${text}</a>
              </td>
            </tr>
          </table>`
        :
          html`<table style="border:1px solid #fff; border-radius:5px; overflow:hidden;" cellpadding="0" cellspacing="0">
            <tr>
              <td class="active-t ${cssClass || 'btn-01'}" align="center" style="mso-padding-alt:5px 24px 11px; font:700 15px/27px Helvetica Neue, Helvetica, Arial, sans-serif;">
                <a style="color:#fff; text-decoration:none; display:block; padding:6px 24px;" href="http://link">${text}</a>
              </td>
            </tr>
          </table>`
      }
    </td>
  </tr>
`;
