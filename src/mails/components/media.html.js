import { html } from 'lit';
// import { styleMap } from 'lit/directives/style-map';
//import Button from './button.html';
//import ButtonLink from './button-link.html';
import defaultState from '../state';
// import { TYPE_TRAFFIC, TYPE_STANDARD } from '../state';

export default (state = defaultState()) => html`
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="pt-35 plr-20 pb-40" style="padding:45px 30px 50px;">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="plr-20 pb-13" align="center" style="padding:0 30px 19px; font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
              ${state.media.title}
            </td>
          </tr>
          <tr>
            <td class="plr-0 pb-30" align="center" style="padding:0 30px 35px; font:300 15px/23px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
            ${state.media.text}
            </td>
          </tr>
          <tr>
            <td class="wi-100p" align="center">
              <img src="${state.media.image}" width="540" style="width:540px; vertical-align:top;" alt="" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>`;
