import { html } from "lit";

import defaultState from '../state'

export default (state = defaultState()) => {
  return html`<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="bg-07-m" bgcolor="#fffffe" background="https://i.imgur.com/dvWfYyI.png" style="background-image:url(https://i.imgur.com/dvWfYyI.png); background-repeat:no-repeat; background-position:center top;">
        <!--[if gte mso 9]>
          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="display:inline-block; width:600px; height:90px;" src="https://i.imgur.com/dvWfYyI.png" />
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="display:inline-block; position:absolute; width:600px; height:90px;">
              <v:fill opacity="0%" color="#fffffe" />
              <v:textbox style="padding-top:0; padding-right:0; padding-bottom:0; padding-left:0;">
                <![endif]-->
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="hm" width="15" height="90"></td>
                      <td class="l-fff pr-20 pl-15 pb-10p" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td class="pb-0" style="padding:8px 0 9px 0;">
                              <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td>
                                    <a style="text-decoration:none;" href="http://link">
                                      <img src="${state.images[state.type].logo}" width="38" style="width:38px; font:20px/22px Arial, Helvetica, sans-serif; color:#fff; vertical-align:top;" alt="DSB" />
                                    </a>
                                  </td>
                                  <td width="20"></td>
                                  <td>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td align="right" style="font:11px/13px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                                          Louise-Frederikke
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="right" style="font:700 11px/13px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                                          149 point
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="hm" width="20"></td>
                    </tr>
                  </table>
                <!--[if gte mso 9]>
              </v:textbox>
            </v:rect>
          </v:image>
        <![endif]-->
      </td>
    </tr>
  </table>`;
  }
