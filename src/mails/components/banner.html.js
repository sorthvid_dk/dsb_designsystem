import { html } from 'lit';
import defaultState from '../state';
import Button from './button.html'
import ButtonLink from './button-link.html'

export default (state = defaultState()) => html`
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td class="bg-08-m" bgcolor="#fffffe" background="${state.banner.background}" style="background-image:url(${state.banner.background}); background-repeat:no-repeat; background-position:center top;">
        <!--[if gte mso 9]>
          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="display:inline-block; width:600px; height:300px;" src="${state.banner.background}" />
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="display:inline-block; position:absolute; width:600px; height:300px;">
              <v:fill opacity="0%" color="#fffffe" />
              <v:textbox style="padding-top:0; padding-right:0; padding-bottom:0; padding-left:0;">
                <![endif]-->
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="hm" width="40" height="300"></td>
                      ${state.banner.splash ? html`<td class="l-fff pt-8p plr-20 pb-7p" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                          <tr class="thold">
                            <th class="tfoot" width="250" align="left" style="vertical-align:top;">
                              <table class="maxw-280" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td style="padding:48px 0 15px; font:700 28px/33px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                                    ${state.banner.title}
                                  </td>
                                </tr>
                                <tr>
                                  <td style="padding:0 0 19px; font:300 15px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                                    ${state.banner.text}
                                  </td>
                                </tr>
                                ${state.banner.cta === 'button' ? Button('solid', state.banner.ctaText) : ''}
                                ${state.banner.cta === 'link' ? ButtonLink(state.banner.ctaText, '/images/mails/ico-09.png', '#fff') : ''}
                              </table>
                            </th>
                            <th class="hm" width="20"></th>
                            <th class="thead" align="left" style="vertical-align:top;">
                              <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td class="pt-0 pb-14p" align="right" style="padding:40px 0 0;">
                                    <table width="110" style="background:#b41730; border-radius:50%; overflow:hidden;" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td height="110">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td align="center" style="font:300 14px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                                                Til og med
                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="center" style="font:700 20px/26px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                                                06.01.19
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </th>
                          </tr>
                        </table>
                      </td>`
                      :
                      html`<td class="l-fff plr-20 pb-15p" valign="top">
                        <table class="w-100p maxw-280" width="250" cellpadding="0" cellspacing="0">
                          <tr>
                            <td class="pt-83p" style="padding:48px 0 15px; font:700 28px/33px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                            ${state.banner.title}
                            </td>
                          </tr>
                          <tr>
                            <td style="padding:0 0 13px; font:300 15px/22px Helvetica Neue, Helvetica, Arial, sans-serif; color:#fff;">
                            ${state.banner.text}
                            </td>
                          </tr>

                          ${state.banner.cta === 'button' ? Button('solid', state.banner.ctaText) : ''}
                          ${state.banner.cta === 'link' ? ButtonLink(state.banner.ctaText, '/images/mails/ico-09.png', '#fff') : ''}

                        </table>
                      </td>`}
                      <td class="hm" width="20"></td>
                    </tr>
                  </table>
                <!--[if gte mso 9]>
              </v:textbox>
            </v:rect>
          </v:image>
        <![endif]-->
      </td>
    </tr>
  </table>
`;
