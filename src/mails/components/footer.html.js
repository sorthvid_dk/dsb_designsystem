import { html } from 'lit';
// import { styleMap } from 'lit/directives/style-map';
// import ButtonLink from './button-link.html';
import defaultState from '../state';


export default (state = defaultState()) => {

  return html`
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="pt-25" style="background:#eeeff0; padding:30px 0 0;">
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
              <td class="pb-23 plr-20-374" style="padding:0 30px 18px;">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="w-53p w-40p-374" valign="top">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <th class="tflex" width="38" align="left" style="vertical-align:top;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td class="pt-3">
                                  <img src="/images/mails/logo-02.png" width="38" style="width:38px; font:18px/20px Arial, Helvetica, sans-serif; color:#b41730; vertical-align:top;" alt="DSB" />
                                </td>
                              </tr>
                            </table>
                          </th>
                          <th class="tflex" width="16" height="8"></th>
                          <th class="tflex" align="left" style="vertical-align:top;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td style="padding:8px 0 16px; line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                                  <img src="/images/mails/text-03.png" width="115" style="width:115px; font:700 13px/23px Arial, Helvetica, sans-serif; color:#000; vertical-align:top;" alt="${state.footer.title}" />
                                </td>
                              </tr>
                              <tr>
                                <td style="font:300 12px/18px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                                  Telegade 2 <br />
                                  2630 Høje Taastrup
                                </td>
                              </tr>
                            </table>
                          </th>
                        </tr>
                      </table>
                    </td>
                    <td width="10"></td>
                    ${state.footer.usps ? html`<td class="w-a" valign="top" width="210">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td style="padding:2px 0 14px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td valign="top" width="20" style="line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                                  <div style="margin:1px 0 0;">
                                    <img src="/images/mails/ico-15.png" width="20" style="width:20px; vertical-align:top;" alt="" />
                                  </div>
                                </td>
                                <td class="w-5-374" width="9"></td>
                                <td>
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td style="padding:2px 0 0; font:14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                                        Hent DSB App
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding:0 0 14px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td valign="top" width="20" style="line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                                  <div style="margin:1px 0 0;">
                                    <img src="/images/mails/ico-16.png" width="20" style="width:20px; vertical-align:top;" alt="" />
                                  </div>
                                </td>
                                <td class="w-5-374" width="9"></td>
                                <td>
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td style="padding:2px 0 0; font:14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                                        Trafikinformation
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding:0 0 14px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td valign="top" width="20" style="line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                                  <div style="margin:1px 0 0;">
                                    <img src="/images/mails/ico-17.png" width="20" style="width:20px; vertical-align:top;" alt="" />
                                  </div>
                                </td>
                                <td class="w-5-374" width="9"></td>
                                <td>
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td style="padding:2px 0 0; font:14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                                        Dine fordele
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding:0 0 14px;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                              <tr>
                                <td valign="top" width="20" style="line-height:1px; font-size:1px; mso-line-height-rule:at-least;">
                                  <div style="margin:1px 0 0;">
                                    <img src="/images/mails/ico-18.png" width="20" style="width:20px; vertical-align:top;" alt="" />
                                  </div>
                                </td>
                                <td class="w-5-374" width="9"></td>
                                <td>
                                  <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td style="padding:2px 0 0; font:14px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                                        Find billetter
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>`: ''}
                  </tr>
                </table>
              </td>
            </tr>
            ${state.footer.links ? html`<tr>
              <td class="plr-20-374" style="padding:9px 30px; border-top:1px solid #ccc;">
                <table cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="font:300 12px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                      Opdater din profil
                    </td>
                    <td class="w-55 w-30-374" width="65"></td>
                    <td style="font:300 12px/20px Helvetica Neue, Helvetica, Arial, sans-serif; color:#232323;">
                      Afmeld
                    </td>
                  </tr>
                </table>
              </td>
            </tr>`
            : ''}
          </table>
        </td>
      </tr>
    </table>`;
}
