import { boolean, color, text, select } from '@storybook/addon-knobs';

const LAYOUT_ID = 'Main Layout';
const HEADER_ID = 'Header';
const HERO_ID = 'Hero';
const FULL_TEXT_ID = 'Full text';
const SPLIT_TEXT_RIGHT_ID = 'Split text';
const ARTICLE_ID = 'Article';
const ARTICLE_LIST_ID = 'Article list';
const BANNER_ID = 'Banner';
const MEDIA_ID = 'Media';
const VIDEO_ID = 'Video';
const CARD_LIST_ID = 'Card list';
const TEASER_GRID_ID = 'Teaser grid';

export const TYPE_STANDARD = 'standard';
export const TYPE_SERVICE = 'service';
export const TYPE_TRAFFIC = 'traffic';

export const BULLITS = 'Bullits';
export const CHECKMARKS = 'Checkmarks (only white bg)';
export const ORDERED = 'Ordered list (only white bg)';

const DSBPlus = {
  label: 'DSB Plus',
  width: 50,
  img: 'https://cdn.sorthvid.design/images/text-01.png'
}
const DSBTrafficInfo = {
  label: 'DSB Trafficinfo',
  id: 1,
  width: 99,
  img: 'https://cdn.sorthvid.design/images/text-02.png'
}


export default (defaults={}) => {

  const fullTextType = defaults.fullText && defaults.fullText.type ? defaults.fullText.type : 'H1';
  const articleBg = defaults.article && defaults.article.bg ? defaults.article.bg : '#fff';
  const articleCtaType = defaults.article && defaults.article.ctaType ? defaults.article.ctaType : 'button';

  return {
    bodyBackground: color('Body Background', '#bebebe', LAYOUT_ID),
    showHeader: boolean('Show header', true, LAYOUT_ID),
    showFooter: boolean('Show footer', true, LAYOUT_ID),
    type: select('Type', {
      'Standard': TYPE_STANDARD,
      'Service': TYPE_SERVICE,
      'Traffic': TYPE_TRAFFIC,
    }, TYPE_STANDARD, LAYOUT_ID),
    bodyStyle: {
      margin: 0,
      padding: 0,
      '-webkit-text-size-adjust': '100%',
      '-ms-text-size-adjust': '100%',
    },
    user: text('Username', "Louise Mathisen", HEADER_ID),
    header: {
      points: text('Points', "149 point", HEADER_ID),
      logoHref: text('Logo link', "https://dsb.dk", HEADER_ID),
      divider: boolean('Divider', true, HEADER_ID),
      hero: boolean('Show hero', true, HEADER_ID),
    },
    footer: {
      title: 'Venlig hilsen DSB',
      links: boolean('Show links', true, 'Footer'),
      usps: boolean('Show USP', true, 'Footer'),
    },
    hero: {
      backgroundImage: select('Background image', [
        'Small',
        'SmallSize',
        'Large',
        'LargeSize',
        'RedSolidSmall',
        'RedSolidLarge',
        'BlueSolidSmall',
      ],
        'Large', HERO_ID),
      trumpet: select('Trumpet', ["None", DSBPlus, DSBTrafficInfo], DSBPlus, HERO_ID),
      title: text('Title', 'Du kan være med til at tage ansvar', HERO_ID),
      manchet: text('Manchet', 'Vi er vel ikke skyld i klima-udfordringerne, men vi tager vores del af ansvaret for at løse dem.', HERO_ID),
      button: text('Button Text', 'Køb billet', HERO_ID),
      buttonLink: text('Button Link', 'https://dsb.dk', HERO_ID),
      buttonOpaque: boolean('Filled button', true, HERO_ID),
    },
    fullText: {
      type: select('Size', ['H1', 'H2'], fullTextType, FULL_TEXT_ID),
      title: text('Heading', 'DSB er på en rejse mod det bæredygtige', FULL_TEXT_ID),
      cta: select('CTA', ['none', 'link', 'button'], 'link', FULL_TEXT_ID),
      linkText: text('Link text', 'Tjek rejsen på Rejseplanen', FULL_TEXT_ID),
    },
    splitText: {
      cta: select('CTA', ['none', 'link', 'button'], 'link', SPLIT_TEXT_RIGHT_ID),
      linkText: text('Link text', 'Tjek rejsen på Rejseplanen', SPLIT_TEXT_RIGHT_ID),
      backgroundColor: select('Background Color', {
        'White': '#ffffff',
        'Grey': '#eeeff0'}, '#ffffff', SPLIT_TEXT_RIGHT_ID),
    },
    article: {
      title: text('Title', 'Er denne information relevant for dig?', ARTICLE_ID),
      linkText: text('Link text', 'Angiv start/stop-station', ARTICLE_ID),
      background: select('Background', {
        'White': '#fff',
        'Blue': '#041231',
        'Red': '#b41730',
      }, articleBg, ARTICLE_ID),
      ctaType: select('CTA type', ['button', 'link'], articleCtaType, ARTICLE_ID),
    },
    articleList: {
      title: text('Title', 'Er denne information relevant for dig?', ARTICLE_LIST_ID),
      background: select('Background', {
        'Hvid': '#fff',
        'Blå': '#041231',
        'Rød': '#b41730',
      }, '#b41730', ARTICLE_LIST_ID),
      items: text('Liste tekst (comma seperaret)', 'Lorem ipsum dolor, sit amet, consectetur', ARTICLE_LIST_ID),
      type: select('Liste type', [BULLITS, CHECKMARKS, ORDERED], BULLITS, ARTICLE_LIST_ID),
    },
    banner: {
      background: select('Background', {
        image: 'https://i.imgur.com/Yjr7Jwn.png',
        size: 'https://via.placeholder.com/600x300'
      }, 'https://i.imgur.com/Yjr7Jwn.png', BANNER_ID),
      title: text('Title', 'Køb din billet til jul og nytår nu', BANNER_ID),
      text: text('Text', 'Salget af togbilletter til jul og nytår er i gang. Du kan købe billetter helt frem til og med d. 6. januar 2019', BANNER_ID),
      splash: boolean('Show splash', true, BANNER_ID),
      cta: select('CTA type', ['button', 'link'], 'button', BANNER_ID),
      ctaText: text('CTA text', 'Køb billet', BANNER_ID),
    },
    media: {
      title: text('Title', 'Kort over S-togsnettet', MEDIA_ID),
      text: text('Text', 'Hos DSB har du mulighed for at lave din egen køreplan. Denne vil løbende blive opdateret i forhold til sporarbejde og andre ændringer. Du er dermed altid på forkant. Du kan til enhver tid finde de aktuelle DSB-køreplaner som PDF.', MEDIA_ID),
      image: select('Image', {
        'image': 'https://cdn.sorthvid.design/images/img-02.png',
        'size': 'https://via.placeholder.com/540x630',
      }, 'https://cdn.sorthvid.design/images/img-02.png', MEDIA_ID),
    },
    video: {
      title: text('Title', 'Kort over S-togsnettet', VIDEO_ID),
      text: text('Text', 'Hos DSB har du mulighed for at lave din egen køreplan. Denne vil løbende blive opdateret i forhold til sporarbejde og andre ændringer. Du er dermed altid på forkant. Du kan til enhver tid finde de aktuelle DSB-køreplaner som PDF.', VIDEO_ID),
    },
    cardList: {
      background: select('Background', ['white', 'grey'], 'white', CARD_LIST_ID),
      items: text('Line Items', 'København:hamburg:223', CARD_LIST_ID),
    },
    uspGrid: {
      background: select('Background', ['white', 'grey'], 'white', 'USP grid'),
    },
    teaserGrid: {
      background: select('Background', ['white', 'grey'], 'white', TEASER_GRID_ID),
      ctaText: text('Link text(rich-text)', 'Elit seddo <span style="color:#b41730;">eius</span> mod <br class="hm"/>tempor', TEASER_GRID_ID),
      bodyText: text('Body Text', 'Køb mad og drikke til togturen i 7-Eleven på DSB’s stationer. Du får 20% rabat på udvalgte varer.', TEASER_GRID_ID),
      buttonText: text('Button text', 'Se alle destinationer', TEASER_GRID_ID),
    },
    images: {
      [TYPE_STANDARD]: {
        logo: 'https://cdn.sorthvid.design/images/logo-01.png',
      },
      [TYPE_SERVICE]: {
        logo: 'https://cdn.sorthvid.design/images/logo-01.png',
      },
      [TYPE_TRAFFIC]: {
        logo: 'https://cdn.sorthvid.design/images/logo-02.png',
      },
    },
    icons: {
      arrow: {
        [TYPE_STANDARD]: 'https://cdn.sorthvid.design/images/ico-01.png',
        [TYPE_SERVICE]: 'https://cdn.sorthvid.design/images/ico-01.png',
        [TYPE_TRAFFIC]: 'https://cdn.sorthvid.design/images/ico-10.png',
        white: 'https://cdn.sorthvid.design/images/ico-09.png'
      }
    },
    colors: {
      [TYPE_STANDARD]: {
        primary: '#b41730',
        text: '#000000',
        textSecondary: '#232323',
      },
      [TYPE_SERVICE]: {
        primary: '#b41730',
        text: '#000000',
        textSecondary: '#232323',
      },
      [TYPE_TRAFFIC]: {
        primary: '#041231',
        text: '#fff',
        textSecondary: '#fff',

      }
    }
  }
};
