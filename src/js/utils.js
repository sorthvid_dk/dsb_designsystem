/**
 * Converts a string to boolean
 * 'false' => false
 * 'true'  => true
 */
export const parseBool = b => !/^(false|0)$/i.test(b) && !!b;

/**
 * Returns true if passed dom element is currently in focus
 */
export const hasFocus = el => el === document.activeElement

/**
 * Converts text that contains text and html tags to a document fragment
 */
export const stringToDocumentFragment = text => {
    var frag = document.createRange().createContextualFragment(text);
    return frag;
}
