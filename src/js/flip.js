// https://css-tricks.com/animating-layouts-with-the-flip-technique/
export const flip = {
  getState(el) {
    const { width, height } = getComputedStyle(el);
    const { left, right, top, bottom } = el.getBoundingClientRect();

    return {
      el,
      width: parseInt(width),
      height: parseInt(height),
      left,
      right,
      top,
      bottom,
    };
  },

  expand(oldState, cb, options = { duration: 0.3 }) {
    requestAnimationFrame(() => {
      const newState = this.getState(oldState.el);
      oldState.el.classList.toggle('flip-is-opening');

      const extractValues = (state) => {
        return Object.entries(state).reduce((acc, [prop, value]) => {
          return {
            ...acc,
            [prop]: typeof value === 'number' ? value + 'px' : value,
          };
        }, {});
      };

      Object.assign(oldState.el.style, extractValues(oldState));

      requestAnimationFrame(() => {
        Object.assign(
          oldState.el.style,
          {
            transitionProperty: 'height',
            transitionDuration: options.duration + 's',
            transitionTimingFunction: 'var(--ease-in-out-quint)',
          },
          extractValues(newState)
        );

        setTimeout(() => {
          newState.el.removeAttribute('style');
          newState.el.classList.toggle('flip-is-opening');
          typeof cb === 'function' && cb();
        }, options.duration * 1000);
      });
    });
  },
};
