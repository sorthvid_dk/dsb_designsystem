import { hasFocus } from '../utils';
import keypress from './keypress';

/**
 * Gives the ability to loop through an input and dropdown list,
 * and change focus with the arrow keys
 */
export default ({ input, options }) => e => {
  /**
   * Only run if the input or dropdown has focus
   */
  if (!hasFocus(input) && ![...options.children].find(hasFocus)) return;

  /**
   * Collect the input and it's dropdown-options in one array
   */
  const collection = [input, ...options.children];

  /**
   * returns the index of the currently focused element in the array
   */
  const getindexWithFocus = (i, el, index) => (i = hasFocus(el) ? index : i);
  const currentlyActive = collection.reduce(getindexWithFocus, 0);

  /**
   * a looping counter that takes the array length in account
   */
  const nextIndex =
    currentlyActive + 1 >= collection.length ? 0 : currentlyActive + 1;
  const prevIndex =
    currentlyActive === 0 ? collection.length - 1 : currentlyActive - 1;

  /**
   * Update dropdown-option and input
   */
  const giveFocus = n => {
    /**
     * Give focus to the new element
     */
    collection[n].focus();

    /**
     * Update the input value to the currently focused option
     */
    const value = collection[n].getAttribute('value') || collection[n].innerHTML
    input.setAttribute('value', value);
    input.innerHTML = value;

    /**
     * If we arrive at the input field, move the caret to after the text
     */
    setTimeout(() => input.setSelectionRange(-1, -1), 0);
  };

  /**
   * Listen for keypresses
   */
  if (keypress(e) === 'ArrowUp') giveFocus(prevIndex);
  if (keypress(e) === 'ArrowDown') giveFocus(nextIndex);
};
