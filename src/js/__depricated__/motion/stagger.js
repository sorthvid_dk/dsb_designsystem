import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

/**
 * removeInlineStyle
 * Run after the the stagger animation is done,
 * reverts the elements to it's previous state.
 */
const removeInlineStyle = ({ from, props }) => {
  if (!from.length) from = [from];

  from.forEach(element => {
    props.forEach(prop => element.style.removeProperty(prop));
  });
};

/**
 * Any element becomes a stagger-element when data-animation="stagger" is applied.
 * By default will every child element with data-stagger-item be treated as a stagger-item,
 * but the parent can also have a custom selector by adding [data-stagger-target=".selector"] attribute.
 *
 * example:
 *
 * <div data-animation="stagger">
 *   <div data-stagger-item></div>
 *   <div data-stagger-item></div>
 * </div>
 *
 * or:
 *
 * <div data-animation="stagger" data-stagger-target=".child">
 *   <div class="child"></div>
 *   <div class="child"></div>
 * </div>
 */
export const stagger = ({ element, scrub = false }) => {
  /**
   * Collect all the stagger items / children
   */
  const items = [
    ...element.querySelectorAll(
      element.dataset.staggerTarget || '[data-stagger-item]'
    )
  ];

  /**
   * Throw warnings if parent element isn't configured right
   */
  if (!items) {
    console.warn(
      `${element} didn't find any children with selector: ${
        element.dataset.staggerTarget
          ? element.dataset.staggerTarget
          : '[data-stagger-item]'
      }`
    );
  }

  /**
   * Setup starting state
   */
  gsap.set(items, {
    opacity: 0,
    x: -50
  });

  /**
   * Setup trigger and after state
   */
  gsap.to(items, {
    scrollTrigger: {
      trigger: items[0],
      start: 'top 90%',
      end: scrub ? 'top 33%' : '',
      scrub: scrub && 0.7
    },
    opacity: 1,
    x: 0,
    duration: 0.7,
    stagger: 0.1,
    ease: 'ease',
    onComplete: () =>
      removeInlineStyle({
        from: items,
        props: ['opacity', 'transform']
      })
  });
};

/**
 * passepartout
 * works much the same as stagger, but animates on scroll,
 * and takes an extra element [data-passepartout]
 */
export const passepartout = ({ element }) => {
  /**
   * Setup the children like normal stagger, but with scrub
   */
  stagger({
    element,
    scrub: true
  });

  /**
   * collect the element that has passepartout
   */
  const passepartoutElement = element.querySelector('[data-passepartout]');

  gsap.set(passepartoutElement, {
    outline: `${window.innerWidth / 20}px solid #ffffff`,
    outlineOffset: `${-window.innerWidth / 20 + 2}px`
  });

  gsap.to(passepartoutElement, {
    scrollTrigger: {
      trigger: passepartoutElement,
      start: 'top bottom',
      end: 'top 33%',
      scrub: 0.7
    },
    outline: '0px solid #ffffff',
    outlineOffset: '2px',
    ease: 'ease',
    onComplete: () =>
      removeInlineStyle({
        from: passepartoutElement,
        props: ['outline', 'outline-offset']
      })
  });
};
