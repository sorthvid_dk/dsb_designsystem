import { stagger, passepartout } from './stagger';
// import CustomEase from './vendor/CustomEase'

// CustomEase.create('dsb-ease-in-out', '0.25, 0.1, 0.25, 1');

export const motion = () => {
  /**
   * Collect all elements that should be animated
   */
  const animatedElements = [...document.querySelectorAll('[data-animation]')];

  /**
   * Dispatch animation on each element
   */
  animatedElements.forEach(element => {
    if (element.dataset.animation === 'stagger') stagger({ element });
    if (element.dataset.animation === 'passepartout') passepartout({ element });
  });
};
