# Animations
[gsap](https://greensock.com/docs/)

GSAP is used as the animation library.\
It has all the functionality we want and a easy to use API.

## Action!

All the animations that can be solved just by CSS, should be.\
The scripts here is meant for elements that's animated when they enter the viewport,\
or animated on scroll. Control that simply can't be achieved be CSS.

## Attributes

There are a few different animations to use across the project,\
and they are added to an element by including the corresponding `[data-animaiton]`-attribute.

Some of the animations might require additional config, you can find this out in each animation's own file.

## Optional

The animations are designed to setup all their own before- and after states,\
without the need for any CSS proberty to be set outside of the animation's own file.

Which means that there should be no problem to add or remove an animation on any component.\
If, by any case, the javascript wouldn't run or the animation would fail to run,\
the user wont be left with a blank screen where an element should have faded in!
