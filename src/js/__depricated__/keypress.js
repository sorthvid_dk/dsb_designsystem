/**
 * Takes a keypress event
 * Returns the name of the pressed key
 */
export default e => {
  // console.log('keypress: ', e)
  return e.key
}
