import ally from 'ally.js';
import getId from '../id';

/**
 * Expandable. Opens and closes a given element, by toggling it's max-height
 * Has two major modes: contained and !contained.
 * Where the first is for having the toggle-button
 * and the toggle content all inside the given element.
 *
 * @param {object}
 * @returns {object} - {open, close, trigger}
 */
export default ({
  id,
  el,
  toggleClass = 'is-open',
  closeOnBodyClick = false,
  disableScroll = false,
  trapFocus = false,

  trigger = null,
  content = null,
  closeButton = null,

  onOpen = () => {},
  onClose = () => {},
} = {}) => {
  if (!el) {
    console.warn(
      'You have tried to initialize expandable.js without passing an element'
    );
  }

  /**
   * Ally, variables
   * To enable ally.js to do it's thing
   */
  let disabledHandle;
  let keyHandle;
  let hiddenHandle;
  let focusedElementBeforeDialogOpened;

  /**
   * separate the parent container and the openable content,
   */
  const parent = el;
  const element = content || el.querySelector('[data-content]');

  const closeButtons = closeButton
    ? [closeButton]
    : [...document.body.querySelectorAll(`[data-expandable-close="${id}"]`)];

  // Toggle button
  const toggleButtons = trigger
    ? [trigger, ...parent.querySelectorAll('[data-trigger]')].filter((e) => e)
    : [...document.body.querySelectorAll(`[data-trigger="${id}"]`)];

  /**
   * Setup initial aria-/html attribute.
   * So we don't have to do it manually each time
   */
  const randomId = getId();
  toggleButtons.forEach((toggle) => {
    toggle.setAttribute('aria-expanded', false);
    toggle.setAttribute('aria-controls', randomId);
  });
  closeButton && closeButton.setAttribute('aria-controls', randomId);
  element.setAttribute('hidden', true);
  element.setAttribute('aria-hidden', true);
  element.id = randomId;

  const open = () => {
    /**
     * Show the dropdown, and disable scrolling of the body
     */
    element.classList.add(toggleClass);
    element.hidden = false;
    element.setAttribute('aria-hidden', false);
    onOpen(element);
    disableScroll && document.body.classList.add('is-fixed');
    /**
     * Make visible to screenreaders
     */
    // openButtons.forEach(el => el.setAttribute('aria-expanded', true));
    closeButtons.forEach((el) => el.setAttribute('aria-expanded', true));
    toggleButtons.forEach((el) => el.setAttribute('aria-expanded', true));

    /**
     * Save reference of the last focused element
     */
    focusedElementBeforeDialogOpened = document.activeElement;

    /**
     * Always give the option to close be pressing escape
     */
    keyHandle = ally.when.key({
      escape: () => setTimeout(close),
    });

    if (trapFocus) {
      // trap focus inside the dropdown
      disabledHandle = ally.maintain.disabled({
        filter: parent,
      });

      // hide the rest of the page to screen readers
      hiddenHandle = ally.maintain.hidden({
        filter: parent,
      });

      // give focus to the first element in the dropdown, or the dropdown itself
      // wait for the dropdown to finish animating
      const giveFocus = () => {
        const el = ally.query.firstTabbable({
          context: parent,
          defaultToContext: true,
        });
        el && el.focus();
      };
      element.addEventListener('transitionend', giveFocus, {
        once: true,
      });
    }

    /**
     * Dropdown should close on click outside of it
     */
    const bodyClick = () => {
      document.body.addEventListener(
        'click',
        (e) => {
          if (e.clientY > element.getBoundingClientRect().bottom) {
            close();
          }
        },
        { once: true }
      );
    };

    if (closeOnBodyClick) {
      element.addEventListener('transitionend', bodyClick, { once: true });
    }
  };

  const close = () => {
    /**
     * Remove open-class
     */
    element.classList.remove(toggleClass);
    onClose(element);
    disableScroll && document.body.classList.remove('is-fixed');
    element.addEventListener(
      'transitionend',
      () => {
        if (
          toggleButtons.length &&
          toggleButtons[0].getAttribute('aria-expanded') === 'false'
        ) {
          element.hidden = true;
          element.setAttribute('aria-hidden', true);
        }
      },
      { once: true }
    );

    /**
     * Tell screen reader that the dropdown isn't visible
     */
    // openButtons.forEach(el => el.setAttribute('aria-expanded', false));
    closeButtons.forEach((el) => el.setAttribute('aria-expanded', false));
    toggleButtons.forEach((el) => el.setAttribute('aria-expanded', false));

    /**
     * Ally, disemble:
     */
    disabledHandle && disabledHandle.disengage();
    keyHandle && keyHandle.disengage();
    hiddenHandle && hiddenHandle.disengage();

    /**
     * Give back focus to last active element
     */
    if (focusedElementBeforeDialogOpened)
      focusedElementBeforeDialogOpened.focus();
  };

  const toggle = () => {
    if (toggleButtons[0].getAttribute('aria-expanded') !== 'false') {
      close();
    } else {
      open();
    }
  };

  // openButtons.forEach(el => el.addEventListener('click', open));
  closeButtons.forEach((el) => el.addEventListener('click', close));
  toggleButtons.forEach((el) => el.addEventListener('click', toggle));

  return { open, close, trigger };
};
