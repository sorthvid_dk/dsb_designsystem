/**
 * Id
 * Generates a random string that's good for id's
 */
export default () =>
  '-' +
  Math.random()
    .toString(36)
    .substr(2, 9);

