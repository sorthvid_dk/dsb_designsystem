# JS

The javascript files in this folder are functions and behaviour that solves a specific purpose.
Generally these sripts are not used directly by a component, but are applied to a custom element that is then used by a component.

## What?

The files in this folder should know what to do! That could for example be changing a style of an element, hiding an element or toggle classing of element.
