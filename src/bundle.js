import * as modules from '@/index'

// this file is bundled into a ready-to-use .js file

Object.values(modules).forEach((module) => {
  if (typeof module === 'function') {
    module();
  }
});
