// import { html } from 'lit';

// import { Header } from '../../components/header/header.html';
// import { header } from '../../components/header/header.state';

// import { Hero } from '../../components/hero/hero.html';
// import { hero } from '../../components/hero/hero.state';

// import { Media } from '../../components/media/media.html';

// import { Article } from '../../components/article/article.html';
// import { article } from '../../components/article/article.state';

// import { Split } from '../../components/split/split.html';
// import { split } from '../../components/split/split.state';

// import { Footer } from '../../components/footer/footer.html';

// export default {
//   title: 'Pages/Campaign S-train',
// };

// export const Default = () => html`
//   <div>
//     ${Header(header({ expanded: false, loggedIn: false }))}
//     ${Hero(hero({ ctaPrice: false, badge: false, noCopy: true }))}
//     ${Article(
//       article({
//         content: { type: 'list' },
//         listStyle: 'list--checked',
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Article(
//       article({
//         color: 'bg--grey',
//         content: { type: 'list', length: 6 },
//         listStyle: 'list--numbered',
//       })
//     )}
//     ${Media({ video: false })}
//     ${Article(
//       article({
//         content: {
//           type: 'text',
//           button: true,
//           buttonStyle: 'button--blue',
//         },
//       })
//     )}
//     ${Article(
//       article({
//         color: 'bg--red',
//         content: { type: 'text', link: true },
//       })
//     )}
//     ${Split(split({ inverted: true, badge: false, link: true }))} ${Footer()}
//   </div>
// `;
