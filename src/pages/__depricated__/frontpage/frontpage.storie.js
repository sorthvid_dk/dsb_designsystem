// import { html } from 'lit';
// import { radios } from '@storybook/addon-knobs';

// import { Header } from 'Components/header/header.html';
// import { Hero } from 'Components/hero/hero.html';
// import { hero } from 'Components/hero/hero.state';
// import { IconLinkRow } from 'Components/icon-link-row/icon-link-row.stories';
// import { FourtySixty } from 'Components/.fourty-sixty/fourty-sixty.html';
// import { Banner } from 'Components/banner/banner.html';
// import { Split } from 'Components/split/split.html';
// import { Footer } from 'Components/footer/footer.html';

// import jpgs from '../../../mock-data/images/jpgs.js';

// export default {
//   title: 'Pages/Frontpage',
// };

// export const Default = () => html`
//   <div>
//     ${Header()}
//     ${Hero(
//       hero({
//         modifiers: radios(
//           `Modifiers`,
//           { Full: 'hero--full', Split: 'hero--split' },
//           'hero--split',
//           'Hero'
//         ),
//         image: true,
//         src: jpgs.hero,
//       })
//     )}
//     ${IconLinkRow()} ${FourtySixty()} ${Banner()} ${Split()} ${Footer()}
//   </div>
// `;
