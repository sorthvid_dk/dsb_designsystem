// import { html } from 'lit';
// import { Header } from '../../components/header/header.html';
// import { header } from '../../components/header/header.state';
// import { Hero } from '../../components/hero/hero.html';
// import { hero } from '../../components/hero/hero.state';
// import { Article } from '../../components/article/article.html';
// import { article } from '../../components/article/article.state';
// import { Split } from '../../components/split/split.html';
// import { split } from '../../components/split/split.state';
// import { Footer } from '../../components/footer/footer.html';

// export default {
//   title: 'Pages/Campaign Destination',
// };

// export const Default = () => html`
//   <div>
//     ${Header(header({ expanded: false, loggedIn: false }))}
//     ${Hero(hero({ breadcrumbs: true, trumpet: false, ctaPrice: false }))}
//     ${Article(
//       article({
//         content: { type: 'list', listStyle: '' },
//         listStyle: 'list--checked',
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Split(split({ background: 'bg--grey', png: true }))}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Split(split({ inverted: true, background: 'bg--grey' }))}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Split(split({ background: 'bg--grey', badgeModifiers: 'badge--white' }))}
//     ${Footer()}
//   </div>
// `;
