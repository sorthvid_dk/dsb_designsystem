// import { html } from 'lit';

// import { Header } from '../../components/header/header.html';
// import { header } from '../../components/header/header.state';

// import { Hero } from '../../components/hero/hero.html';
// import { hero } from '../../components/hero/hero.state';

// import { Article } from '../../components/article/article.html';
// import { article } from '../../components/article/article.state';

// import { Media } from '../../components/media/media.html';

// import { Banner } from '../../components/banner/banner.html';
// import { banner } from '../../components/banner/banner.state';

// import { Split } from '../../components/split/split.html';
// import { split } from '../../components/split/split.state';

// import { Footer } from '../../components/footer/footer.html';

// export default {
//   title: 'Pages/Campaign Christmas',
// };

// export const Default = () => html`
//   <div>
//     ${Header(header({ expanded: false, loggedIn: false }))}
//     ${Hero(hero({ ctaPrice: false, noCopy: true }))}
//     ${Article(
//       article({
//         content: { type: 'list' },
//         listStyle: 'list--checked',
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Banner(banner({ ctaPrice: false }))} ${Media()}
//     ${Split(split({ badge: false, cta: false }))}
//     ${Article(
//       article({
//         headline: { text: true, button: true },
//         content: { type: 'price', length: 3 },
//       })
//     )}
//     ${Split(
//       split({
//         background: 'bg--grey',
//         inverted: true,
//         badge: false,
//         cta: false,
//         video: true,
//       })
//     )}
//     ${Media()} ${Split(split({ badge: false, link: true }))} ${Footer()}
//   </div>
// `;
