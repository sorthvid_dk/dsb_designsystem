// import { html } from 'lit';

// import { Header } from '../../components/header/header.html';
// import { header } from '../../components/header/header.state';

// import { Hero } from '../../components/hero/hero.html';
// import { hero } from '../../components/hero/hero.state';

// import { Article } from '../../components/article/article.html';
// import { article } from '../../components/article/article.state';

// import { Footer } from '../../components/footer/footer.html';

// export default {
//   title: 'Pages/Product',
// };

// export const Default = () => html`
//   <div>
//     ${Header(header({ expanded: false, loggedIn: false }))}
//     ${Hero(hero({ breadcrumbs: true, trumpet: false, ctaPrice: false }))}
//     ${Article(
//       article({
//         headline: { text: true },
//         content: { type: 'list' },
//         listStyle: 'list--bullet',
//       })
//     )}
//     ${Article(
//       article({
//         headline: { text: true },
//         content: { type: 'price' },
//       })
//     )}
//     ${Article(
//       article({
//         headline: { text: true },
//         content: { type: 'price', length: 2 },
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text', button: true },
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     [TABLE NOT MADE YET]
//     ${Article(
//       article({
//         content: { type: 'text' },
//       })
//     )}
//     ${Article(
//       article({
//         color: 'bg--blue',
//         content: {
//           type: 'text',
//           button: true,
//           buttonStyle: 'button--ghost',
//         },
//       })
//     )}
//     ${Article(
//       article({
//         headline: { text: true },
//         content: { type: 'accordion', length: 4 },
//       })
//     )}
//     ${Footer()}
//   </div>
// `;
