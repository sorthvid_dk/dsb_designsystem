import { html } from 'lit';
import '~/icon-link/icon-link';

export default {
  title: 'Pages/Productpage',
};

export const Productpage = () => html`
  <div>
    <dsb-header contentRoot=".page-content">
      <a slot="topbarLeft" href="#">
        <dsb-icon icon="dsb-logo--outlined"></dsb-icon>
      </a>

      <div slot="topbarMiddle">
        <dsb-header-form headline="Køb rejse">
          <dsb-textfield
            slot="one"
            label="From"
            hiddenLabel
            prefix="Fra"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="two"
            label="To"
            hiddenLabel
            prefix="Til"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="three"
            label="Udrejse"
            placeholder="12 / 3 2021"
          ></dsb-textfield>
          <dsb-textfield
            slot="four"
            label="Antal rejsende"
            placeholder="1 Vuxen"
          ></dsb-textfield>
          <dsb-textfield
            slot="five"
            label="Pladsbilleter"
            placeholder="0 Pladser"
          ></dsb-textfield>
          <dsb-icon-link
            slot="six"
            icon="world"
            label="Rejse til udlandet"
            mini
            inline
          ></dsb-icon-link>
          <dsb-button
            slot="seven"
            label="Søg rejse"
            variant="ghost"
            fullwidth="true"
          ></dsb-button>
        </dsb-header-form>
      </div>

      <dsb-icon-button
        slot="topbarRight"
        icon="user"
        ariaLabel="Login"
      ></dsb-icon-button>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button slot="openButton" icon="magnifying-glass">
        </dsb-icon-button>
        <dsb-header-search placeholder="Hvad søger du?"></dsb-header-search>
      </dsb-flyout>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
        <dsb-header-menu>
          <dsb-underline-link slot="left" href="#">DSB Plus</dsb-underline-link>
          <dsb-underline-link slot="left" href="#"
            >Ansvar & Miljø</dsb-underline-link
          >
          <dsb-underline-link slot="left" href="#">Om DSB</dsb-underline-link>
          <dsb-underline-link href="#">Trafikinformation</dsb-underline-link>
          <dsb-underline-link href="#">Billetter og Service</dsb-underline-link>
          <dsb-underline-link href="#">Erhverv</dsb-underline-link>
          <dsb-underline-link href="#"
            >Kundeservice & kontakt</dsb-underline-link
          >
        </dsb-header-menu>
      </dsb-flyout>
    </dsb-header>

    <style>
      .page-content {
        transition: var(--flyout-transition);
      }
    </style>

    <div class="page-content">
      <div class="deck">
        <dsb-hero headline="Orange fri" variant="sub">
          <dsb-breadcrumbs slot="breadcrumbs">
            <dsb-breadcrumbs-item
              href="#"
              label="Forside"
            ></dsb-breadcrumbs-item>
            <dsb-breadcrumbs-item
              label="Billetter & services"
            ></dsb-breadcrumbs-item>
          </dsb-breadcrumbs>

          <p slot="text">
            Når du er ung eller studerende, kan du få rabat på alle dine
            togrejser, rabatkort og lign.
          </p>

          <dsb-button
            slot="cta"
            variant="ghost"
            label="Køb billet nu"
          ></dsb-button>
        </dsb-hero>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Billig billet med frihed til at ændre planer">
          <dsb-ul>
            <dsb-li variant="bullet"
              >Bestil i god tid og opnå den største rabat
            </dsb-li>
            <dsb-li variant="bullet"
              >Gælder til en specifik afgang på dagen.</dsb-li
            >
            <dsb-li variant="bullet"
              >Refunderes op til 30 min. før afgang.</dsb-li
            >
            <dsb-li variant="bullet">Tag 2 børn under 12 med gratis.</dsb-li>
          </dsb-ul>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Over Storebælt">
          <p slot="subheadline">
            Rejs ekstra billigt på tværs af Danmark med DSB Orange
          </p>
          <dsb-button slot="subheadline" label="Køb billet nu"></dsb-button>
          <div
            style="display: flex; flex-direction: column; gap: var(--units-1)"
          >
            <dsb-flatcard
              href="#"
              from="København"
              to="Hamborg"
              price="228,-"
              prefix="fra"
            ></dsb-flatcard>
            <dsb-flatcard
              href="#"
              from="København"
              to="Berlin"
              price="304,-"
              prefix="fra"
            ></dsb-flatcard>
            <dsb-flatcard
              href="#"
              from="København"
              to="Lübeck"
              price="228,-"
              prefix="fra"
            ></dsb-flatcard>
          </div>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Køb din billet i god tid">
          <p>
            Ligesom DSB Orange bliver DSB Orange Fri sat til salg to måneder før
            rejsedagen. Derfor kan det godt betale sig for dig at planlægge i
            god tid, så du har flest DSB Orange Fri-billetter at vælge imellem.
            Du kan købe DSB Orange Fri på DSB.dk og i app’en.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Vælg selv hvor du vil side">
          <p>
            Vælg selv hvor du vil sidde, f.eks. Familiezone eller Stillezone. Du
            kan reservere plads til InterCity, InterCityLyn og udvalgte
            regionaltog, hvis du tilkøber en pladsbillet.
            <br />
            <br />
            Som betalende voksen kan du tage to børn under 12 år med kvit og
            frit. Vi anbefaler, at du køber Pladsbillet til dig og børnene, så I
            er sikre på at kunne sidde sammen.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Børn, unge og pensionister får rabat">
          <p>
            Billetten sælges til nedsat pris til børn 12-15 år, Unge 16-25 år,
            og rejsende fra 65 år (husk blot legitimation på din rejse).
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article
          headline="Få tilbagebetalt din Orange Fri-billet"
          background="blue"
        >
          <div>
            <p class="mb-3">
              Du kan få tilbagebetalt din Orange Fri-billet frem til 30 min før
              afgang. Det præcise tidspunkt fremgår af din kvitteringsmail og
              din billet. Du skal være opmærksom på, at alle billetter i dit køb
              bliver refunderet.
            </p>
            <dsb-button label="Refunder billet" variant="ghost"></dsb-button>
          </div>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Spørgsmål og svar">
          <p slot="subheadline">
            Kan du ikke finde svar på dit spørgsmål her, så sidder vi klar til
            at hjælpe dig.
          </p>
          <dsb-icon-link
            slot="subheadline"
            arrow
            href="#"
            class="ts-action"
            label="Kundeservice"
          ></dsb-icon-link>
          <div
            style="display: flex; flex-direction: column; gap: var(--units-1)"
          >
            <dsb-flatcard-accordion
              label="Gælder Orange Fri også i Stillezonen?"
            >
              <p>
                Consequat elit exercitation pariatur aliqua elit officia
                exercitation dolor nulla minim tempor sit. Elit tempor veniam
                magna ipsum et nostrud veniam aliqua in consectetur duis magna
                Lorem ex. Voluptate est incididunt occaecat esse non anim quis
                elit irure sint sit exercitation.
              </p>
            </dsb-flatcard-accordion>
            <dsb-flatcard-accordion
              label="Hvad gør jeg, hvis min afgang bliver forsinket/aflyst?"
            >
              <p>
                Consequat elit exercitation pariatur aliqua elit officia
                exercitation dolor nulla minim tempor sit. Elit tempor veniam
                magna ipsum et nostrud veniam aliqua in consectetur duis magna
                Lorem ex. Voluptate est incididunt occaecat esse non anim quis
                elit irure sint sit exercitation.
              </p>
            </dsb-flatcard-accordion>
            <dsb-flatcard-accordion
              label="Kan jeg få en DSB Orange Fri-billet til et barn?"
            >
              <p>
                Consequat elit exercitation pariatur aliqua elit officia
                exercitation dolor nulla minim tempor sit. Elit tempor veniam
                magna ipsum et nostrud veniam aliqua in consectetur duis magna
                Lorem ex. Voluptate est incididunt occaecat esse non anim quis
                elit irure sint sit exercitation.
              </p>
            </dsb-flatcard-accordion>
            <dsb-flatcard-accordion
              label="Kan jeg tilkøbe tillæg til DSB 1' med min Orange Fri-billet?"
            >
              <p>
                Consequat elit exercitation pariatur aliqua elit officia
                exercitation dolor nulla minim tempor sit. Elit tempor veniam
                magna ipsum et nostrud veniam aliqua in consectetur duis magna
                Lorem ex. Voluptate est incididunt occaecat esse non anim quis
                elit irure sint sit exercitation.
              </p>
            </dsb-flatcard-accordion>
          </div>
        </dsb-article>
      </div>

      <dsb-footer>
        <li slot="one">
          <dsb-icon-link
            arrow
            href="#"
            label="Kundservice"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="one">
          <dsb-underline-link
            class="ts-headline-3"
            href="tel:#"
            label="70 13 14 15"
          ></dsb-underline-link>
        </li>
        <li slot="one">
          <p class="ts-trumpet">Vi har åbent 07:00 - 20:00</p>
        </li>

        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Billetter og Service"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Erhverv"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="DSB Plus"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Ansvar & miljø"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Om DSB"></dsb-underline-link>
        </li>

        <li slot="three">
          <dsb-icon-link
            arrow
            href="#"
            label="Trafikinformation"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="four">
          <a href="#">Afgangstavler</a>
        </li>
        <li slot="four">
          <a href="#">Ændringer i trafikken</a>
        </li>
        <li slot="four">
          <a href="#">Sporarbejde</a>
        </li>
        <li slot="four">
          <a href="#">Køreplaner</a>
        </li>

        <li slot="bottom">
          <dsb-underline-link
            href="#"
            label="Cookies på DSB.dk"
          ></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Rejseregler"></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Persondata"></dsb-underline-link>
        </li>
      </dsb-footer>
    </div>
  </div>
`;
