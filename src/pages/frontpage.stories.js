import { html } from 'lit';
import '~/icon-link/icon-link';
import './../components/icon-link-row/icon-link-row';

export default {
  title: 'Pages/Frontpage',
};

export const Frontpage = () => html`
  <div>
    <dsb-header contentRoot=".page-content">
      <a slot="topbarLeft" href="#">
        <dsb-icon icon="dsb-logo--outlined"></dsb-icon>
      </a>

      <div slot="topbarMiddle">
        <dsb-header-form headline="Køb rejse" showHeadline>
          <dsb-textfield
            slot="one"
            label="From"
            hiddenLabel
            prefix="Fra"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="two"
            label="To"
            hiddenLabel
            prefix="Til"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="three"
            label="Udrejse"
            placeholder="12 / 3 2021"
          ></dsb-textfield>
          <dsb-textfield
            slot="four"
            label="Antal rejsende"
            placeholder="1 Vuxen"
          ></dsb-textfield>
          <dsb-textfield
            slot="five"
            label="Pladsbilleter"
            placeholder="0 Pladser"
          ></dsb-textfield>
          <dsb-icon-link
            slot="six"
            icon="world"
            label="Rejse til udlandet"
            mini
            inline
          ></dsb-icon-link>
          <dsb-button
            slot="seven"
            label="Søg rejse"
            variant="ghost"
            fullwidth="true"
          ></dsb-button>
        </dsb-header-form>
      </div>

      <dsb-icon-button
        slot="topbarRight"
        icon="user"
        ariaLabel="Login"
      ></dsb-icon-button>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button slot="openButton" icon="magnifying-glass">
        </dsb-icon-button>
        <dsb-header-search placeholder="Hvad søger du?"></dsb-header-search>
      </dsb-flyout>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
        <dsb-header-menu>
          <dsb-underline-link slot="left" href="#">DSB Plus</dsb-underline-link>
          <dsb-underline-link slot="left" href="#"
            >Ansvar & Miljø</dsb-underline-link
          >
          <dsb-underline-link slot="left" href="#">Om DSB</dsb-underline-link>
          <dsb-underline-link href="#">Trafikinformation</dsb-underline-link>
          <dsb-underline-link href="#">Billetter og Service</dsb-underline-link>
          <dsb-underline-link href="#">Erhverv</dsb-underline-link>
          <dsb-underline-link href="#"
            >Kundeservice & kontakt</dsb-underline-link
          >
        </dsb-header-menu>
      </dsb-flyout>
    </dsb-header>

    <style>
      .page-content {
        transition: var(--flyout-transition);
        padding-top: var(--dsb-header-height);
      }
    </style>

    <div class="page-content">
      <div class="deck">
        <dsb-hero
          src="https://source.unsplash.com/random"
          alt="A random image from unsplash"
          trumpet="Trumpet text"
          breadcrumbs="[x,x,x]"
          headline="DSB Title"
        >
          <p slot="text">
            Når du er ung eller studerende, kan du få rabat på alle dine
            togrejser, rabatkort og lign.
            <a href="#">Besparelsen afhænger af</a>, hvor ofte du rejser med
            toget, og hvor din rejse går hen.
          </p>

          <dsb-cta slot="cta" prePrice="Fra" price="149,-">
            <dsb-button variant="ghost" label="Køb nu"></dsb-button>
          </dsb-cta>
        </dsb-hero>
      </div>

      <dsb-icon-link-row>
          <dsb-icon-link
            fill-space
            icon="trafficinfo"
            label="Kundeservice"
          ></dsb-icon-link>

          <dsb-icon-link
            fill-space
            icon="customerservice"
            label="Trafikinformation"
          ></dsb-icon-link>

          <dsb-icon-link
            fill-space
            icon="ticketservice"
            label="Billetter og services"
          ></dsb-icon-link>

          <dsb-icon-link
            fill-space
            icon="percent"
            label="DSB Plus"
          ></dsb-icon-link>

          <dsb-icon-link
            fill-space
            icon="dsbplus"
            label="DSB Orange"
          ></dsb-icon-link>
      </dsb-icon-link-row>


      <div class="split uneven deck page-width px-2-col">
        <dsb-image-link
          overlap
          url="#"
          headline="Don't put a button here"
          text="Only use the Image-link's <slot> for badges."
          src="https://source.unsplash.com/random"
          alt="https://source.unsplash.com/random"
        >
          <dsb-button disabled label="I should not be here"></dsb-button>
        </dsb-image-link>

        <dsb-image-link
          url="#"
          headline="Another one"
          text="City Pass giver mulighed for at rejse ubegrænset med bus, tog og metro, så du kan rejse ubekymret rundt i byen uden at skulle tænke på at købe ny billet hver gang du skal videre."
          src="https://source.unsplash.com/random"
          alt="https://source.unsplash.com/random"
        >
          <dsb-badge slot="right" label="Fra" text="149,-"></dsb-badge>
        </dsb-image-link>
      </div>

      <div class="deck">
        <dsb-banner
          src="https://source.unsplash.com/random"
          alt="My image alt text"
        >
          <dsb-badge slot="badge" label="badge" text="badge"></dsb-badge>
          <h1>My headline</h1>
          <p>
            HTML content, that may include nested <a href="#">links</a> etc.
          </p>
          <dsb-button label="Slotted button"></dsb-button>
        </dsb-banner>
      </div>

      <div class="deck">
        <dsb-split
          src="https://i.ibb.co/3R9F9ft/mountain.jpg"
          alt="Example image alt text"
          trumpet="København"
          headline="Spørgsmål og svar"
          text="Når du er ung eller studerende, kan du få rabat på alle dine togrejser, rabatkort og lign. Besparelsen afhænger af, hvor ofte du rejser med toget, og hvor din rejse går hen."
        >
          <dsb-cta
            ctaText="Køb på stationen"
            ctaSubText="DSB Salg & Service / 7-Eleven"
            prePrice="Fra"
            price="149,-"
          >
          </dsb-cta>
        </dsb-split>
      </div>

      <dsb-footer>
        <li slot="one">
          <dsb-icon-link
            arrow
            href="#"
            label="Kundservice"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="one">
          <dsb-underline-link
            class="ts-headline-3"
            href="tel:#"
            label="70 13 14 15"
          ></dsb-underline-link>
        </li>
        <li slot="one">
          <p class="ts-trumpet">Vi har åbent 07:00 - 20:00</p>
        </li>

        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Billetter og Service"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Erhverv"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="DSB Plus"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Ansvar & miljø"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Om DSB"></dsb-underline-link>
        </li>

        <li slot="three">
          <dsb-icon-link
            arrow
            href="#"
            label="Trafikinformation"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="four">
          <a href="#">Afgangstavler</a>
        </li>
        <li slot="four">
          <a href="#">Ændringer i trafikken</a>
        </li>
        <li slot="four">
          <a href="#">Sporarbejde</a>
        </li>
        <li slot="four">
          <a href="#">Køreplaner</a>
        </li>

        <li slot="bottom">
          <dsb-underline-link
            href="#"
            label="Cookies på DSB.dk"
          ></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Rejseregler"></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Persondata"></dsb-underline-link>
        </li>
      </dsb-footer>
    </div>
  </div>
`;
