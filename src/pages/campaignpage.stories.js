import { html } from 'lit';
import { random } from './../../.storybook/utils';

export default {
  title: 'Pages/Campaignpage',
};

export const Campaignpage = () => html`
  <div>
    <dsb-header contentRoot=".page-content">
      <a slot="topbarLeft" href="#">
        <dsb-icon icon="dsb-logo--outlined"></dsb-icon>
      </a>

      <div slot="topbarMiddle">
        <dsb-header-form headline="Køb rejse">
          <dsb-textfield
            slot="one"
            label="From"
            hiddenLabel
            prefix="Fra"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="two"
            label="To"
            hiddenLabel
            prefix="Til"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="three"
            label="Udrejse"
            placeholder="12 / 3 2021"
          ></dsb-textfield>
          <dsb-textfield
            slot="four"
            label="Antal rejsende"
            placeholder="1 Vuxen"
          ></dsb-textfield>
          <dsb-textfield
            slot="five"
            label="Pladsbilleter"
            placeholder="0 Pladser"
          ></dsb-textfield>
          <dsb-icon-link
            slot="six"
            icon="world"
            label="Rejse til udlandet"
            mini
            inline
          ></dsb-icon-link>
          <dsb-button
            slot="seven"
            label="Søg rejse"
            variant="ghost"
            fullwidth="true"
          ></dsb-button>
        </dsb-header-form>
      </div>

      <dsb-icon-button
        slot="topbarRight"
        icon="user"
        ariaLabel="Login"
      ></dsb-icon-button>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button slot="openButton" icon="magnifying-glass">
        </dsb-icon-button>
        <dsb-header-search placeholder="Hvad søger du?"></dsb-header-search>
      </dsb-flyout>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
        <dsb-header-menu>
          <dsb-underline-link slot="left" href="#">DSB Plus</dsb-underline-link>
          <dsb-underline-link slot="left" href="#"
            >Ansvar & Miljø</dsb-underline-link
          >
          <dsb-underline-link slot="left" href="#">Om DSB</dsb-underline-link>
          <dsb-underline-link href="#">Trafikinformation</dsb-underline-link>
          <dsb-underline-link href="#">Billetter og Service</dsb-underline-link>
          <dsb-underline-link href="#">Erhverv</dsb-underline-link>
          <dsb-underline-link href="#"
            >Kundeservice & kontakt</dsb-underline-link
          >
        </dsb-header-menu>
      </dsb-flyout>
    </dsb-header>

    <style>
      .page-content {
        transition: var(--flyout-transition);
      }
    </style>

    <div class="page-content">
      <div class="deck">
        <dsb-hero
          headline="Aarhus - København"
          src="${random('jpg')}"
          variant="sub"
        >
          <dsb-badge
            slot="badge"
            label="Priser fra"
            text="99,-"
            placement="right"
            color="red"
          ></dsb-badge>

          <p slot="text">
            Med en rejsetid på under 3 timer kommer du hurtigt frem til
            hovedstaden fra Aarhus.
          </p>

          <dsb-button
            slot="cta"
            variant="primary"
            label="Køb billet nu"
          ></dsb-button>
        </dsb-hero>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Tag toget fra Aarhus til København">
          <dsb-ul>
            <dsb-li variant="check"
              >Bestil i god tid og opnå den største rabat
            </dsb-li>
            <dsb-li variant="check"
              >Gælder til en specifik afgang på dagen.</dsb-li
            >
            <dsb-li variant="check"
              >Refunderes op til 30 min. før afgang.</dsb-li
            >
            <dsb-li variant="check">Tag 2 børn under 12 med gratis.</dsb-li>
          </dsb-ul>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Med DSB Orange-billetter sparer du mange penge.">
          <p>
            Du kan spare mange penge med vores DSB Orange-billetter. Faktisk kan
            du også tage to børn under 12 år med på din Standard-billet.
            Kombineret med vores mere end 30 daglige afgange, er det nemt,
            hurtigt og komfortabelt at rejse med toget fra Aarhus til København.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width px-1-col">
        <div class="bg--grey">
          <dsb-split
            contain
            src="${random('png')}"
            alt="Example image alt text"
          >
            <dsb-badge
              slot="badge"
              label="Kaffe & Wi-fi"
              text="Gratis"
            ></dsb-badge>

            <h2>Tak for kaffe, strøm og internet</h2>

            <p class="mb-2">
              Du kan få en lækker kop kaffe i 7-Eleven til turen på Aarhus H,
              eller du kan tage en kop med videre fra København H. Vi har sørget
              for masser af strøm, så du kan bruge vores gratis internet uden at
              være nervøs for strømniveauet.
            </p>

            <dsb-button label="Find din afgang nu"></dsb-button>
          </dsb-split>
        </div>
      </div>

      <div class="deck page-width">
        <dsb-article
          headline="Centrum til centrum på under 3 timer med lyntoget"
        >
          <p>
            Det er nemt og hurtigt at komme fra centrum af Aarhus til centrum af
            København med toget. Vi kører fra Aarhus H til København H. Det er
            er nemt at komme til og fra begge stationer, når du skal videre. Der
            er masser af plads og muligheder for både arbejde og afslapning i
            toget.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Toget eller bussen? Valget er dit">
          <p>
            Skal du fra Århus til København eller omvendt, og har du besluttet
            dig for at lade bilen stå, har du to oplagte muligheder: Toget eller
            bussen.
            <br /><br />
            I toget har du plads. Plads til at arbejde, slappe af og udveksle
            meninger med andre, der ligesom dig har valgt at rejse fra A til B i
            en togkupe. Plads til at vinke til bussen, der kæmper sig frem, og
            plads til at gi’ kæresten et lille klem.
            <br /><br />
            Foretrækker du at krydse landet i ro og mag, kan du i vores
            stillezoner uforstyrret nyde udsigten til by, skov og vand. Eller
            lade dig berige af en bog, en podcast eller seneste single fra dit
            yndlings-band.
            <br /><br />
            Bussen får dig også frem. Men som bekendt kan vejen derhen være lige
            så vigtig som målet – og det vil vi mene er tilfældet her.
            <br /><br />
            Hvornår vil du afsted? Vi har flere end 30 daglige afgange. Toget
            eller bussen? Du bestemmer selv.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-split
          inverted
          background="grey"
          src="${random('jpg')}"
          alt="Example image alt text"
        >
          <dsb-badge slot="right" placement="right" label="Pris fra" text="99,-"></dsb-badge>

          <h2>Rejs Aarhus – København fra bare 99 kroner</h2>

          <p class="mb-2">
            Du kan komme til København fra Aarhus for fra bare 99 kroner. Vi har
            nemlig mange DSB Orange-billetter til vores togafgange. På den måde
            kan du komme billigt til hovedstaden. Pengene kan du eksempelvis
            bruge på noget lækkert til turen, som du er velkommen til at tage
            med i toget.
            <br /><br />
            Vores DSB Orange-billetter sættes typisk til salg ca. 2 måneder før
            rejsetidspunktet. Ved du allerede nu, at du skal rejse på
            strækningen, er det bare med at holde øje. Så kan du komme både nemt
            og billigt til København.
          </p>

          <dsb-button label="Køb Orange-billet"></dsb-button>
        </dsb-split>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Børn rejser gratis med på din billet">
          <p>
            Børn i alderen 0-11 år rejser normalt gratis med toget i selskab med
            en voksen. Du kan nemlig tage op til to børn under 12 år med på din
            egen billet, når du rejser. Det gælder for alle billetter til
            Standard.
            <br /><br />
            Børn i alderen 12-15 år rejser ikke gratis, men de får rabat på
            deres Børnebillet. De kan også tage ét barn gratis med på deres egen
            billet.
            <br /><br />
            <a href="#">Husk at booke pladsbilletter</a>, hvis I vil være sikker
            på at sidde sammen under hele turen.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-article headline="Nem planlægning med mere end 30 daglige afgange">
          <p>
            Vi har gjort det nemt for dig at rejse, når det passer ind i din
            kalender. Vores mere end 30 daglige afgange, gør det nemlig nemt og
            fleksibelt at rejse fra Aarhus til København.
            <br /><br />
            Du vælger selv, hvilken afgang du vil med, når du har købt en
            Standard-billet. Du kan nemlig frit vælge blandt de mange
            togafgange, den dag din billet er gyldig til. På den måde er du ikke
            bundet til et fast tidspunkt, hvis der kommer noget i vejen.
            <br /><br />
            Det bliver ikke nemmere, hurtigere og mere fleksibelt, end når du
            rejser med toget.
          </p>
        </dsb-article>
      </div>

      <div class="deck page-width">
        <dsb-split
          background="grey"
          src="${random('jpg')}"
          alt="Example image alt text"
        >
          <dsb-badge
            slot="badge"
            color="white"
            label="Rejs med"
            text="DSB 1'"
          ></dsb-badge>

          <h2>Lad op i vores DSB 1’ Lounge</h2>

          <p>
            Du får et væld af ekstra fordele, når du rejser med DSB 1’. Du får
            blandt andet adgang til vores <a href="#">DSB 1’ Lounge i både Aarhus og
            København</a>. Her kan du eksempelvis nyde gratis forfriskninger, læse
            dagens aviser og booke mødelokaler til 6 eller 12 personer. Vi har
            gratis internet i hele loungen.
            <br /><br />
            Få gratis kaffe, te eller kildevand i 7-Eleven, inden du stiger på
            toget. Du skal bare vise din billet.
            <br /><br />
            Under turen serverer vi morgenmad indtil kl. 10 samt lidt sødt i
            løbet af dagen. Du får også fri adgang til dagens aviser, kildevand,
            kaffe og te.
          </p>
        </dsb-split>
      </div>

      <div class="deck page-width">
        <dsb-article background="grey" headline="Tag toget fra Aarhus til København">
          <dsb-ul>
            <dsb-li variant="number"
              >Bestil i god tid og opnå den største rabat
            </dsb-li>
            <dsb-li variant="number"
              >Gælder til en specifik afgang på dagen.</dsb-li
            >
            <dsb-li variant="number"
              >Refunderes op til 30 min. før afgang.</dsb-li
            >
            <dsb-li variant="number">Tag 2 børn under 12 med gratis.</dsb-li>
          </dsb-ul>
        </dsb-article>
      </div>

      <dsb-footer>
        <li slot="one">
          <dsb-icon-link
            arrow
            href="#"
            label="Kundservice"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="one">
          <dsb-underline-link
            class="ts-headline-3"
            href="tel:#"
            label="70 13 14 15"
          ></dsb-underline-link>
        </li>
        <li slot="one">
          <p class="ts-trumpet">Vi har åbent 07:00 - 20:00</p>
        </li>

        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Billetter og Service"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Erhverv"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="DSB Plus"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Ansvar & miljø"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Om DSB"></dsb-underline-link>
        </li>

        <li slot="three">
          <dsb-icon-link
            arrow
            href="#"
            label="Trafikinformation"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="four">
          <a href="#">Afgangstavler</a>
        </li>
        <li slot="four">
          <a href="#">Ændringer i trafikken</a>
        </li>
        <li slot="four">
          <a href="#">Sporarbejde</a>
        </li>
        <li slot="four">
          <a href="#">Køreplaner</a>
        </li>

        <li slot="bottom">
          <dsb-underline-link
            href="#"
            label="Cookies på DSB.dk"
          ></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Rejseregler"></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Persondata"></dsb-underline-link>
        </li>
      </dsb-footer>
    </div>
  </div>
`;
