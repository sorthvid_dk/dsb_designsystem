import { html } from 'lit';
import { random } from '../../.storybook/utils';

export default {
  title: 'Pages/Responsibilitypage',
};

export const Responsibilitypage = () => html`
  <div>
    <dsb-header contentRoot=".page-content">
      <a slot="topbarLeft" href="#">
        <dsb-icon icon="dsb-logo--outlined"></dsb-icon>
      </a>

      <div slot="topbarMiddle">
        <dsb-header-form headline="Køb rejse">
          <dsb-textfield
            slot="one"
            label="From"
            hiddenLabel
            prefix="Fra"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="two"
            label="To"
            hiddenLabel
            prefix="Til"
            placeholder="Station, Stoppested, Vej, By, Lokalitet"
          ></dsb-textfield>
          <dsb-textfield
            slot="three"
            label="Udrejse"
            placeholder="12 / 3 2021"
          ></dsb-textfield>
          <dsb-textfield
            slot="four"
            label="Antal rejsende"
            placeholder="1 Vuxen"
          ></dsb-textfield>
          <dsb-textfield
            slot="five"
            label="Pladsbilleter"
            placeholder="0 Pladser"
          ></dsb-textfield>
          <dsb-icon-link
            slot="six"
            icon="world"
            label="Rejse til udlandet"
            mini
            inline
          ></dsb-icon-link>
          <dsb-button
            slot="seven"
            label="Søg rejse"
            variant="ghost"
            fullwidth="true"
          ></dsb-button>
        </dsb-header-form>
      </div>

      <dsb-icon-button
        slot="topbarRight"
        icon="user"
        ariaLabel="Login"
      ></dsb-icon-button>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button slot="openButton" icon="magnifying-glass">
        </dsb-icon-button>
        <dsb-header-search placeholder="Hvad søger du?"></dsb-header-search>
      </dsb-flyout>

      <dsb-flyout slot="topbarRight">
        <dsb-icon-button icon="burger" slot="openButton"></dsb-icon-button>
        <dsb-header-menu>
          <dsb-underline-link slot="left" href="#">DSB Plus</dsb-underline-link>
          <dsb-underline-link slot="left" href="#"
            >Ansvar & Miljø</dsb-underline-link
          >
          <dsb-underline-link slot="left" href="#">Om DSB</dsb-underline-link>
          <dsb-underline-link href="#">Trafikinformation</dsb-underline-link>
          <dsb-underline-link href="#">Billetter og Service</dsb-underline-link>
          <dsb-underline-link href="#">Erhverv</dsb-underline-link>
          <dsb-underline-link href="#"
            >Kundeservice & kontakt</dsb-underline-link
          >
        </dsb-header-menu>
      </dsb-flyout>
    </dsb-header>

    <style>
      .page-content {
        transition: var(--flyout-transition);
      }
    </style>

    <div class="page-content">
      <div class="deck">
        <dsb-hero
          trumpet="Sammen med os"
          headline="Vi tager ansvar for miljø og klima"
          src="${random('jpg')}"
          variant="sub"
        >
        </dsb-hero>
      </div>

      <div class="deck page-width">
        <dsb-accordion>
          <h1 slot="headline">
            Derfor har vi sat 4 ambitiøse miljømål for 2030 og, vi vil rigtig
            gerne høre, hvad du mener om dem.
          </h1>
          <dsb-accordion-item headline="This is an Accordion Item">
            <p>
              Børn i alderen 0-11 år rejser normalt gratis med toget i selskab
              med en voksen. Du kan nemlig tage op til to børn under 12 år med
              på din egen billet, når du rejser. Det gælder for alle billetter
              til Standard.
              <br /><br />
              Børn i alderen 12-15 år rejser ikke gratis, men de får rabat på
              deres Børnebillet. De kan også tage ét barn gratis med på deres
              egen billet.
              <br /><br />
              <a href="#">Husk at booke pladsbilletter</a>, hvis I vil være
              sikker på at sidde sammen under hele turen.
            </p>
          </dsb-accordion-item>
          <dsb-accordion-item headline="This is also an Accordion Item">
            <p>
              Vælg selv hvor du vil sidde, f.eks. Familiezone eller Stillezone.
              Du kan reservere plads til InterCity, InterCityLyn og udvalgte
              regionaltog, hvis du tilkøber en pladsbillet.
              <br /><br />
              Som betalende voksen kan du tage to børn under 12 år med kvit og
              frit. Vi anbefaler, at du køber Pladsbillet til dig og børnene, så
              I er sikre på at kunne sidde sammen.
            </p>
          </dsb-accordion-item>
          <dsb-accordion-item headline="There can be many">
            <p>
              Børn i alderen 0-11 år rejser normalt gratis med toget i selskab
              med en voksen. Du kan nemlig tage op til to børn under 12 år med
              på din egen billet, når du rejser. Det gælder for alle billetter
              til Standard.
              <br /><br />
              Børn i alderen 12-15 år rejser ikke gratis, men de får rabat på
              deres Børnebillet. De kan også tage ét barn gratis med på deres
              egen billet.
              <br /><br />
              <a href="#">Husk at booke pladsbilletter</a>, hvis I vil være
              sikker på at sidde sammen under hele turen.
            </p>
          </dsb-accordion-item>
          <dsb-accordion-item headline="Sooooo many">
            <p>
              Børn i alderen 0-11 år rejser normalt gratis med toget i selskab
              med en voksen. Du kan nemlig tage op til to børn under 12 år med
              på din egen billet, når du rejser. Det gælder for alle billetter
              til Standard.
              <br /><br />
              Børn i alderen 12-15 år rejser ikke gratis, men de får rabat på
              deres Børnebillet. De kan også tage ét barn gratis med på deres
              egen billet.
              <br /><br />
              <a href="#">Husk at booke pladsbilletter</a>, hvis I vil være
              sikker på at sidde sammen under hele turen.
            </p>
          </dsb-accordion-item>
        </dsb-accordion>
      </div>

      <div class="deck page-width">
        <dsb-split src="${random('jpg')}" alt="Example image alt text">
          <h2>Vi kan alle gøre en forskel</h2>

          <p>
            Vi kan alle gøre en forskel og tage ansvar for miljø og klima gennem
            de valg, vi tager i hverdagen. Når du vælger toget, vælger du
            miljørigtigt. Lige som de flere end 500.000 mennesker, som DSB hver
            dag transporterer rundt i landet. Hos DSB tager vi ansvaret for at
            tilbyde miljørigtig transport meget alvorligt. Derfor har vi sat
            nogle ambitiøse miljømål, som vi melder klart ud. På den måde kan du
            også holde øje med dem.
          </p>
        </dsb-split>
      </div>

      <div class="deck page-width">
        <dsb-media
          headline="Tænker du på miljøet, når du vælger transportmiddel? Vi tog forbi Odense Banegård for at spørge de rejsende."
          src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
        ></dsb-media>
      </div>

      <div class="deck page-width">
        <dsb-split inverted src="${random('jpg')}" alt="Example image alt text">
          <h2>Vi sætter strøm til jernbanen</h2>

          <p>
            Alle nye og kommende tog, som DSB køber, vil være el-tog. På langt
            de fleste DSB-strækninger, tager el-togene over i løbet af de næste
            ti år, efterhånden som Banedanmark får sat strøm til jernbanen. Det
            er bedst for vores alle sammens klima.
          </p>
        </dsb-split>
      </div>

      <div class="deck page-width">
        <dsb-slide narrow align-center headline="#SammenMedOS">
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
          <div>
            <img src="${random('jpg')}" alt="Example" />
          </div>
        </dsb-slide>
      </div>

      <dsb-footer>
        <li slot="one">
          <dsb-icon-link
            arrow
            href="#"
            label="Kundservice"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="one">
          <dsb-underline-link
            class="ts-headline-3"
            href="tel:#"
            label="70 13 14 15"
          ></dsb-underline-link>
        </li>
        <li slot="one">
          <p class="ts-trumpet">Vi har åbent 07:00 - 20:00</p>
        </li>

        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Billetter og Service"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Erhverv"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="DSB Plus"></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link
            href="#"
            label="Ansvar & miljø"
          ></dsb-underline-link>
        </li>
        <li slot="two">
          <dsb-underline-link href="#" label="Om DSB"></dsb-underline-link>
        </li>

        <li slot="three">
          <dsb-icon-link
            arrow
            href="#"
            label="Trafikinformation"
            inline=${true}
          ></dsb-icon-link>
        </li>
        <li slot="four">
          <a href="#">Afgangstavler</a>
        </li>
        <li slot="four">
          <a href="#">Ændringer i trafikken</a>
        </li>
        <li slot="four">
          <a href="#">Sporarbejde</a>
        </li>
        <li slot="four">
          <a href="#">Køreplaner</a>
        </li>

        <li slot="bottom">
          <dsb-underline-link
            href="#"
            label="Cookies på DSB.dk"
          ></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Rejseregler"></dsb-underline-link>
        </li>
        <li slot="bottom">
          <dsb-underline-link href="#" label="Persondata"></dsb-underline-link>
        </li>
      </dsb-footer>
    </div>
  </div>
`;
