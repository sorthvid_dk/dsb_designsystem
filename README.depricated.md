# DSB Designsystem 🚂

Development environment for creating components to the DSB Designsystem. The project aims at giving front-end developers & engineers a collection of reusable HTML and SCSS partials to build websites and user interfaces.


## Usage

```bash
yarn add @dsb.dk/designsystem
```

### Requirements

[resolve-url-loader](https://www.npmjs.com/package/resolve-url-loader) - To get the correct relative paths to fonts etc.
[sass](https://www.npmjs.com/package/sass) - Currently the only loader that supports @use syntax

### SCSS

```scss
// my-project.scss
@import '~@dsb.dk/designsystem'

.my-element {
  background: var(--color-red);
}
```

### JS
When a component needs javascript to function, it's made into a [custom html element](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements) which will handle itself without the need of any page-load-eventlisteners etc. This means that everything that is available to import, is a bunch of init-functions for the designsystem's custom elements.

#### Full pack

```js
// imports minified pack, ready to be used
import '@dsb.dk/designsystem'
```

#### ESM

```js
// imports all ES modules
import * as dsb from '@dsb.dk/designsystem/dist/js/dsb.esm.js'

// Define and activate everything
Object.values(dsb).forEach(module => typeof module === 'function' && module())
```

or:

```js
// imports a single component
import { defineSlideShow } from '@dsb.dk/designsystem/dist/js/dsb.esm.js'

// Define the element
defineSlideShow()
```


### HTML

Given that the javascript has been imported and (if needed) activated, you will now have access to the DSB Designsystem custom elements.
The markup for the ready-to-use components are found in the HTML tab under each component in [Storybook](https://dsb.sorthvid.design/)

```html
<!-- my-project.html -->
<slide-show>
  <div data-content>
    <div data-item>Item 1</div>
    <div data-item>Item 2</div>
  </div>
  <div data-ui>
    <button data-prev>prev</button>
    <button data-next>next</button>
  </div>
</slide-show>
```


### CDN
Use jsdelivr for easy access to our built static files. This is great for prototyping and trying out the designsystem without installing anything.

| Scripts | URL                                                                   |
| ------- | --------------------------------------------------------------------- |
| CSS     | https://cdn.jsdelivr.net/npm/@dsb.dk/designsystem/dist/styles/dsb.css |
| ES5     | https://cdn.jsdelivr.net/npm/@dsb.dk/designsystem/dist/js/dsb.full.js |


### Codesandbox
We have individual sandboxes on [Codesandbox](https://codesandbox.io) that shows how to use the project.

- [cdn](https://codesandbox.io/s/cdn-ngiqo)
- [bundler](https://codesandbox.io/s/bundler-kgrwb)
- [Custom element](https://codesandbox.io/s/custom-elements-vanilla-i254l?file=/src/index.js)
- [Custom element (react)](https://codesandbox.io/s/custom-elements-react-7voff?file=/src/App.js)

__TODO:__ Codesandbox can't import scss files from `/node_modules`. So we can't showcase that. But it's tested ok locally.
```scss
@import '~@dsb.dk/designsystem'
.my-local-element {
  background: $color-red;
}
```


## Contribution

This is a living project that aims to include some of the guidelines of an [open source](https://github.com/github/opensource.guide) project. So it's encouraged to add a contribution after reading though the documentation.

- [CONTRIBUTING.md](https://bitbucket.org/sorthvid_dk/dsb_designsystem/src/master/CONTRIBUTING.md)
- [DEVELOPER-NOTES.md](https://bitbucket.org/sorthvid_dk/dsb_designsystem/src/master/DEVELOPER-NOTES.md)


## Brand = Design + Development

- [Storybook designsystem](https://dsb.sorthvid.design/)
- [Zeroheight](https://dsb.zeroheight.com/styleguides)

There are two major parts that makes up the documentation of how to use the DSB brand. [Zeroheight](https://zeroheight.com/) and [Storybook](https://storybook.js.org/). Zeroheight is the main place. There you can find every little in and out of the brand, and where to do what. Storybook is focused on giving guidance on *how* to implement and setup web components and projects.

*An arbitary process of a project could therefore look somthing like:*

1. One of the pages on [dsb.dk](https://www.dsb.dk/) should have a different hero.

2. The person looks in Zeroheight to know what heroes are allowed to be used on the given page.

3. And, with the help of Storybook, knows how to implement that hero.


## Composability

The DSB designsystem encourages composability through combining smaller building blocks into bigger components. Inspiration comes loosely from the [Atomic Design Pattern](https://atomicdesign.bradfrost.com/), but with the difference that all components (currently) lives at the same hierarchical level.


### Forbidden components, but dry

We make use of [Storybook Knobs](https://www.npmjs.com/package/@storybook/addon-knobs) to handle state and options inside, and between, different components. Where each state is written once and exported as a javascript function and imported by any parent component. Because of this inheritance of component state, it is of __*outmost importance*__ necessary to look at a components compositional rules in Zeroheight before implementing the component. What this means is that a component in Storybook could potentially have one or multiple options that composes a forbidden combination. Let's look at an example:

- The __Hero__ component can contain the __Breadcrumb__ component.
- When the Hero is used on the frontpage, it *should never* include the Breadcrumbs.
- This creates the possibility of combining a forbidden combination.

This comes from the fact that every higher order component inherits its childrens Knobs and options. In order to keep the project [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) and maintainable, where there aren't multiple versions of the same state existing with slight differences between them.

If this is the right way to go is still something that's up for discussion.


## License

This project is open source and available under the [MIT License](LICENSE).
