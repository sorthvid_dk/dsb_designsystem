export default {
  hero: 'https://i.ibb.co/pd5X6kx/image-dsb.jpg',
  boats: 'https://i.ibb.co/M2K3DSN/boats.jpg',
  cph: 'https://i.ibb.co/GQzV7mF/cbh.jpg',
  dsbHero: 'https://i.ibb.co/Vx0bLJY/dsb-hero.jpg',
  flower: 'https://i.ibb.co/m8zSsGs/flower.jpg',
  hbg: 'https://i.ibb.co/51FRvgD/hbg.jpg',
  kid: 'https://i.ibb.co/kBCXq34/kid.jpg',
  mountain: 'https://i.ibb.co/3R9F9ft/mountain.jpg',
  rainbow: 'https://i.ibb.co/c6bjkVV/rainbow.jpg'
};
