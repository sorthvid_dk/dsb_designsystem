export default [
  'Gælder Orange Fri også i Stillezonen?',
  'Hvad gør jeg, hvis min afgang bliver forsinket/aflyst?',
  'Kan jeg få en DSB Orange Fri-billet til et barn?',
  `Kan jeg tilkøbe tillæg til DSB 1' med min Orange Fri-billet?`
];
