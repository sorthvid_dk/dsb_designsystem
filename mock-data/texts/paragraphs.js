export default [
  'Når du er ung eller studerende, kan du få rabat på alle dine togrejser, rabatkort og lign. Besparelsen afhænger af, hvor ofte du rejser med toget, og hvor din rejse går hen.',
  'Alle Orange-billetter fra 28. juni - 4. august er lige nu på sommerudsalg. Rejs lige så langt du vil i hele landet for maks. 99 kr.',
  'Med DSB Orange-billetter kan du komme billigt afsted, fx Esbjerg-København eller Aarhus-København fra 99,-',
  'City Pass giver mulighed for at rejse ubegrænset med bus, tog og metro, så du kan rejse ubekymret rundt i byen uden at skulle tænke på at købe ny billet hver gang du skal videre.',
]
