# DSB Designsystem (2.0)

[Storybook](https://dsb.sorthvid.design)

This is a work-in-progress-readme for the upcoming release of the updated DSB Designsystem.

API, syntax and how-to-use is veeery beta. If there's bugs, or if some code doesn't make sense, fear not!

The new release is making heavy use of HTML Custom Elements, and will therefore differ quite a bit in how it's used in compared to the previous release.

This file is meant to guide beta testers on how to get up and running.

## Installation

Get the latest release from npm:

```bash
yarn add @dsb.dk/designsystem
```

## Activation

Activate all components:

```js
// my-project.js
import '@dsb.dk/designsystem';
```

Get single component: (this requires webpack, or other bundler that supports [Bare Module Specifier Resolution in node.js](https://github.com/jkrems/proposal-pkg-exports/))

```js
// my-project.js
import '@dsb.dk/designsystem/button';
```

Every component is available through single imports.
The name is the kebab-case version of the name in Storybook.

To get the padding/margin, deck, grid and other utility css it can be imported through:

```css
/* my-project.css */
@import '@dsb.dk/designsystem/css';
```

## Usage

All the custom elements will instantiate themselfs by default. So as long as you included the javascript file, you're go to go.

The CSS is used to give access to some utility classes, like for example `.deck`. With the purpose of laying out the components on the page, and if needed, give the ability to apply a text style to an element.
You don't have to import any css for the styling of the individual custom elements though.

Each component are then used as:

```html
<!-- my-project.html -->
<dsb-image-link
  overlap
  url="#"
  headline="Visit wonderful Nyhavn"
  text="Grab you friends and come along on a journey you will never forget."
  src="url/to/my/image.jpg"
  alt="Nyhavn in Copenhagen"
></dsb-image-link>
```

## Dev notes

#### Inheritable styles

Most of the styling wont bleed though into the designsystems components. But there are some that does.
[Here's](https://lamplightdev.com/blog/2019/03/26/why-is-my-web-component-inheriting-styles/) an article on the subject.

To get around this, and to ensure that our components will look just like we intend, the `all` css-property should be used:

```css
dsb-component {
  all: initial;
}
```
