import path from 'path';
import fs from 'fs';

/**
 * Updates the package.json*exports with shortcut to every component's export
 *
 * f.ex.
 * // app.js
 * import '@dsb.dk/designsystem/button' // -> '@dsb.dk/designsystem/dist/components/button'
 */

try {
  const files = fs.readdirSync(path.resolve('dist/components'));
  const packageJSON = JSON.parse(
    fs.readFileSync(path.resolve('package.json'), {
      encoding: 'utf8',
    })
  );

  const newPackageJSON = {
    ...packageJSON,
    exports: {
      '.': './dist/js/dsb.esm.js',
      "./css": "./dist/utils.css",
      ...files.reduce((acc, fileName) => {
        return {
          ...acc,
          [`./${fileName.replace('.js', '')}`]: `./dist/components/${fileName}`,
        };
      }, {}),
    },
  };

  fs.writeFileSync('package.json', JSON.stringify(newPackageJSON, null, 2));
} catch (err) {
  console.log('From generateExports.mjs:', err);
}
