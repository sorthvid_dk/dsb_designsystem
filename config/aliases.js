const paths = require("./paths");

/**
 * Alias
 *
 * Globally available alias object,
 * shared with both Storybook and projects Webpack
 *
 * note: need to build files to resolve paths importing files
 */
module.exports = {
  '@': paths.src,
  Components: paths.components,
  '~': paths.components,
  Scss: paths.scss,
  Js: paths.js,
  Images: paths.images,
  Svg: paths.svgs,
  Util: paths.util,
};
