const path = require('path')

module.exports = {
  root: path.resolve(__dirname, "../"),
  src: path.resolve(__dirname, "../src"), // source files
  build: path.resolve(__dirname, "../dist"), // production build files
  static: path.resolve(__dirname, "../static"), // static files to copy to build folder
  util: path.resolve(__dirname, "../.storybook/utils.js"), // utility functions for storybook

  components: path.resolve(__dirname, "../src/components"),
  scss: path.resolve(__dirname, "../src/scss"),
  js: path.resolve(__dirname, "../src/js"),
  images: path.resolve(__dirname, "../static/images"),
  svgs: path.resolve(__dirname, "../static/svg"),
};
