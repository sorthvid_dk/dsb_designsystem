const paths = require('./paths');
const alias = require('./aliases');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  context: paths.root,
  /**
   * Resolve
   *
   * Aliases for more convenient import/exports
   */
  resolve: {
    alias,
  },

  /**
   * Entry
   *
   * The first place Webpack looks to start building the bundle.
   */
  entry: {
    full: [
      paths.src + '/index.js',
      paths.src + '/bundle.js',
      paths.src + '/polyfills.js',
    ],
    // dsb: paths.src + '/index.scss',
    utils: paths.src + '/utils.scss',
    variables: paths.src + '/variables.scss',
    main: paths.src + '/bundle.js',
    polyfills: paths.src + '/polyfills.js',
  },

  /**
   * Plugins
   *
   * Customize the Webpack build process.
   */
  plugins: [
    /**
     * CleanWebpackPlugin
     *
     * Removes/cleans build folders and unused assets when rebuilding.
     */
    new CleanWebpackPlugin(),

    /**
     * CopyPlugin
     *
     * Copies files from target to destination folder.
     */
    new CopyPlugin({
      patterns: [{ from: paths.static, to: 'assets' }],
    }),
  ],
  /**
   * Module
   *
   * Determine how modules within the project are treated.
   */
  module: {
    rules: [
      /**
       * JavaScript
       *
       * Use Babel to transpile JavaScript files.
       */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            // options: {
            //   plugins: [
            //     [
            //       '@babel/plugin-proposal-private-property-in-object',
            //       { loose: true },
            //     ],
            //   ],
            // },
          },
          'eslint-loader',
        ],
      },

      /**
       * Styles
       *
       * Inject CSS into the custom elements.
       */
      {
        test: /\.(scss|css)$/,
        include: /components/,
        use: [
          {
            loader: 'lit-scss-loader',
            options: {
              minify: false,
            },
          },
          {
            loader: 'extract-loader',
          },
          {
            loader: 'css-loader',
            options: { sourceMap: true, importLoaders: 1 },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                plugins: [require('postcss-pxtorem')],
              },
            },
          },
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              implementation: require('sass'),
            },
          },
        ],
      },

      /**
       * Styles
       *
       * Inject CSS into the head with source maps.
       */
      {
        test: /\.(scss|css)$/,
        exclude: /components/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: { sourceMap: true, importLoaders: 1 },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                plugins: [require('postcss-pxtorem')],
              },
            },
          },
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              implementation: require('sass'),
            },
          },
        ],
      },

      /**
       * SVG - inline
       *
       * Inline the SVG's into the code, that's not named `.not-inline.svg`
       */
      // {
      //   test: /\.svg$/,
      //   exclude: /\.not-inline/,
      //   loader: 'svg-inline-loader',
      // },

      /**
       * SVG - not inline
       *
       * Don't inline the SVG's that's named `.not-inline.svg`
       */
      // {
      //   test: /\.svg$/,
      //   loader: 'file-loader',
      //   options: {
      //     name: '[path][name].[ext]',
      //     context: 'src', // prevent display of src/ in filename
      //   },
      // },

      // {
      //   test: /\.svg$/, // your icons directory
      //   loader: 'svg-sprite-loader',
      //   options: {
      //     extract: false,
      //     spriteFilename: `${paths.build}/icons.svg`, // this is the destination of your sprite sheet
      //   },
      // },

      /**
       * Images
       *
       * Copy image files to build folder.
       */
      {
        test: /\.(?:ico|gif|png|jpg|jpeg|webp|mp4)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          context: 'src', // prevent display of src/ in filename
        },
      },
      
      /**
       * Fonts
       *
       */
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'assets/fonts/',
        },
      },

      {
        test: /\.md$/,
        use: [
          {
            loader: 'html-loader',
            // options: {
            //   attributes: {
            //     list: [
            //       {
            //         tag: 'use',
            //         attribute: 'xlink:href',
            //         type: 'src',
            //       },
            //     ],
            //   },
            // },
          },
          {
            loader: 'markdown-loader',
            // options: {
            //   pedantic: true,
            // },
          },
        ],
      },
    ],
  },
};
