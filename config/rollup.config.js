import alias from '@rollup/plugin-alias';
import postcss from 'rollup-plugin-postcss';
import postcssLit from 'rollup-plugin-postcss-lit';
import url from '@rollup/plugin-url';
import resolve from 'rollup-plugin-node-resolve';
import commonJS from 'rollup-plugin-commonjs';
// import { terser } from "rollup-plugin-terser";
import strip from '@rollup/plugin-strip';
const paths = require('./paths');
import multiInput from 'rollup-plugin-multi-input';
import path from 'path';
// import scss from 'rollup-plugin-scss'


export default {
  input: [
    'src/components/**/[!.]!(*.stories).js',
    { ['js/dsb.esm']: 'src/index.js' },
  ],
  output: {
    format: 'esm',
    dir: 'dist',
  },
  plugins: [
    // scss({
    //   include: ['src/utils.scss'],
    //   // exclude: ['src/components/**/*.js', 'src/components/**/*.scss'],
    //   // output: true,
    //   output: 'dsb.css',
    //   sass: require('sass')
    // }),

    alias({
      entries: [{ find: '@', replacement: paths.src }],
    }),

    postcss({
      inject: false,
    }),

    postcssLit(),

    url({
      limit: 0,
    }),

    resolve(),

    commonJS({
      include: 'node_modules/**',
    }),

    strip({
      functions: ['console.log', 'console.warn'],
    }),

    multiInput({
      relative: 'src/',
      transformOutputPath: (output, input) => {

        return `components/${path.basename(output)}`;
      },
    }),
    // terser() // currently save us 2kb
  ],
};
